<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $id_client;

	public $_sideMenu;

	public $breadcrumb;

	public $header;

	public $content;

	public $script;

	public $form;

	public $activeMenu;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public $isClientMenu;

	public $needLogin = true;

	function __construct()
	{
		parent::__construct();
		$this->_sideMenu = array();
		$this->load->library('vms');
		$this->load->library('breadcrumb', array());
		$this->load->library('tablegenerator', array());
		$this->form_validation->set_error_delimiters('', '');
		if ($this->uri->segment(1) != '' && $this->needLogin) {
			if (!$this->session->userdata('admin')&&!$this->session->userdata('user')) {
				redirect(site_url());
			}
		}
	}

	function index($id = null)
	{
		$this->isAdmin();
		$this->breadcrumb = $this->breadcrumb->generate();
		$this->load->library('sideMenu', $this->_sideMenu);
		$user = $this->session->userdata('user');
		$admin = $this->session->userdata('admin');
		$data = array(
			'user' => ($user) ? $user['name'] : $admin['role_name'],
			'sideMenu' => $this->sidemenu->generate($this->activeMenu) ,
			'breadcrumb' => $this->breadcrumb,
			'header' => $this->header,
			'content' => $this->content,
			'script' => $this->script
		);
		$this->parser->parse('template/base', $data);
	}

	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	function isAdmin()
	{
		if ($this->session->userdata('admin')) {
			$this->_sideMenu = array(
				array(
					'group' => 'dashboard',
					'title' => 'Dashboard',
					'url' => site_url() ,
					'role' => array(
						1,
						4,
						5,
						6,
						8,
						12
					)
				) ,
				array(
					'title' => 'Semua Auction',
					'url' => site_url('auction') ,
					'role' => array(
						1,
						7
					)
				) ,
				array(
					'title' => 'Auction Berlangsung',
					'url' => site_url('auction/langsung') ,
					'role' => array(
						1,
						7
					)
				) ,
				array(
					'title' => 'Auction Selesai',
					'url' => site_url('auction/selesai') ,
					'role' => array(
						1,
						7
					)
				) ,
				array(
					'group' => 'master',
					'title' => 'Master',
					'url' => '#' ,
					'role' => array(
						1
					),
					'list' => array(
						array(
							'url' => site_url('master/user') ,
							'title' => 'User',
							'role' => array(
								1
							) ,
						),
						array(
							'url' => site_url('master/sbu') ,
							'title' => 'Unit Organisasi',
							'role' => array(
								1
							) ,
						),
						array(
							'url' => site_url('master/badan_hukum') ,
							'title' => 'Badan Hukum',
							'role' => array(
								1
							) ,
						),
						array(
							'url' => site_url('master/bidang') ,
							'title' => 'Bidang',
							'role' => array(
								1
							) ,
						),
						array(
							'url' => site_url('master/sub_bidang') ,
							'title' => 'Sub Bidang',
							'role' => array(
								1
							) ,
						),
						array(
							'url' => site_url('master/kurs') ,
							'title' => 'Mata Uang',
							'role' => array(
								1
								
							) ,
						),
					)
				) ,
				/*array(
					'title' => 'Reporting',
					'url' => site_url('reporting') ,
					'role' => array(
						1,
						4,
						6,
						8
					)
				) ,*/
				array(
					'title' => 'DPT',
					'url' => site_url('dpt') ,
					'role' => array(
						1,
						4,
						5,
						6,
						8,
						12
					)
				) ,
				array(
					'title' => 'Vendor',
					'url' => '#',
					'role' => array(
						1,
						4,
						5,
						6,
						8,
						12
					) ,
					'list' => array(
						array(
							'url' => site_url('vendor/admin/daftar_vendor') ,
							'title' => 'Daftar Vendor',
							'role' => array(
								1,
								4,
								6,
								8,
								12
							) ,
						) ,
						array(
							'url' => site_url('vendor/admin/daftar_tunggu') ,
							'title' => 'Daftar Tunggu',
							'role' => array(
								1,
								4,
								6,
								8,
								12
							) ,
						) ,
						array(
							'url' => site_url('vendor/admin/blacklist') ,
							'title' => 'Black List',
							'role' => array(
								1,
								4,
								5,
								6,
								8
							) ,
						) ,
						array(
							'url' => site_url('vendor/admin/daftar_beku') ,
							'title' => 'Daftar Beku',
							'role' => array(
								1,
								4,
								6,
								8
							) ,
						) ,
					)
				) ,
			);
		}else if($this->session->userdata('user')) {
			$user = $this->session->userdata('user');
			$this->_sideMenu = array(
				array(
					'title' => 'Beranda',
					'url' => site_url('vendor/dashboard') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Administrasi',
					'url' => site_url('vendor/administrasi') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Akta Perusahaan',
					'url' => site_url('vendor/akta') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Domisili Perusahaan',
					'url' => site_url('vendor/situ') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Tanda Daftar Perusahaan',
					'url' => site_url('vendor/tdp') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Pengurus Perusahaan',
					'url' => site_url('vendor/pengurus') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Kepemilikan Modal',
					'url' => site_url('vendor/pemilik') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Izin Usaha',
					'url' => site_url('vendor/izin') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Pengalaman',
					'url' => site_url('vendor/pengalaman') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Pabrikan/Keagenan/Distributor',
					'url' => site_url('vendor/agen') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Cetak Sertifikat',
					'url' => site_url('certificate/index/'.$user['id_user']) ,
					'target'=> '_blank',
					'role' => array(
						(($user['vendor_status']==2) ? 11 : null)
					),
				),
				array(
					'title' => 'Aspek K3',
					'url' => site_url('vendor/k3') ,
					'role' => array(
						11
					),
				),array(
					'title' => 'Cetak Sertifikat',
					'url' => site_url('certificate/index/'.$user['id_user']) ,
					'target'=> '_blank',
					'role' => array(
						(($user['vendor_status']==2) ? 11 : null)
					),
				),
				array(
					'title' => 'Auction',
					'url' => site_url('auction/vendor') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Bukti Potong / Setor Pajak',
					'url' => site_url('vendor/bukti_pajak') ,
					'role' => array(
						11
					),
				)
			);
		}
	}

	public	function validation($form = null)
	{

		ob_start();

		$_r = false;
		if ($form == null) {
			$form = $this->form['form'];

			foreach($form['form'] as $key =>  $value) {
				if($value['type'] == 'file'||$value['type'] == 'multiple_file'){
					unset($form['form'][$key]);
				}
			}
			// print_r($this->form);

			if($this->form['form'][0]==NULL){
				$_form = array();
				foreach ($this->form['form'] as $key => $value) {
					foreach ($value as $keys => $values) {
						$v = explode('|', $values['label']);
						$_form[$keys]['label'] = $v[0];
						$_form[$keys]['field'] = $values['field'];  
						$_form[$keys]['rules'] = $values['rules']; 
					}
 
				}
				$this->form_validation->set_rules($_form);
			}else{

				foreach($this->form['form'] as $key =>  $value) {
					$v = explode('|', $value['label']);

					$this->form['form'][$key]['label'] = $v[0];
				}

				$this->form_validation->set_rules($this->form['form']);

			}
		}

		if ($this->form_validation->run() == FALSE) {

			$return['status'] = 'error';
			if($this->form['form'][0]==NULL){
				foreach($form as $key=>$value) {
					foreach ($value as $keys => $values) {
						if ($values['type'] == 'file'||$values['type'] == 'multiple_file') {
							$return['file'][$key][$values['field']] = $this->session->userdata($values['field']);
						}

						if ($values['type'] == 'date_range') {
							$return['form'][$key][$values['field'][0]] = form_error($values['field'][0] . '_start');
							$return['form'][$key][$values['field'][1]] = form_error($values['field'][1] . '_start');
						}
						else {
							$return['form'][$key][$values['field']] = form_error($values['field']);
						}
					}
					
				}
			}else{
				foreach($form as $value) {
					if ($value['type'] == 'file'||$value['type'] == 'multiple_file') {
						$return['file'][$value['field']] = $this->session->userdata($value['field']);
					}

					if ($value['type'] == 'date_range') {
						$return['form'][$value['field'][0]] = form_error($value['field'][0] . '_start');
						$return['form'][$value['field'][1]] = form_error($value['field'][1] . '_start');
					}
					else {
						$return['form'][$value['field']] = form_error($value['field']);
					}
				}
			}
			

			$_r = false;
		}
		else {
			$return['status'] = 'success';
			$_r = true;
		}

		echo json_encode($return);
		return $_r;
	}

	public function getData($id = null)
	{
		$config['query'] = $this->getData;
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert()
	{
		$this->form['url'] = $this->insertUrl;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function edit($id = null)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
	 public function getSingleData($id){
        $user  = $this->session->userdata('user');
        $modelAlias = $this->modelAlias;
        $getData   = $this->$modelAlias->selectData($id);

        foreach($this->form['form'] as $key => $value){
            $this->form['form'][$key]['readonly'] = TRUE;
            $this->form['form'][$key]['value'] = $getData[$value['field']];

            if($value['type']=='date_range'){
                foreach($value['field'] as $keyField =>$rowField){
                    $this->form['form'][$key]['value'][] = $getData[$rowField];
                }

            }
            if($value['type']=='money'){
                    $this->form['form'][$key]['value'] = number_format($getData[$value['field']]);
                }
            if($value['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $getData[$value['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($getData[$value['field'][1]]);
            }
        }

        echo json_encode($this->form);
    }
	public function approveOvertimeUser($id)
	{
		$modelAlias = $this->modelAlias;
		$save = $this->input->post();
		$save['edit_stamp'] = timestamp();
		return $this->$modelAlias->update($id, $save);
	}

	public function delete($id)
	{
		$id = base64_decode($id);
		// echo "string ".$id;die;
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		// echo "string ".$id;die;
		$this->formDelete['url'] = site_url($this->deleteUrl . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function upload_lampiran()
	{

		foreach($_FILES as $key => $row) {
			if(is_array($row['name'])){
				foreach ($row['name'] as $keys => $values) {
					$file_name = $row['name'] = $key . '_' . name_generator($_FILES[$key]['name'][$keys]);
					 $_FILES['files']['name']= $file_name;
			        $_FILES['files']['type']= $_FILES[$key]['type'][$keys];
			        $_FILES['files']['tmp_name']= $_FILES[$key]['tmp_name'][$keys];
			        $_FILES['files']['error']= $_FILES[$key]['error'][$keys];
			        $_FILES['files']['size']= $_FILES[$key]['size'][$keys];

					$config['upload_path'] = './assets/lampiran/temp/';
					$config['allowed_types'] = $_POST['allowed_types'];
					$this->load->library('upload');
					$this->upload->initialize($config);

					if (!$this->upload->do_upload('files')) {
						$return['status'] = 'error';
						$return['message'] = $this->upload->display_errors('', '');
					}
					else {
						$return['status'] = 'success';
						$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
						$return['file_name'] = $file_name;
					}

					echo json_encode($return);
				}

			}else{
				$file_name = $_FILES[$key]['name'] = $key . '_' . name_generator($_FILES[$key]['name']);
				$config['upload_path'] = './assets/lampiran/temp/';
				$config['allowed_types'] = $_POST['allowed_types'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if (!$this->upload->do_upload($key)) {
					$return['status'] = 'error';
					$return['message'] = $this->upload->display_errors('', '');
				}
				else {
					$return['status'] = 'success';
					$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
					$return['file_name'] = $file_name;
				}

				echo json_encode($return);
			}

		}
	}

	public function do_upload($field, $db_name = '')
	{
		$file_name = $_FILES[$db_name]['name'] = $db_name . '_' . name_generator($_FILES[$db_name]['name']);
		$config['upload_path'] = './assets/lampiran/' . $db_name . '/';
		$config['allowed_types'] = 'pdf|jpeg|jpg|png|gif';
		$this->load->library('upload');
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($db_name)) {
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('', ''));
			return false;
		}
		else {
			$this->session->set_userdata($db_name, $file_name);
			$_POST[$db_name] = $file_name;
			return true;
		}
	}

	public function deleteTemp($save, $lastData = null)
	{
		if($this->form['form'][0]==NULL){
			$_form = array();
			$i = 0;
			foreach ($this->form['form'] as $key => $value) {
				foreach ($value as $keys => $values) {
					$_form[$i] = $values;  
					$i++;
				}

			}
			$this->form['form'] = $_form;
		}

		foreach($this->form['form'] as $key => $value) {
			if ($value['type'] == 'file') {
				if ($lastData != null && ($save[$value['field']] != $lastData[$value['field']])) {
					if ($lastData[$value['field']] != '') {
						unlink('./assets/lampiran/' . $value['field'] . '/' . $lastData[$value['field']]);
					}
				}

				if ($save[$value['field']] != '') {
					if (file_exists('./assets/lampiran/temp/' . $save[$value['field']])) {
						
						rename('./assets/lampiran/temp/' . $save[$value['field']], './assets/lampiran/' . $value['field'] . '/' . $save[$value['field']]);
					}

				}

			}elseif($value['type'] == 'multiple_file'){
				
			}

			
		}
	}

	public function stillActive($id)
	{
		return $this->db->where('id',$id)->update('ms_vendor',array('is_active'=>1));
	}
}
include('VMS.php');