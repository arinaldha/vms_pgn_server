<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Dashboard_model','dm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('main_model','mm');
		$this->load->model('note_model','nm');
	}

	public function index($id=null)
	{
		
		if($this->session->userdata('admin')['id_role'] == 7) {
			redirect('auction');
		}
		if($this->session->userdata('user')) {
			redirect('vendor/dashboard');
		}
		$user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');
        $data['admin'] = $admin;
        $data['session'] = (($user) ? $user : $admin);
        $data['daftar_tunggu'] = $this->vm->get_total_daftar_tunggu();

		$data['total_dpt'] 	= $this->vm->get_total_dpt();
		$data['notifikasi'] = $this->dm->get_note_admin()->result_array();

		$data['chart'] 		= array(
								'daftar_tunggu_chart' => $this->mm->get_daftar_tunggu_chart(),
								'daftar_hitam_chart'  => $this->mm->daftar_hitam_chart(),
								'dpt_chart'			  => $this->mm->dpt_chart()
							  );
       
		$this->load->model('main_model','mm');
		$this->load->model('note_model','nm');        
		$this->header = 'Selamat Datang '.(($user) ? $user['name'] : $admin['name']);
		if( $admin){
			$this->content = $this->load->view('dashboard/dashboard_admin',$data, TRUE);
			$this->script  = $this->load->view('dashboard/dashboard_js', $data, TRUE);
		}else if($user){
			$this->content = $this->load->view('dashboard/dashboard_vendor',$data, TRUE);
		}
		
		

		parent::index();
	}
	public function to_waiting_list(){
		$user = $this->session->userdata('user');
		// echo print_r($user);
		$waiting_list = $this->vm->to_waiting_list();
		if($waiting_list['status'] === true){
				$set_session = array(
					'id_user' 		=> 	$user['id_user'],
					'name'			=>	$user['name'],
					'id_sbu'		=>	$user['id_sbu'],
					'vendor_status'	=>	1,
					'is_active'		=>	$user['is_active'],
					'id_role'		=>	$user['id_role'],
					'npwp_code'		=>  $user['npwp_code']
				);
				
				
				$this->session->set_userdata('user',$set_session);

				redirect(site_url());

		} else {
			echo '<script>alert("'.$waiting_list['message'].'"); window.location.href="'.site_url('vendor/administrasi').'"</script>';
		}
	}

	public function get_daftar_tunggu($id = null)
	{
		$config['query'] = $this->dm->get_total_daftar_tunggu();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_blacklist($id = null)
	{
		$config['query'] = $this->dm->get_blacklist();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_Dpt($id = null)
	{
		$config['query'] = $this->dm->get_dpt();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function search_vendor(){
		echo json_encode($this->dm->search_vendor());
	}
	
}
