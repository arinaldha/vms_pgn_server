<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Auction extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');
		$this->form = array(
			'form'=>array(
				// array(
		  //           'field'	=> 	'auction_type',
		  //           'type'	=>	'radioList',
		  //           'label'	=>	'Tipe Auction',
		  //           'source'=>	array('reverse_auction' => 'Reverse Auction','forward_auction'=>'Forward Auction'),
		  //           'rules' =>	'required'
	   //       	),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pengadaan Barang/Jasa',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_source',
		            'type'	=>	'radioList',
		            'label'	=>	'Sumber Anggaran',
		            'source'=>	array('apgn' => 'APGN','non_apgn'=>'Non-APGN'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_pejabat_pengadaan',
		            'type'	=>	'dropdown',
		            'label'	=>	'Penyelenggara Pengadaan Barang/Jasa',
		            'source'=>	$this->am->get_pejabat(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'auction_jenis',
		            'type'	=>	'radioList',
		            'label'	=>	'Jenis Auction',
		            'source'=>	array('rfi' => 'RFI','rfi_pembelian'=>'RFQ-Pembelian','rfi_penunjukan'=>'RFI-Penunjukan','auction'=>'Auction'),
		            'rules' =>	'required'
	         	),
	         	/*array(
		            'field'	=> 	'work_area',
		            'type'	=>	'radioList',
		            'label'	=>	'Area Kerja',
		            'source'=>	array('internet' => 'Internet','intranet'=>'Intranet'),
		            'rules' =>	'required'
	         	),*/
	         	array(
		            'field'	=> 	'room',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi Auction'
	         	),
	         	array(
		            'field'	=> 	'id_lokasi',
		            'type'	=>	'dropdown',
		            'label'	=>	'Unit/Satuan Kerja',
		            'source'=>	$this->am->get_lokasi(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'duration_type',
		            'type'	=>	'radioList',
		            'label'	=>	'Tipe Durasi Auction',
		            'source'=>	array('days' => 'Hari','hours' => 'Jam','minutes' => 'Menit'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'duration',
		            'type'	=>	'text',
		            'label'	=>	'Durasi Auction',
		            'rules' =>	'required'
	         	),
	         	// array(
		         //    'field'	=> 	'auction_end',
		         //    'type'	=>	'dateTime',
		         //    'label'	=>	'Waktu Berakhir',
		         //    'rules' =>	'required'
	         	// ),
	         	array(
		            'field'	=> 	'budget_holder',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pengguna Pengadaan Barang/Jasa',
		            'source'=>	$this->am->get_budget_holder(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_spender',
		            'type'	=>	'dropdown',
		            'label'	=>	'Satuan Kerja Penyelenggara Pengadaan',
		            'source'=>	$this->am->get_budget_spender(),
		            'rules' =>	'required'
	         	)
			),
		);
		$this->insertUrl = site_url('auction/save/');
		$this->updateUrl = site_url('auction/update');
		$this->deleteUrl = 'auction/delete/';
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->header = 'Daftar Auction';
		$this->content = $this->load->view('auction/admin/list_auction',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_auction_js', $data, TRUE);
		parent::index();
	}

	public function langsung()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction Berlangsung'
		));
		$this->header = 'Daftar Auction Berlangsung';
		$this->content = $this->load->view('auction/admin/list_langsung',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_langsung_js', $data, TRUE);
		parent::index();
	}

	public function selesai()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction Selesai'
		));
		$this->header = 'Daftar Auction Selesai';
		$this->content = $this->load->view('auction/admin/list_selesai',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_selesai_js', $data, TRUE);
		parent::index();
	}

	public function getDataAuction($id = null)
	{
		$config['query'] = $this->am->get_auction_list();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function getDataLangsung($id = null)
	{
		$config['query'] = $this->am->get_auction_langsung();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function getDataSelesai($id = null)
	{
		$config['query'] = $this->am->get_auction_selesai();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cek_auction_data($id)
	{
		$this->am->cek_auction_data($id);
	}

	public function vendor()
	{
		$data['user'] = $this->session->userdata('user');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->header = 'Daftar Auction';
		$this->content = $this->load->view('vendor/auction/list',$data, TRUE);
		$this->script = $this->load->view('vendor/auction/list_js', $data, TRUE);
		parent::index();
	}

	public function getDataAuctionVendor($id = null)
	{
		$config['query'] = $this->am->get_auction_vendor();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$admin = $this->session->userdata('admin');
			if ($save['duration_type'] == 'days') {
				$save['auction_duration'] = $save['duration'] * 1440;
			} elseif ($save['duration_type'] == 'hours') {
				$save['auction_duration'] = $save['duration'] * 60;
			} else {
				$save['auction_duration'] = $save['duration'];
			}
			$save['auction_type'] = 'reverse_auction';
			$save['entry_stamp'] 	= date('Y-m-d H:i:s');
			$save['auction_date'] 	= $save['auction_start'];
			$save['id_mekanisme'] 	= 1;
			$save['id_sbu'] 		= $admin['id_sbu'];

			if ($this->$modelAlias->save_data($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
			// echo print_r($save);
			// $save['start_time']		= date('Y-m-d H:i:s',strtotime($save['auction_start']));
			// $save['time_limit']		= date('Y-m-d H:i:s',strtotime($save['auction_end']));
			// unset($save['auction_start']);
			// unset($save['auction_end']);
			// // $save['auction_duration'] = (strtotime($save['auction_end']) - strtotime($save['auction_start']))/60;
		}
	}

	public function editAuction($id)
	{
		$id = base64_decode($id);
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('auction/editAuction/'.$id),
			'title' => 'Edit Auction'
		));
		$data['id'] = $id;
		$data['cek'] = $this->am->check_auction_data($id);
		$this->header  = 'Edit Auction';
		$this->content = $this->load->view('auction/view_tab',$data, TRUE);
		$this->script  = $this->load->view('auction/view_tab_js',$data, TRUE);
		parent::index();
	}

	public function viewEditAuction($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pengadaan Barang/Jasa',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_source',
		            'type'	=>	'radioList',
		            'label'	=>	'Sumber Anggaran',
		            'source'=>	array('perusahaan' => 'Perusahaan','non_perusahaan'=>'non-perusahaan'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_pejabat_pengadaan',
		            'type'	=>	'dropdown',
		            'label'	=>	'Penyelenggara Pengadaan Barang/Jasa',
		            'source'=>	$this->am->get_pejabat(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'auction_jenis',
		            'type'	=>	'radioList',
		            'label'	=>	'Jenis Auction',
		            'source'=>	array('rfi' => 'RFI','rfi_pembelian'=>'RFQ-Pembelian','rfi_penunjukan'=>'RFI-Penunjukan','auction'=>'Auction'),
		            'rules' =>	'required'
	         	),
	         	// array(
		         //    'field'	=> 	'work_area',
		         //    'type'	=>	'radioList',
		         //    'label'	=>	'Area Kerja',
		         //    'source'=>	array('internet' => 'Internet','intranet'=>'Intranet'),
		         //    'rules' =>	'required'
	         	// ),
	         	array(
		            'field'	=> 	'room',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi Auction'
	         	),
	         	array(
		            'field'	=> 	'id_lokasi',
		            'type'	=>	'dropdown',
		            'label'	=>	'Unit/Satuan Kerja',
		            'source'=>	$this->am->get_lokasi(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'duration_type',
		            'type'	=>	'radioList',
		            'label'	=>	'Tipe Durasi Auction',
		            'source'=>	array('days' => 'Hari','hours' => 'Jam','minutes' => 'Menit'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'duration',
		            'type'	=>	'text',
		            'label'	=>	'Durasi Auction',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_holder',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pengguna Pengadaan Barang/Jasa',
		            'source'=>	$this->am->get_budget_holder(),
		            'rules' =>	'required'
	         	),
	         	// array(
		         //    'field'	=> 	'budget_spender',
		         //    'type'	=>	'dropdown',
		         //    'label'	=>	'Satuan Kerja Penyelenggara Pengadaan',
		         //    'source'=>	$this->am->get_budget_spender(),
		         //    'rules' =>	'required'
	         	// ),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>	'hidden',
		            'rules'	=>	'hidden'
	         	),
			),
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataAuction($id);
		// print_r($data);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];

			if ($this->form['form'][$key]['value'] == 'minutes') {
				$this->form['form'][$key]['value'] = 'Menit';
			} elseif ($this->form['form'][$key]['value'] == 'hours') {
				$this->form['form'][$key]['value'] = 'Jam';
			} elseif ($this->form['form'][$key]['value'] == 'days') {
				$this->form['form'][$key]['value'] = 'Hari';
			}

			if ($this->form['form'][$key]['value'] == 'rfi') {
				$this->form['form'][$key]['value'] = 'RFI';
			} elseif ($this->form['form'][$key]['value'] == 'rfi_penunjukan') {
				$this->form['form'][$key]['value'] = 'RFI-Penunjukan';
			} elseif ($this->form['form'][$key]['value'] == 'rfi_pembelian') {
				$this->form['form'][$key]['value'] = 'RFI-Pembelian';
			} elseif ($this->form['form'][$key]['value'] == 'auction') {
				$this->form['form'][$key]['value'] = 'Auction';
			} elseif ($this->form['form'][$key]['value'] == 'reverse_auction') {
				$this->form['form'][$key]['value'] = 'Reverse Auction';
			} elseif ($this->form['form'][$key]['value'] == 'forward_auction') {
				$this->form['form'][$key]['value'] = 'Forward Auction';
			}

			if ($this->form['form'][$key]['value'] == 'perusahaan') {
				$this->form['form'][$key]['value'] = 'Perusahaan';
			} elseif ($this->form['form'][$key]['value'] == 'non_perusahaan') {
				$this->form['form'][$key]['value'] = 'Non-perusahaan';
			}
		}

		echo json_encode($this->form);
	}

	public function editDataAuction($id)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataAuction($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}

		$this->form['url'] = site_url('auction/updateAuction/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function updateAuction($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$admin = $this->session->userdata('admin');

			if ($save['duration_type'] == 'days') {
				$save['auction_duration'] = $save['duration'] * 1440;
			} elseif ($save['duration_type'] == 'hours') {
				$save['auction_duration'] = $save['duration'] * 60;
			} else {
				$save['auction_duration'] = $save['duration'];
			}
			// $save['auction_duration'] = (strtotime($save['auction_end']) - strtotime($save['auction_start']))/60;
			
			// $save['auction_date'] 	= date('Y-m-d');
			$save['edit_stamp'] 	= date('Y-m-d H:i:s');
			// $save['auction_date'] 	= $save['auction_start'];
			$save['id_mekanisme'] 	= 1;
			$save['id_sbu'] 		= $admin['id_sbu'];
			$lastData = $this->$modelAlias->selectDataAuction($id);
			if ($this->$modelAlias->update_auction($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function view_edit_auction($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit', $data, FALSE);
		$this->load->view('auction/tab/view_edit_js', $data, FALSE);
	}

	public function duplikatAuction($id)
	{
		$jumlah = $this->input->post();
		$_id = $this->am->duplikat($id,$jumlah['jumlah']);
		if($_id){
			echo '<script>alert("Berhasil Duplikat Data"); window.location.href="'.site_url('auction').'"</script>';
		}
		$this->form = array(
			'form' => array(
				array(
					'field' => 'jumlah',
					'label' => 'Masukkan jumlah duplikat',
					'type'	=> 'number',
					'rules' => 'required'
				)
			)
		);
		$this->form['url'] = site_url('auction/duplikat/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Duplikat',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function duplikat($id)
	{
		$modelAlias = $this->modelAlias;
		// if ($this->validation()) {
			$save = $this->input->post();
		
			if ($this->$modelAlias->duplikat_data($id,$save['jumlah'])) {
				echo json_encode(array('status'=>'success'));
				return true;
			}
		// }
	}
	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_auction($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function deleteAuction($id)
	{
		$this->formDelete['url'] = site_url('auction/delete/'.$id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}