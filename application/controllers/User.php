<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {
	public $form;
	public $modelAlias = 'um';
	public $alias = 'ms_user';
	public $module = 'User';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('master/User_model','um');
	}
		// $this->load->model('Role_model','rm');
		/*$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'password',
						'type'	=>	'password',
						'label'	=>	'Password',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama',
					),
					
					array(
						'field'	=> 	'id_role',
						'type'	=>	'dropdown',
						'label'	=>	'Role',
						'source'=>	array(
							1=>'Tata Usaha Barang 1',
							2=>'Tata Usaha Barang 2',
							3=>'Tata Usaha Gudang',
							4=>'Super Admin'
							)
					),
					array(
						'field'	=> 	'id_gudang',
						'type'	=>	'dropdown',
						'label'	=>	'Gudang',
						 'source'=>	$this->gm->getGudang(),
					)					
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->insertUrl = site_url('user/save/'.$this->id_client);
		$this->updateUrl = 'user/update';
		$this->deleteUrl = 'user/delete/';
		$this->getData = $this->um->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('user'),
			'title' => 'User'
		));
		$this->header = 'User';
		$this->content = $this->load->view('user/list',$data, TRUE);
		$this->script = $this->load->view('user/list_js', $data, TRUE);
		parent::index();
	}
	public function save(){
		$_POST['raw_password'] = $_POST['password'];
		$_POST['password'] = do_hash($_POST['password'],'sha1');
		parent::save();
	}
	public function update($id){
		if($_POST['password']!=''){
			$_POST['raw_password'] = $_POST['password'];
			$_POST['password'] = do_hash($_POST['password'],'sha1');
		}else{
			unset($_POST['password']);
			$this->form['form']['password']['rules'] = '';
		}
		
		parent::update($id);
	}
	public function edit($id=null){
		$modelAlias = $this->modelAlias;
		$data   = $this->$modelAlias->selectData($id);
		foreach($this->form['form'] as $key => $element){
			if($this->form['form'][$key]['type']!='password'){
				$this->form['form'][$key]['value'] = $data[$element['field']];
			}else{
				$this->form['form'][$key]['label'] = 'Password (Tinggalkan kosong bila tidak diganti)';
			}
		}

		$this->form['url'] = site_url($this->updateUrl .'/'.$id);
		$this->form['button'] = array(
						array(
								'type'=>'submit',
								'label'=>'Ubah'
						),
						array(
								'type'=>'cancel',
								'label'=>'Batal'
						)
				);
		echo json_encode($this->form);
	}*/
	
	function daftarGudang($id){
		
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('user'),
			'title' => 'User'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('user/gudang'),
			'title' => 'Daftar Gudang'
		));
		$data['id'] = $id;
		$this->header = 'Daftar Gudang';
		$this->content = $this->load->view('user/daftarGudang',$data, TRUE);
		$this->script = $this->load->view('user/daftarGudang_js', $data, TRUE);
		parent::index();
	}
	function tambahGudangByUser($id_gudang, $id_user){
		$this->um->tambahGudangByUser($id_gudang, $id_user);
	}
	function hapusGudangByUser($id_gudang, $id_user){
		$this->um->hapusGudangByUser($id_gudang, $id_user);
	}
	function set_vendor_password(){
		foreach($this->db->get('ms_login')->result() as $value){
			$this->db->update('ms_login', array('password'=>do_hash($value->password_raw,'sha1')));
		}
	}
}
