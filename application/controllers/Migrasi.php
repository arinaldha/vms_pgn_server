<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Migrasi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Migrasi_model','mm');	
	}
	public function migrasi_admin_dan_user(){
		$this->mm->migrasi_admin_dan_user();
	}
	public function proses_beku(){
		$this->mm->proses_beku();
	}
}
