<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends MY_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->model('Note_model','nm');
		$this->load->model('vendor/Vendor_model','vm');
		$this->load->library('email');
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Vendor',
		            'readonly'=>true
		        ),
		        array(
		            'field'	=> 	'document_type',
		            'type'	=>	'text',
		            'label'	=>	'Dokumen',
		            'readonly'=>true
		        ),
		        array(
		            'field'	=> 	'value',
		            'type'	=>	'textarea',
		            'label'	=>	'Note',
		            'rules' => 	'required',
	         	)
	         )
		);
	}
	public function insert($table, $id){
		$data = $this->db->where('id',$id)->get($table)->row_array();
		$vendor_data = $this->vm->get_vendor_name($data['id_vendor']);
		$note =$this->db->where('id_document',$id)->where('document_type',$table)->get('tr_note')->result_array();
		$list_dokumen = array(
			'ms_vendor_admistrasi'=>'Data Administrasi Vendor',
			'ms_akta'=>'Akta',
			'ms_situ'=>'SITU/Domisili',
			'ms_tdp'=>'TDP',
			'ms_pengurus'=>'Pengurus',
			'ms_pemilik'=>'Kepemilikan Saham',
			'ms_ijin_usaha'=>'Izin Usaha',
			'ms_agen'=>'Pabrikan/Keagenan/Distributor',
			'ms_pengalaman'=>'Pengalaman');
		$this->form['form'][0]['value'] = $vendor_data['name'];
		$this->form['form'][1]['value'] = $list_dokumen[$table];
		if($table=='ms_akta'){
			if($data['type']=='pendirian'){
				$this->form['form'][1]['value'] = 'Akta Pendirian Perusahaan';
			}else{
				$this->form['form'][1]['value'] = 'Akta Perubahan Terakhir';
			}
			
		}
		if($table=='ms_ijin_usaha'){
			$list_ijin_usaha =	array(
								'siujk'=>'SIUJK',
								'sbu'=>'SBU',
								'siup'=>'SIUP',
								'ijin_lain'=>'Surat Izin Usaha Lainnya',
								'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
							);
			$this->form['form'][1]['value'] = $list_ijin_usaha[$data['type']];
		}
		
		$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
		$nama_orang = array('ms_pengurus','ms_pemilik');
		
		$return = array();
		foreach ($this->form['form'] as $key => $value) {
			$return[] = $value;
			if($key==0){
				if(in_array($table, $dokumen)){
					$return[] = array(
			            'field'	=> 	'no',
			            'type'	=>	'text',
			            'label'	=>	'Nomor Dokumen',
			            'readonly'=>true,
			            'value'=>$data['no']
			        );
				}
				if(in_array($table, $nama_orang)){
					$return[] = array(
			            'field'	=> 	'name',
			            'type'	=>	'text',
			            'label'	=>	'Nama',
			            'readonly'=>true,
			            'value'=>$data['name']
			        );
				}
				if($table=='ms_pengalaman'){
					$return[] = array(
			            'field'	=> 	'name',
			            'type'	=>	'text',
			            'label'	=>	'Nama Pekerjaan',
			            'readonly'=>true,
			            'value'=>$data['job_name']
			        );
				}
				
			}
		}
		$this->form['note'] = $note;
		$this->form['form'] = $return;
		$this->form['url'] = site_url('note/save/'.$table.'/'. $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Kirim',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
		
	}
	public function save($table, $id){
		$data = $this->db->where('id',$id)->get($table)->row_array();
		$query_vendor = $this->vm->get_data($data['id_vendor']);
		$data_note = $this->nm->save($table, $id, $data);
		if($data_note){
			$note = $this->db->where('id', $this->db->insert_id())->get('tr_note')->row_array();
			if($table=='ms_vendor_admistrasi'){
				$document = 'Dokumen 	: Dokumen Administrasi';
			}else{
				$document = 'Dokumen 	: '.$note['document'];
				$subject = $note['subject_field'].' : '.$note['subject'];
				$value = 'Catatan : '.$note['value'];
			}
			$message = "Anda mendapatkan catatan pada tahap verifikasi Aplikasi Vendor Management System PT. Perusahaan Gas Negara Tbk atas : <br/><br/>
				
				$subject<br/>
				$document<br/>
				$value<br/>
				
				Untuk selanjutnya, silahkan memperbaiki data - data dan dokumen saudara di aplikasi VMS dengan mengakses link berikut <a href='vms.pgn.co.id'>vms.pgn.co.id</a>.<br/><br/>
				Terima kasih.<br/>
				PT Perusahaan Gas Negara";
			// echo $query_vendor['vendor_email'];
			// echo $query_vendor['pic_email'];
			email($save['vendor_email'], $message, 'Catatan verifikasi Dokumen VMS PT Perusahaan Gas Negara Tbk');
			email($save['pic_email'], $message, 'Catatan verifikasi Dokumen VMS PT Perusahaan Gas Negara Tbk');
			
			echo json_encode(array('status'=>'success'));
		}
	}
	public function close($id){
		$this->form_close['url'] = site_url('note/process_close/'. $id);
		$this->form_close['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Ya',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form_close);
	}
	public function process_close($id){
		if($this->nm->process_close($id)){
			echo json_encode(array('status'=>'success'));
		}
	}
}
