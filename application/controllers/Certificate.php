<?php
class Certificate extends MY_Controller{
	// protected $id_user;
	
	function __construct()
	{	
		error_reporting(0);
		ini_set("memory_limit","2048M");
		parent::__construct();
		
		require_once(BASEPATH."plugins/dompdf2/dompdf_config.inc.php");
		$this->load->model('vendor/Vendor_model','vm');
		$this->load->model('Reporting_model','rm');
		$this->load->model('vendor/izin_model','im');
		$this->load->model('vendor/K3_model','km');
		
		$this->load->library('utility');
		//$this->output->enable_profiler(TRUE);
	}
	
	function index($id = ""){
		$id = base64_decode($id);
		$no_certif = '';
		$id_user = $this->id_user = $id;
		$bsb = $this->rm->get_dpt($id);
		
		$bsb_date = $this->rm->get_first($id);
		$bsb_date = $bsb_date['start_date'];	
		
		$fill = $this->vm->get_data($id_user);

		// print_r($fill);
		if($fill['id_sbu'] == 40) $sbu = "LFM";
		if($fill['id_sbu'] == 41) $sbu = "PMO";
		if($fill['id_sbu'] == 42) $sbu = "Corporate support and service division";
		if($fill['id_sbu'] == 43) $sbu = "Seluruh anak perusahaan PGN";
		
		$no_id = (6 - strlen($fill['id']));

		for($i=0;$i<$no_id;$i++) $no_certif .= "0";
		$no_certif .= $fill['id'];

		$no_certif = $no_certif."/PGN/".$sbu."/".date("d/m/Y", strtotime($bsb_date));
		
		$no_telp = $fill['vendor_phone']; 
		if($fill['vendor_fax']) $no_telp .= "/".$fill['vendor_fax'];
		
		
		$klasifikasi = "";

		foreach($bsb->result() as $_bsb){
			// if($_bsb->id_dpt_type == 1)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Konsultan Non Konstruksi";
			// if($_bsb->id_dpt_type == 2)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Pengadaan Barang";
			// if($_bsb->id_dpt_type == 3)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Jasa Lainnya";
			// if($_bsb->id_dpt_type == 4)
			// 	$klasifikasi .= "<div> - Konstruksi / Jasa Pekerjaan Konstruksi";
			// if($_bsb->id_dpt_type == 5)
			// 	$klasifikasi .= "<div> - Konstruksi / Jasa Konsultan Perencana/Pengawas Konstruksi";
			if($_bsb->id_dpt_type == 1)
				$klasifikasi .= "<div> - Konsultan Non Konstruksi / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 2)
				$klasifikasi .= "<div> - Pengadaan Barang / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 3)
				$klasifikasi .= "<div> - Jasa Lainnya / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 4)
				$klasifikasi .= "<div> - Jasa Pekerjaan Konstruksi / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 5)
				$klasifikasi .= "<div> - Jasa Konsultan Perencana/Pengawas Konstruksi / ".$_bsb->bidang_name;
			
			$klasifikasi .= ' <i style="font-size : 10px">(pengukuhan terakhir pada '.date("d M Y", strtotime($_bsb->start_date)).')</i></div>';
		}


		$k3 = $this->km->get_csms($id);

		$return ='<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					<style type="text/css">
						html, body{
							margin : 0px;
							padding : 0px; 
						 }
						 div.page{
							font-family : "Helvetica";
							margin : 0px;
							padding : 0px;
							font-size : 13px;
						}
						div#first-page{
							background-image : url('.base_url('/assets/images/report-kiri.gif').'); 
							background-repeat : repeat-y;
							height : 793px;
							page-break-inside: avoid;
						}
						div#second-page{
							background-image : url('.base_url('/assets/images/report-second.gif').'); 
							background-repeat : repeat-y;
							height : 753px;
							page-break-inside : auto;
						}
						div.table-separator{
							page-break-inside : avoid;
						}
						table.std-table{
							border-collapse : collapse;
							border : 1px solid #000;
							margin : 15px;
						}
						table.std-table th{
							background : #000;
							color : #fff;
							border : 1px solid #000;
						}
						.qrcode {
							position: absolute;
							top:710px;
							width:80px;
							right:220px;
						}
					</style>
				</head>
				<body>
					<div class="page" id="first-page">
					<img class="qrcode" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.site_url('qrcode/view_qr/'.$id).'%2F&choe=UTF-8" title="Link to Detail Vendor" />
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="220px" valign="top"><font style="font-size : 9px; color : #fff">Dicetak pada tanggal : '.date("d/m/Y, H:i:s").'</font></td>
								<td width="680px">
									<table cellpadding="0" cellspacing="0" border="0" width="680px">
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center"><img src="'.base_url('assets/images/logo_pgn.png').'" width="65"></td>
										</tr>
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="color : #00aeef; font-size : 20px">SERTIFIKAT PENYEDIA BARANG/JASA TERDAFTAR</div>
												<div style="color : #4e5f6e">'.$no_certif.'</div>
											</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td height="10px" align="center"><b style="font-size : 20px">'.$fill['legal_name'].' '.$fill['name'].'</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="margin-left : 70px; border-bottom : 1px solid #000; border-top : 1px solid #000; width : 500px; padding : 20px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td width="140px" valign="top">NPWP</td>
															<td width="10px" valign="top">:</td>
															<td width="350px" valign="top">'.$fill['npwp_code'].'</td>
														</tr>
														<tr>
															<td valign="top">Alamat</td>
															<td valign="top">:</td>
															<td valign="top">'.nl2br($fill['vendor_address']).'</td>
														</tr>
														<tr>
															<td valign="top">No. Telp/Fax</td>
															<td valign="top">:</td>
															<td valign="top">'.$no_telp.'</td>
														</tr>
														<tr>
															<td valign="top">Klasifikasi</td>
															<td valign="top">:</td>
															<td valign="top">'.$klasifikasi.'</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>

											<td height="30px"></td>

										</tr>
										<tr>
											<td align="center">Jakarta, '.date("d/m/Y", strtotime($bsb_date)).'</td>
										</tr>
										<tr>
											<td height="80px"></td>
										</tr>
										<tr>
											<td align="center">'.$fill['sbu_name'].'</td>
										</tr>
										<tr>
											<td height="10px"></td>
										</tr>
										<tr>
											<td align="center">
												<font style="font-size : 8px">
													dicetak oleh Aplikasi Manajemen Penyedia Barang/Jasa PT Perusahaan Gas Negara Tbk.<br/>
													Dokumen ini resmi tanpa stempel dan/atau tanda tangan pejabat
												</font>
											</td>
										</tr>
									</table>
								</td>
								<td width="220px">&nbsp;</td>
							</tr>
						</table>
					</div>
					<div class="page" id="second-page">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td>'
									.$this->table_query_param('pengurus', $fill['id'], 'Pengurus Perusahaan').
								'</td>
							</tr>
						</table>'
						.$this->table_query_param('akta', $fill['id'],'Akta Pendirian / Perubahan Terakhir Perusahaan')
						.$this->table_query_param('situ', $fill['id'], 'Surat Keterangan Domisili Perusahaan / Surat Ijin Tempat Usaha')
						.$this->table_query_param('tdp', $fill['id'], 'Tanda Daftar Perusahaan')
						.$this->table_query_param('siup', $fill['id'], 'SIUP')
						.$this->table_query_param('siujk', $fill['id'], 'SIUJK')
						.$this->table_query_param('ijin_lain', $fill['id'], 'Surat Ijin Usaha Lainnya')
						.$this->table_query_param('sbu', $fill['id'], 'SBU');

						if($k3) $return .= $this->table_query_param('k3', $fill['id'], 'Penilaian Aspek K3');
						
		$return .=	
					'</div>
				</body>
			</html>';

		// echo $return;die;
		// return;

		$dompdf = new DOMPDF();  
	    $dompdf->load_html($return);  
	    $dompdf->set_paper('A4','landscape');
	    $dompdf->render();
									
        $dompdf->stream("sertifikat-vendor.pdf",array('Attachment' => false));
	}

	function index_($id = ""){
		$no_certif = '';
		$id_user = $this->id_user = $id;
		$bsb = $this->rm->get_dpt($id);
		
		$bsb_date = $this->rm->get_first($id);
		$bsb_date = $bsb_date['start_date'];	
		
		$fill = $this->vm->get_data($id_user);

		// print_r($fill);
		if($fill['id_sbu'] == 40) $sbu = "LFM";
		if($fill['id_sbu'] == 41) $sbu = "PMO";
		if($fill['id_sbu'] == 42) $sbu = "Corporate support and service division";
		if($fill['id_sbu'] == 43) $sbu = "Seluruh anak perusahaan PGN";
		
		$no_id = (6 - strlen($fill['id']));

		for($i=0;$i<$no_id;$i++) $no_certif .= "0";
		$no_certif .= $fill['id'];

		$no_certif = $no_certif."/PGN/".$sbu."/".date("d/m/Y", strtotime($bsb_date));
		
		$no_telp = $fill['vendor_phone']; 
		if($fill['vendor_fax']) $no_telp .= "/".$fill['vendor_fax'];
		
		
		$klasifikasi = "";

		foreach($bsb->result() as $_bsb){
			// if($_bsb->id_dpt_type == 1)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Konsultan Non Konstruksi";
			// if($_bsb->id_dpt_type == 2)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Pengadaan Barang";
			// if($_bsb->id_dpt_type == 3)
			// 	$klasifikasi .= "<div> - Non Konstruksi / Jasa Lainnya";
			// if($_bsb->id_dpt_type == 4)
			// 	$klasifikasi .= "<div> - Konstruksi / Jasa Pekerjaan Konstruksi";
			// if($_bsb->id_dpt_type == 5)
			// 	$klasifikasi .= "<div> - Konstruksi / Jasa Konsultan Perencana/Pengawas Konstruksi";
			if($_bsb->id_dpt_type == 1)
				$klasifikasi .= "<div> - Konsultan Non Konstruksi / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 2)
				$klasifikasi .= "<div> - Pengadaan Barang / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 3)
				$klasifikasi .= "<div> - Jasa Lainnya / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 4)
				$klasifikasi .= "<div> - Jasa Pekerjaan Konstruksi / ".$_bsb->bidang_name;
			if($_bsb->id_dpt_type == 5)
				$klasifikasi .= "<div> - Jasa Konsultan Perencana/Pengawas Konstruksi / ".$_bsb->bidang_name;
			
			$klasifikasi .= ' <i style="font-size : 10px">(pengukuhan terakhir pada '.date("d M Y", strtotime($_bsb->start_date)).')</i></div>';
		}


		$k3 = $this->km->get_csms($id);

		$return ='<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					<style type="text/css">
						html, body{
							margin : 0px;
							padding : 0px; 
						 }
						 div.page{
							font-family : "Helvetica";
							margin : 0px;
							padding : 0px;
							font-size : 13px;
						}
						div#first-page{
							background-image : url('.base_url('/assets/images/report-kiri.gif').'); 
							background-repeat : repeat-y;
							height : 793px;
							page-break-inside: avoid;
						}
						div#second-page{
							background-image : url('.base_url('/assets/images/report-second.gif').'); 
							background-repeat : repeat-y;
							height : 753px;
							page-break-inside : auto;
						}
						div.table-separator{
							page-break-inside : avoid;
						}
						table.std-table{
							border-collapse : collapse;
							border : 1px solid #000;
							margin : 15px;
						}
						table.std-table th{
							background : #000;
							color : #fff;
							border : 1px solid #000;
						}
						.qrcode {
							position: absolute;
							top:710px;
							width:80px;
							right:220px;
						}
					</style>
				</head>
				<body>
					<div class="page" id="first-page">
					<img class="qrcode" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.site_url('qrcode/view_qr/'.$id).'%2F&choe=UTF-8" title="Link to Detail Vendor" />
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="220px" valign="top"><font style="font-size : 9px; color : #fff">Dicetak pada tanggal : '.date("d/m/Y, H:i:s").'</font></td>
								<td width="680px">
									<table cellpadding="0" cellspacing="0" border="0" width="680px">
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center"><img src="'.base_url('assets/images/logo_pgn.png').'" width="65"></td>
										</tr>
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="color : #00aeef; font-size : 20px">SERTIFIKAT PENYEDIA BARANG/JASA TERDAFTAR</div>
												<div style="color : #4e5f6e">'.$no_certif.'</div>
											</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td height="10px" align="center"><b style="font-size : 20px">'.$fill['legal_name'].' '.$fill['name'].'</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="margin-left : 70px; border-bottom : 1px solid #000; border-top : 1px solid #000; width : 500px; padding : 20px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td width="140px" valign="top">NPWP</td>
															<td width="10px" valign="top">:</td>
															<td width="350px" valign="top">'.$fill['npwp_code'].'</td>
														</tr>
														<tr>
															<td valign="top">Alamat</td>
															<td valign="top">:</td>
															<td valign="top">'.nl2br($fill['vendor_address']).'</td>
														</tr>
														<tr>
															<td valign="top">No. Telp/Fax</td>
															<td valign="top">:</td>
															<td valign="top">'.$no_telp.'</td>
														</tr>
														<tr>
															<td valign="top">Klasifikasi</td>
															<td valign="top">:</td>
															<td valign="top">'.$klasifikasi.'</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>

											<td height="30px"></td>

										</tr>
										<tr>
											<td align="center">Jakarta, '.date("d/m/Y", strtotime($bsb_date)).'</td>
										</tr>
										<tr>
											<td height="80px"></td>
										</tr>
										<tr>
											<td align="center">'.$fill['sbu_name'].'</td>
										</tr>
										<tr>
											<td height="10px"></td>
										</tr>
										<tr>
											<td align="center">
												<font style="font-size : 8px">
													dicetak oleh Aplikasi Manajemen Penyedia Barang/Jasa PT Perusahaan Gas Negara Tbk.<br/>
													Dokumen ini resmi tanpa stempel dan/atau tanda tangan pejabat
												</font>
											</td>
										</tr>
									</table>
								</td>
								<td width="220px">&nbsp;</td>
							</tr>
						</table>
					</div>
					<div class="page" id="second-page">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td>'
									.$this->table_query_param('pengurus', $fill['id'], 'Pengurus Perusahaan').
								'</td>
							</tr>
						</table>'
						.$this->table_query_param('akta', $fill['id'],'Akta Pendirian / Perubahan Terakhir Perusahaan')
						.$this->table_query_param('situ', $fill['id'], 'Surat Keterangan Domisili Perusahaan / Surat Ijin Tempat Usaha')
						.$this->table_query_param('tdp', $fill['id'], 'Tanda Daftar Perusahaan')
						.$this->table_query_param('siup', $fill['id'], 'SIUP')
						.$this->table_query_param('siujk', $fill['id'], 'SIUJK')
						.$this->table_query_param('ijin_lain', $fill['id'], 'Surat Ijin Usaha Lainnya')
						.$this->table_query_param('sbu', $fill['id'], 'SBU');

						if($k3) $return .= $this->table_query_param('k3', $fill['id'], 'Penilaian Aspek K3');
						
		$return .=	
					'</div>
				</body>
			</html>';

		echo $return;die;
		// return;

		$dompdf = new DOMPDF();  
	    $dompdf->load_html($return);  
	    $dompdf->set_paper('A4','landscape');
	    $dompdf->render();
									
        $dompdf->stream("sertifikat-vendor.pdf",array('Attachment' => false));
	}
	
	function table_query_param($type = '', $id = '', $title = ''){
		$return = '';
		if($type == "pengurus"){
			$this->load->model('vendor/Pengurus_model');
			
			$query = $this->Pengurus_model->get_data_by_vendor($id);
			$param = array(
				array('header' => 'Nama','field' => 'name','type' => 'text'),
				array('header' => 'Jabatan','field' => 'position','type' => 'text'),
				//array('header' => 'Masa Berlaku Jabatan','field' => 'masa_berlaku_jabatan','type' => 'date'),
			);
			
			
		}
		else if($type == "akta"){
			$this->load->model('vendor/Akta_model');
			
			$param = array(
				array('header' => 'No. Akta','field' => 'no','type' => 'text'),
				array('header' => 'Notaris','field' => 'notaris','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Lembaga Pengesah','field' => 'authorize_by','type' => 'text'),
				array('header' => 'No. Pengesahan','field' => 'authorize_no','type' => 'text'),
				array('header' => 'Tanggal Pengesahan','field' => 'authorize_date','type' => 'date'),
				array('header' => 'Jenis','field' => 'type','type' => 'text')
			);
			$query = $this->Akta_model->get_data_by_vendor($id);			
		}
		else if($type == "situ"){
			$this->load->model('vendor/Situ_model');
			
			$param = array(
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Alamat','field' => 'address','type' => 'text'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->Situ_model->get_data_by_vendor($id);				
		}
		else if($type == "tdp"){
			$this->load->model('vendor/Tdp_model');
			
			$param = array(
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->Tdp_model->get_data_by_vendor($id);				
		}
		else if($type == "siup"){
			$this->load->model('vendor/Izin_model');
			
			$param = array(
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Kualifikasi','field' => 'qualification','type' => 'text'),
				array('header' => 'Bidang/Sub-Bidang','field' => 'bsb','type' => 'text'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->Izin_model->get_data_by_vendor($id, 'siup');	
			
			
		}
		else if($type == "siujk"){
			$this->load->model('vendor/Izin_model');
			
			$param = array(
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Kualifikasi','field' => 'qualification','type' => 'text'),
				array('header' => 'Bidang/Sub-Bidang','field' => 'bsb','type' => 'text'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->Izin_model->get_data_by_vendor($id, 'siujk');	
			
			
		}
		else if($type == "ijin_lain"){
			$this->load->model('vendor/Izin_model');
			
			$param = array(
				array('header' => 'Lembaga Penerbit','field' => 'authorize_by','type' => 'text'),
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Kualifikasi','field' => 'qualification','type' => 'text'),
				array('header' => 'Bidang/Sub-Bidang','field' => 'bsb','type' => 'text'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->Izin_model->get_data_by_vendor($id, 'ijin_lain');
			
			
		}
		else if($type == "sbu"){
			$this->load->model('vendor/vendor_model');
			
			$param = array(
				array('header' => 'Anggota Asosiasi','field' => 'authorize_by','type' => 'text'),
				array('header' => 'No.','field' => 'no','type' => 'text'),
				array('header' => 'Tanggal','field' => 'issue_date','type' => 'date'),
				array('header' => 'Masa Berlaku','field' => 'expire_date','type' => 'date'),
			);
			$query = $this->vendor_model->get_data_sbu($id, 'sbu');
			
			
		}
		else if($type == "k3"){
			
			$data = $this->km->get_csms($id);

			$return .= '<div class="table-separator">';
			$return .= '<table cellpadding="2" cellspacing="0" width="95%" border="1" class="std-table">';
				$return .= '<thead>';
					$return .= '<tr>';
						$return .= '<th style="background : #006aa1; color : #eee" height="20" colspan="3">'.$title.'</th>';
					$return .= '</tr>';
					$return .= '<tr>';
						$return .= '<th width="33%" style="background : #00a2dc">Klasifikasi</th>';
						$return .= '<th width="33%" style="background : #00a2dc">Skor</th>';
						$return .= '<th width="33%" style="background : #00a2dc">Masa Berlaku</th>';
					$return .= '</tr>';
				$return .= '</thead>';
				
				$return .= '<tbody>';
					$return .= '<tr>';
						$return .= '<td>Perusahaan '.$data['classification'].'</td>';
						$return .= '<td align="center">'.number_format($data['score'], 2).'</td>';
						$return .= '<td>'.default_date($data['expiry_date']).'</td>';
					$return .= '</tr>';
				$return .= '</tbody>';
			$return .= '</table>';

			return $return;
		}
		
		return $this->table_generator($query, $param, $title, '95%', $type, $id);
	}
	
	function table_generator($query = array(), $param = array(), $title = '', $width = '95%', $type = '',  $id_user = ''){
		$return = '';
		if(!$query->num_rows()) return " ";
		
		$return .= '<div class="table-separator">';
			$return .= '<table cellpadding="2" cellspacing="0" width="'.$width.'" border="1" class="std-table">';
				$return .= '<thead>';
					$return .= '<tr>';
						$return .= '<th style="background : #006aa1; color : #eee" height="20" colspan="'.count($param).'">'.$title.'</th>';
					$return .= '</tr>';
					$return .= '<tr>';
						
						foreach($param as $header)
						$return .= '<th style="background : #00a2dc">'.$header['header'].'</th>';
						
					$return .= '</tr>';
				$return .= '</thead>';
				$return .= '<tbody>';
				
				foreach($query->result() as $data){			
					$return .= '<tr>';
						
						foreach($param as $header){
							$return .= '<td>';
							
							if($header['field'] == "klasifikasi"){
								if($data->sub_type == "pengadaan") 
									$return .= "Badan Usaha non-Konstruksi / Pengadaan Barang";
								else if($data->sub_type == "konsultan") 
									$return .= "Badan Usaha non-Konstruksi / Jasa Konsultan non-Konstruksi";
								else if($data->sub_type == "lainnya") 
									$return .= "Badan Usaha non-Konstruksi / Jasa Lainnya";
							}
							else if($header['field'] == "bsb"){
								$bsb = $this->rm->get_detail_data($data->id, $type);
						
								$cur_bid = "";
								foreach($bsb->result() as $data1){
									if($cur_bid != $data1->id_bidang) $return .= '<div>'.$data1->bidang_name.'</div>';
									$cur_bid = $data1->id_bidang;
									
									$return .= '<div style="margin-left : 10px"> - '.$data1->sub_bidang_name.'</div>';
									
									if($title == 'SBU'){
										$return .= '<div style="margin-left : 25px"> &raquo; Grade : '.$data1->grade.'</div>'
												.'<div style="margin-left : 25px"> &raquo; No. Kode : '.$data1->no_kode.'</div>'
										 		.'<div style="margin-left : 25px"> &raquo; Kualifikasi : '.$data1->kualifikasi.'</div>';
										
									}
								}
							}
							else {
		
								if($data->{$header['field']} == ""){
									$return .= " - ";
								}	

								else if($header['type'] == 'date'){
									if($data->{$header['field']} != "lifetime") $return .= date("d M Y", strtotime($data->{$header['field']}));
									else $return .= "<i>Selama perusahaan masih berdiri</i>";
								}else{

									$return .= $data->{$header['field']};
								}

							}
							
							$return .= '</td>';
						}
						
						
					$return .= '</tr>';
					
				}
				
				$return .= '</tbody>';
			$return .= '</table>';
		$return .= '</div>';
		
		return $return;
	}


	function test($id = ""){
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L');
		$no_certif = '';
		$id_user = $this->id_user = $id;
		$bsb = $this->rm->get_dpt($id);
		
		$bsb_date = $this->rm->get_first($id);
		$bsb_date = $bsb_date['start_date'];	
		
		$fill = $this->vm->get_data($id_user);

		// print_r($fill);
		if($fill['id_sbu'] == 1) $sbu = "LOG";
		if($fill['id_sbu'] == 2) $sbu = "LUPP";
		if($fill['id_sbu'] == 3) $sbu = "SBU1";
		if($fill['id_sbu'] == 4) $sbu = "SBU2";
		if($fill['id_sbu'] == 5) $sbu = "SBU3";
		if($fill['id_sbu'] == 6) $sbu = "TSJ";
		
		$no_id = (6 - strlen($fill['id']));

		for($i=0;$i<$no_id;$i++) $no_certif .= "0";
		$no_certif .= $fill['id'];

		$no_certif = $no_certif."/PGN/".$sbu."/".date("d/m/Y", strtotime($bsb_date));
		
		$no_telp = $fill['vendor_phone']; 
		if($fill['vendor_fax']) $no_telp .= "/".$fill['vendor_fax'];
		
		
		$klasifikasi = "";

		foreach($bsb->result() as $_bsb){
			if($_bsb->id_dpt_type == 1)
				$klasifikasi .= "<div> - Non Konstruksi / Konsultan Non Konstruksi";
			if($_bsb->id_dpt_type == 2)
				$klasifikasi .= "<div> - Non Konstruksi / Pengadaan Barang";
			if($_bsb->id_dpt_type == 3)
				$klasifikasi .= "<div> - Non Konstruksi / Jasa Lainnya";
			if($_bsb->id_dpt_type == 4)
				$klasifikasi .= "<div> - Konstruksi / Jasa Pekerjaan Konstruksi";
			if($_bsb->id_dpt_type == 5)
				$klasifikasi .= "<div> - Konstruksi / Jasa Konsultan Perencana/Pengawas Konstruksi";
			
			$klasifikasi .= ' <i style="font-size : 10px">(pengukuhan terakhir pada '.date("d M Y", strtotime($_bsb->start_date)).')</i></div>';
		}
		

		$k3 = $this->km->get_csms($id);

		$return ='<html>
				<head>
				</head>
				<body>
					<div class="page" id="first-page">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="220px" valign="top"><font style="font-size : 9px; color : #fff">Dicetak pada tanggal : '.date("d/m/Y, H:i:s").'</font></td>
								<td width="680px">
									<table cellpadding="0" cellspacing="0" border="0" width="680px">
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center"><img src="'.base_url('assets/images/logo_pgn.png').'" width="65"></td>
										</tr>
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="color : #00aeef; font-size : 20px">SERTIFIKAT PENYEDIA BARANG/JASA TERDAFTAR</div>
												<div style="color : #4e5f6e">'.$no_certif.'</div>
											</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td height="10px" align="center"><b style="font-size : 20px">'.$fill['legal_name'].' '.$fill['name'].'</td>
										</tr>
										<tr>
											<td height="50px"></td>
										</tr>
										<tr>
											<td align="center">
												<div style="margin-left : 70px; border-bottom : 1px solid #000; border-top : 1px solid #000; width : 500px; padding : 20px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td width="140px" valign="top">NPWP</td>
															<td width="10px" valign="top">:</td>
															<td width="350px" valign="top">'.$fill['npwp_code'].'</td>
														</tr>
														<tr>
															<td valign="top">Alamat</td>
															<td valign="top">:</td>
															<td valign="top">'.nl2br($fill['vendor_address']).'</td>
														</tr>
														<tr>
															<td valign="top">No. Telp/Fax</td>
															<td valign="top">:</td>
															<td valign="top">'.$no_telp.'</td>
														</tr>
														<tr>
															<td valign="top">Klasifikasi</td>
															<td valign="top">:</td>
															<td valign="top">'.$klasifikasi.'</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td height="100px"></td>
										</tr>
										<tr>
											<td align="center">Jakarta, '.date("d/m/Y", strtotime($bsb_date)).'</td>
										</tr>
										<tr>
											<td height="80px"></td>
										</tr>
										<tr>
											<td align="center">'.$fill['sbu_name'].'</td>
										</tr>
										<tr>
											<td height="10px"></td>
										</tr>
										<tr>
											<td align="center">
												<font style="font-size : 8px">
													dicetak oleh Aplikasi Manajemen Penyedia Barang/Jasa PT Perusahaan Gas Negara (Persero) Tbk.<br/>
													Dokumen ini resmi tanpa stempel dan atau tanda tangan pejabat
												</font>
											</td>
										</tr>
									</table>
								</td>
								<td width="220px">&nbsp;</td>
							</tr>
						</table>
					</div>
					<div class="page" id="second-page">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td>'
									.$this->table_query_param('pengurus', $fill['id'], 'Pengurus Perusahaan').
								'</td>
							</tr>
						</table>'
						.$this->table_query_param('akta', $fill['id'],'Akta Pendirian / Perubahan Terakhir Perusahaan')
						.$this->table_query_param('situ', $fill['id'], 'Surat Keterangan Domisili Perusahaan / Surat Ijin Tempat Usaha')
						.$this->table_query_param('tdp', $fill['id'], 'Tanda Daftar Perusahaan')
						.$this->table_query_param('siup', $fill['id'], 'SIUP')
						.$this->table_query_param('siujk', $fill['id'], 'SIUJK')
						.$this->table_query_param('ijin_lain', $fill['id'], 'Surat Ijin Usaha Lainnya')
						.$this->table_query_param('sbu', $fill['id'], 'SBU');

						if($k3) $return .= $this->table_query_param('k3', $fill['id'], 'Penilaian Aspek K3');
						
		$return .=	
					'</div>
				</body>
			</html>';

		// echo $return;
		// return;

		// $dompdf = new DOMPDF();  
	 //    $dompdf->load_html($return);  
	 //    $dompdf->set_paper('A4','landscape');
	 //    $dompdf->render();
									
  //       $dompdf->stream("sertifikat-vendor.pdf",array('Attachment' => false));
		$stylesheet = file_get_contents('http://vms.pgn.co.id/assets/css/style_certificate.css');
		// echo $stylesheet;
		$pdf->WriteHTML($stylesheet,1);
		$pdf->WriteHTML($return,2);
		$pdf->Output('Test.pdf','D');
	}
}