<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Reporting extends MY_Controller
{
	public $field = array(
		array(
			"header"=>"Administrasi",
			"field"=>array(
				"vendor_city"=>"Kota",
				"satuan_unit"=>"Satuan Unit/Kerja",
				"vendor_phone"=>"No.Telp",
				"vendor_fax"=>"No.Fax",
				"vendor_email"=>"Email",
				"nppkp_code"=>"NPPKP",
				// "bsb"=>"Klasifikasi",
			)
		),
		array(
			"header"=>"Data Vendor",
			"field"=>array(
				"tdp"=>array(
					"title" => "TDP",
					"field" => array(
						"no_tdp" => "No.",
						"expire_date_tdp" => "Masa Berlaku s/d"
					)
				),
				"akta"=>array(
					"title" => "Akta Perusahaan",
					"field" => array(
						"no_akta" => "No.Akta",
						"notaris_akta" => "Notaris",
						"issue_date_akta" => "Tanggal"
					)	
				),
				"pengurus"=>array(
					"title" => "Pengurus",
					"field" => array(
						"komisaris" => "Komisaris",
						"direktur" => "Direktur"
					)
				),
				"asosiasi"=>array(
					"title" => "Sertifikasi Asosiasi/Lainnya",
					"field" => array(
						"authorize_by_asosiasi" => "Lembaga Penerbit",
						"expire_date_asosiasi" => "Masa Berlaku s/d"
					)
				),
				"situ"=>array(
					"title" => "SITU/Keterangan Domisili",
					"field" => array(
						"address" => "Alamat",
						"expire_date_situ" => "Masa Berlaku"
					)
				),
				"siup"=>array(
					"title" => "SIUP",
					"field" => array(
						"no_siup" => "No",
						"bsb_siup" => "Klasifikasi",
						"expire_date_siup" => "Masa Berlaku"
					)
				),
				"siujk"=>array(
					"title" => "SIUJK",
					"field" => array(
						"authorize_by_siujk" => "Anggota Asosiasi",
						"bsb_siujk" => "Klasifikasi",
						"expire_date_siujk" => "Masa Berlaku"
					)
				),
				"sbu"=>array(
					"title" => "SBU",
					"field" => array(
						"qualification_sbu" => "Kualifikasi",
						"bsb_sbu" => "Klasifikasi",
						"expire_date_sbu" => "Masa Berlaku"
					)
				),
				"ijin_lain"=>array(
					"title" => "Surat Ijin Usaha Lainnya",
					"field" => array(
						"authorize_by_lainnya" => "Anggota Asosiasi",
						"bsb_lainnya" => "Klasifikasi",
						"expire_date_lainnya" => "Masa Berlaku"
					)
				),
				"agen"=>array(
					"title" => "Pabrikan/Keagenan/Distributor",
					"field" => array(
						"type_agen" => "Jenis Keagenan",
						"produk" => "Produk/Merk",
						"expire_date_agen" => "Masa Berlaku"
					)
				),
				// "barang"=>array(
				// 	"title" => "Penyedia Barang",
				// 	"field" => array(
				// 		"penyedia_barang" => "Penyedia Barang",
				// 	)
				// ),
				// "jasa_lainnya"=>array(
				// 	"title" => "Jasa Lainnya",
				// 	"field" => array(
				// 		"jasa_lainnya" => "Jasa Lainnya",
				// 	)
				// ),
				// "non_kontuksi"=>array(
				// 	"title" => "Jasa Konsultan Non Konstruksi",
				// 	"field" => array(
				// 		"non_kontuksi" => "Jasa Non Kontruksi",
				// 	)
				// ),
				// "kontruksi"=>array(
				// 	"title" => "Jasa Pekerjaan Konstruksi",
				// 	"field" => array(
				// 		"kontruksi" => "Jasa Kontruksi",
				// 	)
				// ),
				// "konsultan_perencana"=>array(
				// 	"title" => "Jasa Konsultan Perencana / Pengawas Kosntruksi",
				// 	"field" => array(
				// 		"konsultan_perencana" => "Jasa Konsultan Perencana / Pengawas Kosntruksi",
				// 	)
				// ),
				// "vendor_type"=>array(
				// 	"title" => "Status Perusahaan",
				// 	"field" => array(
				// 		"vendor_type" => "Status Perusahaan",
				// 	)
				// ),
			)
		),
	);
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('reporting_model','rm');
		$this->load->model('get_model','gm');
		// $this->form = array(
		// 	'filter'=> array(
		// 		array(
		// 			'field' => 'barang',
		// 			'type'	=> 'dropdown',
		// 			'label' => 'Penyedia Barang',
		// 			// 'source'=> $this->gm->get_report_barang()
		// 		),
		// 		array(
		// 			'field' => 'jasa_lainnya',
		// 			'type'	=> 'dropdown',
		// 			'label' => 'Jasa Lainnya',
		// 			// 'source'=> $this->gm->get_report_jasalain()
		// 		),
		// 	)
		// );
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('reporting'),
			'title' => 'Custom Report'
		));
		$this->header = 'Custom Report';
		$this->content = $this->load->view('reporting/list',$data, TRUE);
		$this->script = $this->load->view('reporting/list_js', $data, TRUE);
		parent::index();
	}

	public function view_list()
	{
		foreach($this->form['form'] as $key => $element) {
		 	// $this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }

		$this->form['url'] = site_url('reporting/custom_report');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Submit',
			)
		);

		echo json_encode($this->form);
	}

	public function getHeader(){
 		echo json_encode($this->field);
 	}

 	public function cetak()
 	{
 		$html = '';
		$no  = 1;
		$header = "";
		$arrayField = array();
		$_POST['filter'] = $_GET['filter'];
		foreach ($this->field as $_key => $_value) {
			foreach($_value['field'] as $sub_key => $sub_value){
				if($_POST['filter'][$sub_key]==1){
					$arrayField[$_key]['field'][$sub_key]=$sub_value;
				}
			}
			if(count($arrayField[$_key]['field'])>0){
				$arrayField[$_key]['header']=$_value['header'];
			}
		}
		$this->field = $arrayField;

		foreach($this->field as $key=>$row){
			$lv1 ="<td colspan={{colspan}}>".$row['header']."</td>"; //LEVEL 1
			$colspan = 0;
			
			foreach ($row['field'] as $keylv1 => $valuelv1) {
				$rowspan = 3;
				// $rowspan--;
				if(is_array($valuelv1)){
					$rowspan--;
					$colspan+=count($valuelv1['field']);
					
					$colspan2=0;

					foreach ($valuelv1['field'] as $keylv2 => $valuelv2) {

						if(is_array($valuelv2)){
							$colspan2+=count($valuelv2['field']);

							$lv3="<td rowspan={{rowspan}} colspan={{colspan2}}>".$valuelv2['title']."</td>";
							
							foreach ($valuelv2['field'] as $keylv3 => $valuelv3) {
								
								
								
								$_lv4.="<td >".$valuelv3."</td>";
								
							}
							$colspan+=1;
							$rs = 1;
						}else{
							$colspan2+=1;
							$lv3="<td rowspan={{rowspan}}>".$valuelv2."</td>";
							$rs = $rowspan;
						}
						
						// $_lv3 .= $lv3;
						$_lv3 .= str_replace(array('{{rowspan}}','{{colspan2}}'), array($rs, $colspan2), $lv3);
						
					}
					
					$lv2="<td rowspan=1 colspan={{colspan2}}>".$valuelv1['title']."</td>";
				}else{
					
					$lv2="<td rowspan={{rowspan}}>".$valuelv1."</td>";
					$colspan+=1;
				}

				
				
				
				// $lv2="<td rowspan={{rowspan}}>".$valuelv1."</td>";
				$_lv2 .= str_replace(array('{{rowspan}}','{{colspan2}}'), array($rowspan,$colspan2), $lv2);

			}

			$lv1 = str_replace('{{colspan}}', $colspan, $lv1);
			$_lv1 .= $lv1;
			$cs +=(($colspan==0) ? 1 : $colspan);

		}
		$data = '';
		
			$dpt = $this->rm->get_dpt_list();
			// echo print_r($dpt);
			$id = 1;
			$_data = '';
			
			foreach ($dpt as $key => $value) {
				// echo print_r($value);
				$klasifikasi = $this->rm->getKlasifikasi($value['id']);
				$more = array();
				$rowspan = count(max($value));

					$_data .= "<tr>
								<td rowspan={{rowspan}}>".$id."</td>
								<td rowspan={{rowspan}}>".$value['name']."</td>
								<td rowspan={{rowspan}}>".$value['badan_usaha']."</td>
								<td rowspan={{rowspan}}>".$value['vendor_address']."</td>
								<td rowspan={{rowspan}}>".$value['npwp_code']."</td>
								<td rowspan={{rowspan}}>".$klasifikasi."</td>";
					$emptyField = array();
					foreach ($this->field as $keys => $values) {
						
						$_data .= $this->recursive($values, $value, $more);
						foreach($value as $key_multi => $valMulti){
							if(is_array($valMulti)){
								// foreach($moreValue as $key_more => $mores){
									$emptyField[$key_multi] = 1;
								// }
							}
						}
					}
					$_data .="</tr>";
					
					if(count($more)>0){
						// foreach($more as $moreKey => $moreValue){
						// 	foreach($moreValue as $key_more => $mores){
						// 		$emptyField[$key_more] = 1;
						// 	}
						// }

						foreach($more as $moreKey => $moreValue){
							
							$_data  .= "<tr>";

							foreach($emptyField as $key_more => $more){

								$_data .= "<td ".$key_more.">".$moreValue[$key_more]."</td>";
							}
							$_data .="</tr>";
						}
					}
					$max_colspan = 1;
					$id++;
				
				$_data = str_replace('{{rowspan}}', $rowspan, $_data);
				
			}
			$data.=$_data;
		$header .="	<tr>
						<td rowspan=4><b>No</b></td>
						<td rowspan=4><b>NAMA Badan Usaha</b></td>
						<td rowspan=4><b>Badan Usaha</b></td>
						<td rowspan=4><b>Alamat</b></td>
						<td rowspan=4><b>NPWP</b></td>
						<td rowspan=4><b>Klasifikasi</b></td>
						".$_lv1."
					</tr><tr>".$_lv2."</tr><tr>".$_lv3."</tr><tr>".$_lv4."</tr>";
		
		
		$html .= '<html>
				<head>
					
					<style type="text/css">

						@page{
							size: A4 portrait;
							page-break-after : always;
							margin : 10px;
						}
						
						@media all{
							ol{
								padding-left : 20px;
								padding-top : -15px;
								padding-bottom : -15px;
							}
							
							table { page-break-inside:avoid; }
						    tr    { page-break-inside: avoid; }
						    thead { display:table-header-group; }
					    }
					table{
						width: 100%;
					}
    				</style>
				</head>
				<body>
					<table  border=1>
						'.$header.'
						'.$data.'
					</table>
				</body>
				</html>
				';
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=Laporan.xls");
		echo $html;
 	}

 	function recursive($array,$values, &$more){
		$_return =  array();
		$return='';
		$is_first = 1;
		
		foreach($array['field'] as $key => $value){

			if(is_array($value)){
				$r = $this->recursive($value, $values, $more);
				$return.= $r;
			}else{
				if(is_array($values[$key]) && count($values[$key])>0){
					
					switch ($key) {
						case 'contract_price':
						case 'contract_price_kurs':
						case 'idr_value_bap':
							$return.="<td  >".number_format($values[$key][0])."</td>";
						break;
						case 'tanggal_kontrak':
						case 'tanggal_kerja':
						case 'work_end':
						case 'date_bap':
						case 'date_bast':

							$return.="<td >".$values[$key][0]."</td>";
						break;
						case 'authorize_by_asosiasi':
						case 'expire_date_asosiasi':
						case 'durasi':
						case 'nomor_bap':
						case 'nomor_bast':
						default:
							$return.="<td >".$values[$key][0]."</td>";
						break;
					}
					
					
					$otherData = $values[$key];

					unset($otherData[0]);

					if(count($values[$key])>0){
						foreach ($otherData as $_key => $_value) {
							switch ($key) {
								case 'contract_price':
								case 'contract_price_kurs':
								case 'idr_value_bap':
									$_value=number_format($_value);
								break;
								case 'tanggal_kontrak':
								case 'tanggal_kerja':
								case 'work_end':
								case 'date_bap':
								case 'date_bast':
									$_value=default_date($_value);
								break;
								case 'authorize_by_asosiasi':
								case 'expire_date_asosiasi':
								case 'no_kontrak':
								case 'durasi':
								case 'nomor_bap':
								case 'nomor_bast':
								default:
									$_value=$_value;
								break;
							}
							$more[$_key][$key] = $_value;
						}
					}
					

				}else{
					// 	
					switch ($key) {
						case 'issue_date':
						case 'date_evaluasi':
							if($values[$key]!=''){
								$return.="<td rowspan={{rowspan}}>".default_date($values[$key])."</td>";
							}else{
								$return.="<td rowspan={{rowspan}}></td>";
							}
							
							break;
						case 'hps':

							$return.="<td rowspan={{rowspan}}>".number_format($values[$key])."</td>";
							break;

						default:
							$return.="<td rowspan={{rowspan}}>".$values[$key]."</td>";
							break;
					}
				}
			}
		}

		return $return;
	}
}