<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Template_test extends CI_Controller {


	function index() {
		$this->load->view('template/3-starter-kit/build/index.php');
	}

	function show($param) {
		$this->load->view('template/3-starter-kit/build/'.$param.'.php');
	}

}