<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){

		parent::__construct();
		// if(!$this->session->userdata('user')){
		// 	redirect(site_url());
		// }
		$this->load->model('vendor/dashboard_model','dm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('approval_model','am');
		$this->load->model('note_model','nm');
		
		$this->insertUrl = site_url('vendor/dashboard/save/');
		$this->updateUrl = 'vendor/dashboard/update/';
		$this->deleteUrl = 'vendor/dashboard/delete/';
	}

	public function index($id=null)
	{
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');
        $data['admin'] = $admin;
        $data['session'] = (($user) ? $user : $admin);
		$data = $this->vm->get_data($user['id_user']);
		$data['pengalaman_bsb']	 = $this->am->checkPengalamanBSB($user['id_user']);
		$data['approval_data'] = $this->am->get_total_data($user['id_user']);
		$data['notifikasi'] = $this->nm->get_note($user['id_user']);
		$data['bar'] = array(
									'pending'=>array(
										'percentage'=>(count($data['approval_data'][0]))/$data['approval_data']['total']*100,
										'value'=>count($data['approval_data'][0])
									),
									'approved'=>array(
										'percentage'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))
									),
									'rejected'=>array(
										'percentage'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))
									)
								);
        
		$this->header = 'Selamat Datang '.(($user) ? $user['name'] : $admin['name']);

		$this->content = $this->load->view('vendor/dashboard/list',$data, TRUE);
		$this->script  = $this->load->view('vendor/dashboard/list_js', $data, TRUE);

		parent::index();
	}

	function pernyataan(){
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
					'field'=>'pic_name',
					'label'=>'Nama',
					'rules'=>'required'
					),
				array(
					'field'=>'pic_position',
					'label'=>'Jabatan',
					'rules'=>'required'
					),
				array(
					'field'=>'pic_phone',
					'label'=>'No Telp',
					'rules'=>'required|numeric'
					),
				array(
					'field'=>'pic_email',
					'label'=>'Email',
					'rules'=>'required|valid_email'
					),
				array(
					'field'=>'pic_address',
					'label'=>'Alamat',
					'rules'=>'required'
					),
			),
		);
	}
	
	
}
