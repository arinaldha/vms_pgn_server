<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Agen extends VMS {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_agen';

	public $module = 'Agen';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/agen_model','am');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form_produk = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'produk',
		            'type'	=>	'text',
		            'label'	=>	'Produk',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'merk',
		            'type'	=>	'text',
		            'label'	=>	'Merk',
		            'rules' => 	'required',
	         	),
	         ),
			'filter'=>array(
	         	array(
		            'field'	=> 	'produk',
		            'type'	=>	'text',
		            'label'	=>	'Produk',
	         	),
	         	array(
		            'field'	=> 	'merk',
		            'type'	=>	'text',
		            'label'	=>	'Merk',
	         	),
	         ),
		);

		$this->form_bsb = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'id_bidang',
		            'type'	=>	'dropdown',
		            'label'	=>	'Nama Bidang',
		            'source'=>  $this->am->getBidangAgen(),
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id_bsb',
		            'type'	=>	'dropdown',
		            'label'	=>	'Nama Sub Bidang',
		            'source'=>  $this->am->getSubBidangAgen(),
		            'rules' => 	'required',
	         	),
	         ),
			'filter'=>array(
	         	array(
		            'field'	=> 	'id_bidang',
		            'type'	=>	'dropdown',
		            'label'	=>	'Nama Bidang',
		            'source'=>  $this->am->getBidangAgen(),
	         	),
	         	array(
		            'field'	=> 	'id_bsb',
		            'type'	=>	'dropdown',
		            'label'	=>	'Nama Sub Bidang',
		            'source'=>  $this->am->getSubBidangAgen(),
	         	),
	         ),
		);

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'agen_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/agen_file/'),
					'upload_url'=>site_url('vendor/agen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
		            'rules' => 	'required'
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
			)
		);
		$this->getData = $this->am->getData($this->form);
		$this->insertUrl = site_url('vendor/agen/save/');
		$this->updateUrl = 'vendor/agen/update/';
		$this->deleteUrl = 'vendor/agen/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/agen'),
			'title' => 'Pabrikan/Keagenan/Distributor'
		));
		$this->header = 'Pabrikan/Keagenan/Distributor';
		$this->content = $this->load->view('vendor/agen/list',$data, TRUE);
		$this->script = $this->load->view('vendor/agen/list_js', $data, TRUE);
		parent::index();
	}

	 public function produk($id) 
	{
		$id = base64_decode($id);
	 	$user = $this->session->userdata('user');
		$dataProduk = $this->am->getDataProduk($id);
		$this->id_client = $id;

		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/agen'),
			'title' => 'Pabrikan/Keagenan/Distributor'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/agen/produk/'.$id),
			'title' => 'Produk'
		));
		$data['id'] = $id;
		$data['dataProduk'] = $dataProduk;
		$data['user'] = $user;
		$this->header = 'Produk';
		$this->content = $this->load->view('vendor/agen/list_produk',$data, TRUE);
		$this->script = $this->load->view('vendor/agen/list_produk_js', $data, TRUE);
		parent::index();
	 }

	 public function bsb($id) {
	 	$id = base64_decode($id);
	 	$user = $this->session->userdata('user');
		$dataBsb = $this->am->getDataBsb($id);
		// print_r($this->db->last_query());die;
		$this->id_client = $id;

		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/agen/'),
			'title' => 'Pabrikan/Keagenan/Distributor'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/agen/bsb/'.$id),
			'title' => 'Bidang / Sub Bidang '
		));
		$data['id'] = $id;
		$data['dataBsb'] = $dataBsb;
		$data['user'] = $user;
		$this->header = 'Bidang / Sub Bidang ';
		$this->content = $this->load->view('vendor/agen/list_bsb',$data, TRUE);
		$this->script = $this->load->view('vendor/agen/list_bsb_js', $data, TRUE);
		parent::index();
	 }

	 public function produkView($id = null)
	 {
	 	$id = base64_decode($id);
		$config['query'] = $this->am->getDataProduk($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	 }

	 public function bsbView($id = null)
	 {
	 	$id = base64_decode($id);
		$config['query'] = $this->am->getDataBsb($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	 }

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/agen/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$this->load->library('VMS');
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = timestamp();
	 		$id = $this->$modelAlias->insert($save);
	 		if ($id) {
	 			$file_agen = $save['type'];
	 			$data = array(
	 				'id'=>$id,
	 				'expire_date'=>$save['expire_date'],
	 				'doc_type'=>'ms_agen',
	 				'no'=>$save['no'],
	 				'doc'=>$file_agen
	 			);
	 			$this->stillActive($user['id_user']);
	 			// $this->vms->setEmailBlast($data);
	 			$this->deleteTemp($save);
	 		}
	 	}
	 }

	 public function insertProduk($id)
	 {
	 	$id = base64_decode($id);
	 	$this->form = $this->form_produk;
	 	$this->form['url'] = site_url('vendor/agen/saveProduk/'.base64_encode($id));
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function saveProduk($id)
	 {
	 	$id = base64_decode($id);
	 	$user = $this->session->userdata('user');
	 	$this->form = $this->form_produk;
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_agen'] = $id;
	 		$save['entry_stamp'] = timestamp();
	 		if ($this->$modelAlias->insert_produk($save)) {
	 			$this->stillActive($user['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	 public function insertBsb($id)
	 {
	 	$id = base64_decode($id);
	 	$this->form = $this->form_bsb;
	 	$this->form['url'] = site_url('vendor/agen/saveBsb/'.base64_encode($id));
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function saveBsb($id)
	 {
	 	$id = base64_decode($id);
	 	$user = $this->session->userdata('user');
	 	$this->form = $this->form_bsb;
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_agen'] = $id;
			$save['id_vendor'] = $user['id_user'];
			$save['id_bidang'] = $save['id_bidang'];
			$save['id_bsb'] = $save['id_bsb'];
			$save['entry_stamp'] = date('Y-m-d H:i:s');
	 		if ($this->$modelAlias->insert_bsb($save)) {
	 			$this->stillActive($user['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }


	 public function editProduk($id = null)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$this->form = $this->form_produk;
		$data = $this->$modelAlias->selectDataProduk($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url('vendor/agen/updateProduk/'. base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function updateProduk($id)
	{
		$id = base64_decode($id);
		$user = $this->session->userdata('user');
		$this->form = $this->form_produk;
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->$modelAlias->selectDataProduk($id);
			if ($this->$modelAlias->update_produk($id, $save)) {
				$this->stillActive($user['id_user']);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function editBsb($id = null)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$this->form = $this->form_bsb;
		$data = $this->$modelAlias->selectDataBsb($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url('vendor/agen/updateBsb/'. base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function updateBsb($id)
	{
		$id = base64_decode($id);
		$user = $this->session->userdata('user');
		$this->form = $this->form_bsb;
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->$modelAlias->selectDataBsb($id);
			if ($this->$modelAlias->update_bsb($id, $save)) {
				$this->stillActive($user['id_user']);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}


	public function deleteProduk($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_produk($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function removeProduk($id)
	{
		$id = base64_decode($id);
		$this->formDelete['url'] = site_url('vendor/agen/deleteProduk/'. base64_encode($id));
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function deleteBsb($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_bsb($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function removeBsb($id)
	{
		$this->formDelete['url'] = site_url('vendor/agen/deleteBsb/'. $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function agenView($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if($this->form['form'][$key]['value'] == 'lifetime'){
			// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
			// }
		}
		echo json_encode($this->form);
	}

	public function cekBsbView($id){
		$id = base64_decode($id);
		$this->form = $this->form_bsb;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataBsb($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}
		echo json_encode($this->form);
	}

	public function cekProdukView($id){
		$id = base64_decode($id);
		$this->form = $this->form_produk;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataProduk($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}
		echo json_encode($this->form);
	}

	function formFilterProduk()
	{
		$this->form = $this->form_produk;
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	function formFilterBsb()
	{
		$this->form = $this->form_bsb;
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

}
