<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Administrasi extends VMS {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Administrasi';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/administrasi_model','am');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'=>'id_sbu',
					'type' =>'hidden',
					'rules'=>'hidden'
		        ),
		        array(
		            'field'=>'id_legal',
					'label'=>'Badan Usaha',
					'type' =>'dropdown',
					'source' =>	$this->gm->getdatalegal()
	         	),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Badan Usaha',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'npwp_code',
		            'type'	=>	'npwp',
		            'label'	=>	'NPWP',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'npwp_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Pengukuhan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'npwp_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/npwp_file/'),
					'upload_url'=>site_url('vendor/administrasi/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'nppkp_code',
		            'type'	=>	'text',
		            'label'	=>	'NPPKP', 
	         	),
	         	array(
	         		'field'	=> 	'nppkp_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Pengukuhan', 
	         	),
	         	array(
		            'field'	=> 	'nppkp_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/nppkp_file/'),
					'upload_url'=>site_url('vendor/administrasi/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
	         		'field'	=> 	'vendor_office_status',
		            'type'	=>	'radio',
		            'label'	=>	'Status',
		            'source'=> array(
		            			'pusat' => 'Pusat',
		            			'cabang'=> 'Cabang'
		            ),
	         	),
	         	array(
		            'field'	=> 	'vendor_address',
		            'type'	=>	'textarea',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_city',
		            'type'	=>	'text',
		            'label'	=>	'Kota/Kab',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_province',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_country',
		            'type'	=>	'text',
		            'label'	=>	'Negara',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_postal',
		            'type'	=>	'text',
		            'label'	=>	'Kode Pos',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_phone',
		            'type'	=>	'text',
		            'label'	=>	'No Telp',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_fax',
		            'type'	=>	'text',
		            'label'	=>	'Fax'
	         	),
	         	array(
		            'field'	=> 	'vendor_email',
		            'type'	=>	'text',
		            'label'	=>	'Email',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_website',
		            'type'	=>	'text',
		            'label'	=>	'Website'
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	),
	         	array(
		            'field'	=> 	'is_nppkp',
		            'type'	=>  'hidden'
	         	),
	         )
		);
		$this->insertUrl = 'vendor/administrasi/save/';
		$this->updateUrl = 'vendor/administrasi/update/';
		$this->deleteUrl = 'vendor/administrasi/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');

		 if($this->vm->check_pic($data['id_user'])==0){
		 	redirect(site_url('vendor/pernyataan'));
		 }
		 $this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/administrasi'),
			'title' => 'Administrasi'
		));
		$this->header = 'Administrasi';
		$this->content = $this->load->view('vendor/administrasi/view',$data, TRUE);
		$this->script = $this->load->view('vendor/administrasi/view_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/administrasi/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }
	public function view(){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	/*if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}*/
		 }
		echo json_encode($this->form);

	 }

	 public function editAdministrasi($id = null){
		 $id = base64_decode($id);
	 	 $user = $this->session->userdata('user');
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['field'] == 'id_sbu') {
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		// $this->form['form']['is_nppkp']['value'] ==;
		$this->form['url'] = site_url('vendor/administrasi/update/'.base64_encode($id));
		$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan Perubahan',
	 		)
	 	);
		echo json_encode($this->form);
	 }

	public function update($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
//print_r($save);die;
			$save['edit_stamp'] = date('Y-m-d H:i:s');
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				//$this->dpt->non_iu_change($user['id_user']);
				$this->stillActive($id);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function getDataBank($id = null)
	{
		$config['query'] = $this->am->get_bank($this->session->userdata('user')['id_user']);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

}
