<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pernyataan extends MY_Controller {

	public $form;

	public $modelAlias = 'vm';

	public $alias = 'ms_vendor_pic';

	public $module = 'Surat Pernyataan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/vendor_model','vm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
					'field'=>'pic_name',
					'label'=>'Nama',
					'type' => 'text',
					'rules'=>'required'
					),
				array(
					'field'=>'pic_position',
					'label'=>'Jabatan',
					'type' => 'text',
					'rules'=>'required'
					),
				array(
					'field'=>'pic_phone',
					'label'=>'No Telp',
					'type' => 'text',
					'rules'=>'required|numeric'
					),
				array(
					'field'=>'pic_email',
					'label'=>'Email',
					'type' => 'text',
					'rules'=>'required|valid_email'
					),
				array(
					'field'=>'pic_address',
					'label'=>'Alamat',
					'type' => 'textarea',
					'rules'=>'required'
					),
			),
		);
		$this->insertUrl = site_url('vendor/pernyataan/save/');
		$this->updateUrl = 'vendor/pernyataan/update/';
		$this->deleteUrl = 'vendor/pernyataan/delete/';
	}
	
	public function index(){
		$data['user'] = $this->session->userdata('user');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/pernyataan'),
			'title' => 'Surat Pernyataan'
		));
		$this->header = 'Surat Pernyataan';
		$this->content = $this->load->view('vendor/pernyataan/list',$data, TRUE);
		$this->script = $this->load->view('vendor/pernyataan/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/pernyataan/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = date('Y-m-d H:i:s');
	 		if ($this->$modelAlias->save_pic($save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }
}
