<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengalaman extends VMS {

	public $form;

	public $modelAlias = 'pm';

	public $alias = 'ms_pengalaman';

	public $module = 'Pengalaman';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/pengalaman_model','pm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');
		$id_vendor = $user['id_user'];

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'id_iu_bsb',
		            'type'	=> 'dropdown',
		            'label'	=> 'Bidang/Sub-Bidang Pekerjaan',
		            'source'=> $this->pm->getBsbIu($id_vendor),
		        ),
		        array(
		            'field'	=> 	'job_name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Kerjaan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'job_location',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi',
	         	),
	         	array(
		            'field'	=> 	'job_giver',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pemberi Kerja',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'phone_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Telp',
	         	),
	         	array(
		            'field'	=> 	'contract_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Kontrak',
		            'rules' =>	'required',
	         	),
	         	array(
		            'field'	=> 	array('contract_start','contract_end'),
		            'type'	=>	'date_range',
		            'label'	=>	'Tanggal Kontrak',
	         	),
	         	array(
		            'field'	=> 	'price_idr',
		            'type'	=>	'money',
		            'label'	=>	'Nilai Kontrak',
	         	),
	         	 array(
		            'field'	=> 	array('currency','price_foreign'),
		            'type'	=>	'money_asing',
		            'label'	=>	'Nilai Kontrak Kurs',
		            'source' =>  $this->gm->getKurs(),
		            'caption'=> '<span class="english-caption">*Diisi bila ada.</span>'
	         	 ),
	         	array(
		            'field'	=> 	'contract_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen Kontrak)',
		            'upload_path'=>base_url('assets/lampiran/contract_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'bast_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Selesai Sesuai BAST',
	         	),array(
		            'field'	=> 	'bast_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen BAST)',
		            'upload_path'=>base_url('assets/lampiran/bast_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	       
			),
			'filter' => array(
				array(
		            'field'	=> 'job_name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Paket Kerjaan'
		        ),
		        array(
		            'field'	=> 	'b|name',
		            'type'	=>	'text',
		            'label'	=>	'Bidang'
	         	),
			)
		);
		$this->getData = $this->pm->getData($this->form);

		$this->insertUrl = site_url('vendor/pengalaman/save/');
		$this->updateUrl = 'vendor/pengalaman/update/';
		$this->deleteUrl = 'vendor/pengalaman/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/pengalaman'),
			'title' => 'Pengalaman'
		));
		$this->header = 'Pengalaman';
		$this->content = $this->load->view('vendor/pengalaman/list',$data, TRUE);
		$this->script = $this->load->view('vendor/pengalaman/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/pengalaman/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();

	 		$save['id_ijin_usaha'] = $this->pm->get_id_ijin_usaha($save['id_iu_bsb']);

	 		$save['price_idr'] = currency($save['price_idr']);
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['price_foreign'] = currency($save['price_foreign']);

			unset($save['contract_period']);		
	 		$save['entry_stamp'] = timestamp();
	 		if ($this->$modelAlias->insert($save)) {
	 			$this->stillActive($this->session->userdata('user')['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	 public function pengalamanView($id)
	 {
	 	$id = base64_decode($id);
	 	$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'id_iu_bsb',
		            'type'	=> 'dropdown',
		            'label'	=> 'Bidang/Sub-Bidang Pekerjaan',
		            'source'=> $this->pm->getBsbIu($id)
		        ),
		        array(
		            'field'	=> 	'job_name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Kerjaan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'job_location',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi',
	         	),
	         	array(
		            'field'	=> 	'job_giver',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pemberi Kerja',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'phone_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Telp',
	         	),
	         	array(
		            'field'	=> 	'contract_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		           'field'	=> 	'contract_start',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Mulai Kontrak',
	         	),
	         	array(
		           'field'	=> 	'contract_end',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Berakhir Kontrak',
	         	),
	         	array(
		            'field'	=> 	'price_idr',
		            'type'	=>	'money',
		            'label'	=>	'Nilai Kontrak',
	         	),
	         	 array(
		            'field'	=> 	array('currency','price_foreign'),
		            'type'	=>	'money_asing',
		            'label'	=>	'Nilai Kontrak Kurs',
		            'source' =>  $this->gm->getKurs(),
		            'caption'=> '<span class="english-caption">*Diisi bila ada.</span>'
	         	 ),
	         	array(
		            'field'	=> 	'contract_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen Kontrak)',
		            'upload_path'=>base_url('assets/lampiran/contract_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'bast_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Selesai Sesuai BAST',
	         	),array(
		            'field'	=> 	'bast_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen BAST)',
		            'upload_path'=>base_url('assets/lampiran/bast_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' =>	'required'
	         	),
	       
			),
		);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if( $element['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $data[$element['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($data[$element['field'][1]]);
            }  
		}
		echo json_encode($this->form);
	 }
	 public function edit($id = null)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if( $element['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $data[$element['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($data[$element['field'][1]]);
            }  
            if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();
				
				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
					
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}

		$this->form['data'] = $data;
		$this->form['url'] = site_url($this->updateUrl . '/' . base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function update($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_ijin_usaha'] = $this->pm->get_id_ijin_usaha($save['id_iu_bsb']);
			$save['price_idr'] = currency($save['price_idr']);
	 		$save['price_foreign'] = currency($save['price_foreign']);
	 		$save['contract_period'] = json_decode($save['contract_period']);
	 		//$save['contract_start'] = $save['contract_period']->start;
			//$save['contract_end'] = $save['contract_period']->end;	 
			unset($save['contract_period']);		
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->stillActive($this->session->userdata('user')['id_user']);
				$this->dpt->non_iu_change($user['id_user']);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}
