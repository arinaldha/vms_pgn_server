<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Blacklist_vendor extends MY_Controller {

	public $form;

	public $module = 'Blacklist';

	public $modelAlias = 'bm';

	public $alias = 'tr_blacklist';
	
	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/blacklist_model','bm');

		$this->form = array(
			'form' => array(
				array(
						'field'	=> 	'id_vendor',
						'type'	=>	'text',
						'label'	=>	'Vendor'
				),
				array(
						'field'	=> 	'start_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Mulai',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'end_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Akhir',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'remark',
						'type'	=>	'textarea',
						'label'	=>	'Alasan',
						'rules' =>	'required'
				),
				array(
		            'field'	=> 	'blacklist_file',
		            'type'	=>	'file',
		            'label'	=>	'lampiran',
		            'upload_path'=>base_url('assets/lampiran/blacklist_file/'),
					'upload_url'=>site_url('vendor/admin/black_list/blacklist_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
			),
			'filter'=>array(
	         	array(
		            'field'	=> 	'a|name',
					'type'	=>	'text',
					'label'	=>	'Vendor'
	         	),
	         	array(
		            'field'	=> 	'c|npwp_code',
					'type'	=>	'text',
					'label'	=>	'NPWP'
	         	)
			),

		);

		$this->form_aktif = array(
			'form' => array(
				array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Vendor'
				),
				array(
						'field'	=> 	'white_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Mulai',
						'rules' =>	'required'
				),
				array(
		            'field'	=> 	'white_file',
		            'type'	=>	'file',
		            'label'	=>	'lampiran',
		            'upload_path'=>base_url('assets/lampiran/blacklist_file/'),
					'upload_url'=>site_url('vendor/admin/black_list/blacklist_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
			)
		);

		$this->form_edit = array(
			'form' => array(
				array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Vendor'
				),
				array(
						'field'	=> 	'start_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Mulai',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'end_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Akhir',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'remark',
						'type'	=>	'textarea',
						'label'	=>	'Alasan',
						'rules' =>	'required'
				),
				array(
		            'field'	=> 	'blacklist_file',
		            'type'	=>	'file',
		            'label'	=>	'lampiran',
		            'upload_path'=>base_url('assets/lampiran/blacklist_file/'),
					'upload_url'=>site_url('vendor/admin/black_list/blacklist_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
			),
		);
		$this->updateUrl = 'vendor/admin/black_list/blacklist_vendor/update';
	}
	
	public function index(){
		$data['data'] = $this->bm->get_blacklist_vendor();
	 	$this->load->view('vendor/admin/blacklist/list_vendor',$data, false);
	 	$this->load->view('vendor/admin/blacklist/list_vendor_js', $data, false);
	}

	public function getData($id = null)
	{
		$config['query'] = $this->bm->get_blacklist_vendor();;
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function edit($id = null)
	{
		$id = base64_decode($id);
		$this->form = $this->form_edit;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->select_by_vendor($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['field']=='name'){
				$this->form['form'][$key]['readonly'] = true;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$this->form_validation->set_rules($this->form['form']);
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->$modelAlias->select_by_vendor($id);
			if ($this->$modelAlias->edit_by_vendor($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function aktif($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->select_by_vendor($id);
		$this->form = $this->form_aktif;

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['field']=='name'){
				$this->form['form'][$key]['readonly'] = true;
			}
		}


		$this->form['url'] = site_url('vendor/admin/black_list/blacklist_vendor/aktif_vendor/' . base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Aktifkan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function aktif_vendor($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$this->form = $this->form_aktif;
		$this->form_validation->set_rules($this->form);
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id'] = $id;
			$lastData = $this->$modelAlias->select_by_vendor($id);
			if ($this->$modelAlias->aktif_by_vendor($save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function search_vendor(){
		echo json_encode($this->bm->search_vendor());
	}

	public function insert()
	{
		$this->form = array(
			'form' => array(
				array(
						'field'	=> 	'id_vendor',
						'type'	=>	'hidden',
						'label'	=>	'ID Vendor'
				),
				array(
						'field'	=> 	'name',
						'type'	=>	'search',
						'label'	=>	'Vendor',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'start_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Mulai',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'end_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Akhir',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'remark',
						'type'	=>	'textarea',
						'label'	=>	'Alasan',
						'rules' =>	'required'
				),
				array(
		            'field'	=> 	'blacklist_file',
		            'type'	=>	'file',
		            'label'	=>	'lampiran',
		            'upload_path'=>base_url('assets/lampiran/blacklist_file/'),
					'upload_url'=>site_url('vendor/admin/black_list/blacklist_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
			)
		);
		$this->form['form'][0]['field'] = 'id_vendor';
		$this->form['url'] = site_url('vendor/admin/black_list/blacklist_vendor/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_vendor'] = $save['id_vendor']; 
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->simpan_by_vendor($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function report()
	{
		$data = $this->bm->report_vendor();
		$table 	=	"<table cellpadding='0' cellspacing='0' border='1'>
						<tr>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>No.</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Nama</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Alasan (Remark)</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Tanggal Mulai</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Tanggal Tanggal Akhir</td>
						</tr>";
		$no = 1;
		foreach($data as $key=>$value){
			$table	.= "<tr>
							<td>".$no."</td>
							<td width='auto'>".$value['name']."</td>
							<td width='auto'>".$value['remark']."</td>
							<td width='auto'>".$value['start_date']."</td>
							<td width='auto'>".$value['end_date']."</td>
						</tr>";
			$no++;
		}
		$table .= "</table>";
		header('Content-type: application/ms-excel');

    	header('Content-Disposition: attachment; filename=Export'.date('YmdHis').'.xls');



		echo $table;
	}
}
