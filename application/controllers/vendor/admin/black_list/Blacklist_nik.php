<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Blacklist_nik extends MY_Controller {

	public $form;

	public $modelAlias = 'bm';

	public $module = 'Blacklist';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/blacklist_model','bm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form' => array(
				array(
						'field'	=> 	'nik',
						'type'	=>	'search',
						'label'	=>	'NIK'
				),
				array(
						'field'	=> 	'start_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Mulai',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'end_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Akhir',
						'rules' =>	'required'
				),
				array(
						'field'	=> 	'remark',
						'type'	=>	'textarea',
						'label'	=>	'Alasan',
						'rules' =>	'required'
				),
				array(
		            'field'	=> 	'blacklist_file',
		            'type'	=>	'file',
		            'label'	=>	'lampiran',
		            'upload_path'=>base_url('assets/lampiran/blacklist_file/'),
					'upload_url'=>site_url('vendor/admin/black_list/blacklist_nik/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
			),
			'filter'=>array(
	         	array(

						'field'	=> 	'nik',
						'type'	=>	'text',
						'label'	=>	'NIK'
				)
			),
		);
		$this->getData = $this->bm->get_blacklist_nik($this->form);
		$this->updateUrl = 'vendor/admin/black_list/blacklist_nik/update';
		$this->getData = $this->bm->get_blacklist_nik($this->form);
	}
	
	public function index(){
		$data['data'] = $this->bm->get_blacklist_nik();
	 	$this->load->view('vendor/admin/blacklist/list_nik',$data, false);
	 	$this->load->view('vendor/admin/blacklist/list_nik_js', $data, false);
	}

	// public function getData($id = null)
	// {
	// 	$config['query'] = $this->bm->get_blacklist_nik();;
	// 	$return = $this->tablegenerator->initialize($config);
	// 	echo json_encode($return);
	// }

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->select_by_nik($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['field']=='nik'){
				$this->form['form'][$key]['readonly'] = true;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function insert()
	{
		$this->form['url'] = site_url('vendor/admin/black_list/blacklist_nik/save');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->$modelAlias->select_by_nik($id);
			if ($this->$modelAlias->edit_by_nik($save, $id)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function aktif($id)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->select_by_nik($id);

		$this->formDelete['url'] = site_url('vendor/admin/black_list/blacklist_nik/aktif_nik/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function aktif_nik($id)
	{
		$modelAlias = $this->modelAlias;
		// if ($this->validation()) {
			$lastData = $this->$modelAlias->select_by_nik($id);
			if ($this->$modelAlias->aktif_by_nik($id)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
				echo json_encode(array('status' => 'success'));
			}
		// }
	}

	public function search_nik()
	{
		echo json_encode($this->bm->search_by_nik());
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->simpan_by_nik($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	public function fitur_blacklist_report()
	{
		$this->form = array(
			'form' => array(
				array(
					'field'	=> 	array(
									'day_start',
									'day_end'
								),
					'type'	=>	'date_range',
					'label'	=>	'Tanggal'
				)
			),
			'button'=>array(
				array(
					'type'=>'submit',
					'label'=>'Report'
				),
				array(
					'type'=>'cancel',
					'label'=>'Batal'
				)	
			),
			'url'=>site_url('vendor/admin/black_list/blacklist_nik/report')
		);
		echo json_encode($this->form);
	}

	public function report()
	{
		$data = $this->bm->report_nik();
		$table 	=	"<table cellpadding='0' cellspacing='0' border='1'>
						<tr>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>No.</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>NIK</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Nama</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Vendor</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Alasan (Remark)</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Tanggal Mulai</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Tanggal Tanggal Akhir</td>
						</tr>";
		$no = 1;
		foreach($data as $key=>$value){
			$table	.= "<tr>
							<td>".$no."</td>
							<td width='auto'>".$value['nik']."</td>
							<td width='auto'>".$value['nama_pengurus']."</td>
							<td width='auto'>".$value['vendor_name']."</td>
							<td width='auto'>".$value['remark']."</td>
							<td width='auto'>".$value['start_date']."</td>
							<td width='auto'>".$value['end_date']."</td>
						</tr>";
			$no++;
		}
		$table .= "</table>";
		header('Content-type: application/ms-excel');

    	header('Content-Disposition: attachment; filename=Export'.date('YmdHis').'.xls');



		echo $table;
	}
}
