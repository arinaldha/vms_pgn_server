<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_tunggu extends MY_Controller {

	public $form;

	public $modelAlias = 'dtm';

	public $alias = 'ms_vendor';

	public $module = 'Daftar Tunggu Vendor';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/daftar_tunggu_model','dtm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Vendor'
					),
					array(
						'field'	=> 	'id_sbu',
						'type'	=>	'dropdown',
						'label'	=>	'Pilih SBU',
						'source'=>  $this->gm->getSbu(),
						'rules' => 	'required'
					)
				),
				'filter'=>array(
					array(
						'field'	=> 	'a|name',
						'type'	=>	'text',
						'label'	=>	'Vendor',
					),
					array(
						'field'	=> 	'b|name',
						'type'	=>	'text',
						'label'	=>	'Badan Usaha',
					)
				)
		);
	}
	
    public function index($status){
    	$status = 0;
	 	$admin = $this->session->userdata('admin');

	 	$daftarTunggu = $this->dtm->getDaftarTunggu($status);
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/admin/daftar_tunggu'),
	 		'title' => 'Daftar Tunggu'
	 	));

	 	$data['id'] = $id;
	 	$data['daftarTunggu'] = $daftarTunggu;
	 	$data['admin'] = $admin;
	 	$data['status'] = $status;
	 	$this->header = 'Daftar Tunggu Vendor';
	 	$this->content = $this->load->view('vendor/admin/daftar_tunggu/view',$data, TRUE);
	 	$this->script = $this->load->view('vendor/admin/daftar_tunggu/view_js', $data, TRUE);
	 	parent::index();

	 }

	public function ubahLokasi($id){
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if ($this->form['form'][$key]['field'] == 'name') {
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url('vendor/admin/daftar_tunggu/update/'. $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function get_daftar_tunggu_dashboard($id)
	 {
		$config['query'] = $this->dtm->getDaftarTungguDashboard();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	 }
}
