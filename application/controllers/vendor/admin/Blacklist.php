<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Blacklist extends MY_Controller {

	public $form;

	public $module = 'Blacklist';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/blacklist_model','bm');
	}
	
	public function index(){

	 	$blacklist = $this->bm->get_blacklist_vendor();
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/admin/daftar_tunggu'),
	 		'title' => 'Blacklist'
	 	));
	 	$data['blacklist'] = $blacklist;
	 	$data['admin'] = $admin;
	 	$this->header = 'Blacklist Vendor';
	 	$this->content = $this->load->view('vendor/admin/blacklist/view',$data, TRUE);
	 	$this->script = $this->load->view('vendor/admin/blacklist/view_js', $data, TRUE);
	 	parent::index();

	 }
}
