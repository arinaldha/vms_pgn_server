<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_vendor extends MY_Controller {

	public $form;

	public $modelAlias = 'dvm';

	public $alias = 'ms_vendor';

	public $module = 'Daftar Vendor';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/daftar_vendor_model','dvm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
				'filter'=>array(
					array(
						'field'	=> 	'a|name',
						'type'	=>	'text',
						'label'	=>	'Nama Vendor',
					),
					array(
						'field'	=> 	'b|name',
						'type'	=>	'text',
						'label'	=>	'Badan Usaha',
					),
					array(
						'field'	=> 	'd|npwp_code',
						'type'	=>	'text',
						'label'	=>	'NPWP',
					),
					array(
						'field'	=> 	'd|entry_stamp',
						'type'	=>	'text',
						'label'	=>	'Tanggal Daftar Vendor',
					),
					array(
						'field'	=> 	'vendor_type',
						'type'	=>	'dropdown',
						'label'	=>	'Status Perusahaan',
						'source'=>	array(0 =>'Pilih Disini','Anak Perusahaan PGN' => 'Anak Perusahaan PGN','Anak Perusahaan Terafiliasi PGN' => 'Anak Perusahaan Terafiliasi PGN','BUMN' => 'BUMN','Anak Perusahaan BUMN/Afiliasi BUMN' => 'Anak Perusahaan BUMN/Afiliasi BUMN','Swasta' => 'Swasta')
					)
				)
		);

		$this->getData   = $this->dvm->getData($this->form);
		$this->deleteUrl = 'vendor/admin/daftar_vendor/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/admin/daftar_vendor'),
			'title' => 'Daftar Vendor'
		));
		$this->header = 'Daftar Vendor';
		$this->content = $this->load->view('vendor/admin/daftar_vendor/list',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/daftar_vendor/list_js', $data, TRUE);
		parent::index();
	}

	public function export(){
		$table = '';
		$no  = 1;
		
			$dataMaster = $this->dvm->getDaftarVendor();
			$no = 1;
			
			foreach ($dataMaster as $keyMaster) {

				$table.=' <tr>
							  <td>'.$no.'</td>'
							.'<td>'.$keyMaster['name'].'</td>'
							.'<td>'.$keyMaster['legal_name'].'</td>'
							.'<td>'.$keyMaster['username'].'</td>'
							.'<td>'.$keyMaster['password'].'</td>
							</tr>';
				$no++;
			}
		
		$html = '<html>
				<head>
					
					<style type="text/css">

						@page{
							size: A4 portrait;
							page-break-after : always;
							margin : 10px;
						}
						
						@media all{
							ol{
								padding-left : 20px;
								padding-top : -15px;
								padding-bottom : -15px;
							}
							
							table { page-break-inside:avoid; }
						    tr    { page-break-inside: avoid; }
						    thead { display:table-header-group; }
					    }
					table{
						width: 100%;
					}
    				</style>
				</head>
				<body>
					<table>
						
						<tr>
							<td>No</td>
							<td>Nama Vendor</td>
							<td>Badan Usaha</td>
							<td>Username</td>
							<td>Password</td>
						</tr>
						'.$table.'
					</table>
					
				</body>
				</html>
				';
		 header("Content-type: application/vnd.ms-excel");
		 header("Content-Disposition: attachment; filename=Laporan Daftar Vendor ".default_date(date('Y-m-d')).".xls");
		echo $html;
	}
}
