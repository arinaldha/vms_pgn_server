<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Daftar_beku extends MY_Controller
{
	public $form;

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/admin/daftar_beku_model','dbm');
		// $this->dbm->proses_beku();

		$this->form = array(
			'filter' => array(
				array(
		            'field'	=> 	'b|name',
		            'type'	=>	'text',
		            'label'	=>	'Badan Usaha'
	         	),
	         	array(
		            'field'	=> 	'a|name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Badan Usaha'
	         	),
	         	array(
		            'field'	=> 	'npwp_code',
		            'type'	=>	'text',
		            'label'	=>	'NPWP'
	         	),
			)
		);

		$this->getData = $this->dbm->getData($this->form);
	}

	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/admin/daftar_beku'),
			'title' => 'Daftar Beku'
		));
		$this->header = 'Daftar Beku';
		$this->content = $this->load->view('vendor/admin/daftar_beku/list',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/daftar_beku/list_js', $data, TRUE);
		parent::index();
	}

	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}
}