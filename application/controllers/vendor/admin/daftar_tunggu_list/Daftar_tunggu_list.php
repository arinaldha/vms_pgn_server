<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_tunggu_list extends MY_Controller {

	public $form;

	public $modelAlias = 'dtm';

	public $alias = 'ms_vendor';

	public $module = 'Daftar Tunggu Vendor';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/admin/daftar_tunggu_model','dtm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Vendor'
					),
					array(
						'field'	=> 	'id_sbu',
						'type'	=>	'dropdown',
						'label'	=>	'Pilih SBU',
						'source'=>  $this->gm->getSbu(),
						'rules' => 	'required'
					)
				),
				'filter'=>array(
					array(
						'field'	=> 	'a|name',
						'type'	=>	'text',
						'label'	=>	'Vendor',
					),
					array(
						'field'	=> 	'b|name',
						'type'	=>	'text',
						'label'	=>	'Badan Usaha',
					),
					array(
						'field'	=> 	'mva|npwp_code',
						'type'	=>	'text',
						'label'	=>	'NPWP',
					)
				)
		);
	}
	
    public function index($status){

	 	$data['status'] = $status;
	 	$data['admin'] = $this->session->userdata('admin');

	 	$this->load->view('vendor/admin/daftar_tunggu/list',$data, false);
	 	$this->load->view('vendor/admin/daftar_tunggu/list_js', $data, false);
	 }

	public function get_list_daftar_tunggu($status)
	 {
		$config['query'] = $this->dtm->getDaftarTunggu($status);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	 }

	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}
}
