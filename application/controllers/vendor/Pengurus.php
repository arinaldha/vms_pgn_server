<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengurus extends VMS {

	public $form;

	public $modelAlias = 'pm';

	public $alias = 'ms_pengurus';

	public $module = 'Pengurus';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/pengurus_model','pm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		            'rules' => 'required',
		        ),
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'No.Identitas (KTP/Passport/KITAS)',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'position_expire',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id_akta',
		            'type'	=>	'dropdown',
		            'label'	=>	'No.Akta Pengangkatan',
		            'source'=>	$this->gm->getnoakta($user['id_user'])
	         	),
	         	array(
		            'field'	=> 	'pengurus_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (KTP/Passport/KITAS)',
		            'upload_path'=>base_url('assets/lampiran/pengurus_file/'),
					'upload_url'=>site_url('vendor/pengurus/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
					'rules' => 	'required'
	         	)
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	),
			)
		);
		$this->getData = $this->pm->getData($this->form);
		$this->insertUrl = site_url('vendor/pengurus/save/');
		$this->updateUrl = 'vendor/pengurus/update/';
		$this->deleteUrl = 'vendor/pengurus/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/pengurus'),
			'title' => 'Pengurus'
		));
		$this->header = 'Pengurus';
		$this->content = $this->load->view('vendor/pengurus/list',$data, TRUE);
		$this->script = $this->load->view('vendor/pengurus/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/pengurus/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['position_expire'] = date('Y-m-d', strtotime($this->pm->get_akta_data($save['id_akta'])['issue_date']. '+5 years'));
			$save['id_vendor'] = $user['id_user'];
			$save['entry_stamp'] = date("Y-m-d H:i:s");
			$save['data_status'] = 0;
	 		if ($this->$modelAlias->insert($save)) {
	 			$this->stillActive($this->session->userdata('user')['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	 public function pengurusView($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 // 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 // 	}
		}
		echo json_encode($this->form);
	}
	 public function lihatDataPengurus($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);
		$this->form['form'][5]['source'] = $this->gm->getnoakta($data['id_vendor']);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 // 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 // 	}
		}
		echo json_encode($this->form);
	}
}
