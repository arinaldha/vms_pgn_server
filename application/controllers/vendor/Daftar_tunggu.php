<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_tunggu extends MY_Controller {

	public $form;

	public $module = 'Daftar Tunggu';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/vendor_model','vm');
		$admin = $this->session->userdata('admin');

		
		$this->getData = $this->vm->getDataTunggu($this->form);
		$this->insertUrl = site_url('vendor/daftar_tunggu/save/');
		$this->updateUrl = 'vendor/daftar_tunggu/update';
		$this->deleteUrl = 'vendor/daftar_tunggu/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/daftar_tunggu'),
			'title' => 'Daftar Tunggu Vendor'
		));
		$this->header = 'Daftar Tunggu Vendor';
		$this->content = $this->load->view('vendor/daftar_tunggu/list',$data, TRUE);
		$this->script = $this->load->view('vendor/daftar_tunggu/list_js', $data, TRUE);
		parent::index();
	}
}
