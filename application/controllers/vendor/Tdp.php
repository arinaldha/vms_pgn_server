<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tdp extends VMS {

	public $form;

	public $modelAlias = 'tm';

	public $alias = 'ms_tdp';

	public $module = 'Tdp';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Tdp_model','tm');

		$this->load->model('vendor/Vendor_model','vm');

		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Dikeluarkan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Berlaku s/d',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'tdp_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Scan Dokumen TDP',
		            'upload_path'=>base_url('assets/lampiran/tdp_file/'),
					'upload_url'=>site_url('vendor/tdp/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'

	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Dikeluarkan',
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit',
	         	),
			)
		);
		$this->getData = $this->tm->getData($this->form);
		$this->insertUrl = site_url('vendor/tdp/save/');
		$this->updateUrl = 'vendor/tdp/update/';
		$this->deleteUrl = 'vendor/tdp/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/tdp'),
			'title' => 'Tanda Daftar Perusahaan'
		));
		$this->header = 'Tanda Daftar Perusahaan';
		$this->content = $this->load->view('vendor/tdp/list',$data, TRUE);
		$this->script = $this->load->view('vendor/tdp/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/tdp/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = timestamp();
	 		if ($this->$modelAlias->insert($save)) {
	 			$this->stillActive($this->session->userdata('user')['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	 public function tdpView($id)
	 {
	 	$id = base64_decode($id);
	 	$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 // 		$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 // 	}
		}

		echo json_encode($this->form);
	 }
}
