<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin extends MY_Controller {

	public $form;

	public $modelAlias = 'im';

	public $alias = 'ms_ijin_usaha';

	public $module = 'Izin';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/izin_model','im');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');
		$this->formWizard = array(
			'step'=>array(
				'klasifikasi'=>array(
					'label'=>'Klasifikasi Usaha',
					'form'=>array(
								array(
									'field'	=> 	'id_dpt_type',
									'type'	=>	'radioList',
									'label'	=>	'Klasifikasi Usaha',
									'rules' => 	'required',
									'source'=> $this->gm->getDptType(),
									'full'=>true
								)
							),
					'button'=>array(
						array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)

				),
				'siu'=>array(
					'label'=>'Surat Izin Usaha',
					'form'=>array(
								array(
									'field'	=> 	'type',
									'type'	=>	'radioList',
									'label'	=>	'Surat Izin Usaha',
									'rules' => 	'required',
									'full'=>true,
									'source'=>	array(
													'siujk'=>'SIUJK',
													'sbu'=>'Sertifikat Badan Usaha',
													'siup'=>'SIUP',
													'ijin_lain'=>'Surat Izin Usaha Lainnya',
													'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
												)
								)
								
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)
				),
				'data'=>array(
					'label'=>'Pengisian Data',
					'form'=>array(
								array(
									'field'	=> 	'authorize_by',
									'type'	=>	'text',
									'label'	=>	($this->input->post('type')=='asosiasi') ? 'Nama Asosiasi / Keanggotaan Lainnya': 'Lembaga Penerbit',
									// 'rules' => 	'required'
								),array(
									'field'	=> 	'no',
									'type'	=>	'text',
									'label'	=>	'Nomor',
									'rules' => 	'required'
								),
								array(
									'field'	=> 	'issue_date',
									'type'	=>	'date',
									'label'	=>	'Tanggal',
									'rules' => 	'required|forward_date'
								),
								array(
									'field'	=> 	'expire_date',
									'type'	=>	'lifetimeDate',
									'label'	=>	'Masa Berlaku',
									'rules' => 	'required'
								),								
								array(
									'field'	=> 	'qualification',
									'type'	=>	'radio',
									'label'	=>	'Kualifikasi',
									// 'rules' => 	'required',
									'source'=>	array(
													'kecil'=>'Kecil',
													'non-kecil'=>'Non-kecil',
													// 'besar'=>'Besar'
												)
								),
								array(
									'field'	=> 	'grade',
									'type'	=>	'dropdown',
									'label'	=>	'Grade',
									// 'rules' => 	'required',
									'source'=>	array(
													1=>1,
													2=>2,
													3=>3,
													4=>4,
													5=>5,
													6=>6,
													7=>7
												)
								),
								
								array(
									'field'	=> 	'izin_file',
									'type'	=>	'file',
									'label'	=>	'Lampiran',
									'rules' => 	'required',
									'upload_path'=>base_url('assets/lampiran/izin_file/'),
									'upload_url'=>site_url('vendor/izin/upload_lampiran'),
									'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
									'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            				'rules' => 	'required'
								),

							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)
				),
				'bsb'=>array(
					'label'=>'Bidang / Sub-Bidang',
					'form'=>array(
								array(
									'field'	=> 	'bsb',
									'label'	=>	'Bidang Sub-Bidang',
									'rules' => 	'callback_requiredTree'
								)
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Simpan',
		                     'class'=>'btn-next'
		                )
					)
				)
		    ),
		);
		$this->form_bidang = array(
			'filter' =>array(
				array(
					'field'	=> 	'id_dpt_type',
					'type'	=>	'dropdown',
					'label'	=>	'Bidang',
					'source'=>  $this->gm->getDptType()
				),
				array(
					'field'	=> 	'no',
					'type'	=>	'text',
					'label'	=>	'Nomor',
				),
				array(
					'field'	=> 	'issue_date',
					'type'	=>	'date',
					'label'	=>	'Tanggal'
				),
			)
		);

		$this->form_edit = array(
			'form'=> array(
				array(
									'field'	=> 	'no',
									'type'	=>	'text',
									'label'	=>	'Nomor',
									'rules' => 	'required'
								),
								array(
									'field'	=> 	'issue_date',
									'type'	=>	'date',
									'label'	=>	'Tanggal',
									'rules' => 	'required|forward_date'
								),
								array(
									'field'	=> 	'expire_date',
									'type'	=>	'lifetimeDate',
									'label'	=>	'Masa Berlaku',
									'rules' => 	'required'
								),								
								array(
									'field'	=> 	'qualification',
									'type'	=>	'radio',
									'label'	=>	'Kualifikasi',
									// 'rules' => 	'required',
									'source'=>	array(
													'kecil'=>'Kecil',
													'menengah'=>'Non-kecil',
													// 'besar'=>'Besar'
												)
								),
								array(
									'field'	=> 	'izin_file',
									'type'	=>	'file',
									'label'	=>	'Lampiran',
									'rules' => 	'required',
									'upload_path'=>base_url('assets/lampiran/izin_file/'),
									'upload_url'=>site_url('vendor/izin/upload_lampiran'),
									'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
									'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
								),
			)
		);

		$this->form = $this->formWizard['step']['data'];

		$this->getData = $this->im->getData($this->form);
		$this->insertUrl = site_url('vendor/izin/save/');
		$this->updateUrl = 'vendor/izin/update';
		$this->deleteUrl = 'vendor/izin/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/izin'),
			'title' => 'Izin Usaha'
		));
		$this->header = 'Izin Usaha';
		$data['bsb'] = $this->im->checkIzinUsaha($data['id_user']);

		$this->content = $this->load->view('vendor/izin/list',$data, TRUE);
		$this->script = $this->load->view('vendor/izin/list_js', $data, TRUE);
		parent::index();
	}

	public function view_bsb($id)
	{
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/izin'),
			'title' => 'Izin Usaha'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/izin/view_bsb/'.$id),
			'title' => 'Bidang / Sub Bidang'
		));
		$data['id'] = $id;
		$this->header = 'Bidang / Sub Bidang';
		$this->content = $this->load->view('vendor/izin/list_bsb',$data, TRUE);
		$this->script = $this->load->view('vendor/izin/list_bsb_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$id = base64_decode($id);
		$this->form = $this->form_edit;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}

		$this->form['data'] = $data;
		$this->form['url'] = site_url($this->updateUrl . '/' . base64_encode($id));
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function getDataBsb($id = null)
	{
		$config['query'] = $this->im->get_bidang_sub_bidang($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	public function insert(){
		echo json_encode($this->formWizard);
	}
	 public function simpan(){
		$__validation = $this->formWizard['step'][$_POST['validation']]['form'];
		if($_POST['validation']=='data'){
			$submitted = array();
			switch ($this->input->post('type')) {
				case 'siup':
					$submitted = array(1,3,4,5,6);
					break;
				case 'siupal':
					$submitted = array(1,3,4,5,6);
					break;
				
				case 'sbu':
					$submitted = array(0,1,3,5,6);
					break;
				case 'asosiasi':
					$submitted = array(0,1,3,5,6);
					break;
				case 'ijin_lain':
					$submitted = array(0,1,3,4,5,6);
					break;
				case 'siujk':
					$submitted = array(1,2,3,4,5,6);
					break;
			}
			$__val = array();
			foreach ($submitted as $key => $value) {
				$__val[$key] = $__validation[$value];
			}
			// print_r($__val);
			$this->form_validation->set_rules($__val);
			$this->validation($__val);
		}else{

			$this->form_validation->set_rules($this->formWizard['step'][$_POST['validation']]['form']);
			$this->validation($__validation);
		}	
	}
	public function simpan_bsb($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_ijin_usaha'] = $id;
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert_bsb_vendor($save)) {

				$this->stillActive($this->session->userdata('user')['id_user']);
				
				// $this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}
	public function insert_izin(){
		$data = $this->input->post();
		 $__form = array(
        	'id_dpt_type',
			'type',
			'authorize_by',
			'no',
			'grade',
			'issue_date',
			'qualification',
			'expire_date',
			'izin_file'
        );
        switch ($data['type']) {
			case 'siup':
				$submitted = array(0,1,3,5,6,7,8);
				break;
			
			case 'sbu':
				$submitted = array(0,1,2,3,5,7,8);
				break;
			case 'asosiasi':
				$submitted = array(0,1,2,3,5,7,8);
				break;
			case 'ijin_lain':
				$submitted = array(0,1,2,3,5,6,7,8);
				break;
			case 'siujk':
				$submitted = array(0,1,3,4,5,6,7,8);
				break;
			case 'siupal':
				$submitted = array(0,1,3,5,6,7,8);
				break;
		}
		foreach ($submitted as $key => $value) {
			$post[$__form[$value]] = $_POST[$__form[$value]];
		}
		$post['bsb'] = $_POST['bsb'];
		
		$id =$this->im->insert_izin($post);
		if($id){
			$this->stillActive($this->session->userdata('user')['id_user']);
			$this->deleteTemp($this->input->post(), null, $this->formWizard['step']['data']);
			$this->dpt->iu_change($id);
			echo json_encode(array('status'=>'success'));
		}
	}
	public function editBSB($id){
		$id = base64_decode($id);
		$getData = $this->im->selectTypeDPT($id);
		$return['url'] = site_url('vendor/izin/updateBSB/'.base64_encode($id));
		$return['button'] = array(
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                ),array(
                    'type'=>'submit',
                    'label'=>'Simpan',
                )
            );

		$bsb = $this->im->getBidangSubBidang($getData);
		$vendorBSB = $this->im->getVendorBidangSubBidang($id);
		foreach($bsb as $key => $value){
			foreach ($value['data'] as $keyBSB => $valueBSB) {
				$bsb[$key]['data'][$keyBSB]['value'] = 
				$vendorBSB[$key][$keyBSB];
			}
		}
		$return['tree'] = $bsb;
		echo json_encode($return);
	}
	public function updateBSB($id){
		$id =base64_decode($id);
		$bsb = $this->input->post('bsb');
		$getData = $this->im->selectData($id);

		$this->db->where('id_ijin_usaha', $id)->delete('ms_iu_bsb');
		foreach ($bsb as $key => $value) {
			foreach($value as $keybsb){
				$this->table = 'ms_iu_bsb';
				$databsb['id_vendor'] = $getData['id_vendor'];
				$databsb['id_ijin_usaha'] = $id;
				$databsb['id_bidang'] = $key;
				$databsb['id_sub_bidang'] = $keybsb;
				$databsb['entry_stamp'] = timestamp();
				$this->im->insert_bsb($databsb);
			}
		}
		$this->stillActive($this->session->userdata('user')['id_user']);
		$this->dpt->iu_change($id);
		echo json_encode(array('status'=>'success'));
	}
	public function proses_edit_bsb()
	{
		$data = $this->input->post();
		$id = $this->im->update_bsb($data);
		if ($id) {
			$this->stillActive($this->session->userdata('user')['id_user']);
			// $this->deleteTemp($this->input->post(), null, $this->formWizard['step']['data']);
			// $this->dpt->iu_change($id);
			echo json_encode(array('status'=>'success'));	
		}
	}

	 public function izinView($id)
	 {
	 	$id = base64_decode($id);
	 	$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
			// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
			// }
		}
		unset($this->form['button']);
		$this->form['data'] = $data;
		$this->form['id'] = $id;
		echo json_encode($this->form);
	 }

	function getBidangSubBidang(){
    	echo json_encode($this->im->getBidangSubBidang($this->input->post('id_dpt_type')));
    }
    public function requiredTree($str){
		if(count($this->input->post('bsb'))==0||$this->input->post('bsb')==null){
			$this->form_validation->set_message('requiredTree', 'Bidang Sub-Bidang tidak boleh kosong');
			return false;
		}else{
			return true;
		}
	}

	// public function getBSB(){
	// 	echo json_encode($this->im->getBSBList($this->input->post('id_ijin_usaha')));
	// }

	public function insert_bsb($id){
		$data_ijin = $this->im->get_data_ijin($id);
		$this->form = array(
			'form'=>array(
						array(
							'field'	=> 	'id_bidang',
							'type'	=>	'dropdown',
							'label'	=>	'Bidang Sub-Bidang',
							'source' => $this->im->getBidang($data_ijin['id_dpt_type']),
							'rules'	 => 'required'
						)
					)
		);
		$this->form['url'] = site_url('vendor/izin/simpan_bsb/'.$id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function cek_bsb($id)
	{
		$this->form = array(
			'form'=>array(
						array(
							'field'	=> 	'bidang_name',
							'label'	=>	'Bidang',
							'type'	=> 	'text'
						),
						array(
							'field'	=> 	'sub_bidang_name',
							'label'	=>	'Sub-Bidang',
							'type'	=> 	'text'
						)
					),
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->cek_bidang_sub_bidang($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}
		echo json_encode($this->form);
	}


	public function edit_bsb($id,$id_bidang)
	{
		$data_ijin = $this->im->get_data_ijin($id);
		$this->form = array(
			'form'=>array(
						array(
							'field'	=> 	'id_bidang',
							'type'	=>	'dropdown',
							'label'	=>	'Bidang Sub-Bidang',
							'source' => $this->im->getBidang($data_ijin['id_dpt_type']),
							'rules'	 => 'required'
						)
					)
		);
		$this->form['url'] = site_url('vendor/izin/proses_update_bsb/'.$id.'/'.$id_bidang);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function proses_update_bsb($id,$id_bidang)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_ijin_usaha'] = $id;
			$save['entry_stamp'] = timestamp(); 
			$lastData = $this->$modelAlias->cek_bidang_sub_bidang($id);
			if ($this->$modelAlias->update_bsb($id, $id_bidang,$save)) {
				$this->stillActive($this->session->userdata('user')['id_user']);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function get_sub_bidang_by_id_bidang()
	{
		$output = '';
		$output .= '<label>Sub Bidang</label> <select name="id_sub_bidang" class="form-control input-mandatory">';
		$data = $this->im->ambilSb($this->input->post('id_bidang'));
		foreach ($data as $sb) {
			$output .= '<option value="'.$sb->id.'">'.$sb->name.'</option>';
		}
		$output .= '</select>';
		echo json_encode($output);
	}
	public function getBidangSubBidangByIdIjinUsaha($id)
	{
		$data_ijin = $this->im->get_data_ijin($id);
		echo json_encode($this->im->getBidang($data_ijin['id_dpt_type']));
	}
	public function remove_bsb($id)
	{
		$this->formDelete['url'] = site_url('vendor/izin/hapus_bsb/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
	public function hapus_bsb($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_bsb($id)) {
			$this->stillActive($this->session->userdata('user')['id_user']);
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	function formFilterBidang()
	{
		$this->form = $this->form_bidang;
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->stillActive($this->session->userdata('user')['id_user']);
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

}
