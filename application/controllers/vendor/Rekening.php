<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Rekening extends MY_Controller {

	public $form;

	public $modelAlias = 'rm';

	public $alias = 'ms_rekening';

	public $module = 'Rekening';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/pemilik_model','rm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('vendor/administrasi_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'No Rekening',
		        ),array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),array(
		            'field'	=> 'bank_name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Bank',
		        ),array(
		            'field'	=> 'unit',
		            'type'	=> 'text',
		            'label'	=> 'Unit / Cabang',
		        ),
		        array(
		            'field'	=> 	'bank_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/bank_file/'),
					'upload_url'=>site_url('vendor/rekening/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
				'caption' => '*harap melampirkan scan halaman depan buku rekening yang memuat informasi seperti: nama, nomor rekening dan cabang bank'
	         	),
	        )
		);
		$this->getData = $this->rm->getData($this->form);
		$this->insertUrl = site_url('vendor/rekening/save/');
		$this->updateUrl = 'vendor/rekening/update/';
		$this->deleteUrl = 'vendor/rekening/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/rekening'),
			'title' => 'Rekening'
		));
		$this->header = 'Rekening';
		$this->content = $this->load->view('vendor/rekening/list',$data, TRUE);
		$this->script = $this->load->view('vendor/rekening/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/rekening/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	// if ($this->validation()) {
	 		$save = $this->input->post();
			$save['id_vendor'] = $user['id_user'];
			$save['entry_stamp'] = date("Y-m-d H:i:s");
			$save['del'] = 0;
	 		if ($this->am->insert_bank($save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			echo json_encode(array('status'=>'success'));
	 		}
	 	// }
	 }

	 public function pemilikView($id)
	 {
	 	$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}

		echo json_encode($this->form);
	 }

	public function delete($id)
	{
		if ($this->am->delete_bank($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/rekening/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}
