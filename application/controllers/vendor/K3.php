<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class K3 extends MY_Controller
{
	public $modelAlias = 'km';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/K3_model','km');
		$this->form = array(
			'form'=>array(
	         	
			),
		);
		$this->insertUrl = site_url('K3/save/');
		$this->updateUrl = site_url('K3/update');
		$this->getData = $this->km->getData();
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Aspek K3'
		));
		$this->header = 'Aspek K3';
		$this->content = $this->load->view('vendor/k3/list',$data, TRUE);
		$this->script = $this->load->view('vendor/k3/list_js', $data, TRUE);
		parent::index();
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$admin = $this->session->userdata('admin');
			if ($save['duration_type'] == 'days') {
				$save['auction_duration'] = $save['duration'] * 1440;
			} elseif ($save['duration_type'] == 'hours') {
				$save['auction_duration'] = $save['duration'] * 60;
			} else {
				$save['auction_duration'] = $save['duration'];
			}
			$save['auction_type'] = 'reverse_auction';
			$save['entry_stamp'] 	= date('Y-m-d H:i:s');
			$save['auction_date'] 	= $save['auction_start'];
			$save['id_mekanisme'] 	= 1;
			$save['id_sbu'] 		= $admin['id_sbu'];

			if ($this->$modelAlias->save_data($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}
}