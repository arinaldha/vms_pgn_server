<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bukti_pajak extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
	}

	public function index()
	{
		$loc = './assets/lampiran/pajak/';
		$directory = directory_map($loc, 1);
		$data['file'] = $directory;
		$data['user'] = $this->session->userdata('user');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/bukti_pajak'),
			'title' => 'Bukti Potong / Setor Pajak'
		));
		$this->header = 'Bukti Potong / Setor Pajak';
		$this->content = $this->load->view('vendor/bukti_pajak/view',$data, TRUE);
		$this->script = $this->load->view('vendor/bukti_pajak/view_js', $data, TRUE);
		parent::index();
	}

	public function view($bulan)
	{
		$data['user'] = $this->session->userdata('user');
		$data['bulan'] = $bulan;
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/bukti_pajak'),
			'title' => 'Pajak Vendor'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/bukti_pajak/view/'.$bulan),
			'title' => 'Folder Pajak '.$bulan
		));

		$this->header = 'File Pajak '.$bulan;
		$this->content = $this->load->view('vendor/bukti_pajak/list_file',$data, TRUE);
		$this->script = $this->load->view('vendor/bukti_pajak/list_file_js', $data, TRUE);
		parent::index();
	}

	public function viewFile($bulan,$namaFile)
	{ 
		$data['user'] = $this->session->userdata('user');
		$data['bulan'] = $bulan;
		$data['namaFile'] = $namaFile;
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/bukti_pajak'),
			'title' => 'Pajak Vendor'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/bukti_pajak/view/'.$namaFile),
			'title' => 'Folder Pajak '.$namaFile
		));

		$this->header = 'File / '.$bulan.' / '.$namaFile;
		$this->content = $this->load->view('vendor/bukti_pajak/list_view_file',$data, TRUE);
		$this->script = $this->load->view('vendor/bukti_pajak/list_view_file_js', $data, TRUE);
		parent::index();
	}

	public function getFolder()
	{
		$output = '';
		$dataSession = $this->session->userdata('user');
		$cleanNpwp = $this->clean($dataSession['npwp_code']);
		$loc = './assets/lampiran/pajak/';
// <<<<<<< HEAD
// 		$directory = directory_map($loc, FALSE, TRUE);
// 		$document = array('PPH_4_Ayat_2','PPH_22','PPN');

// 	    foreach ($directory as $key_bulan => $bulan) { 

// 	    	$output .='<li><i class="fas fa-folder"></i><span class="caret">'.$key_bulan.'</span>
// 							<ul class="nested">';
	    	 
// 	    	foreach ($bulan as $key_folder => $isiFolder) {
// 	    	 	$output .= '<li><i class="fas fa-folder"></i><span class="caret">'.$key_folder.'</span><ul class="nested">';

// 	    	 		if(count($isiFolder)>0){
	    			
// 		    			foreach ($isiFolder as $keyFile => $valueFile) {
		    				
// 		    				switch ($key_folder) {
// 		    					case "PPH_4_Ayat_2\\":
// 		    					case 'PPH_22\\':

// 		    						$doc = explode('_',$valueFile);
// 			    					$npwp = explode('.', end($doc));
// 			    					$_npwp = array_pop($npwp);
// 			    					$_npwp =  implode($npwp);

// 			    					if($this->clean($_npwp) == $cleanNpwp){
// 			    						$output .='<li><a href="'.base_url('assets/lampiran/pajak/'.$bulan.'/'.$key_folder).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
// 			    					}	
// 		    					break;
// 		    					case 'PPN\\':
// 		    						$doc = explode('_',$valueFile);
// 			    					$npwp = explode('.', end($doc));
// 			    					$_npwp = $npwp[5];
// 			    					$_npwp =  implode($npwp);
// 			    					if($this->clean($_npwp) == $cleanNpwp){
// 			    						$output .='<li><a href="'.base_url('assets/lampiran/pajak/'.$bulan.'/'.$key_folder).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
// 			    					}	
// 		    					break;
// 		    				}
// 		    			}
// 		    		}
// 	    	 	$output .='</ul></li>';
// 	    	}
// 	    	$output .='</ul></li>';
// 	    }
// 		return $output;    	

// 	}
		
	
// =======
		$directory = directory_map($loc, 1);
		
	    foreach ($directory as $bulan) { 

	    	$output .='<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret">'.$bulan.'</span>
							<ul class="nested">';

	    	 $locFolder = './assets/lampiran/pajak/'.$bulan;
	    	 
	    	 $dirFolder = directory_map($locFolder, 0);
	    	 
	    	  foreach ($dirFolder as $key_folder => $isiFolder) {
	    	  		$output .= '<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret">'.$key_folder.'</span>
								<ul class="nested">';
					if (count($isiFolder) > 0) {
						foreach ($isiFolder as $keyFile => $valueFile) {
							 if($key_folder=='PPH_4_Ayat_2/'){
	    					
		    		 			$doc = explode('_',$valueFile);
		    		 			$npwp = explode('.', end($doc));
		    		 			$_npwp = array_pop($npwp);
		    		 			$_npwp =  implode($npwp);
		    		 			if($this->clean($_npwp) == $cleanNpwp){
		    						$output .= '<li><a href="'.site_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		    		 			}	
		    		 		}
		    		 		elseif($key_folder == 'PPH_22/'){
		    	 				$doc = explode('_', $valueFile);
		    	 				$npwp = explode('.',end($doc));
		    	 				$_npwp = array_pop($npwp);
		    	 				$_npwp = implode($npwp);
		    	 				if ($this->clean($_npwp) == $cleanNpwp) {
		    	 					$output .= '<li><a href="'.site_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		    	 				}
		    	 			}
		    	 			elseif ($key_folder == 'PPH_23/') {
		    	 				$doc = explode('_', $valueFile);
		    	 				$npwp = explode('.',end($doc));
		    	 				$_npwp = array_pop($npwp);
		    	 				$_npwp = implode($npwp);
		    	 				if ($this->clean($_npwp) == $cleanNpwp) {
		    	 					$output .= '<li><a href="'.site_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		    	 				}
		    	 			}
		    	 			elseif ($key_folder == 'PPN/'){
	    	 					$doc = explode('_',$valueFile);
		    					$_npwp = $npwp[5];
		    					$_npwp =  implode($npwp);
		    					if($this->clean($_npwp) == $cleanNpwp){
		    						$output .='<li><a href="'.site_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		    					}
		    	 				// $doc = explode('_',$valueFile, -2);
		    	 				// $npwp = explode('.', end($doc));
		    	 				// $_npwp = array_pop($npwp);
		    	 				// $_npwp = implode($npwp);
		    	 				// if ($this->clean($_npwp) == $cleanNpwp) {
		    	 				// 	echo $valueFile;
		    	 				// 	$output .= '<li><a href="'.base_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'">'.$valueFile.'</a></li>';
		    	 				// }
		    	 			}
						}
					}
					$output .= '</ul></li>';
	    	  	}
	    	  	$output .= '</ul></li>';
	    	 }
	    	 echo $output;
	}
	public function clean($string){
		return preg_replace('/[^A-Za-z0-9]/', '', $string);
	}

	public function download($folder,$secFolder,$final_file)
	{
		$loc = 'assets/lampiran/pajak/'.$folder.'/'.$secFolder.'/'.$final_file;
		if (!empty($loc)) {
			$this->load->helper('download');
			force_download($loc, null);
		}
		else {
			echo "Kosong";
		}
	}
}
// $directory = directory_map($loc, FALSE, TRUE);
		// $document = array('PPH_4_Ayat_2','PPH_22','PPN');
		// foreach ($directory as $key_bulan => $bulan) { 
		// 	$output .='<li><i class="fas fa-folder"></i><span class="caret">'.$key_bulan.'</span>
		// 					<ul class="nested">';
		// 	foreach ($bulan as $key_folder => $isiFolder) {
	 //    	 	$output .= '<li><i class="fas fa-folder"></i><span class="caret">'.$key_folder.'</span><ul class="nested">';
	 //    		if(count($isiFolder)>0){
	 //    			foreach ($isiFolder as $keyFile => $valueFile) {
	 //    				switch ($key_folder) {
		//     					case "PPH_4_Ayat_2/":
		//     					case 'PPH_22/':

		//     						$doc = explode('_',$valueFile);
		// 	    					$npwp = explode('.', end($doc));
		// 	    					$_npwp = array_pop($npwp);
		// 	    					$_npwp =  implode($npwp);

		// 	    					if($this->clean($_npwp) == $cleanNpwp){
		// 	    						$output .='<li><a href="'.base_url('assets/lampiran/pajak/'.$bulan.'/'.$key_folder).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		// 	    					}	
		//     					break;
		//     					case 'PPN/':
		//     						$doc = explode('_',$valueFile);
		// 	    					$npwp = explode('.', end($doc));
		// 	    					$_npwp = $npwp[5];
		// 	    					$_npwp =  implode($npwp);
		// 	    					if($this->clean($_npwp) == $cleanNpwp){
		// 	    						$output .='<li><a href="'.base_url('assets/lampiran/pajak/'.$bulan.'/'.$key_folder).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
		// 	    					}	
		//     					break;
 	// 	    				}
 	// 	    			}
 	// 	    		$output .='</ul></li>';
	 //    	}
	 //    	$output .='</ul></li>';
 	//     }
	 //    	return $output;
 	//    }