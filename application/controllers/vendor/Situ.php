<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Situ extends VMS {

	public $form;

	public $modelAlias = 'sm';

	public $alias = 'ms_situ';

	public $module = 'Situ';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/situ_model','sm');
		$this->load->model('vendor/vendor_model','vm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Nama Surat',
		            'rules' => 'required',
		            'source'=> array(
		            	'Surat Keterangan Domisili Perusahaan (SKDP)' => 'Surat Keterangan Domisili Perusahaan (SKDP)',
		            	'Surat Izin Tempat Usaha (SITU)' => 'Surat Izin Tempat Usaha (SITU)',
		            	'Herregisterasi SKDP' => 'Herregisterasi SKDP' 
		            )
		        ),
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issued_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'address',
		            'type'	=>	'text',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'file_photo',
		            'type'	=>	'file',
		            'label'	=>	'Foto Lokasi',
		            'upload_path'=>base_url('assets/lampiran/file_photo/'),
					'upload_url'=>site_url('vendor/situ/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'

	         	),
	         	array(
		            'field'	=> 	'situ_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/situ_file/'),
					'upload_url'=>site_url('vendor/situ/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor'
	         	),
	         	array(
		            'field'	=> 	'issued_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit'
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal'
	         	)
			),
		);
		$this->getData = $this->sm->getData($this->form);
		$this->insertUrl = site_url('vendor/situ/save/');
		$this->updateUrl = 'vendor/situ/update';
		$this->deleteUrl = 'vendor/situ/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/situ'),
			'title' => 'Domisili Perusahaan'
		));
		$this->header = 'Domisili Perusahaan';
		$this->content = $this->load->view('vendor/situ/list',$data, TRUE);
		$this->script = $this->load->view('vendor/situ/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/situ/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = timestamp();
	 		if ($this->$modelAlias->insert($save)) {
	 			$this->stillActive($this->session->userdata('user')['id_user']);
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	 public function situView($id = null)
	 {
	 	$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 	// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	// }
		 }
		echo json_encode($this->form);
	 }
}
