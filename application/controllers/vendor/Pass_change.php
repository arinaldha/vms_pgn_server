<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pass_change extends MY_Controller {

	public $form;

	public $modelAlias = 'pcm';

	public $alias = 'ms_vendor';

	public $module = 'Ubah Password';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/pass_change_model','pcm');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'=>	'password',
					'label'=>	'Password',
					'type' =>	'password',
					'rules'=>	'required'
		        ),
		        array(
		            'field'=>	'new_password',
					'label'=>	'Password Baru',
					'type' =>	'password',
					'rules'=>	'required'
	         	),
	         	array(
		            'field'	=> 	'conf_password',
		            'type'	=>	'password',
		            'label'	=>	'Konfirmasi Password',
		            'rules' => 	'required',
	         	),
	         )
		);
		$this->insertUrl = site_url('vendor/pass_change/save/');
		$this->updateUrl = 'vendor/pass_change/update/';
		$this->deleteUrl = 'vendor/pass_change/delete/';
	}
	
	public function index(){
		
		$data['user'] = $this->session->userdata('user');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/pass_change'),
			'title' => 'Ubah Password'
		));
		$this->header = 'Ubah Password';
		$this->content = $this->load->view('vendor/pass_change/list',$data, TRUE);
		$this->script = $this->load->view('vendor/pass_change/list_js', $data, TRUE);
		parent::index();
	}

	public function view(){
		$user = $this->session->userdata('user');
		$user_id = $user['id_user'];
		$this->form['url'] = site_url('vendor/pass_change/change_password/'.$user_id);
		$this->form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Ubah Password'
                )
            );
		echo json_encode($this->form);
	}

	public function change_password($id){
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$password = do_hash($save['password'],'sha1');
			$new_password = $save['new_password'];
			$conf_password = $save['conf_password'];
			$save['edit_stamp'] = date('Y-m-d H:i:s');
			$getPassword = $this->$modelAlias->getCurrentPassword($id);
			$lastData = $this->$modelAlias->selectData($id);

			if ($getPassword->password == $password) {
				if ($new_password == $conf_password) {
					if ($this->$modelAlias->change_password($id, $save, $password)) {
						$this->stillActive($this->session->userdata('user')['id_user']);
						$this->session->set_userdata('alert', $this->form['successAlert']);
						$this->deleteTemp($save, $lastData);
					}
					else {
						echo "Gagal merubah password!!";
					}
				}
				else{
					echo "Password baru anda tidak sama dengan konfirmasi password!!";
				}
			}
			else{
				echo "Password Lama Anda Salah!!";
			}
		}
    }
}
