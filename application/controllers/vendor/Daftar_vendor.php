<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_vendor extends MY_Controller {
	public $form;
	public $modelAlias = 'vm';
	public $module = 'Daftar_vendor';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('vendor/vendor_model','vm');
		$user = $this->session->userdata('user');
		$this->getData = $this->vm->getDataDaftar($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/daftar_vendor'),
			'title' => 'Daftar Vendor'
		));
		$this->header = 'Daftar Vendor';
		$this->content = $this->load->view('vendor/daftar_vendor/list',$data, TRUE);
		$this->script = $this->load->view('vendor/daftar_vendor/list_js', $data, TRUE);
		parent::index();
	}

}
