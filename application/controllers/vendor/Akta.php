<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Akta extends VMS {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_akta';

	public $module = 'Akta';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
		$this->load->library('VMS');
		$this->load->model('vendor/akta_model','am');
		$this->load->model('vendor/vendor_model','vm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'rules' => 'required',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'notaris',
		            'type'	=>	'text',
		            'label'	=>	'Notaris',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'akta_file',
		            'type'	=>	'file',
		            'label'	=>	'Akta File',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'authorize_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Ditetapkan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Scan Dokumen Penetapan',
		            'upload_path'=>base_url('assets/lampiran/authorize_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
	         ),
			'filter'=>array(
				array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
	         	),
	         	array(
		            'field'	=> 	'notaris',
		            'type'	=>	'text',
		            'label'	=>	'Notaris',
	         	),
			),
		);
		$this->getData = $this->am->getData($this->form);
		$this->insertUrl = site_url('vendor/akta/save/');
		$this->updateUrl = 'vendor/akta/update/';
		$this->deleteUrl = 'vendor/akta/delete/';
	}
	
	public function index(){
		$data = $this->session->userdata('user');
		if($this->vm->check_pic($data['id_user'])==0){
			redirect(site_url('vendor/pernyataan'));
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/akta'),
			'title' => 'Akta Perusahaan'
		));
		$this->header = 'Akta Perusahaan';
		$this->content = $this->load->view('vendor/akta/list',$data, TRUE);
		$this->script = $this->load->view('vendor/akta/list_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/akta/save/');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	public function save($data = null){
	 	$this->load->library('VMS');
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = timestamp();
	 		$id = $this->$modelAlias->insert($save);
	 		// echo $id;
	 		if ($id) {
	 			$this->stillActive($user['id_user']);
	 			// echo "string";
	 			$this->dpt->non_iu_change($user['id_user']);
	 			$jenis_akta = ($save['type']=='pendirian') ? 'Akta Pendirian':'Akta Perubahan';
	 			$data = array(
	 				'id'=>$id,
	 				'expire_date'=>$save['expire_date'],
	 				'doc_type'=>'ms_akta',
	 				'no'=>$save['no'],
	 				'doc'=>$jenis_akta
	 			);

	 			// $this->vms->setEmailBlast($data);
	 			$this->deleteTemp($save);
	 		}
	 	}
	 }

	 public function aktaView($id = null)
	 {
	 	$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
	 }

}
