<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Agen extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_agen';

	public $module = 'Agen';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		$this->form_produk = array(
			'form'=>array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor Surat Dari Lembaga Pemerintah yang Berwenang',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'agen_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/agen_file/'),
					'upload_url'=>site_url('vendor/agen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'produk',
		            'type'	=>	'text',
		            'label'	=>	'Produk',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'merk',
		            'type'	=>	'text',
		            'label'	=>	'Merk',
		            'rules' => 	'required',
	         	),
	         ),
		);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'agen_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/agen_file/'),
					'upload_url'=>site_url('vendor/agen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
			)
		);
	}
	
	public function index($id, $process=false){
		
			$data['data'] = $this->am->get_data_agen($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/agen/list', $data, FALSE);
			$this->load->view('vendor/admin/view/agen/list_js', $data, FALSE);
		
		
	}

	public function view($id)
	{
		$config['query'] = $this->am->get_data_agen($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function verifikasi($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_agen($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	}
		 }
		echo json_encode($this->form);

	}

	public function verifikasiProduk($id){
		 $this->form = $this->form_produk;
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_agen_produk($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	}
		 }
		echo json_encode($this->form);

	}

	public function get_produk($id)
	{
		echo json_encode($this->am->verifikasi_agen_produk($id));
	}

}
