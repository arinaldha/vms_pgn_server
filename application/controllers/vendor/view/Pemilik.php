<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pemilik extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pemilik';

	public $module = 'Pemilik';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		$this->form = array(
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	)
			)
		);
	}
	
	public function index($id){
		
			$data['data'] = $this->am->get_data_pemilik($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/pemilik/list', $data, FALSE);
			$this->load->view('vendor/admin/view/pemilik/list_js', $data, FALSE);
	}

	public function view($id = null)
	{
		$config['query'] = $this->am->get_data_pemilik($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cekData($id){

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		            'rules' => 'required',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'percentage',
		            'type'	=>	'decimal',
		            'label'	=>	'Persentase',
		            'rules' => 	'required',
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	)
			)
		);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pemilik($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }
}
