<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_ijin_usaha';

	public $module = 'Izin';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/izin_model','im');
	}
	
	public function index($id, $type){
		
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/view_tab', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/view_tab_js', $data, FALSE);
		

	}

	public function siup($id, $type='siup')
	{
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/list', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/list_js', $data, FALSE);
	}

	public function siul($id, $type='ijin_lain')
	{
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/list_siul', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/list_siul_js', $data, FALSE);
	}

	public function asosiasi($id, $type='asosiasi')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/list_asosiasi', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/list_asosiasi_js', $data, FALSE);
	}

	public function siujk($id, $type='siujk')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/list_siujk', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/list_siujk_js', $data, FALSE);
	}

	public function sbu($id, $type='sbu')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/izin/list_sbu', $data, FALSE);
			$this->load->view('vendor/admin/view/izin/list_sbu_js', $data, FALSE);
	}

	public function view($id = null, $type)
	{
		$config['query'] = $this->am->get_data_izin($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cekData($id,$type)
	{
		if ($type == 'sbu') {
			$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'dpt_name',
		            'type'	=> 'text',
		            'label'	=> 'DPT Tipe',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 'authorize_by',
		            'type'	=> 'text',
		            'label'	=> 'Anggota Asosiasi',
		            'rules' => 'required'
		        ),
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'Nomor',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'qualification',
		            'type'	=>	'radioList',
		            'label'	=>	'Kualifikasi',
		            'rules' => 	'required',
		            'source'=>  array(
		            			'kecil' => 'Kecil',
		            			'non-kecil' => 'Non-kecil'
		            ),
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' =>  'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
		}
		elseif($type = 'siup'){
			$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'dpt_name',
		            'type'	=> 'text',
		            'label'	=> 'DPT Tipe',
		            'rules' => 'required'
		        ),
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'Nomor',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'qualification',
		            'type'	=>	'radioList',
		            'label'	=>	'Kualifikasi',
		            'rules' => 	'required',
		            'source'=>  array(
		            			'kecil' => 'Kecil',
		            			'non-kecil' => 'Non-kecil'
		            ),
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' =>  'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
		}
		elseif ($type = 'asosiasi') {
			$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'dpt_name',
		            'type'	=> 'text',
		            'label'	=> 'DPT Tipe',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 'authorize_by',
		            'type'	=> 'text',
		            'label'	=> 'Lembaga Penerbit',
		            'rules' => 'required'
		        ),
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'Nomor',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' =>  'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
		}
		elseif ($type = 'ijin_lain' && $type = 'siujk') {
			$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'dpt_name',
		            'type'	=> 'text',
		            'label'	=> 'DPT Tipe',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 'authorize_by',
		            'type'	=> 'text',
		            'label'	=> 'Lembaga Penerbit',
		            'rules' => 'required'
		        ),
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'Nomor',
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'qualification',
		            'type'	=>	'radioList',
		            'label'	=>	'Kualifikasi',
		            'rules' => 	'required',
		            'source'=>  array(
		            			'kecil' => 'Kecil',
		            			'non-kecil' => 'Non-kecil'
		            ),
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' =>  'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
		}
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_izin($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['field'] != 'mandatory' && $this->form['form'][$key]['field'] != 'status' ) {
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		
		echo json_encode($this->form);
	}

}
