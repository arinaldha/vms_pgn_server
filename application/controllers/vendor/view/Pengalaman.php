<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengalaman extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pengalaman';

	public $module = 'Pengalaman';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		$this->form = array(
			'filter' => array(
				array(
		            'field'	=> 'job_name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Paket Kerjaan'
		        ),
		        array(
		            'field'	=> 	'b|name',
		            'type'	=>	'text',
		            'label'	=>	'Bidang'
	         	),
	         	array(
		            'field'	=> 'id_sbu',
		            'type'	=> 'dropdown',
		            'label'	=> 'Satuan Unit/Kerja',
		            'source'=> $this->gm->getSbu($id_vendor)
		        ),
			)
		);
	}
	
	public function index($id){
		
			$data['data'] = $this->am->get_data_pengalaman($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/pengalaman/list', $data, FALSE);
			$this->load->view('vendor/admin/view/pengalaman/list_js', $data, FALSE);
	}

	public function view($id = null)
	{
		$config['query'] = $this->am->get_data_pengalaman($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cekDataPekerjaan($id){

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'id_iu_bsb',
		            'type'	=> 'dropdown',
		            'label'	=> 'Bidang/Sub-Bidang Pekerjaan',
		            'rules' => 'required',
		            'source'=> $this->gm->getBsbIuAdmin($id)
		        ),
		        array(
		            'field'	=> 	'job_name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Kerjaan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'job_location',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 'id_sbu',
		            'type'	=> 'dropdown',
		            'label'	=> 'Satuan Unit/Kerja',
		            'rules' => 'required',
		            'source'=> $this->gm->getSbu()
		        ),
	         	array(
		            'field'	=> 	'job_giver',
		            'type'	=>	'text',
		            'label'	=>	'Nama Instansi Pemberi Tugas',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'phone_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Telp',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_start',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_end',
		            'type'	=>	'date',
		            'label' =>  's/d',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'price_idr',
		            'type'	=>	'money',
		            'label'	=>	'Nilai Kontrak (Rp)',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	array('currency','price_foreign'),
		            'type'	=>	'money_asing',
		            'label'	=>	'Nilai Kontrak Kurs',
		            'source' =>  $this->gm->getKurs(),
		            'caption'=> '*Diisi Bila Ada'
	         	 ),
	         	array(
		            'field'	=> 	'contract_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen Kontrak)',
		            'upload_path'=>base_url('assets/lampiran/contract_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '*Format Data Harus PDF|JPEG|JPG|PNG|GIF|RAR|ZIP|DOC|DOCX'
	         	),
	         	array(
		            'field'	=> 	'bast_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Selesai Sesuai BAST',
		            'rules' =>	'required'
	         	),array(
		            'field'	=> 	'bast_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen BAST)',
		            'upload_path'=>base_url('assets/lampiran/bast_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '*Format Data Harus PDF|JPEG|JPG|PNG|GIF|RAR|ZIP|DOC|DOCX'
	         	),
	       
			),
			'filter' => array(
				array(
		            'field'	=> 'job_name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Paket Kerjaan'
		        ),
		        array(
		            'field'	=> 	'b|name',
		            'type'	=>	'text',
		            'label'	=>	'Bidang'
	         	),
	         	array(
		            'field'	=> 'id_sbu',
		            'type'	=> 'dropdown',
		            'label'	=> 'Satuan Unit/Kerja',
		            'source'=> $this->gm->getSbu($id_vendor)
		        ),
			)
		);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pengalaman($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }

	 public function cekData($id){

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'job_name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Kerjaan'
	         	),
	         	array(
		            'field'	=> 'bidang',
		            'type'	=> 'text',
		            'label'	=> 'Bidang'
		        ),
		        array(
		            'field'	=> 'sub_bidang',
		            'type'	=> 'text',
		            'label'	=> 'Sub-Bidang Pekerjaan'
		        ),
	         	array(
		            'field'	=> 	'contract_start',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Kontrak'
	         	),
			),
		);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pengalaman($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }
}
