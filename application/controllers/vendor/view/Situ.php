<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Situ extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_situ';

	public $module = 'Situ';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Nama Surat',
		            'rules' => 'required',
		            'source'=> array(
		            	'Surat Keterangan Domisili Perusahaan (SKDP)' => 'Surat Keterangan Domisili Perusahaan (SKDP)',
		            	'Surat Izin Tempat Usaha (SITU)' => 'Surat Izin Tempat Usaha (SITU)',
		            	'Herregisterasi SKDP' => 'Herregisterasi SKDP' 
		            )
		        ),
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issued_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'address',
		            'type'	=>	'text',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'file_photo',
		            'type'	=>	'file',
		            'label'	=>	'Foto Lokasi',
		            'upload_path'=>base_url('assets/lampiran/file_photo/'),
					'upload_url'=>site_url('vendor/situ/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'

	         	),
	         	array(
		            'field'	=> 	'situ_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/situ_file/'),
					'upload_url'=>site_url('vendor/situ/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor'
	         	),
	         	array(
		            'field'	=> 	'issued_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit'
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal'
	         	)
			),
		);
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_situ($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/situ/list', $data, FALSE);
			$this->load->view('vendor/admin/view/situ/list_js', $data, FALSE);
		
	}

	public function view($id)
    {
        $config['query'] = $this->am->get_data_situ($id);
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }

	public function cekData($id){
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_situ($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	}
		 }
		echo json_encode($this->form);

	}
}
