<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengurus extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pengurus';

	public $module = 'Pengurus';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		$this->form = array(
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	),
			)
		);
	}
	
	public function index($id){
			$data['data'] = $this->am->get_data_pengurus($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/pengurus/list', $data, FALSE);
			$this->load->view('vendor/admin/view/pengurus/list_js', $data, FALSE);
		
	}

	public function view($id = null)
	{
		$config['query'] = $this->am->get_data_pengurus($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cekData($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		            'rules' => 'required',
		        ),
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'No.Identitas (KTP/Passport/KITAS)',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Seumur Hidup'
	         	),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'b|no_akta',
		            'type'	=>	'dropdown',
		            'label'	=>	'No.Akta Pengangkatan',
		            'source'=>	$this->gm->getnoakta($id)
	         	),
	         	array(
		            'field'	=> 	'pengurus_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (KTP/Passport/KITAS)',
		            'upload_path'=>base_url('assets/lampiran/pengurus_file/'),
					'upload_url'=>site_url('vendor/pengurus/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'remark',
		            'type'	=>	'textarea',
		            'label'	=>	'Remark',
		            'rules' => 	'required',
		            'caption' => '*Diisikan dengan semua direksi dan semua komisaris yang tercantum dalam akta.'
	         	),
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	),
			)
		);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pengurus($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}
}
