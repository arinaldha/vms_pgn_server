<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Akta extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Akta';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'rules' => 'required',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'notaris',
		            'type'	=>	'text',
		            'label'	=>	'Notaris',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
	         	),
	         	array(
		            'field'	=> 	'akta_file',
		            'type'	=>	'file',
		            'label'	=>	'Akta File',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'authorize_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Ditetapkan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Scan Dokumen Penetapan',
		            'upload_path'=>base_url('assets/lampiran/authorize_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
			'filter'=>array(
				array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
	         	),
	         	array(
		            'field'	=> 	'notaris',
		            'type'	=>	'text',
		            'label'	=>	'Notaris',
	         	),
			),
		);
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_akta($id);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/akta/view_tab', $data, FALSE);
			$this->load->view('vendor/admin/view/akta/view_tab_js', $data, FALSE);
		
	}

	public function pendirian($id, $type)
	{
			$data['data'] = $this->am->get_data_akta($id, $type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/akta/list', $data, FALSE);
			$this->load->view('vendor/admin/view/akta/list_js', $data, FALSE);	
	}

	public function perubahan($id, $type)
	{
			$data['data'] = $this->am->get_data_akta($id, $type);
			$data['id'] = $id;
			$this->load->view('vendor/admin/view/akta/list_perubahan', $data, FALSE);
			$this->load->view('vendor/admin/view/akta/list_perubahan_js', $data, FALSE);	
	}

	public function view($id = null, $type)
	{
		$config['query'] = $this->am->get_data_akta($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cekData($id){
    	$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_akta($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
    }
}
