<?php defined('BASEPATH') OR exit('No direct script access allowed');
class View_data extends MY_Controller {

	public $form;

	public $modelAlias = 'apm';

	public $module = 'View Data';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			$this->load->model('approval_model','apm');
			$this->load->model('get_model','gm');
			if (! $this->session->userdata('admin')) {
				redirect(site_url());
			} 
	}

	 public function index($id){

	 	$admin = $this->session->userdata('admin');
	 	$dataAdm = $this->apm->get_data_administrasi($id);
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/admin/daftar_tunggu'),
	 		'title' => 'Daftar Vendor'
	 	));
	 	$this->breadcrumb->addlevel(2, array(
	 		'url' => site_url('approval/administrasi'),
	 		'title' => 'Info Data Vendor'
	 	));
	 	$data['id'] = $id;
	 	$data['dataAdm'] = $dataAdm;
	 	$data['admin'] = $admin;
	 	$this->header = 'Info Data Vendor';
	 	$this->content = $this->load->view('vendor/admin/view/view',$data, TRUE);
	 	$this->script = $this->load->view('vendor/admin/view/view_js', $data, TRUE);
	 	parent::index();

	 }
	 public function pengurusView($id)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 // 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 // 	}
		}
		echo json_encode($this->form);
	}
	 public function lihatDataPengurus($id)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);
		$this->form['form'][5]['source'] = $this->gm->getnoakta($data['id_vendor']);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 // 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 // 	}
		}
		echo json_encode($this->form);
	}
}
