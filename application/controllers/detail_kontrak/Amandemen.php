<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Amandemen extends MY_Controller {

	public $form;
	public $modelAlias = 'am';
	public $alias = 'ms_amandemen';
	public $module = 'Amendemen';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');	
		$this->load->model('detail_kontrak/Amandemen_model','am');
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/amandemen/save/'.$this->id_procurement);
		$this->updateUrl = 'detail_kontrak/amandemen/update';
		$this->deleteUrl = 'detail_kontrak/amandemen/delete/';
		$this->form = array(
			'form'=>array(
				array(
					'field'	=> 	'sequence',
					'type'	=>	'text',
					'label'	=>	'Amendemen ke-',
					'rules' =>	'required',
				),array(
					'field'	=> 	'about',
					'type'	=>	'text',
					'label'	=>	'Perubahan Kegiatan Pekerjaan',
					'rules' =>	'required',
				),
				array(
					'field'	=> 	'no',
					'type'	=>	'text',
					'label'	=>	'Nomor',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal',
					'field'	=>	'date',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Nilai Amendemen',
					'field'	=>	'idr_value',
				),
				
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Berakhir Pekerjaan',
					'field'	=>	'work_end',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Berakhir Garansi / Pemeliharaan',
					'field'	=>	'contract_end',
				),	
				array(
					'type'	=>	'textarea',
					'label'	=>	'Keterangan Amandemen',
					'field'	=>	'remark',
				),				
				array(
					'field'	=> 	'amandemen_file',
					'type'	=>	'file',
					'label'	=>	'Lampiran Amendemen',
					'rules' => 	'required',
					'upload_path'=>base_url('assets/lampiran/amandemen_file/'),
					'upload_url'=>site_url('detail_kontrak/amandemen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
				)
			)
		);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
        $config['query']    = $this->am->getData($form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
	public function index($id){
		$data['id'] = $id;
		$data['dataAmandemen'] = $this->am->selectAmandemen($id);
		
		$this->content = $this->load->view('detail_kontrak/amandemen/view',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/amandemen/view_js', $data, FALSE);
	}	

    public function save($id){
        $modelAlias = $this->modelAlias;
        
        if($this->validation()){
            $save = $this->input->post();
            $save['id_procurement'] = $id;
            $save['entry_stamp'] = timestamp();
             $save['idr_value'] = currency($save['idr_value']);
            $save['kurs_value'] = currency($save['kurs_value']);
            if($this->$modelAlias->insert($save)){
                $this->deleteTemp($save);
                return true;
            }
        }
    }

	public function delete($id){
		$this->am->deleteAmandemenFromGraph();
		parent::delete($id);

	}
	public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
            $save['idr_value'] = currency($save['idr_value']);
            $save['kurs_value'] = currency($save['kurs_value']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    
}
