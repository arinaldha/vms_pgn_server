<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bap extends MY_Controller {

	public $form;
	public $modelAlias = 'bm';
	public $alias = 'ms_bap';
	public $module = 'BAP';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');	
		$this->load->model('detail_kontrak/Bap_model','bm');
		$this->load->model('detail_kontrak/Barang_model','bgm');
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/bap/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/bap/update';
		$this->deleteUrl = 'detail_kontrak/bap/delete/';
		$this->form = array(
			'form'=>array(
				array(
					'type'	=>	'dropdown',
					'label'	=>	'Termin',
					'field'	=>	'id_termin',
					'source'=>	$this->bm->getTerminBap($this->id_procurement),
					'detail'=>	$this->bm->getTerminBapDetail($this->id_procurement)
				),
				array(
					'type'	=>	'dropdown',
					'label'	=>	'Barang / Jasa',
					'field'	=>	'id_barang',
					'source'=>	$this->bm->getBarangBap($this->id_procurement),
					'detail'=>	$this->bm->getBarangBapDetail($this->id_procurement)
				),
				array(
					'field'	=> 	'sequence',
					'type'	=>	'text',
					'label'	=>	'BAP ke-',
					'rules' =>	'required',
				),
				array(
					'field'	=> 	'no',
					'type'	=>	'text',
					'label'	=>	'Nomor',
					
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal',
					'field'	=>	'date',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Nilai',
					'field'	=>	'value'
				),
				
				array(
					'type'	=>	'date_range',
					'label'	=>	'Jangka Waktu Denda',
					'name'	=>	'denda',
					'field'	=> 	array(
										'denda_start',
										'denda_end'
									),
				),
				array(
					'type'	=>	'decimal',
					'label'	=>	'Persentase Perhitungan Denda',
					'field'	=>	'percentage'
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Besaran Perhitungan Denda',
					'field'	=>	'denda'
				),
				
				array(
					'field'	=> 	'bap_file',
					'type'	=>	'file',
					'label'	=>	'Lampiran',
					'rules' => 	'required',
					'upload_path'=>base_url('assets/lampiran/bap_file/'),
					'upload_url'=>site_url('detail_kontrak/bap/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
				)
			)
		);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
        $config['query']    = $this->bm->getData($form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
	public function index($id){
		$data['id'] = $id;
		$this->content = $this->load->view('detail_kontrak/bap/list',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/bap/list_js', $data, FALSE);
	}	
	public function insert()
	{
		$this->form['url'] = $this->insertUrl;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
    public function save($id){
        $modelAlias = $this->modelAlias;
        
        if($this->validation()){
            $save = $this->input->post();
             $save['value'] = currency($save['value']);
             $save['denda'] = currency($save['denda']);
            $save['id_procurement'] = $id;
            $save['entry_stamp'] = timestamp();
            if($this->$modelAlias->insert($save)){
                $this->deleteTemp($save);
                return true;
            }
        }
    }

    public function edit($id_procurement, $id){
    	$this->id_procurement = $id_procurement;
    	parent::edit($id);
    }

	public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
            $save['idr_value'] = currency($save['idr_value']);
            $save['kurs_value'] = currency($save['kurs_value']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    
}
