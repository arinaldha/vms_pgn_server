<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {

	public $form;
	public $modelAlias = 'im';
	public $alias = 'ms_invoice';
	public $module = 'Invoice';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');	
		$this->load->model('detail_kontrak/Invoice_model','im');
		$this->load->model('master/Pusat_biaya_model','pbm');
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/invoice/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/invoice/update';
		$this->deleteUrl = 'detail_kontrak/invoice/delete/';
		$this->form = array(
			'form'=>array(
				array(
					'field'	=> 	'no',
					'type'	=>	'text',
					'label'	=>	'No Surat Permohonan',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Surat Permohonan',
					'field'	=>	'date',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Nilai',
					'field'	=>	'value',
					'rules' =>	'required',
				),
				array(
						'field'=>'budget_center',
						'label'=>'Pusat Biaya',
						'type'=>'multiple',
						'source'=>	$this->pbm->getPusatBiayaByKontrak($this->id_procurement)
					),
				array(
						'field'	=> 	'detail',
						'type'	=>	'checkboxList',
						'label'	=>	'Detail Surat Permohonan Pembayaran',
						'source'	=>	array(
										'invoice'=>'Invoice',
										'kwitansi'=>'Kwitansi',
										'faktur'=>'Faktur Pajak / Surat Keterangan non - PKP',
										'lainnya'=>'Lainnya',

									)
					),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Tagihan Masuk',
					'field'	=>	'date_tagihan_masuk',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Input Pembayaran',
					'field'	=>	'date_input_pembayaran',
				),
				array(
					'type'	=>	'text',
					'label'	=>	'Nomor POPAY',
					'field'	=>	'no_popay',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Verifikasi Pajak',
					'field'	=>	'date_verifikasi_pajak',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Approve I',
					'field'	=>	'date_approve_1',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Approve II',
					'field'	=>	'date_approve_2',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Approve III',
					'field'	=>	'date_approve_3',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Masuk Treasury',
					'field'	=>	'date_masuk_treasury',
				),	
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Dibayar',
					'field'	=>	'date_dibayar',
				),	
				array(
					'field'	=> 	'invoice_file',
					'type'	=>	'file',
					'label'	=>	'Lampiran',
					'rules' => 	'required',
					'upload_path'=>base_url('assets/lampiran/invoice_file/'),
					'upload_url'=>site_url('detail_kontrak/invoice/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
				)
			)
		);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
        $config['query']    = $this->im->getData($form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
	public function index($id){
		$data['id'] = $id;
		$data['admin']= $this->session->userdata('admin');
		
		$this->content = $this->load->view('detail_kontrak/invoice/list',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/invoice/list_js', $data, FALSE);
	}	

    public function save($id){
        $modelAlias = $this->modelAlias;
        
        if($this->validation()){
            $save = $this->input->post();
            $save['id_procurement'] = $id;
             $save['value'] = currency($save['value']);
            $save['entry_stamp'] = timestamp();
            if($this->$modelAlias->insert($save)){
                $this->deleteTemp($save);
                return true;
            }
        }
    }

    public function edit($id_procurement, $id){
    	$this->id_procurement = $id_procurement;
    	parent::edit($id);
    }

	public function delete($id){
		parent::delete($id);

	}
	public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
             $save['value'] = currency($save['value']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    
}
