<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bast extends MY_Controller {

	public $form;
	public $modelAlias = 'bm';
	public $alias = 'ms_bast';
	public $module = 'BAST';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');	
		$this->load->model('detail_kontrak/Bast_model','bm');
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/bast/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/bast/update';
		$this->deleteUrl = 'detail_kontrak/bast/delete/';
		$this->form = array(
			'form'=>array(
				array(
					'field'	=> 	'sequence',
					'type'	=>	'text',
					'label'	=>	'BAST ke-',
					'rules' =>	'required',
				),
				array(
					'field'	=> 	'no',
					'type'	=>	'text',
					'label'	=>	'Nomor',
					
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal',
					'field'	=>	'date',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Nilai',
					'field'	=>	'value',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'date_range',
					'label'	=>	'Jangka Waktu Denda',
					'name'	=>	'denda',
					'field'	=> 	array(
										'denda_start',
										'denda_end'
									),
				),
				array(
					'type'	=>	'decimal',
					'label'	=>	'Persentase Perhitungan Denda',
					'field'	=>	'percentage'
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Besaran Perhitungan Denda',
					'field'	=>	'denda'
				),
				
				
				array(
					'field'	=> 	'bast_file',
					'type'	=>	'file',
					'label'	=>	'Lampiran',
					'rules' => 	'required',
					'upload_path'=>base_url('assets/lampiran/bast_file/'),
					'upload_url'=>site_url('detail_kontrak/bast/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
				)
			)
		);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
        $config['query']    = $this->bm->getData($form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
	public function index($id){
		$data['id'] = $id;
		$this->content = $this->load->view('detail_kontrak/bast/list',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/bast/list_js', $data, FALSE);
	}	

    public function save($id){
        $modelAlias = $this->modelAlias;
        
        if($this->validation()){
            $save = $this->input->post();
            $save['id_procurement'] = $id;
            $save['entry_stamp'] = timestamp();
            $save['value'] = currency($save['value']);
             $save['denda'] = currency($save['denda']);
            
            if($this->$modelAlias->insert($save)){
                $this->deleteTemp($save);
                return true;
            }
        }
    }

    public function edit($id_procurement, $id){
    	$this->id_procurement = $id_procurement;
    	parent::edit($id);
    }
	public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
           $save['value'] = currency($save['value']);
             $save['denda'] = currency($save['denda']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    
}
