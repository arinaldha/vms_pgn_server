<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends MY_Controller {

	public $form;
	public $modelAlias = 'bm';
	public $alias = 'ms_barang';
	public $module = 'Barang';
	public function __construct(){
		parent::__construct();		
		$this->load->model('detail_kontrak/Barang_model','bm');		
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/barang/save/');
		$this->updateUrl = 'detail_kontrak/barang/update';
		$this->deleteUrl = 'detail_kontrak/barang/delete/';
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'nama_barang',
						'type'	=>	'text',
						'label'	=>	'Nama Barang / Jasa',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'volume',
						'type'	=>	'number',
						'label'	=>	'Jumlah Barang',
						'rules' => 	'required',
					),array(
						'field'	=> 	'satuan',
						'type'	=>	'text',
						'label'	=>	'Satuan',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'nilai_hps',
						'type'	=>	'money',
						'label'	=>	'Harga Satuan',
						'rules' => 	'required',
					),array(
						'field'	=> 	'remark',
						'type'	=>	'textarea',
						'label'	=>	'Keterangan'
					),
					
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'type'	=>	'text',
						'label'	=>	'Nama',
						'field' =>  'name'
					)
				)
			);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
        $config['query']    = $this->bm->getData($form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
	public function index($id){
		$data['id'] = $id;
		$this->content = $this->load->view('detail_kontrak/barang/list',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/barang/list_js', $data, FALSE);
	}	

    public function save($id){
        $modelAlias = $this->modelAlias;
        
        if($this->validation()){
            $save = $this->input->post();
            $save['id_procurement'] = $id;
            $save['entry_stamp'] = timestamp();
             $save['nilai_hps'] = currency($save['nilai_hps']);
            // $save['kurs_value'] = currency($save['kurs_value']);
            if($this->$modelAlias->insert($save)){
                $this->deleteTemp($save);
                return true;
            }
        }
    }
    public function insert($id){
    	$this->insertUrl = site_url('detail_kontrak/barang/save/'.$id);
    	parent::insert();
    }
   
	public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
            $save['nilai_hps'] = currency($save['nilai_hps']);
            // $save['kurs_value'] = currency($save['kurs_value']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    public function getBarang($id){

		$config['query'] = $this->bm->getBarang($this->form, $id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	
    }
    
}
