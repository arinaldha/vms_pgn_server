<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pemenang extends MY_Controller {

	public $form;
	public $modelAlias = 'pgm';
	public $alias = 'ms_procurement_peserta';
	public $module = 'Pemenang';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');		
		$this->load->model('detail_kontrak/Pemenang_model','pgm');	
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/pemenang/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/pemenang/update';

		$this->form = array(
				'form'=>array(
					array(
						'field'	=> 	'vendor_name',
						'type'	=>	'text',
						'label'	=>	'Nama Penyedia Barang / Jasa',
						'rules' =>	'required',
					),
					array(
						'type'	=>	'money',
						'label'	=>	'Nilai Pengadaan',
						'field'	=>	'idr_kontrak',
						'rules' =>	'required',
						'caption'=>	'(Dalam Rupiah)',
					),
					array(
						'type'	=>	'money_asing',
						'label'	=>	'Nilai Pengadaan dalam mata uang asing',
						'name'	=>	'kurs_kontrak',
						'rules' =>	'required',
						'field' => 	array(
										'id_kurs_kontrak',
										'kurs_kontrak'
									),
						'source'=>	$this->mm->getKurs()
					),
				)
			);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function getData($id){
		$this->getSingleData($id);
    }
	public function index($id){
		$data['id'] = $id;
		$this->content = $this->load->view('detail_kontrak/pemenang/view',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/pemenang/view_js', $data, FALSE);
	}	
	public function isPayung($id){
		if($this->pm->getKontrakPayung($id)){
			echo 'true';
		}else{
			echo 'false';
		}
	}
}
