<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Progress_pengadaan extends MY_Controller {

	public $form;
	public $modelAlias = 'ppm';
	public $alias = 'tr_progress_pengadaan';
	public $module = 'Progress Pengadaan';
	public $filter;
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('detail_kontrak/Progress_pengadaan_model','ppm');	
		$user = $this->session->userdata('user');
		
		$this->insertUrl = site_url('detail_kontrak/Progress_pengadaan/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/Progress_pengadaan/update';
		$this->deleteUrl = 'detail_kontrak/Progress_pengadaan/delete/';
	}
	public function getData($id){
	#print_r($id);
		$dataBerkas = $this->km->selectData($id);
		$_POST['order'] = 'orders';
		$_POST['sort'] = 'asc';
        $config['query']    = $this->ppm->getData($form, $id, $dataBerkas);
        $return = $this->tablegenerator->initialize($config);

        echo json_encode($return);
    }
	public function index($id){

		$data['admin'] = $this->session->userdata('admin');
		$data['dataBerkas'] = $this->km->selectData($id);
		$data['pengadaan'] = $this->ppm->get_paket_progress($id);
		$data['progress'] 	= $this->ppm->get_progress_pengadaan($id);
		
		$data['step_pengadaan'] = $this->ppm->get_pengadaan_step($dataBerkas['id_mekanisme']);

		$total_progress = count($data['step_pengadaan']);


		$data['id'] = $id;
		$this->content = $this->load->view('detail_kontrak/progress_pengadaan/list',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/progress_pengadaan/list_js', $data, FALSE);
	}	
 	public function updateProgress($id, $id_progress){
 		$this->form = $array =  array(
				'form' => array(
					array(
			            'field'	=> 	'plan_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Rencana',
		         	),array(
			            'field'	=> 	'real_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Realisasi',
			            'rules' => 	'required',
		         	),array(
						'field'	=> 	'progress_file',
						'type'	=>	'multiple_file',
						'label'	=>	'Lampiran',
						'upload_path'=>base_url('assets/lampiran/progress_file/'),
						'upload_url'=>site_url('kontrak/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx'
					),array(
			            'field'	=> 	'remark',
			            'type'	=>	'textarea',
			            'label'	=>	'Keterangan',
		         	),
					
					
	         	),
				'successAlert'=>'Berhasil mengubah data!',
			);
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->updateProgress($id, $id_progress, $save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	
 	}
 	public function update($id, $id_progress){
		$admin = $this->session->userdata('admin');
		$kontrak = $this->km->selectData($id);
		$progressDetail = $this->ppm->progressDetail($id, $id_progress);
		$ro = ($kontrak['step'] ==0) ? true : false;
		$ro_ = ($kontrak['step'] ==1) ? true : false;
		$this->form = $array =  array(
				'form' => array(
					array(
			            'field'	=> 	'plan_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Rencana',
			            'readonly'=>$ro_,
			            'value'=>$progressDetail['plan_date']
		         	),array(
			            'field'	=> 	'real_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Realisasi',
			            'rules' => 	'required',
			            'readonly'=>$ro,
						'value'=>$progressDetail['real_date']
		         	),array(
						'field'	=> 	'progress_file',
						'type'	=>	'multiple_file',
						'label'	=>	'Lampiran',
						'upload_path'=>base_url('assets/lampiran/progress_file/'),
						'upload_url'=>site_url('kontrak/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'readonly'=>$ro,
						'value'=>$progressDetail['progress_file']
					),array(
			            'field'	=> 	'remark',
			            'type'	=>	'textarea',
			            'label'	=>	'Keterangan',
			            'readonly'=>$ro,
						'value'=>$progressDetail['remark']
		         	),
					
					
	         	),
				'successAlert'=>'Berhasil mengubah data!',
			);
		$this->insertUrl = site_url('detail_kontrak/progress_pengadaan/updateProgress/'.$id.'/'.$id_progress);
		parent::insert();
	
	}
	function simpan($id){
		
		$data = $this->input->post();
		if($this->ppm->prosesProgress($id)){
			redirect(site_url('kontrak/view/'.$id));
		}else{
			redirect(site_url('kontrak/view/'.$id));
		}
	}
	function lanjut($id){
		if($this->ppm->lanjut($id)){
			redirect(site_url('kontrak/view/'.$id));
		}else{
			redirect(site_url('kontrak/view/'.$id));
		}
	}
	
	function show($id, $id_progress){
		
		
		$data = $this->db->where('id_procurement', $id)->where('id_progress', $id_progress)->update('tr_progress_pengadaan', array('is_hidden' => 0));

		if($data){
			redirect(site_url('kontrak/view/'.$id.'#progress_pengadaan'));
		}else{
			redirect(site_url('kontrak/view/'.$id.'#progress_pengadaan'));
		}
	}
	function hide($id, $id_progress){
		
		
		$data = $this->db->where('id_procurement', $id)->where('id_progress', $id_progress)->update('tr_progress_pengadaan', array('is_hidden' => 1));

		if($data){
			redirect(site_url('kontrak/view/'.$id.'#progress_pengadaan'));
		}else{
			redirect(site_url('kontrak/view/'.$id.'#progress_pengadaan'));
		}
	}
}
