<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ttd_kontrak extends MY_Controller {

	public $form;
	public $modelAlias = 'tkm';
	public $alias = 'ms_kontrak';
	public $module = 'Penandatanganan_kontrak';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Kontrak_model','km');		
		$this->load->model('Main_model','mm');	
		$this->load->model('detail_kontrak/Ttd_kontrak_model','tkm');	
		$this->load->model('detail_kontrak/Pemenang_model','pgm');
		$this->load->model('master/Pusat_biaya_model','pbm');	
		$user = $this->session->userdata('user');
		$this->id_procurement = $this->uri->segment(4);
		$this->insertUrl = site_url('detail_kontrak/ttd_kontrak/save/'.$user['id_user']);
		$this->updateUrl = 'detail_kontrak/ttd_kontrak/update';

		$this->form = array(
			'form'=>array(
				array(
					'field'	=> 	'vendor_name',
					'type'	=>	'text',
					'label'	=>	'Nama Penyedia Barang / Jasa',
				),
				array(
					'field'	=> 	'address',
					'type'	=>	'textarea',
					'label'	=>	'Alamat',
				),array(
					'field'	=> 	'phone',
					'type'	=>	'text',
					'label'	=>	'Telepon',
				),array(
					'field'	=> 	'fax',
					'type'	=>	'text',
					'label'	=>	'Fax',
				),array(
					'field'	=> 	'email',
					'type'	=>	'text',
					'label'	=>	'Email',
				),array(
					'field'	=> 	'bsb',
					'type'	=>	'textarea',
					'label'	=>	'Bidang Sub Bidang',
				),
				array(
					'field'=>'budget_center',
					'label'=>'Pusat Biaya',
					'type'=>'multiple',
					'source'=>	$this->pbm->getPusatBiayaByKontrak($this->id_procurement)
				),
				array(
					'type'	=>	'text',
					'label'	=>	'Nomor SPPPBJ',
					'field'	=>	'no_sppbj',
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal SPPPBJ',
					'field'	=>	'sppbj_date',

				),
				array(
					'type'	=>	'text',
					'label'	=>	'Nomor Kontrak/PO/SPK',
					'field'	=>	'no_contract',
					'rules' =>	'required',
				),
				
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal Kontrak/PO/SPK',
					'field'	=>	'contract_date',
					'rules' =>	'required',
				),
				array(
					'type'	=>	'money',
					'label'	=>	'Nilai  Kontrak/PO/SPK',
					'field'	=>	'contract_price',
					'rules' =>	'required',
					'caption'=>	'(Dalam Rupiah)',
				),
				array(
					'type'	=>	'text',
					'label'	=>	'Nomor SPMK',
					'field'	=>	'no_spmk',
				),
				array(
					'type'	=>	'date',
					'label'	=>	'Tanggal SPMK',
					'field'	=>	'spmk_date',
				),
				array(
					'type'	=>	'date_range',
					'label'	=>	'Jangka Waktu Pelaksanaan Pekerjaan',
					'name'	=>	'work',
					'field'	=> 	array(
										'start_work',
										'end_work'
									),
				),
				array(
					'type'	=>	'date_range',
					'label'	=>	'Jangka Waktu Pemeliharaan/Garansi',
					'name'	=>	'contract',
					'field'	=> 	array(
										'start_contract',
										'end_contract'
									),
				),
				array(
						'field'	=> 	'contract_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran Kontrak',
						'upload_path'=>base_url('assets/lampiran/contract_file/'),
						'upload_url'=>site_url('kontrak/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
					),
				array(
						'field'	=> 	'price_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran Harga Satuan',
						'upload_path'=>base_url('assets/lampiran/price_file/'),
						'upload_url'=>site_url('kontrak/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
					)
			)
		);
		
		// $this->form_validation->set_rules($this->form['form']);
	}
	public function index($id){
		$data['id'] = $id;
		$data['dataBerkas'] = $this->km->selectData($id);
		// if($data['dataBerkas']['contract_type']==5||$data['dataBerkas']['contract_type']==2){

			$data['dataKontrak'] = $this->tkm->selectKontrak($id);
		// }
		$this->content = $this->load->view('detail_kontrak/ttd_kontrak/view',$data, FALSE);
		$this->script = $this->load->view('detail_kontrak/ttd_kontrak/view_js', $data, FALSE);
	}	
	public function getSingleData($id_procurement, $id_kontrak, $type=1){
        $user  = $this->session->userdata('user');
        $modelAlias = $this->modelAlias;
        $getData   = $this->$modelAlias->selectData($id_procurement, $id_kontrak);
      	if($type==2){
			$_unset = array(0,1,2,3,4,5,7,8,12,13,17);
			foreach ($_unset as $key => $value) {
				unset($this->form['form'][$value]);
			}
		}
        foreach($this->form['form'] as $key => $value){
            $this->form['form'][$key]['readonly'] = TRUE;
            $this->form['form'][$key]['value'] = $getData[$value['field']];
           
            if($value['type']=='date_range'){
                foreach($value['field'] as $keyField =>$rowField){
                    $this->form['form'][$key]['value'][] = $getData[$rowField];
                }
                
            }
            if($value['type']=='money'){
                    $this->form['form'][$key]['value'] = number_format($getData[$value['field']]);
                }
            if($value['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $getData[$value['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($getData[$value['field'][1]]);
            }   
        }

        echo json_encode($this->form);
    }
	public function update($id){
        if($this->validation()){
            $save = $this->input->post();
            $save['id_procurement'] = $id;
            $save['budget_center'] = implode($_POST['budget_center']);
            $save['contract_price'] = currency($save['contract_price']);
            $save['contract_price_kurs'] = currency($save['contract_price_kurs']);

            $lastData = $this->tkm->selectData($id);

            if($this->tkm->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }
    public function save($id, $type=1)
	{
		$modelAlias = $this->modelAlias;

		if($type==2){
			$_unset = array(0,1,2,3,4,5,7,8,12,13,17);
			foreach ($_unset as $key => $value) {
				unset($this->form['form'][$value]);
			}
		}
		$this->form_validation->set_rules($this->form['form']);
		if ($this->validation($this->form['form'])) {
			$save = $this->input->post();
			$save['type'] = $type;
			$save['budget_center'] = implode($_POST['budget_center']);
			$save['contract_price'] = currency($_POST['contract_price']);
			$save['id_procurement'] = $id;
			$save['entry_stamp'] = timestamp();

			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}
    public function termin($id_procurement, $id_contract){

		$config['query'] = $this->tkm->getDataTermin($this->form, $id_procurement, $id_contract);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	
    }
    public function insert($id, $type=1){
   		$proc = $this->db->where('id', $id)->get('ms_procurement')->row_array();
		$this->form['url'] = site_url('detail_kontrak/ttd_kontrak/save/'.$id.'/'.$type);
		if($type==2){
			$_unset = array(0,1,2,3,4,5,7,8,12,13,17);
			foreach ($_unset as $key => $value) {
				unset($this->form['form'][$value]);
			}
		}
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		$this->form_validation->set_rules($this->form['form']);
		echo json_encode($this->form);
	}
	public function edit($id, $type=1){
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();
				
				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
					
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}
   		$proc = $this->db->where('id', $id)->get('ms_procurement')->row_array();
		$this->form['url'] = site_url('detail_kontrak/ttd_kontrak/updateKontrak/'.$id.'/'.$type);
		if($type==2){
			$_unset = array(0,1,2,3,4,5,7,8,12,13,17);
			foreach ($_unset as $key => $value) {
				unset($this->form['form'][$value]);
			}
		}
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function updateKontrak($id, $type){
		$modelAlias = $this->modelAlias;
		if($type==2){
			$_unset = array(0,1,2,3,4,5,7,8,12,13,17);
			foreach ($_unset as $key => $value) {
				unset($this->form['form'][$value]);
			}
		}
		if ($this->validation()) {
			$save = $this->input->post();
			 $save['id_procurement'] = $id;
			 $save['budget_center'] = implode($_POST['budget_center']);
			$save['contract_price'] = currency($_POST['contract_price']);
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save, $type)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
   	public function insertTermin($id, $type=1){
   		if($type==1){
   			$proc = $this->db->where('id_procurement', $id)->get('ms_contract')->row_array();
   		}else{
   			$proc = $this->db->select('idr_value contract_price')->where('id', $id)->get('ms_amandemen')->row_array();
   		}
   		
   		
		$this->formTermin['form'] = array(
				array(
					'field'	=> 	'nilai_kontrak',
					'type'	=>	'money',
					'label'	=>	'Nilai Kontrak',
					'readonly'	=>true,
					'value'  =>      $proc['contract_price'],
				),array(
					'field'	=> 	'name',
					'type'	=>	'text',
					'rules' =>	'required',
					'label'	=>	'Termin ke -'
				),
				array(
					'field'	=> 	'value',
					'type'	=>	'money',
					'label'	=>	'Besaran (Rp)'
				),
				
			);
		$this->formTermin['url'] = site_url('detail_kontrak/ttd_kontrak/saveTermin/'.$id.'/'.$type);
		$this->formTermin['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		$this->form_validation->set_rules($this->formTermin['form']);
		echo json_encode($this->formTermin);
	}
	public function saveTermin($id, $type=1){
		if($type==1){
   			$proc = $this->db->where('id_procurement', $id)->get('ms_contract')->row_array();
   			$proc_ = $this->db->select_sum('percen')->where('id_procurement', $id)->where('del', 0)->get('ms_termin')->row_array();
   		}else{
   			$proc = $this->db->select('id_procurement, id, idr_value contract_price')->where('id', $id)->get('ms_amandemen')->row_array();
   		}
   		
		
		 $modelAlias = $this->modelAlias;

            $save = $this->input->post();
            $save['type'] = $type;
            $save['id_procurement'] = $proc['id_procurement'];
            $save['id_contract'] = $proc['id'];
            $save['entry_stamp'] = timestamp();
            // $save['value'] = ($save['percen'] / 100) * $proc['contract_price'];
            $save['value'] = currency($save['value']);
            unset($save['nilai_kontrak']);
          	
          	if($type==1){
          #cek total termin lebih dari 100%
			  	// $percentase = $proc_['percen'] + $save['percen'];	
			  	// if($percentase <= "100"){
			  		if($this->$modelAlias->insertTermin($id,$save)){
			        	$this->deleteTemp($save);
			        	echo json_encode(array('status'=>'success'));
			   		}
			  	// }else{
			  		// echo json_encode(array('status'=>'error', 'errorMessage'=>'Total termin harus dibawah 100%!'));
			  	// }
			}else{
				if($this->$modelAlias->insertTermin($id,$save)){
		        	$this->deleteTemp($save);
		        	echo json_encode(array('status'=>'success'));
		   		}
			}
        
	}
	public function editTermin($id){
		$data = $this->tkm->selectDataTermin($id);
		$this->form['form'] = array(
				array(
					'field'	=> 	'name',
					'type'	=>	'text',
					'label'	=>	'Termin ke -'
				),array(
					'field'	=> 	'value',
					'type'	=>	'money',
					'label'	=>	'Besaran (Rp)'
				),
				
			);
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();
				
				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
					
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url('detail_kontrak/ttd_kontrak/updateTermin/' . $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function updateTermin($id)
	{
		$dataTermin = $this->db->where('id', $id)->get('ms_termin')->row_array();
		$proc = $this->db->where('id_procurement', $dataTermin['id_procurement'])->get('ms_contract')->row_array();
		 $modelAlias = $this->modelAlias;
			$save = $this->input->post();
			$save['value'] = ($save['percen'] / 100) * $proc['contract_price'];
            $save['value'] = currency($save['value']);

             unset($save['nilai_kontrak']);
			 $percentase = $proc_['percen'] + $save['percen'];	
			 
	          if($percentase <= "100"){
	          	if($this->$modelAlias->updateTermin($id,$save)){
	                	$this->deleteTemp($save);
	                	echo json_encode(array('status'=>'success'));
	           	}
	          }else{
	          	echo json_encode(array('status'=>'error', 'errorMessage'=>'Total termin harus dibawah 100%!'));
	          }
	}
	public function deleteTermin($id){
		$this->deleteUrl = 'detail_kontrak/ttd_kontrak/removeTermin/';
		parent::remove($id);
	}
	public function removeTermin($id){
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->deleteTermin($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}
}
