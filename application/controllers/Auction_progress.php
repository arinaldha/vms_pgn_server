<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Auction_progress extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');
		$this->load->model('pre_auction_model','pam');
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->header = 'Daftar Auction';
		$this->content = $this->load->view('auction/admin/list_auction',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_auction_js', $data, TRUE);
		parent::index();
	}

	public function langsung()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction Berlangsung'
		));
		$this->header = 'Daftar Auction Berlangsung';
		$this->content = $this->load->view('auction/admin/list_langsung',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_langsung_js', $data, TRUE);
		parent::index();
	}

	public function selesai()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction Selesai'
		));
		$this->header = 'Daftar Auction Selesai';
		$this->content = $this->load->view('auction/admin/list_selesai',$data, TRUE);
		$this->script = $this->load->view('auction/admin/list_selesai_js', $data, TRUE);
		parent::index();
	}

	public function getDataAuction($id = null)
	{
		$config['query'] = $this->am->get_auction_list();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function getDataLangsung($id = null)
	{
		$config['query'] = $this->am->get_auction_langsung();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function getDataSelesai($id = null)
	{
		$config['query'] = $this->am->get_auction_selesai();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function cek_auction_data($id)
	{
		$this->am->cek_auction_data($id);
	}

	public function vendor()
	{
		$data['user'] = $this->session->userdata('user');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->header = 'Daftar Auction';
		$this->content = $this->load->view('vendor/auction/list',$data, TRUE);
		$this->script = $this->load->view('vendor/auction/list_js', $data, TRUE);
		parent::index();
	}

	public function getDataAuctionVendor($id = null)
	{
		$config['query'] = $this->am->get_auction_vendor();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$admin = $this->session->userdata('admin');
			$save['entry_stamp'] 	= date("Y-m-d H:i:s");
			$save['auction_date'] 	= $save['auction_start'];
			$save['id_mekanisme'] 	= 1;
			$save['id_sbu'] 		= $admin['id_sbu'];	
			$save['start_time']	= date('Y-m-d H:i:s',strtotime($save['auction_start']));
			$save['time_limit']	= date('Y-m-d H:i:s',strtotime($save['auction_end']));
			if ($this->$modelAlias->save_data($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function editAuction($id)
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction'),
			'title' => 'Daftar Auction'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('auction/editAuction/'.$id),
			'title' => 'Edit Auction'
		));
		$data['id'] = $id;
		$this->header = 'Edit Auction';
		$this->content = $this->load->view('auction/view_tab',$data, TRUE);
		$this->script = $this->load->view('auction/view_tab_js', $data, TRUE);
		parent::index();
	}

	public function viewEditAuction($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Auction',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_source',
		            'type'	=>	'radioList',
		            'label'	=>	'Sumber Anggaran',
		            'source'=>	array('Perusahaan' => 'Perusahaan','Non-Perusahaan'=>'Non-Perusahaan'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_pejabat_pengadaan',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pejabat Pengadaan',
		            'source'=>	$this->am->get_pejabat(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'auction_type',
		            'type'	=>	'radioList',
		            'label'	=>	'Tipe Auction',
		            'source'=>	array('Reserve Auction' => 'Reserve Auction','Forward Auction'=>'Forward Auction'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'auction_jenis',
		            'type'	=>	'radioList',
		            'label'	=>	'Jenis Auction',
		            'source'=>	array('RFI' => 'RFI','RFQ-Pembelian'=>'RFQ-Pembelian','RFI-Penunjukan'=>'RFI-Penunjukan','Auction'=>'Auction'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'work_area',
		            'type'	=>	'radioList',
		            'label'	=>	'Area Kerja',
		            'source'=>	array('Internet' => 'Internet','Intranet'=>'Intranet'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'room',
		            'type'	=>	'text',
		            'label'	=>	'Ruangan',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_lokasi',
		            'type'	=>	'dropdown',
		            'label'	=>	'Unit Organisasi',
		            'source'=>	$this->am->get_lokasi(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'start_time',
		            'type'	=>	'dateTime',
		            'label'	=>	'Waktu Mulai',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'time_limit',
		            'type'	=>	'dateTime',
		            'label'	=>	'Waktu Berakhir',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_holder',
		            'type'	=>	'dropdown',
		            'label'	=>	'Budget Holder',
		            'source'=>	$this->am->get_budget_holder(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'budget_spender',
		            'type'	=>	'dropdown',
		            'label'	=>	'Budget Spender',
		            'source'=>	$this->am->get_budget_spender(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>	'hidden',
		            'rules' =>	'hidden'
	         	),
			),
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataAuction($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}

		echo json_encode($this->form);
	}

	public function editDataAuction($id)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataAuction($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}

		$this->form['url'] = site_url('auction/updateAuction/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function updateAuction($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$admin = $this->session->userdata('admin');
			$save['entry_stamp'] 	= date("Y-m-d H:i:s");
			$save['auction_date'] 	= $save['auction_start'];
			$save['id_mekanisme'] 	= 1;
			$save['id_sbu'] 		= $admin['id_sbu'];	
			$save['start_time']	= date('Y-m-d H:i:s',strtotime($save['auction_start']));
			$save['time_limit']	= date('Y-m-d H:i:s',strtotime($save['auction_end']));
			$lastData = $this->$modelAlias->selectDataAuction($id);
			if ($this->$modelAlias->update_auction($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function view_edit_auction($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit', $data, FALSE);
		$this->load->view('auction/tab/view_edit_js', $data, FALSE);
	}
}