<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Vendor_auction extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('vendor_auction_model','vam');
	}

	public function index($id_lelang)
	{
		$id_lelang = base64_encode($id_lelang);
		$data['fill'] = $fill = $this->vam->get_data($id_lelang);
		$data['barang'] = $this->vam->get_barang($id_lelang);
		$data['kurs_info'] = $this->vam->kurs_info($id_lelang);
		
		$data['id_lelang'] = $id_lelang;
		$data['penawaran'] = $this->vam->get_penawaran($id_lelang);
		// echo print_r($data['penawaran']->result_array());
		// die();
		$data['syarat'] = $this->vam->get_syarat($id_lelang);
		$data['kurs'] = $this->vam->get_kurs($id_lelang);
		
		$data['action'] = site_url('vendor_auction/save_penawaran/');
		

		if($fill['auction_type'] == "forward_auction"){ $data['limit'] = "tertinggi"; $data['persentase'] = "Kenaikan"; }
		if($fill['auction_type'] == "reverse_auction"){ $data['limit'] = "terendah"; $data['persentase'] = "Penurunan"; }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor_auction'),
			'title' => 'Auction'
		));
		$this->header = 'Auction';
		$this->content = $this->load->view('vendor/auction/list_mulai_auction',$data, TRUE);
		$this->script = $this->load->view('vendor/auction/list_mulai_auction_js', $data, TRUE);
		parent::index();
	}

	function save_penawaran(){
		$fill = $this->vam->get_data($_POST['id_lelang']);
		$id_lelang = $_POST['id_lelang'];
		$barang = $_POST['id_barang'];
		$barang_temp = $_POST['id_barang_temp'];
		$kurs = $_POST['id_kurs'];
		$is_lock = $_POST['lock-offer'];
		$harga_satuan = $_POST['satuan'];
		$harga_satuan_temp = $_POST['satuan_temp'];

		$return = array();
		$curr_highest = $curr_lowest = 0;
		$is_first = (isset($_POST['is_first'])) ? $_POST['is_first'] : NULL;
		$max_nilai = $this->vam->check_last_value($id_lelang);

		foreach ($barang as $key => $value) {
			$barang[$key] = str_replace(",", "", $value);
			$harga_satuan[$key] = str_replace(",", "", $harga_satuan[$key]);
		}

		if($fill['kriteria_pemenang']=='lump_sum'&&!$is_first){
			if($max_nilai>=array_sum($barang) && $fill['auction_type'] == "forward_auction"){
				if($fill['auction_jenis']=='auction'){
					die(json_encode(array('status' => 'fail', 'message' => 'Masukan harga penawaran yang lebih tinggi !')));	
				}else{
					$this->session->set_flashdata('msgSuccess','<p class="alert alert-danger alert-notif">Masukan harga penawaran yang lebih tinggi !</p>');
					die(redirect(site_url('vendor_auction/index/'.$id_lelang)));
				}
			}else if($max_nilai<=array_sum($barang) && $fill['auction_type'] == "reverse_auction"){

				if($fill['auction_jenis']=='auction'){
					die(json_encode(array('status' => 'fail', 'message' => 'Masukan harga penawaran yang lebih rendah!')));
				}else{
					$this->session->set_flashdata('msgSuccess','<p class="alert alert-danger alert-notif">Masukan harga penawaran yang lebih rendah !</p>');
					die(redirect(site_url('vendor_auction/index/'.$id_lelang)));
				}
			}
		}
		// echo print_r($barang);
		// die();
		for($i=0;$i<count($barang);$i++){
			$id_barang 			= key($barang);
			$harga_penawaran	= ($barang[$id_barang]) ? $barang[$id_barang] : $barang_temp[$id_barang];
			$satuan				= ($harga_satuan[$id_barang]) ? $harga_satuan[$id_barang] : $harga_satuan_temp[$id_barang];

			$_is_lock			= $is_lock[$id_barang];


			if($harga_penawaran){

				$harga_penawaran 	= str_replace(",", "", $harga_penawaran);
				$satuan 			= str_replace(",", "", $satuan);
				// echo $harga_penawaran;
				$rate 				= $kurs[$id_barang];
				$hps_info			= $this->vam->cek_hps($id_lelang, $id_barang);
				$hps				= $hps_info['nilai_hps'];
									
				$penawaran_in_idr 	= $this->vam->convert_to_idr($harga_penawaran, $rate, $id_lelang);
				$satuan_in_idr 	= $this->vam->convert_to_idr($satuan, $rate, $id_lelang);
				// echo $rate;
				$hps_in_idr			= $this->vam->convert_to_idr($hps, $hps_info['id_kurs'], $id_lelang);

	
				$percent			= $this->vam->cek_percentage($id_lelang, $id_barang, $penawaran_in_idr);

				if(!$is_first){
					if($fill['kriteria_pemenang']=='harga_satuan'){
						//convert to rupiah first
						if($fill['auction_type'] == "forward_auction"){
							$curr_highest = $this->vam->cek_highest($id_lelang, $id_barang);
							$curr_highest = $curr_highest['nilai'];
							// if($curr_highest>0)
							if($curr_highest >= $penawaran_in_idr){
								if($fill['auction_jenis']=='auction'){
									die(json_encode(array('status' => 'fail', 'message' => 'Masukan harga penawaran yang lebih tinggi !')));	
								}else{
									$this->session->set_flashdata('msgSuccess','<p class="alert alert-danger alert-notif">Masukan harga penawaran yang lebih tinggi !</p>');
									die(redirect(site_url('vendor_auction/index/'.$id_lelang)));
								}
							} 
									
						}
						else if($fill['auction_type'] == "reverse_auction"){
							$curr_lowest = $this->vam->cek_lowest($id_lelang, $id_barang);
							$curr_lowest = $curr_lowest['nilai'];
							if($curr_lowest <= $penawaran_in_idr) {
								if($fill['auction_jenis']=='auction'){
									die(json_encode(array('status' => 'fail', 'message' => 'Masukan harga penawaran yang lebih rendah !')));
								}else{
									$this->session->set_flashdata('msgSuccess','<p class="alert alert-danger alert-notif">Masukan harga penawaran yang lebih rendah z!</p>');
									die(redirect(site_url('vendor_auction/index/'.$id_lelang)));
								}
							}	
						}

					}
				}
				
				//set parameters 
				$param = array(
					'id_lelang' => $id_lelang,
					'id_vendor' => $this->session->userdata('user')['id_user'],
					'id_barang' => $id_barang,
					'id_rate' => $rate,
					'nilai' => $harga_penawaran,
					'nilai_satuan' => $satuan,
					'in_rate' => $penawaran_in_idr,
					'in_rate_satuan' => $satuan_in_idr,
					'down_percent' => abs(number_format(preg_replace("/[,]/", "", $percent), 2)),
					'entry_stamp' => date("Y-m-d H:i:s")
				);

				if($is_first){
					$param['entry_stamp'] = $fill['entry_stamp'];
				}
				$id = $this->vam->save_penawaran($param, 'id');

				if($fill['auction_jenis']!='auction'){
					$config['upload_path'] = './lampiran/file_penawaran/';
					$config['allowed_types'] = 'pdf|jpeg|jpg|png|gif|docx|zip|rar';
					if(isset($_FILES['file_penawaran']['name'][$id_barang])){
						$files = $_FILES;
						$_FILES['file']['type']		= $files['file_penawaran']['type'][$id_barang];
						$_FILES['file']['tmp_name']		= $files['file_penawaran']['tmp_name'][$id_barang];
						$_FILES['file']['error']		= $files['file_penawaran']['error'][$id_barang];
						$_FILES['file']['size']		= $files['file_penawaran']['size'][$id_barang];
						$file_name = $_FILES['file']['name'] = 'file_penawaran_'.name_generator($files['file_penawaran']['name'][$id_barang]);
			
						$this->upload->initialize($config);
						
        				if($this->upload->do_upload('file')){

        					$data_upload['id_procurement'] 	= 	$id_lelang;
							$data_upload['id_barang'] 	= 	$id_barang;
							$data_upload['id_vendor'] 	= 	$this->session->userdata('user')['id_user'];
							$data_upload['file']			=	$file_name;
							$data_upload['entry_stamp']		=	date('Y-m-d H:i:s');
							$this->vam->save_upload_penawaran($data_upload);
        				}
					}	
				}
				$data_barang = $this->vam->get_data_barang($id_barang);

				$return['barang'][$i]['id']			= $id_barang;
				$return['barang'][$i]['nilai']		= number_format($harga_penawaran);
				$return['barang'][$i]['in_rate']	= number_format($penawaran_in_idr);
				$return['barang'][$i]['persentase'] = $percent;
				$return['barang'][$i]['nilai_satuan'] = number_format($satuan);
				$return['barang'][$i]['satuan'] 	= ($data_barang['satuan']==null) ? '' : $data_barang['satuan'];
				$return['barang'][$i]['id_kurs'] 	= $rate;
				$return['barang'][$i]['volume'] 	= $data_barang['volume'];
				$return['barang'][$i]['name']		= $data_barang['nama_barang'];
				$return['is_first']					= $is_first;
							
				if($fill['auction_type'] == "forward_auction"){
					if($fill['auction_jenis']=='rfi_penunjukan'||$fill['auction_jenis']=='auction'){
						if($hps_in_idr >= $penawaran_in_idr){
							$return['barang'][$i]['is_higher'] = true;
						}
					}
				} else if($fill['auction_type'] == "reverse_auction"){
					if($fill['auction_jenis']=='rfi_penunjukan'||$fill['auction_jenis']=='auction'){
						if($hps_in_idr <= $penawaran_in_idr){

							$return['barang'][$i]['is_higher'] = true;
						}
					}
				}
			}else if($harga_penawaran==''){
				
			}

			next($kurs);
			next($barang);
			next($is_lock);
			next($harga_satuan);
		}
		if($fill['auction_jenis']=='auction'){
			$return['status'] = "success";
			die(json_encode($return));
		}
		else{
			$this->session->set_flashdata('msgSuccess','<p class="alert alert-success temp">Sukses Melakukan Penawaran!</p>');
			redirect(site_url('vendor_auction/index/'.$id_lelang));
		}
	}
}