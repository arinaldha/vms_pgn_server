<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('Main_model', 'mm');
		$this->load->model('get_model','gm');

	}
	public function index(){
		if($this->session->userdata('admin')){

			if($this->session->userdata('admin')['id_role'] == 7) {
				redirect('auction');
			}else{
				redirect('dashboard');
			}
		}else if($this->session->userdata('user')) {
			redirect('vendor/dashboard');
		}else{
			$this->login();
		}
	}
	public function login(){
		if($this->session->userdata('admin')){
			if($this->session->userdata('admin')['id_role'] == 7) {
				redirect('auction');
			}else{
				redirect('dashboard');
			}
		}else if($this->session->userdata('user')) {
			redirect('vendor/dashboard');
		}
		$this->load->view('login_page');
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function check(){
		$return['status'] = 'error';
		$return['message']= 'Terjadi Kesalahan';

		$result = $this->mm->check($this->input->post());
		if($result){
			// echo $this->db->last_query();
			$return['status'] = 'success';
			$return['message']= '<div class="login-notif" id="sukses">Login Berhasil</div>';
			$return['url'] = site_url('dashboard');

		}else{
			// echo $this->db->last_query();
			$return['status'] = 'error';
			$return['message']= '<div class="login-notif" id="gagal">Data tidak dikenal. Silahkan login kembali!</div>';
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($return);
	}	
	public function template_debug(){
		$this->load->view('template/layout-folder');
	}
	public function add_vendor(){
		$data['id_sbu'] = $this->gm->getdatasbu();
		$data1['id_role'] = $this->gm->getdatalegal();
		$this->load->view('daftar_vendor',array_merge($data,$data1));
	}

	function proses_add(){

	}
}