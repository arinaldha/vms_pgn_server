<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {

	public $form;

	public $modelAlias = 'um';

	public $alias = 'ms_admin';

	public $module = 'User';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('master/user_model','um');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'status',
		            'type'	=>	'radioList',
		            'label'	=>	'Status Karyawan',
		            'source'=>	array('organik' => 'Karyawan Organik','non-organik' => 'Karyawan Non-organik')
		        ),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama User'
		        ),
		        array(
		            'field'	=> 	'password',
		            'type'	=>	'password',
		            'label'	=>	'Password'
	         	),
	         	array(
		            'field'	=> 	'email',
		            'type'	=>	'text',
		            'label'	=>	'Email'
	         	),
	         	array(
					'field'=>'id_sbu',
					'label'=>'Unit Organisasi',
					'type' =>'dropdown',
					'source' =>	$this->gm->getSbu(),
				),
				array(
					'field'=>'id_role',
					'label'=>'Role',
					'type' =>'dropdown',
					'source' =>	$this->gm->getRole(),
				),
				array(
		            'field'	=> 	'username',
		            'type'	=>	'text',
		            'label'	=>	'Username'
	         	)
	         )
		);
		$this->getData = $this->um->getData($this->form);
		$this->insertUrl = site_url('master/user/save/');
		$this->updateUrl = 'master/user/update/';
		$this->deleteUrl = 'master/user/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/user'),
			'title' => 'User'
		));
		$this->header = 'User';
		$this->content = $this->load->view('master/user/list',$data, TRUE);
		$this->script = $this->load->view('master/user/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		$this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('master/user/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		error_reporting(E_ALL);
		$modelAlias = $this->modelAlias;
		$save = $this->input->post();
		
		if ($this->$modelAlias->insert($save)) {
			echo json_encode(array('status'=>'success'));
			$this->deleteTemp($save);
		}	

	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		$save = $this->input->post();
		
		$lastData = $this->$modelAlias->selectData($id);
		if ($this->$modelAlias->update($id, $save)) {
			echo json_encode(array('status'=>'success'));
			$this->deleteTemp($save, $lastData);
		}
	}

	public function delete($id) {
		
		if ($this->um->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('master/user/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
	//
}
