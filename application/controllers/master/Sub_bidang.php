<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sub_bidang extends MY_Controller {

	public $form;

	public $modelAlias = 'sbm';

	public $alias = 'tb_sub_bidang';

	public $module = 'Sub Bidang';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('master/sub_bidang_model','sbm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
		        array(
					'field'=>'id_bidang',
					'label'=>'Kategori Bidang',
					'type' =>'dropdown',
					'source' =>	$this->gm->getBidang()
				),
				array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Sub Bidang',
		            'rules' => 	'required',
		        )
	         )
		);
		$this->getData = $this->sbm->getData($this->form);
		$this->insertUrl = site_url('master/sub_bidang/save/');
		$this->updateUrl = 'master/sub_bidang/update';
		$this->deleteUrl = 'master/sub_bidang/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/sub_bidang'),
			'title' => 'Sub Bidang'
		));
		$this->header = 'Sub Bidang';
		$this->content = $this->load->view('master/sub_bidang/list',$data, TRUE);
		$this->script = $this->load->view('master/sub_bidang/list_js', $data, TRUE);
		parent::index();
	}

	public function insert()
	{
		$this->form['url'] = site_url('master/sub_bidang/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_bidang'] = $save['id_bidang'];
			$save['name'] = $save['name'];
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function delete($id) {
		
		if ($this->sbm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('master/sub_bidang/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Non-aktifkan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function active($id) {
		
		if ($this->sbm->active($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function aktif($id)
	{
		$this->formDelete['url'] = site_url('master/sub_bidang/active/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Aktifkan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}
