<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Badan_hukum extends MY_Controller {

	public $form;

	public $modelAlias = 'bhm';

	public $alias = 'tb_legal';

	public $module = 'Badan Hukum';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('master/badan_hukum_model','bhm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Badan Hukum',
		            'rules' => 	'required',
		        )
	         )
		);
		$this->getData = $this->bhm->getData($this->form);
		$this->insertUrl = site_url('master/badan_hukum/save/');
		$this->updateUrl = 'master/badan_hukum/update';
		$this->deleteUrl = 'master/badan_hukum/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/badan_hukum'),
			'title' => 'Badan Hukum'
		));
		$this->header = 'Badan Hukum';
		$this->content = $this->load->view('master/badan_hukum/list',$data, TRUE);
		$this->script = $this->load->view('master/badan_hukum/list_js', $data, TRUE);
		parent::index();
	}
}
