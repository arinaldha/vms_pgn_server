<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Kurs extends MY_Controller {

	public $form;

	public $modelAlias = 'km';

	public $alias = 'tb_kurs';

	public $module = 'Kurs';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('master/kurs_model','km');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Mata Uang',
		            'rules' => 	'required',
		        ),
		        array(
		            'field'	=> 	'symbol',
		            'type'	=>	'text',
		            'label'	=>	'Simbol Kurs',
		            'rules' => 	'required',
	         	)
	         )
		);
		$this->getData = $this->km->getData($this->form);
		$this->insertUrl = site_url('master/kurs/save/');
		$this->updateUrl = 'master/kurs/update';
		$this->deleteUrl = 'master/kurs/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/kurs'),
			'title' => 'Mata Uang'
		));
		$this->header = 'Mata Uang';
		$this->content = $this->load->view('master/kurs/list',$data, TRUE);
		$this->script = $this->load->view('master/kurs/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['symbol'] = $save['symbol'];
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}
