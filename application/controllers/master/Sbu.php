<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sbu extends MY_Controller {

	public $form;

	public $modelAlias = 'sm';

	public $alias = 'tb_sbu';

	public $module = 'Unit Organisasi';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('master/sbu_model','sm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Unit Organisasi',
		            'rules' => 	'required',
		        )
	         )
		);
		$this->getData = $this->sm->getData($this->form);
		$this->insertUrl = site_url('master/sbu/save/');
		$this->updateUrl = 'master/sbu/update';
		$this->deleteUrl = 'master/sbu/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/sbu'),
			'title' => 'Unit Organisasi'
		));
		$this->header = 'Unit Organisasi';
		$this->content = $this->load->view('master/sbu/list',$data, TRUE);
		$this->script = $this->load->view('master/sbu/list_js', $data, TRUE);
		parent::index();
	}
}