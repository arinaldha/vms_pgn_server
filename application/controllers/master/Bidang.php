<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Bidang extends MY_Controller {

	public $form;

	public $modelAlias = 'bm';

	public $alias = 'tb_bidang';

	public $module = 'Bidang';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('master/bidang_model','bm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
		        array(
					'field'=>'id_dpt_type',
					'label'=>'Kategori Bidang',
					'type' =>'dropdown',
					'source' =>	$this->gm->getDptType(),
				),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Bidang',
		            'rules' => 	'required',
		        )
	         )
		);
		$this->getData   = $this->bm->getData($this->form);
		$this->insertUrl = site_url('master/bidang/save/');
		$this->updateUrl = 'master/bidang/update';
		$this->deleteUrl = 'master/bidang/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/bidang'),
			'title' => 'Bidang'
		));
		$this->header = 'Bidang';
		$this->content = $this->load->view('master/bidang/list',$data, TRUE);
		$this->script = $this->load->view('master/bidang/list_js', $data, TRUE);
		parent::index();
	}

	public function delete($id) {
		
		if ($this->bm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('master/bidang/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Non-aktifkan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function active($id) {
		
		if ($this->bm->active($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function aktif($id)
	{
		$this->formDelete['url'] = site_url('master/bidang/active/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Aktifkan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}
