<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Regis_vendor extends MY_Controller {

	public $form;

	public $modelAlias = 'rvm';

	public $alias = 'ms_vendor';

	public $module = 'Daftar Vendor';

	public $isClientMenu = true;

	public $needLogin = false;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('regis_vendor_model','rvm');
		$this->load->model('get_model','gm');
		$this->load->library('email');
		$this->form = array(
			'form'=>array(
				'left'=>array(
					array(
			            'field'=>'id_sbu',
						'label'=>'Unit Organisasi|Organization Unit',
						'type' =>'dropdown',
						'source' =>	$this->gm->getSbu()
			        ),
			        array(
			            'field'=>'id_legal',
						'label'=>'Badan Hukum|Title',
						'type' =>'dropdown',
						'source' =>	$this->gm->getdatalegal()
		         	),
		         	array(
			            'field'	=> 	'name',
			            'type'	=>	'text',
			            'label'	=>	'Nama Badan Usaha|Company Name',
			            'rules' => 	'required',
			            'caption' => '<span class="english-caption">*Enter company name without title. ex: Perusahaan Gas Negara, Tbk(<strike>PT</strike> Perusahaan Gas Negara, Tbk).</span>'
		         	),
		         	array(
			            'field'	=> 	'npwp_code',
			            'type'	=>	'npwp',
			            'label'	=>	'NPWP|NPWP',
			            'rules' => 'required|callback_check_npwp'
		         	),
		         	array(
		         		'field'	=> 	'npwp_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Pengukuhan|Date',
			            'rules' => 'required'
		         	),
		         	array(
			            'field'	=> 	'npwp_file',
			            'type'	=>	'file',
			            'label'	=>	'Lampiran|NPWP File',
			            'upload_path'=>base_url('assets/lampiran/npwp_file/'),
						'upload_url'=>site_url('regis_vendor/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
			            'rules' => 'required'
		         	),
		         	array(
			            'field'	=> 	'nppkp_code',
			            'type'	=>	'text',
			            'label'	=>	'NPPKP|NPPKP'
			            // 'rules' => 	'required'
		         	),
		         	array(
		         		'field'	=> 	'nppkp_date',
			            'type'	=>	'date',
			            'label'	=>	'Tanggal Pengukuhan|Date'
		         	),
		         	array(
			            'field'	=> 	'nppkp_file',
			            'type'	=>	'file',
			            'label'	=>	'Lampiran|NPPKP File',
			            'upload_path'=>base_url('assets/lampiran/nppkp_file/'),
						'upload_url'=>site_url('regis_vendor/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
		         	),
		         	array(
		         		'field'	=> 	'',
			            'type'	=>	'text',
			            'label'	=>	'User Manual',
			            'readonly' => true,
			            'value'	=> '<a href="'.base_url('assets/user_manual/user_manual.pdf').'" target="blank" style="margin-left: auto;margin-right: auto;"><i class="fa fa-download"></i><span class="icon-text"> User Manual</span></a>'
		         	)
				),
				'right'=>array(
					array(
		         		'field'	=> 	'vendor_office_status',
			            'type'	=>	'radioList',
			            'label'	=>	'Status',
			            'source'=> array(
			            			'pusat' => 'Pusat',
			            			'cabang'=> 'Cabang'
			            ),
			            'rules' => 'required'
		         	),
		         	array(
			            'field'	=> 	'vendor_address',
			            'type'	=>	'text',
			            'label'	=>	'Alamat|Address',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_city',
			            'type'	=>	'text',
			            'label'	=>	'Kota/Kab|City',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_province',
			            'type'	=>	'text',
			            'label'	=>	'Provinsi|Province',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_country',
			            'type'	=>	'text',
			            'label'	=>	'Negara|Country',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_postal',
			            'type'	=>	'text',
			            'label'	=>	'Kode Pos|Post Code',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_phone',
			            'type'	=>	'text',
			            'label'	=>	'No Telp|Phone',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_fax',
			            'type'	=>	'text',
			            'label'	=>	'Fax|Faximile'
		         	),
		         	array(
			            'field'	=> 	'vendor_email',
			            'type'	=>	'text',
			            'label'	=>	'Email',
			            'rules' => 	'required',
		         	),
		         	array(
			            'field'	=> 	'vendor_website',
			            'type'	=>	'text',
			            'label'	=>	'Website',
		         	),
		    //      	array(
			   //          'field'	=> 	'vendor_norek',
			   //          'type'	=>	'text',
			   //          'label'	=>	'No.Rekening',
		    //      	),
		    //      	array(
			   //          'field'	=> 	'vendor_bank',
			   //          'type'	=>	'text',
			   //          'label'	=>	'Bank',
		    //      	),
		    //      	array(
			   //          'field'	=> 	'vendor_file',
			   //          'type'	=>	'file',
			   //          'label'	=>	'Lampiran|Bank Statement',
			   //          'upload_path'=>base_url('assets/lampiran/vendor_file/'),
						// 'upload_url'=>site_url('regis_vendor/upload_lampiran'),
						// 'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						// 'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
		    //      	),
		         	array(
			            'field'	=> 	'captcha',
			            'type'	=>	'google_captcha',
			            'label'	=>	'CAPTCHA',
			            'rules' => 	'callback_check_captcha',
		         	)
				)
			)
		);
		$this->insertUrl = site_url('vendor_regis/save/');
		$this->updateUrl = 'vendor_regis/update/';
		$this->deleteUrl = 'vendor_regis/delete/';
	}
	
	public function index(){
		
		// $data = array(
		// 	'header' => 'Isian Data Administrasi',
		// 	'content' => ,
		// 	'script' => $this->load->view('vendor_regis/view_js', null, TRUE)
		// );
		// $this->parser->parse('template/blank', $data);
		$this->load->view('vendor_regis/view',null,FALSE);
		$this->load->view('vendor_regis/view_js', null, FALSE);
	}
	public function check_captcha($str){

		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".GOOGLE_RECAPTCHA_SECRET_KEY."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		
		if($response['success'] == false)
        {
        	$this->form_validation->set_message('check_captcha', 'CAPTCHA wajib di isi!');
			return false;
        }else{
        	return true;
        }
	}
	public function insert(){
		$this->form['url'] = site_url('regis_vendor/save');
		$this->form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Simpan'
                )
            );
		echo json_encode($this->form);
	}

	public function save($data=null){
		$modelAlias = $this->modelAlias;

			$_form = array();
			$i = 0;
			foreach ($this->form['form'] as $key => $value) {
				foreach ($value as $keys => $values) {
					$_form[$i] = $values;  
					$i++;
				}

			}
			$this->form['form'] = $_form;
		
		$this->form_validation->set_rules($this->form['form']);
		
        if($this->validation()){
            $save = $this->input->post();
            $vendor_data['id_role'] = 11;
            $vendor_data['vendor_status'] = 0;
            $vendor_data['npwp_code'] = $save['npwp_code'];
            $vendor_data['name'] = $save['name'];
            $vendor_data['is_active'] = 1;
            $vendor_data['ever_blacklisted'] = 0;
            $vendor_data['entry_stamp'] = timestamp();
            $vendor_data['del'] = 0;
	    $vendor_data['id_sbu'] = $save['id_sbu'];
            $id = $this->$modelAlias->insert_vendor($vendor_data);

            $vendor_administrasi_data['id_vendor'] = $id;
            // $vendor_administrasi_data['email'] = $save['email'];
            $vendor_administrasi_data['npwp_code'] = $save['npwp_code'];
            $vendor_administrasi_data['npwp_date'] = $save['npwp_date'];
            $vendor_administrasi_data['npwp_file'] = $save['npwp_file'];
            $vendor_administrasi_data['is_nppkp'] = ($save['is_nppkp'] =='') ? null:$save['is_nppkp'];
            $vendor_administrasi_data['nppkp_code'] = $save['nppkp_code'];
            $vendor_administrasi_data['nppkp_date'] = ($save['nppkp_date'] == '') ? null:$save['nppkp_date'];
            $vendor_administrasi_data['nppkp_file'] = $save['nppkp_file'];
            $vendor_administrasi_data['vendor_office_status'] = $save['vendor_office_status'];
            $vendor_administrasi_data['vendor_address'] = $save['vendor_address'];
            $vendor_administrasi_data['vendor_country'] = $save['vendor_country'];
            $vendor_administrasi_data['vendor_province'] = $save['vendor_province'];
            $vendor_administrasi_data['vendor_city'] = $save['vendor_city'];
            $vendor_administrasi_data['vendor_phone'] = $save['vendor_phone'];
            $vendor_administrasi_data['vendor_fax'] = $save['vendor_fax'];
            $vendor_administrasi_data['vendor_email'] = $save['vendor_email'];
            $vendor_administrasi_data['vendor_postal'] = $save['vendor_postal'];
            $vendor_administrasi_data['vendor_website'] = $save['vendor_website'];
            $vendor_administrasi_data['id_legal'] = $save['id_legal'];
            $vendor_administrasi_data['entry_stamp'] = timestamp();
            $vendor_administrasi_data['del'] = 0;
            $this->$modelAlias->insert_data_vendor('ms_vendor_admistrasi',$vendor_administrasi_data);

            $password = password_generator();
            $vendor_login['id_user'] = $id;
            $vendor_login['type'] = 'user';
            $vendor_login['username'] = $save['vendor_email'];
            $vendor_login['password'] = do_hash($password, 'sha1');
            $vendor_login['password_raw'] = $password;
            $vendor_login['entry_stamp'] = timestamp();
            $vendor_login['del'] = 0;
            $this->$modelAlias->insert_data_vendor('ms_login',$vendor_login);
            $this->session->set_userdata('vendor_email',$save['vendor_email']);
            $this->deleteTemp($save);
           	$message = 'Perusahaan saudara telah terdaftar kedalam sistem Vendor Management System PT Perusahaan Gas Negara Tbk.
				Berikut username &amp; password saudara : <br/><br/>
				
				Username : '.(isset($save['vendor_email'])? $save['vendor_email']:'').'<br/>
				Password : '.(isset($password)?$password:'').'<br/><br/>
				
				Untuk selanjutnya, silahkan melengkapi data - data dan dokumen saudara di aplikasi VMS dengan mengakses link berikut <a href="vms.pgn.co.id">vms.pgn.co.id</a>.<br/><br/>
				Terima kasih.<br/>
				PT Perusahaan Gas Negara';
			// email($save['vendor_email'], $message, 'Authentikasi Login VMS PT Perusahaan Gas Negara Tbk');
			$this->email->from(EMAIL_HOSTNAME, 'VMS PGN');
			$this->email->to($save['vendor_email']); 


			$this->email->subject('Authentikasi Login PT Perusahaan Gas Negara Tbk Vendor Management System');
			
			$this->email->message($message);	
			$this->email->send();
		}
        
    }
    public function confirmation(){
		$data = array(
			'header' => 'Confirmation',
			'content' => $this->load->view('vendor_regis/confirmation', null, TRUE),
			// 'script' => $this->load->view('vendor_regis/view_js', null, TRUE)
		);
		$this->parser->parse('template/blank', $data);
    }

    public function check_npwp($field, $opt)
		{
			$this->load->model('regis_vendor_model','rvm');
			$no_npwp = $this->rvm->get_npwp($field);
			if ($no_npwp->num_rows() > 0) {
				$this->form_validation->set_message('check_npwp', 'Nomor NPWP sudah dipakai!');
				return false;
			}else{
				return true;
			}
		}
}