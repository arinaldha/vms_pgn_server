<?php 
/**
 * 
 */
class Qrcode extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('vendor/izin_model','im');
		$this->load->model('get_model','gm');
	}

	public function view_qr($id)
	{
		$data['administrasi']	= $this->vm->get_administrasi_list($id,TRUE);
		$data['surat_izin']		= $this->im->get_izin_report($id,TRUE);
		$data['situ']			= $this->im->get_situ_report($id,TRUE);
		$data['tdp']			= $this->im->get_tdp_report($id,TRUE);
		$data['keagenan']		= $this->im->get_keagenan_report($id,TRUE);
		$data['pengalaman']		= $this->im->get_pengalaman_report($id,TRUE);
		$data['klasifikasi']	= array(1=>'non-Konstruksi', 2=>'non-Konstruksi',3=>'non-Konstruksi',4=>'Konstruksi',5=>'Konstruksi');
		$this->load->view('dpt/view_print',$data,false);
	}
}