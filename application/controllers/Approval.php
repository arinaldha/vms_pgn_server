<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Approval extends MY_Controller {

	public $form;

	public $modelAlias = 'apm';

	public $module = 'Approval';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			$this->load->model('approval_model','apm');
			$this->load->model('get_model','gm');
			
			if (! $this->session->userdata('admin')) {
				redirect(site_url());
			} 
	}

	 public function index($id){
	 	$id = base64_decode($id);

	 	$admin = $this->session->userdata('admin');
	 	if($this->session->userdata('admin')['id_role']==2){
	 		redirect('approval/k3/'.$id);
	 	}
	 	$dataAdm = $this->apm->get_data_administrasi($id);
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/admin/daftar_tunggu'),
	 		'title' => 'Daftar Tunggu'
	 	));
	 	$this->breadcrumb->addlevel(2, array(
	 		'url' => site_url('approval/administrasi'),
	 		'title' => 'Cek Data Administrasi'
	 	));
	 	$data['id'] = $id;
	 	$data['dataAdm'] = $dataAdm;
	 	$data['admin'] = $admin;
	 	$this->header = 'Cek Data Administrasi';
	 	$this->content = $this->load->view('approval/view',$data, TRUE);
	 	$this->script = $this->load->view('approval/view_js', $data, TRUE);
	 	parent::index();

	}
}
