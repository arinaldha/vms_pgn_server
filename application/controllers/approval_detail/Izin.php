<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_ijin_usaha';

	public $module = 'Izin';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/izin_model','im');
	}
	
	public function index($id, $type){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/view_tab_js', $data, FALSE);
		

	}

	public function siup($id, $type='siup')
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/list', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_js', $data, FALSE);
	}

	public function siul($id, $type='ijin_lain')
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/list_siul', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_siul_js', $data, FALSE);
	}

	public function asosiasi($id, $type='asosiasi')
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/list_asosiasi', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_asosiasi_js', $data, FALSE);
	}

	public function siujk($id, $type='siujk')
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/list_siujk', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_siujk_js', $data, FALSE);
	}

	public function sbu($id, $type='sbu')
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/izin/list_sbu', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_sbu_js', $data, FALSE);
	}

	public function view($id = null, $type)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_izin($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_izin($id);
    	$data['id'] = base64_encode($id);
    	$this->load->view('approval/approval_detail/izin/view', $data, FALSE);
		$this->load->view('approval/approval_detail/izin/view_js', $data, FALSE);
    }

	public function verifikasi($id)
	{
		$id = base64_decode($id);
		$data_ijin = $this->im->get_data_ijin($id);

		$this->form = array(
			'id'=>$id,
			'type'=>$data_ijin['type'],
			'form'=>array(
				array(
		            'field'	=> 'dpt_name',
		            'type'	=> 'text',
		            'label'	=> 'DPT Tipe',
		            'rules' => 'required'
		        ),
		        array(
					'field'	=> 	'authorize_by',
					'type'	=>	'text',
					'label'	=>	($this->input->post('type')=='asosiasi') ? 'Nama Asosiasi / Keanggotaan Lainnya': 'Lembaga Penerbit',
					'rules' => 	'required'
				),
	         	array(
		            'field'	=> 'no',
		            'type'	=> 'text',
		            'label'	=> 'Nomor',
		            'rules' => 'required'
		        ),

		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'qualification',
		            'type'	=>	'radioList',
		            'label'	=>	'Kualifikasi',
		            'rules' => 	'required',
		            'source'=>  array(
		            			'kecil' => 'Kecil',
		            			'non-kecil' => 'Non-kecil'
		            ),
	         	),
	         	 array(
					'field'	=> 	'grade',
					'type'	=>	'dropdown',
					'label'	=>	'Grade',
					'rules' => 	'required',
					'source'=>	array(
									1=>1,
									2=>2,
									3=>3,
									4=>4,
									5=>5,
									6=>6,
									7=>7
								)
				),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' =>  'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);

		$modelAlias = $this->modelAlias;
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_izin($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['field'] != 'mandatory' && $this->form['form'][$key]['field'] != 'status' ) {
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id){
		$id = base64_decode($id);
		$data = $this->am->verifikasi_izin($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_ijin_usaha','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function getCekDataBsb($id){
		$id = base64_decode($id);
		// $id_dpt = $this->im->get_data_ijin($id);
		echo json_encode($this->im->getCekDataBsb($id));
	}
}
