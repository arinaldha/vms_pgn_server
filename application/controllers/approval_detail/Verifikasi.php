<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Verifikasi extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('approval_model','am');
		$this->load->model('note_model','nm');
		$this->load->model('vendor/vendor_model','vm');
	}

	public function index($id)
	{

		$admin = $this->session->userdata('admin');
		
		// $data = $this->vm->getDataDpt($id);
		$data = $this->vm->get_data($id);
		$data['pengalaman_bsb']	 = $this->am->checkPengalamanBSB($id);
		$data['approval_data'] = $this->am->get_total_data($id);
		$data['notifikasi'] = $this->nm->get_note_admin($id)->result_array();
		$data['bar'] = array(
									'pending'=>array(
										'percentage'=>(count($data['approval_data'][0]))/$data['approval_data']['total']*100,
										'value'=>count($data['approval_data'][0])
									),
									'approved'=>array(
										'percentage'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))
									),
									'rejected'=>array(
										'percentage'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))
									)
								);

		$data['id'] = $id;
		$data['admin'] = $admin;
		$this->load->view('approval/approval_detail/verifikasi/approval',$data,FALSE);
		$this->load->view('approval/approval_detail/verifikasi/approval_js',$data,FALSE);

	}
	public function approveForm($id){
		$admin = $this->session->userdata('admin');
		$this->form['button'] = array(
					array(
						'type' => 'submit',
						'label' => 'Proses'
					),
					array(
						'type' => 'cancel',
						'label' => 'Batal'
					)
				);

		$this->form['successAlert']='Berhasil!';
		$this->form['url']= site_url('approval_detail/verifikasi/approve/'.$id);
		
		echo json_encode($this->form);
	
	}
	public function testAngkat($id){
		
		$this->am->approve($id);
	}
	public function approve($id){
		$modelAlias = $this->modelAlias;
		$admin = $this->session->userdata('admin');
		$data_vendor = $this->vm->get_data($id);
		if ($this->am->angkat_vendor($id)) {

			$message = $admin['name'].' mengajukan pengangkatan DPT '.$data_vendor['legal_name'].' '.$data_vendor['name'];
			// $this->utility->set_admin_note(8, $admin['id_sbu'], $message);

			$subject = 'Pengangkatan Vendor PT. Perusahaan Gas Negara, Tbk';
			$message = $data_vendor['legal_name'].' '.$data_vendor['name'].' telah dimasukkan ke dalam daftar supervisor. <br>
			Harap segera mengkonfirmasi pengangkatan vendor.<br> Terima kasih.<br/>
				PT Perusahaan Gas Negara, Tbk';

			$recipient = $this->am->get_recipient_admin(8,$admin['id_sbu']);

			foreach ($recipient as $key => $value) {
				$this->email->from(EMAIL_HOSTNAME, 'VMS PGN');
				$this->email->to($value['email']); 
				$this->email->subject($subject);
				$this->email->message($message);	
				$this->email->send();
			}
			

			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data terkirim untuk disetujui!</p>');	
			
			
			echo json_encode(array('status'=>'success'));
		}
	}
	public function angkatForm($id){
		$this->form['button'] = array(
					array(
						'type' => 'submit',
						'label' => 'Angkat menjadi DPT'
					),
					array(
						'type' => 'cancel',
						'label' => 'Batal'
					)
				);

		$this->form['successAlert']='Berhasil!';
		$this->form['url']= site_url('approval_detail/verifikasi/angkat/'.$id);
		
		echo json_encode($this->form);
	
	}
	
	public function reject($id)
	{
		$this->formDelete['url'] = site_url('approval_detail/verifikasi/process_reject/'.$id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Reject'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function process_reject($id){
		$data['vendor'] = $this->vm->get_data($id);
		$message = '<a href="'.site_url('vendor/view/administrasi/'.$data['vendor']['id']).'">'.$data['vendor']['legal_name'].' '.$data['vendor']['name'].'</a> :'.$this->input->post('remark');

		// if($this->input->post('kirim')){
			// $this->utility->set_admin_note(1, $admin['id_sbu'], $message);
			$res = $this->am->reject($id);
			if($res){
				$return['status'] = 'success';
			}
			else {
				$return['status'] = 'error';
			}

			echo json_encode($return);
		// }
		

		// $layout['content'] =  $this->load->view('reject',$data,TRUE);
		
		// $item['header'] = $this->load->view('admin/header',$this->session->userdata('admin'),TRUE);
		
		// $item['content'] = $this->load->view('admin/dashboard',$layout,TRUE);
		// $this->load->view('template',$item);
	}
	public function angkat($id){
		$this->load->model('approval_model','am');
		$data_vendor 	= $this->vm->get_data($id);
		$admin 			= $this->session->userdata('admin');
		$res 			= $this->am->approve($id);
		if($res){
			$subject = 'Pengangkatan Vendor PT. Perusahaan Gas Negara, Tbk';
			$message = $data_vendor['legal_name'].' '.$data_vendor['name'].' telah berhasil diangkat menjadi DPT. <br> Terima kasih.<br/><br>PT Perusahaan Gas Negara, Tbk';

			$recipient = $this->am->get_recipient_admin(1,$admin['id_sbu']);
			foreach ($recipient as $key => $value) {
				email($value['email'], $message, $subject);
			}
			

			$subject = 'Pemberitahuan Pengangkatan Vendor PT. Perusahaan Gas Negara, Tbk';
			$message = $data_vendor['legal_name'].' '.$data_vendor['name'].' telah berhasil diangkat menjadi DPT. <br> Terima kasih.<br/>
				PT Perusahaan Gas Negara';
			email($data_vendor['vendor_email'], $message, $subject);
			$this->generate_email_blast($id);
			echo json_encode(array('status'=>'success'));
		}else{
			echo json_encode(array('status'=>'fail'));
		}

	}
	public function set_expire(){
		$query = $this->db->query('SELECT * FROM ms_vendor WHERE vendor_status > 0');
		foreach ($query->result_array() as $key =>$value) {
			$this->generate_email_blast($value['id']);
		}
	}
	public function generate_email_blast($id_vendor){
		$this->db->where('id_vendor', $id_vendor)->delete('tr_email_blast');
		$list_dokumen = array(
			'ms_akta'=>'Akta',
			'ms_situ'=>'SITU/Domisili',
			'ms_tdp'=>'TDP',
			'ms_pengurus'=>'Pengurus',
			'ms_ijin_usaha'=>'Izin Usaha',
			'ms_agen'=>'Pabrikan/Keagenan/Distributor',
			'ms_pengalaman'=>'Pengalaman');
		
		foreach ($list_dokumen as $key => $value) {
			
			$data_dokumen = $this->db->where('id_vendor', $id_vendor)->where('data_status', 1)->get($key);
			foreach ($data_dokumen->result_array() as $key_dokumen => $value_dokumen) {

				if($key=='ms_akta'){
					if($value_dokumen['type']=='pendirian'){
						$subject = 'Akta Pendirian Perusahaan';
					}else{
						$subject = 'Akta Perubahan Terakhir';
					}
					
				}
				if($key=='ms_ijin_usaha'){
					$list_ijin_usaha =	array(
										'siujk'=>'SIUJK',
										'sbu'=>'SBU',
										'siup'=>'SIUP',
										'ijin_lain'=>'Surat Izin Usaha Lainnya',
										'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
									);
					$subject = $list_ijin_usaha[$value_dokumen['type']];
				}

				$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
				$nama_orang = array('ms_pengurus','ms_pemilik');	
				if(in_array($key, $dokumen)){
					$subject_name=	'Nomor Dokumen';
				}
				if(in_array($key, $nama_orang)){
					$subject_name=	'Nama';
				}
				if($key=='ms_pengalaman'){
					$subject_name='Nama Pekerjaan';
				}


				if( $key=='ms_pengurus'){
					$this->set_email_blast($value_dokumen['id'], $key, $value, $value_dokumen['position_expire'], $value_dokumen);
				}else{
					$this->set_email_blast($value_dokumen['id'], $key, $value, $value_dokumen['expire_date'], $value_dokumen);
				}
				
			}
			
		}
		
	}
	function checkmydate($date) {
	  	$tempDate = explode('-', $date);
	  	return checkdate($tempDate[1], $tempDate[2], $tempDate[0]);
	}
	public function set_email_blast($id_doc,$doc_type,$name_file,$expire_date, $document){

		
		$this->db->where('id_doc', $id_doc)->where('doc_type', $doc_type)->delete('tr_email_blast');

		if($expire_date != 'lifetime'&& $expire_date != ''&& $this->checkmydate($expire_date)){

			$array[30]['date'] = date('Y-m-d',strtotime($expire_date.' -30 days'));
			for($i = 7; $i>=0;$i--){
				$array[$i]['date'] = date('Y-m-d',strtotime($expire_date.' -'.$i.' days'));
			}

			$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
			$nama_orang = array('ms_pengurus','ms_pemilik');	
			if(in_array($doc_type, $dokumen)){
				$subject_field=	'no';
			}
			if(in_array($doc_type, $nama_orang)){
				$subject_field=	'name';
			}
			if($doc_type=='ms_pengalaman'){
				$subject_field='job_name';
			}
			$no = $document[$subject_field];
			
			foreach($array as $key=>$val){
				

				if(strtotime($val['date']) >= strtotime(date('Y-m-d'))){
					$data_array = array(
							'id_doc'=>$id_doc,
							'doc_type'=>$doc_type,
							'distance'=>$key,
							'date'=>$val['date'],
							'message'=>$this->set_message($subject, $no,$name_file,$key),
							'id_vendor'=>$document['id_vendor']
						);
					$result = $this->db->insert('tr_email_blast',$data_array);
				}
				
			}
		}
	}

	public function set_message($subject, $no,$name_file,$distance){
		$txt = '';
		if($distance==0){
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' sudah habis masa berlakunya.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}else if($distance==30){
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' menyisakan 30 hari sebelum masa berlakunya habis.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}else{
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' menyisakan '.$distance.' hari sebelum masa berlakunya habis.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}
		return $txt;
	}

}