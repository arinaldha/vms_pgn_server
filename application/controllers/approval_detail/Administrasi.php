<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Administrasi extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Administrasi';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'=>'id_sbu',
					'label'=>'Lokasi Pendaftaran',
					'type' =>'dropdown',
					'source' =>	$this->gm->getSbuView()
		        ),
		        array(
		            'field'=>'id_legal',
					'label'=>'Badan Usaha',
					'type' =>'dropdown',
					'source' =>	$this->gm->getdatalegal()
	         	),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Badan Usaha',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'npwp_code',
		            'type'	=>	'npwp',
		            'label'	=>	'NPWP',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'npwp_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Pengukuhan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'npwp_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/npwp_file/'),
					'upload_url'=>site_url('regis_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'nppkp_code',
		            'type'	=>	'text',
		            'label'	=>	'NPPKP',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'nppkp_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Pengukuhan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'nppkp_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/nppkp_file/'),
					'upload_url'=>site_url('regis_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
	         		'field'	=> 	'vendor_office_status',
		            'type'	=>	'radio',
		            'label'	=>	'Status',
		            'source'=> array(
		            			'pusat' => 'Pusat',
		            			'cabang'=> 'Cabang'
		            ),
	         	),
	         	array(
		            'field'	=> 	'vendor_address',
		            'type'	=>	'textarea',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_city',
		            'type'	=>	'text',
		            'label'	=>	'Kota/Kab',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_province',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_country',
		            'type'	=>	'text',
		            'label'	=>	'Negara',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_postal',
		            'type'	=>	'text',
		            'label'	=>	'Kode Pos',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_phone',
		            'type'	=>	'text',
		            'label'	=>	'No Telp',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_fax',
		            'type'	=>	'text',
		            'label'	=>	'Fax',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_email',
		            'type'	=>	'text',
		            'label'	=>	'Email',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_website',
		            'type'	=>	'text',
		            'label'	=>	'Website',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'vendor_type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Status Perusahaan',
		           	'source'=>	$this->gm->getTypeCompany(),
		           	'rules'	=> 'required'

	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	)
	         )
		);
	}
	
	public function index($id, $process=false){
		$id = base64_decode($id);
		// print_r($this->input->post()['vendor_type']);
		if($process==true){
			if ($this->input->post()['vendor_type'] != 0) {
				$data = $this->am->get_data_administrasi($id);
				$result = $this->data_process->check($id, $this->input->post(),$data['id'],'ms_vendor_admistrasi','id_vendor');
				$this->am->update_ms_vendor_administrasi($id,$this->input->post());
				if($result){
					echo json_encode(array('status'=>'success'));
				}
			}
			 else {
				echo json_encode(array('status'=>'fail'));
			}
			
		}else{
			$data = $this->am->get_data_administrasi($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/administrasi/list', $data, FALSE);
			$this->load->view('approval/approval_detail/administrasi/list_js', $data, FALSE);
		}
		
	}

	public function view($id){
		$id = base64_decode($id);
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_administrasi($id);

		 foreach($this->form['form'] as $key => $element) {
		 	if($key!=19){
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		 $this->form['data'] = $data;
		echo json_encode($this->form);

	}

	public function getDataBank($id)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_bank($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}
