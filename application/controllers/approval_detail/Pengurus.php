<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengurus extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pengurus';

	public $module = 'Pengurus';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_pengurus($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/pengurus/list', $data, FALSE);
			$this->load->view('approval/approval_detail/pengurus/list_js', $data, FALSE);
		
	}

	public function view($id = null)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_pengurus($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	public function viewVerifikasi($id,$id_vendor){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_pengurus($id,$id_vendor);
    	$data['id'] = base64_encode($id);
    	$data['id_vendor'] = $id_vendor;
    	$this->load->view('approval/approval_detail/pengurus/view', $data, FALSE);
		$this->load->view('approval/approval_detail/pengurus/view_js', $data, FALSE);
    }
	public function verifikasi($id,$id_vendor)
	{
		$id = base64_decode($id);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		            'rules' => 'required',
		        ),
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'No.Identitas (KTP/Passport/KITAS)',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Seumur Hidup'
	         	),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id_akta',
		            'type'	=>	'dropdown',
		            'label'	=>	'No.Akta Pengangkatan',
		            'source'=>	$this->gm->getnoakta($id_vendor)
	         	),
	         	array(
		            'field'	=> 	'pengurus_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (KTP/Passport/KITAS)',
		            'upload_path'=>base_url('assets/lampiran/pengurus_file/'),
					'upload_url'=>site_url('vendor/pengurus/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         ),
		);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pengurus($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);
	}
	public function commit_approve($id){
		$id = base64_decode($id);
		$data = $this->am->verifikasi_pengurus($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_pengurus','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}
