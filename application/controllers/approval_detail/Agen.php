<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Agen extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_agen';

	public $module = 'Agen';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		$this->form_produk = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'produk',
		            'type'	=>	'text',
		            'label'	=>	'Produk',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'merk',
		            'type'	=>	'text',
		            'label'	=>	'Merk',
		            'rules' => 	'required',
	         	),
	         ),
		);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pabrikan/Keagenan/Disitributor',
		     		'source'=>	array(
		     			'Pabrikan' => 'Pabrikan',
		     			'Agen Tunggal' => 'Agen Tunggal',
		     			'Distributor Tunggal' => 'Distributor Tunggal'
		     		)
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'agen_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/agen_file/'),
					'upload_url'=>site_url('vendor/agen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         ),
		);
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_agen($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/agen/list', $data, FALSE);
			$this->load->view('approval/approval_detail/agen/list_js', $data, FALSE);
		
		
	}

	public function view($id)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_agen($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	public function getProduk($id)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_product($id);
		$return = $this->tablegenerator->initialize($config);
		foreach ($return['data'] as $key => $value) {
			$return['data'][$key]->is_mandatory = $this->data_process->set_mandatory($value->data_status);
			$return['data'][$key]->is_false = $this->data_process->set_yes_no(0,$value->data_status);
			$return['data'][$key]->is_true = $this->data_process->set_yes_no(1,$value->data_status);
		}
		echo json_encode($return);
	}
	public function viewVerifikasi($id){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_agen($id);
    	$data['id'] = base64_encode($id);
    	$this->load->view('approval/approval_detail/agen/view', $data, FALSE);
		$this->load->view('approval/approval_detail/agen/view_js', $data, FALSE);
    }

    public function viewVerifikasiProduk($id){
    	$id = base64_decode($id);
    	$data = $this->am->verifikasi_agen_produk($id);
    	$data['id'] = base64_encode($id);
    	$this->load->view('approval/approval_detail/agen/view_agen', $data, FALSE);
		$this->load->view('approval/approval_detail/agen/view_agen_js', $data, FALSE);
    }

	public function verifikasi($id){
		$id = base64_decode($id);
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_agen($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 	// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	// }
		 }
		echo json_encode($this->form);

	}

	public function verifikasiProduk($id){
		$id = base64_decode($id);
		 $this->form = $this->form_produk;
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_agen_produk($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	}
		 }
		echo json_encode($this->form);

	}

	public function commit_approve($id){
		$id = base64_decode($id);
		$data = $this->am->verifikasi_agen($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_agen','id');
		foreach ($_POST['mandatoryProduct'] as $key => $value) {
			
			$_POST['mandatory'] = $value;
			$_POST['status'] = $_POST['statusProduct'][$key];
			$this->data_process->check($id['id_vendor'], $this->input->post(),$key,'ms_agen_produk','id');
		}
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}
