<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tdp extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_tdp';

	public $module = 'Tdp';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Dikeluarkan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'expire_date',
		            'type'	=>	'lifetimeDate',
		            'label'	=>	'Berlaku s/d',
		            'rules' => 	'required',
		            'sub_caption' => 'Selama Perusahaan Berdiri'
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Penerbit',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'tdp_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Scan Dokumen TDP',
		            'upload_path'=>base_url('assets/lampiran/tdp_file/'),
					'upload_url'=>site_url('vendor/tdp/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         )
		);
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_tdp($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/tdp/list', $data, FALSE);
			$this->load->view('approval/approval_detail/tdp/list_js', $data, FALSE);

	}

	public function viewVerifikasi($id){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_tdp($id);
    	$data['id'] = base64_encode($id);
    	$this->load->view('approval/approval_detail/tdp/view', $data, FALSE);
		$this->load->view('approval/approval_detail/tdp/view_js', $data, FALSE);
    }
	public function view($id = null)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_tdp($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function verifikasi($id){
		$id = base64_decode($id);
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_tdp($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 	// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	// }
		 }
		 echo json_encode($this->form);

	 }
	 public function commit_approve($id){
	 	$id = base64_decode($id);
		$data = $this->am->verifikasi_tdp($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_tdp','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}
