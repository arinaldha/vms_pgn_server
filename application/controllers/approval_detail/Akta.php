<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Akta extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Akta';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'rules' => 'required',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
		        array(
		            'field'	=> 	'issue_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'notaris',
		            'type'	=>	'text',
		            'label'	=>	'Notaris',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'akta_file',
		            'type'	=>	'file',
		            'label'	=>	'Akta File',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	),
	         	array(
		            'field'	=> 	'authorize_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_by',
		            'type'	=>	'text',
		            'label'	=>	'Lembaga Pengesahan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Ditetapkan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'authorize_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Scan Dokumen Penetapan',
		            'upload_path'=>base_url('assets/lampiran/authorize_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_akta($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/akta/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/akta/view_tab_js', $data, FALSE);
		
	}

	public function pendirian($id)
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_akta($id, 'pendirian');
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/akta/list', $data, FALSE);
			$this->load->view('approval/approval_detail/akta/list_js', $data, FALSE);	
	}

	public function perubahan($id, $type)
	{
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_akta($id, $type);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/akta/list_perubahan', $data, FALSE);
			$this->load->view('approval/approval_detail/akta/list_perubahan_js', $data, FALSE);	
	}

	public function view($id = null, $type)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_akta($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_akta($id);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/akta/view', $data, FALSE);
		$this->load->view('approval/approval_detail/akta/view_js', $data, FALSE);
    }

	public function verifikasi($id)
	{
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_akta($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id){
		$id = base64_decode($id);
		$data = $this->am->verifikasi_akta($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_akta','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	 public function aktaCek($id = null)
	 {
	 	$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_akta($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
	 }
}
