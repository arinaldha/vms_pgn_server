<?php defined('BASEPATH') OR exit('No direct script access allowed');
class K3 extends MY_Controller {

	public $form;

	public $modelAlias = 'km';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/k3_model','km');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'csms_file',
		            'type'	=>	'file',
		            'label'	=>	'Sertifikat CSMS',
		            'upload_path'=>base_url('assets/lampiran/csms_file/'),
					'upload_url'=>site_url('approval_detail/k3/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
		        ),
	         	array(
		            'field'	=> 	'expiry_date',
		            'type'	=>	'date',
		            'label'	=>	'Masa Berlaku',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'score',
		            'type'	=>	'decimal',
		            'label'	=>	'Skor',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'category',
		            'type'	=>	'text',
		            'label'	=>	'Kategori'
	         	),
	         	array(
		            'field'	=> 	'remark',
		            'type'	=>	'textarea',
		            'label'	=>	'Alasan'
	         	)
	         ),
		);
	}
	
	public function index($id, $process=false){
		$data = $this->km->get_k3_dataform($id);
		$data['id'] = $id;
		$this->load->view('approval/approval_detail/k3/list', $data, FALSE);
		$this->load->view('approval/approval_detail/k3/list_js', $data, FALSE);
	}

	public function viewForm($id){
	 $modelAlias = $this->modelAlias;
	 $data = $this->km->get_k3_dataform($id);
	 
	 foreach($this->form['form'] as $key => $element) {
	 	$this->form['form'][$key]['readonly'] = TRUE;
	 	$this->form['form'][$key]['value'] = $data[$element['field']];
	 }
	 echo json_encode($this->form);
	}

	public function update($id)
	{
	 $modelAlias = $this->modelAlias;
	 $data = $this->km->get_k3_dataform($id);
	 
	 foreach($this->form['form'] as $key => $element) {
	 	$this->form['form'][$key]['value'] = $data[$element['field']];
	 }
	 $this->form['url'] = site_url('approval_detail/k3/proses_edit/'.$id);
	 $this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
	 echo json_encode($this->form);
	}

	public function add($id)
	{
		$this->form['url'] = site_url('approval_detail/k3/proses_add/'.$id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function proses_add($id)
	{
		$data_vendor = $this->vm->get_data($id);
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			$save['category'] = $this->getCat($save['score']);
			if ($this->$modelAlias->save_csms_data($save,$id)) {
				if($save['score'] <= 55.00){

						$this->session->set_flashdata('msgSuccess','<p class="errorMsg">Vendor masuk ke dalam blacklist</p>');

						$data_blacklist['url'] 			  = current_url();
						$data_blacklist['id_vendor']	  = $id;
						$data_blacklist['name'] 		  = $data_vendor['name'];
						$data_blacklist['id_procurement'] = $data_vendor['id_procurement'];
						$data_blacklist['remark']		  = $save['remark'];
						$this->km->insert_blacklist($data_blacklist);
					}
				
				// $this->session->set_flashdata('msg', $this->successMessage);
				//echo json_encode(array('status'=>'success'));
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	function getCat($val)
	{
		if ($val >= '70.00') {
			$category = 'Tinggi';
		} else if($val >= '50.00' && $val < '70.00'){
			$category = 'Menengah';
		} else if($val >= '35.00' && $val < '50.00'){
			$category = 'Rendah';
		} else if($val < '35.00'){
			$category = 'Tidak Lulus';
		}
		return $category;
	}

	public function proses_edit($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$save['category'] = $this->getCat($save['score']);
			$lastData = $this->km->get_k3_dataform($id);
			if ($this->$modelAlias->edit_csms_data($save,$id)) {
				if($save['score'] <= 55.00){

					$this->session->set_flashdata('msgSuccess','<p class="errorMsg">Vendor masuk ke dalam blacklist</p>');

					$data_blacklist['url'] 			  = current_url();
					$data_blacklist['id_vendor']	  = $id;
					$data_blacklist['name'] 		  = $data_vendor['name'];
					$data_blacklist['id_procurement'] = $data_vendor['id_procurement'];
					$data_blacklist['remark']		  = $save['remark'];
					$this->km->insert_blacklist($data_blacklist);
				}
				
				// $this->session->set_flashdata('msg', $this->successMessage);
				echo json_encode(array('status'=>'success'));
				$this->deleteTemp($save,$lastData);
				return true;
			}
		}
	}
}
