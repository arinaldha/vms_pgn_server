<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pemilik extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pemilik';

	public $module = 'Pemilik';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_pemilik($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/pemilik/list', $data, FALSE);
			$this->load->view('approval/approval_detail/pemilik/list_js', $data, FALSE);

	}

	public function view($id = null)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_pemilik($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	 public function viewVerifikasi($id,$id_vendor){
	 	$id = base64_decode($id);
    	$data = $this->am->verifikasi_pemilik($id,$id_vendor);
    	$data['id'] = base64_encode($id);
    	$data['id_vendor'] = $id_vendor;
    	$this->load->view('approval/approval_detail/pemilik/view', $data, FALSE);
		$this->load->view('approval/approval_detail/pemilik/view_js', $data, FALSE);
    }
	public function verifikasi($id,$id_vendor){
		$id = base64_decode($id);
		// $id_vendor = base64_decode($id_vendor);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		            'rules' => 'required',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id_akta',
		            'type'	=>	'dropdown',
		            'label'	=>	'No.Akta Pengangkatan',
		            'source'=>	$this->gm->getnoakta($id_vendor)
	         	),
	         	array(
		            'field'	=> 	'percentage',
		            'type'	=>	'decimal',
		            'label'	=>	'Persentase',
		            'rules' => 	'required',
	         	),
	         ),
		);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_pemilik($id);
		 
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);
	 }
    public function commit_approve($id){
    	$id = base64_decode($id);
		$data = $this->am->verifikasi_pemilik($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_pemilik','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}
