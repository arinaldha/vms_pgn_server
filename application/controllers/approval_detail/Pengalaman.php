<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengalaman extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_pengalaman';

	public $module = 'Pengalaman';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');

		
	}
	
	public function index($id, $process=false){
			$id = base64_decode($id);
			$data['data'] = $this->am->get_data_pengalaman($id);
			$data['id'] = base64_encode($id);
			$this->load->view('approval/approval_detail/pengalaman/list', $data, FALSE);
			$this->load->view('approval/approval_detail/pengalaman/list_js', $data, FALSE);
	}

	public function view($id)
	{
		$id = base64_decode($id);
		$config['query'] = $this->am->get_data_pengalaman($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id){
		$id = base64_decode($id);
    	$data = $this->am->verifikasi_pengalaman($id);
    	$data['id'] = base64_encode($id);
    	$this->load->view('approval/approval_detail/pengalaman/view', $data, FALSE);
		$this->load->view('approval/approval_detail/pengalaman/view_js', $data, FALSE);
    }

	public function verifikasi($id){
		$id = base64_decode($id);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->verifikasi_pengalaman($id);

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'id_iu_bsb',
		            'type'	=> 'dropdown',
		            'label'	=> 'Bidang/Sub-Bidang Pekerjaan',
		            'rules' => 'required',
		            'source'=> $this->gm->getBsbIu($data['id_vendor'])
		        ),
		        array(
		            'field'	=> 	'job_name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Kerjaan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'job_location',
		            'type'	=>	'text',
		            'label'	=>	'Lokasi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'job_giver',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pemberi Kerja',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'phone_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Telp',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_no',
		            'type'	=>	'text',
		            'label'	=>	'No.Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_start',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Mulai Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'contract_end',
		            'type'	=>	'date',
		            'label' =>  'Tanggal Berakhir Kontrak',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'price_idr',
		            'type'	=>	'money',
		            'label'	=>	'Nilai Kontrak (Rp)',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	array('currency','price_foreign'),
		            'type'	=>	'money_asing',
		            'label'	=>	'Nilai Kontrak Kurs',
		            'source' =>  $this->gm->getKurs(),
		            'caption'=> '*Diisi Bila Ada'
	         	 ),
	         	array(
		            'field'	=> 	'contract_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen Kontrak)',
		            'upload_path'=>base_url('assets/lampiran/contract_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '*Format Data Harus PDF|JPEG|JPG|PNG|GIF|RAR|ZIP|DOC|DOCX'
	         	),
	         	array(
		            'field'	=> 	'bast_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Selesai Sesuai BAST',
		            'rules' =>	'required'
	         	),array(
		            'field'	=> 	'bast_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran (Dokumen BAST)',
		            'upload_path'=>base_url('assets/lampiran/bast_file/'),
					'upload_url'=>site_url('vendor/pengalaman/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '*Format Data Harus PDF|JPEG|JPG|PNG|GIF|RAR|ZIP|DOC|DOCX'
	         	),
	       
			),
		);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if( $element['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $data[$element['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($data[$element['field'][1]]);
            }  
		 }
		echo json_encode($this->form);

	 }
	 public function commit_approve($id){
	 	$id = base64_decode($id);
		$data = $this->am->verifikasi_pengalaman($id);
		$result = $this->data_process->check($id['id_vendor'], $this->input->post(),$data['id'],'ms_pengalaman','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}
