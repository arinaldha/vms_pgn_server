<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Auction_progress extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');
		$this->load->model('pre_auction_model','pam');
	}

	function checker($id_lelang = ''){
		$return = $this->pre_auction_model->get_status($id_lelang);
		die(json_encode($return));
	}
	
	function server_time(){
		$now = new DateTime(); 
		echo $now->format("M j, Y H:i:s O")."\n"; 
	}	
	
	function start($id_lelang){
		$return = $this->pre_auction_model->start_auction($id_lelang);
		die(json_encode(array('status' => 'success', 'time' => $return)));
	}

	public function index($id_lelang)
	{
		$data['id_lelang'] = $id_lelang;
		
		$data['fill'] = $fill = $this->pam->select_data($id_lelang);
		$data['barang'] = $this->pam->get_barang($id_lelang); 
				
		if($fill['metode_auction'] == "reverse_auction") $data['symbol'] = '<'; else $data['symbol'] = '>';
		// if($fill['auction_jenis']=='rfi'||$fill['auction_jenis']=='rfi_pembelian'){
		$is_total = ($data['fill']['kriteria_pemenang']=='lump_sum');
		$data['penawaran']	= $this->pam->get_penawaran($id_lelang,$id_vendor, false);
		// } 
		if($fill['auction_type'] == "forward_auction"){ $data['limit'] = "tertinggi"; $data['persentase'] = "Kenaikan"; }
		if($fill['auction_type'] == "reverse_auction"){ $data['limit'] = "terendah"; $data['persentase'] = "Penurunan"; }
		date_default_timezone_set("Asia/Jakarta");
		if(strtotime(date('Y-m-d H:i:s'))>strtotime($fill['time_limit'])&&$data['fill']['is_started']){
			$this->end_auction($id_lelang, true);
		}
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction_tabs/auction_progress/index/'.$id_lelang),
			'title' => 'Auction'
		));
		$this->header = 'Auction';
		$this->content = $this->load->view('auction/admin/view_master',$data, TRUE);
		$this->script = $this->load->view('auction/admin/view_master_js', $data, TRUE);
		parent::index();
	}

	public function view_data_form($id_lelang)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Paket Auction'
	         	),
	         	array(
		            'field'	=> 	'metode_penawaran',
		            'type'	=>	'radioList',
		            'label'	=>	'Metode Penawaran'
	         	),
	         	array(
		            'field'	=> 	'kriteria_pemenang',
		            'type'	=>	'text',
		            'label'	=>	'Kriteria Pemenang'
	         	),
	         	array(
		            'field'	=> 	'auction_duration',
		            'type'	=>	'text',
		            'label'	=>	'Durasi Lelang'
	         	),
	         	array(
		            'field'	=> 	'auction_type',
		            'type'	=>	'text',
		            'label'	=>	'Tipe Auction'
	         	),
	         	array(
		            'field'	=> 	'auction_jenis',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Auction'
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>	'hidden',
		            'rules' =>	'hidden'
	         	),
			),
		);
		$data = $this->pam->select_data($id_lelang);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'action') {
			// 	$this->form['form'][$key]['value'] = default_date($this->form['form'][$key]['field']['auction_duration'].' Menit');
			// } else {
			// 	$this->form['form'][$key]['value'] = default_date($this->form['form'][$key]['field']['start_time'].' - '.$this->form['form'][$key]['field']['time_limit']);
			// }
			// if ($this->form['form'][$key]['field']['auction_type'] == 'action') {
			// 	# code...
			// }
		}

		echo json_encode($this->form);
	}

	function end_auction($id_lelang = '', $status = false){

		$return = array();
		$fill = $this->pam->select_data($id_lelang);
		$query = $this->pam->get_barang($id_lelang);
		foreach($query->result() as $data){
			
			$hps = $this->pam->convert_to_idr($data->nilai_hps, $data->id_kurs, $id_lelang);
			if($fill['auction_jenis']=='rfi_penunjukan'||$fill['auction_jenis']=='auction'){
				if($fill['auction_type'] == 'reverse_auction'){
					$lowest = $this->pam->get_lowest_price($id_lelang, $data->id);
					if($hps < $lowest['nilai']){
						$return['status'] = 'fail';
						$return['message'] = 'Semua penawaran masih diatas HPS. Pengadaan akan kembali di lanjutkan untuk 10 menit';
					}
				}
				if($fill['auction_type'] == 'forward_auction'){					
					$highest = $this->pam->get_highest_price($id_lelang, $data->id);
					if($hps > $highest['nilai']){
						$return['status'] = 'fail';
						$return['message'] = 'Semua penawaran masih dibawah HPS. Pengadaan akan kembali di lanjutkan untuk 10 menit';
					}
				}
			}
		}
		
		if($return['status']=='fail' && $fill['is_fail_auction']==0){
			$this->pam->mark_as_extend($id_lelang);
		}else{
			echo $fill['is_fail_auction'];
			$this->pam->end_auction($id_lelang);
			$this->pam->generate_catalogue($id_lelang);
			$return['status'] = 'success';
		}
		if($status==false){
			die(json_encode($return));	
		}
		
	}

	function by_vendor($id_lelang = '',$id_vendor=''){
		$data['id_lelang'] = $id_lelang;
		
		$data['fill'] = $fill = $this->pam->select_data($id_lelang);
		$data['barang'] = $this->pam->get_barang($id_lelang); 
				
		if($fill['metode_auction'] == "reverse_auction") $data['symbol'] = '<'; else $data['symbol'] = '>';
		// if($fill['auction_jenis']=='rfi'||$fill['auction_jenis']=='rfi_pembelian'){
			$data['penawaran']	= $this->pam->get_penawaran($id_lelang,$id_vendor, true);
		// } 
		if($fill['auction_type'] == "forward_auction"){ $data['limit'] = "tertinggi"; $data['persentase'] = "Kenaikan"; }
		if($fill['auction_type'] == "reverse_auction"){ $data['limit'] = "terendah"; $data['persentase'] = "Penurunan"; }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auction_tabs/auction_progress/index/'.$id_lelang),
			'title' => 'Auction'
		));
		$this->header = 'Auction';
		$this->content = $this->load->view('auction/admin/by_vendor',$data, TRUE);
		$this->script = $this->load->view('auction/admin/by_vendor_js', $data, TRUE);
		parent::index();
	}

	function extend_auction($id_lelang = ''){
		$time = $this->pam->extend_lelang($id_lelang);
		die(json_encode(array('status' => 'success', 'time' => $time)));
	}
}