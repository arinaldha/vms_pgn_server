<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Peserta extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');

		$this->deleteUrl = 'auction_tabs/peserta/delete/';

		$this->form = array(
			'filter' => array(
				array(
					'field' => 'name',
					'type'	=> 'text',
					'label' => 'Nama Vendor'
				)
			)
		);
	}

	public function index($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit_peserta', $data, FALSE);
		$this->load->view('auction/tab/view_edit_peserta_js', $data, FALSE);
	}

	public function get_data_peserta($id)
	{
		$config['query'] = $this->am->get_data_procurement($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_data_tambah_peserta($id)
	{
		$config['query'] = $this->am->getPesertaDPT($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	public function add_peserta($id,$id_vendor){
		$list = $this->session->userdata('list_peserta');
		$list[$id][$id_vendor] = 1;
		$this->session->set_userdata('list_peserta',$list);
		if($this->session->userdata('list_peserta')[$id][$id_vendor]){
			echo json_encode(array('status'=>'success'));
		}

	}
	public function tambahDPT($id_procurement, $id){
    	if($this->am->tambahDPT($id_procurement, $id)){
    		// redirect('auction/editAuction/'.$id_procurement);
    		echo json_encode(array('status'=>'success'));
    	}else{
    		// echo '<script>alert("Gagal!"); window.location.href="'.site_url('auction/editAuction/'.$id_procurement).'"</script>';
    		echo json_encode(array('status'=>'fail'));
    	}
    }
	public function save($id)
	{
		if (isset($_POST['id'])) {
			foreach($_POST['id'] as $id_vendor) {
				$this->db->insert('ms_procurement_peserta',array('id_vendor'=>$id_vendor,'id_proc'=>$id,'entry_stamp' => date('Y-m-d H:i:s')));
			}
		}
	}

	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_peserta($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url($this->deleteUrl . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}