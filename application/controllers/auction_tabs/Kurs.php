<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Kurs extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pilih Metode Auction',
		            'source'=>	$this->am->getKurs(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'rate',
		            'type'	=>	'text',
		            'label'	=>	'Rate'
	         	)
			)
		);
		$this->insertUrl = site_url('auction_tabs/kurs/save');
		$this->deleteUrl = 'auction_tabs/kurs/delete/';
	}

	public function index($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit_kurs', $data, FALSE);
		$this->load->view('auction/tab/view_edit_kurs_js', $data, FALSE);
	}

	public function get_data_kurs($id)
	{
		$config['query'] = $this->am->get_procurement_kurs($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert($id)
	{
		$this->form['url'] = $this->insertUrl.'/'.$id;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_procurement'] = $id;
			$save['id_kurs'] 		= $save['id_kurs'];
			$save['rate'] 			= ($save['id_kurs']=='1') ? 1 : $save['rate'];
			$save['entry_stamp'] 	= date('Y-m-d H:i:s');
			$save['del'] = 0;
			if ($this->$modelAlias->insert_kurs($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete($id,'ms_procurement_kurs')) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}
}