<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Keterangan extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');
	}

	public function index($id)
	{
		$data['id'] = $id;
		$data['keterangan'] = $this->am->get_procurement_keterangan($id); 
		$this->load->view('auction/tab/view_edit_keterangan', $data, FALSE);
	}

	public function edit($id){
			$save = $this->input->post();
			$save['id'] = $id;
			$save['remark'] = $save['remark'];
			$save['edit_stamp'] = date('Y-m-d H:i:s'); 
			$process = $this->am->edit_keterangan($id,$save);
			
			if($process){
				echo '<script> window.location.href="'.site_url('auction/editAuction/'.$id).'"</script>';
			}
	}
}