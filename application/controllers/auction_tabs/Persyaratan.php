<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Persyaratan extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pilih Metode Auction',
		            'source'=>	$this->am->getKurs(),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'rate',
		            'type'	=>	'text',
		            'label'	=>	'Rate'
	         	)
			)
		);
		$this->insertUrl = site_url('auction_tabs/kurs/save');
		$this->deleteUrl = 'auction_tabs/kurs/delete/';
	}

	public function index($id)
	{
		$data['id'] = $id;
		$data['persyaratan'] = $this->am->get_procurement_persyaratan($id);
		// echo print_r($this->am->get_procurement_persyaratan($id));
		$this->load->view('auction/tab/view_edit_persyaratan', $data, FALSE);
		// $this->load->view('auction/tab/view_edit_kurs_js', $data, FALSE);
	}

	public function edit($id){
			$save = $this->input->post();
			$save['id_proc'] = $id;
			$save['description'] = $save['description'];
			$save['edit_stamp'] = date('Y-m-d H:i:s'); 
			$process = $this->am->edit_persyaratan($id,$save);
			
			if($process){
				echo '<script> window.location.href="'.site_url('auction/editAuction/'.$id).'"</script>';
			}
	}

	public function save($id){
			$save = $this->input->post();
			$save['id_proc'] = $id;
			$save['description'] = $save['description'];
			$save['entry_stamp'] = date('Y-m-d H:i:s'); 
			$process = $this->am->insert_persyaratan($save);
			
			if($process){
				echo '<script> window.location.href="'.site_url('auction/editAuction/'.$id).'"</script>';
			}
	}

	
	// public function save($id){
	// 	$save = $this->input->post();
		
	// 	$this->am->insert_persyaratan($save);
		
	// 	$json = array(
	// 		'status' => 'success',
	// 		'message' => 'Data persyaratan auction telah di simpan !'
	// 	);
	// 	die(json_encode($json));
	// }
	// public function get_data_kurs($id)
	// {
	// 	$config['query'] = $this->am->get_procurement_kurs($id);
	// 	$return = $this->tablegenerator->initialize($config);
	// 	echo json_encode($return);
	// }

	// public function insert($id)
	// {
	// 	$this->form['url'] = $this->insertUrl.'/'.$id;
	// 	$this->form['button'] = array(
	// 		array(
	// 			'type' => 'submit',
	// 			'label' => 'Simpan',
	// 		) ,
	// 		array(
	// 			'type' => 'cancel',
	// 			'label' => 'Batal'
	// 		)
	// 	);
	// 	echo json_encode($this->form);
	// }

	// public function save($id)
	// {
	// 	$modelAlias = $this->modelAlias;
	// 	if ($this->validation()) {
	// 		$save = $this->input->post();
	// 		$save['id_procurement'] = $id;
	// 		$save['id_kurs'] 		= $save['id_kurs'];
	// 		$save['rate'] 			= ($save['id_kurs']=='1') ? 1 : $save['rate'];
	// 		$save['entry_stamp'] 	= date('Y-m-d H:i:s');
	// 		if ($this->$modelAlias->insert_kurs($save)) {
	// 			$this->session->set_flashdata('msg', $this->successMessage);
	// 			$this->deleteTemp($save);
	// 			return true;
	// 		}
	// 	}
	// }

	// public function delete($id)
	// {
	// 	$modelAlias = $this->modelAlias;
	// 	if ($this->$modelAlias->delete($id,'ms_procurement_kurs')) {
	// 		$return['status'] = 'success';
	// 	}
	// 	else {
	// 		$return['status'] = 'error';
	// 	}

	// 	echo json_encode($return);
	// }
		// $this->load->view('auction/tab/view_edit_persyaratan', $data, FALSE);
		// $this->load->view('auction/tab/view_edit_persyaratan_js', $data, FALSE);
	// }

	public function view_edit_persyaratan($id)
	{
		$this->form = $this->form;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataTatacara($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if ($this->form['form'][$key]['value'] == 'lump_sump') {
				$this->form['form'][$key]['value'] = 'Total';
			}
			if ($this->form['form'][$key]['value'] == 'harga_satuan') {
				$this->form['form'][$key]['value'] = 'Itemize';
			}
		}

		echo json_encode($this->form);
	}
}