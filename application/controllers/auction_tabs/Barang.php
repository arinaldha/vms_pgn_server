<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Barang extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');		
		$this->insertUrl = site_url('auction_tabs/barang/save');
		$this->deleteUrl = 'auction_tabs/barang/delete/';
	}

	public function index($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit_barang', $data, FALSE);
		$this->load->view('auction/tab/view_edit_barang_js', $data, FALSE);
	}

	public function get_data_barang($id)
	{
		$config['query'] = $this->am->get_procurement_barang($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'nama_barang',
		            'type'	=>	'text',
		            'label'	=>	'Barang',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'source'=>	$this->am->get_procurement_kurs_barang(),
		            'label'	=>	'Mata Uang'
	         	),
	         	array(
		            'field'	=> 	'volume',
		            'type'	=>	'text',
		            'source'=>	$this->am->get_procurement_kurs_barang($id),
		            'label'	=>	'Mata Uang',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'volume',
		            'type'	=>	'number',
		            'label'	=>	'Volume',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'satuan',
		            'type'	=>	'text',
		            'label'	=>	'Satuan',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'nilai_hps',
		            'type'	=>	'text',
		            'label'	=>	'Nilai HPS'
	         	)
			)
		);
		$this->form['url'] = site_url('auction_tabs/barang/save/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	/*public function index($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit_barang', $data, FALSE);
		$this->load->view('auction/tab/view_edit_barang_js', $data, FALSE);
	}

	public function get_data_barang($id)
	{
		$config['query'] = $this->am->get_procurement_barang($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}*/

	/*public function insert($id)
	{
		$data_auction = $this->am->get_auction($id);
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'nama_barang',
		            'type'	=>	'text',
		            'label'	=>	'Barang',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'source'=>	$this->am->get_procurement_kurs_barang($id),
		            'label'	=>	'Mata Uang'
	         	),
	         	array(
		            'field'	=> 	'volume',
		            'type'	=>	'text',
		            'label'	=>	'Volume',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'satuan',
		            'type'	=>	'text',
		            'label'	=>	'Satuan',
		            'rules'	=>	'required'
	         	),
	         	array(
					'field' => 'nilai_hps',
					'label' => 'Nilai HPS',
					'type'	=> 'text'
				)
			)
		);
		$this->form['url'] = site_url('auction_tabs/barang/save/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}*/

	public function save($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'nama_barang',
		            'type'	=>	'text',
		            'label'	=>	'Barang',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'source'=>	$this->am->get_procurement_kurs_barang($id),
		            'label'	=>	'Mata Uang'
	         	),
	         	array(
		            'field'	=> 	'volume',
		            'type'	=>	'text',
		            'label'	=>	'Volume',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'satuan',
		            'type'	=>	'text',
		            'label'	=>	'Satuan',
		            'rules'	=>	'required'
	         	),
	         	array(
					'field' => 'nilai_hps',
					'label' => 'Nilai HPS',
					'type'	=> 'text'
				)
			)
		);
		$modelAlias = $this->modelAlias;
		$this->form_validation->set_rules($this->form['form']);
		// if ($this->validation()) {
			$save = $this->input->post();
			$save['id_procurement'] = $id;
			$save['nama_barang'] 	= $save['nama_barang'];
			$save['nilai_hps'] 		= preg_replace("/[,]/", "", $save['nilai_hps']);
			$save['id_kurs'] 		= $save['id_kurs'];
			$save['volume'] 		= $save['volume'];
			$save['satuan'] 		= $save['satuan'];
			$save['entry_stamp'] 	= date('Y-m-d H:i:s');
			if ($this->$modelAlias->insert_barang($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				echo json_encode(array('status'=>'success'));
				return true;
			}
		// }
	}

	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete_barang($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('auction_tabs/barang/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function edit($id_proc,$id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'nama_barang',
		            'type'	=>	'text',
		            'label'	=>	'Barang',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'id_kurs',
		            'type'	=>	'dropdown',
		            'source'=>	$this->am->get_procurement_kurs_barang($id_proc),
		            'label'	=>	'Mata Uang'
	         	),
	         	array(
		            'field'	=> 	'volume',
		            'type'	=>	'text',
		            'label'	=>	'Volume',
		            'rules'	=>	'required'
	         	),
	         	array(
		            'field'	=> 	'satuan',
		            'type'	=>	'text',
		            'label'	=>	'Satuan',
		            'rules'	=>	'required'
	         	),
	         	array(
					'field'=>'nilai_hps',
					'label'=>'Nilai HPS',
					'type'=>'text'
				)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataBarang($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}


		$this->form['url'] = site_url('auction_tabs/barang/update/' . $id_proc.'/'.$id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id_proc,$id)
	{
		$modelAlias = $this->modelAlias;
		// if ($this->validation()) {
			$save = $this->input->post();
			$save['id_procurement'] = $id_proc;
			$save['nama_barang'] 	= $save['nama_barang'];
		    $save['nilai_hps'] 		= preg_replace("/[,]/", "", $save['nilai_hps']);
			$save['id_kurs'] 		= $save['id_kurs'];
			$save['volume'] 		= $save['volume'];
			$save['satuan'] 		= $save['satuan'];
			$save['edit_stamp'] 	= date('Y-m-d H:i:s');
			$lastData = $this->$modelAlias->selectDataBarang($id);
			if ($this->$modelAlias->update_barang($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
				echo json_encode(array('status'=>'success'));
				return true;
			}
		// }
	}

	public function rfi_or_auction($id_proc)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'auction_jenis',
		            'type'	=>	'radioList',
		            'label'	=>	'Jenis Auction',
		            'source'=>	array('rfi' => 'RFI','rfi_pembelian'=>'RFQ-Pembelian','rfi_penunjukan'=>'RFI-Penunjukan','auction'=>'Auction'),
		            'rules' =>	'required'
	         	),
	         	array(
					'field'=>'id',
					'label'=>'ID Proc',
					'type'=>'text'
				)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataAuction($id_proc);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
		}
		echo json_encode($this->form);
	}

	public function cek_data_isAuction_or_rfiPenunjukkan($id)
	{
		$result = $this->am->get_data_isAuction_or_rfiPenunjukkan($id);
		if ($result['auction_jenis'] == 'auction' || $result['auction_jenis'] == 'rfi_penunjukkan') {
			return true;
		} else {
			return false;
		}
		echo json_encode($result);
	}

}