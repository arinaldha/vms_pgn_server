<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Tatacara extends MY_Controller
{
	public $modelAlias = 'am';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('auction_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'metode_auction',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Metode Auction',
		            'source'=>	array('posisi'=>'Posisi/Ranking','indikator'=>'Indikator'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'metode_penawaran',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Metode Penawaran',
		            'source'=>	array('lump_sum' => 'Total','harga_satuan'=>'Itemize'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'kriteria_pemenang',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Kriteria Pemenang',
		            'source'=>	array('lump_sum' => 'Total','harga_satuan'=>'Itemize'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>	'hidden',
		            'rules' =>	'hidden'
	         	),
			),
		);
		$this->insertUrl = site_url('auction_tabs/tatacara/save');
		$this->updateUrl = site_url('auction_tabs/tatacara/update');
	}

	public function index($id)
	{
		$data['id'] = $id;
		$this->load->view('auction/tab/view_edit_tatacara', $data, FALSE);
		$this->load->view('auction/tab/view_edit_tatacara_js', $data, FALSE);
	}

	public function viewEditTatacara($id)
	{
		$this->form = $this->form;
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataTatacara($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if ($this->form['form'][$key]['value'] == 'lump_sum') {
				$this->form['form'][$key]['value'] = 'Total';
			}
			if ($this->form['form'][$key]['value'] == 'harga_satuan') {
				$this->form['form'][$key]['value'] = 'Itemize';
			}
		}

		echo json_encode($this->form);
	}

	public function insert($id)
	{
		$this->form['url'] = $this->insertUrl.'/'.$id;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($id)
	{
		$auction = $this->am->get_auction_for_barang($id);

		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_procurement'] 	= $id;
			$save['metode_auction'] 	= $save['metode_auction'];
			$save['metode_penawaran'] 	= $save['metode_penawaran'];
			$save['kriteria_pemenang'] 	= ($save['metode_penawaran']=='lump_sum') ? 'lump_sum' : $save['kriteria_pemenang'];
			$save['edit_stamp'] 		= date('Y-m-d H:i:s');
			if ($save['metode_penawaran'] == 'lump_sum') {
				$data_procurement['id_procurement'] = $id;
				$data_procurement['nama_barang'] 	= $auction['name'];
				$data_procurement['volume'] 		= 1;
				$data_procurement['satuan'] 		= 'Lumpsum';
				$this->am->insert_barang($data_procurement);
			}
			if ($this->$modelAlias->insert_tatacara($save)) {
				$this->set_syarat($id);
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function edit($id)
	{
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'metode_auction',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Metode Auction',
		            'source'=>	array('posisi'=>'Posisi/Ranking','indikator'=>'Indikator'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'metode_penawaran',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Metode Penawaran',
		            'source'=>	array('lump_sum' => 'Total','harga_satuan'=>'Itemize'),
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'kriteria_pemenang',
		            'type'	=>	'radioList',
		            'label'	=>	'Pilih Kriteria Pemenang',
		            'source'=>	array('lump_sum' => 'Total','harga_satuan'=>'Itemize'),
		            'rules' =>	'required'
	         	)
			),
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataTatacara($id);

		foreach($this->form['form'] as $key => $element) {
			// $this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $data[$element['field']];
			// if ($this->form['form'][$key]['value'] == 'lump_sump') {
			// 	$this->form['form'][$key]['value'] = 'Total';
			// }
		}

		$this->form['url'] = $this->updateUrl.'/'.$id;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}


	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_procurement'] 	= $id;
			$save['metode_auction'] 	= $save['metode_auction'];
			$save['metode_penawaran'] 	= $save['metode_penawaran'];
			$save['kriteria_pemenang'] 	= ($save['metode_penawaran']=='lump_sum') ? 'lump_sum' : $save['kriteria_pemenang'];
			$save['edit_stamp'] 		= date('Y-m-d H:i:s');
			$lastData = $this->$modelAlias->selectDataTatacara($id);
			if ($this->$modelAlias->update_tatacara($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	function set_syarat($id_lelang = '', $is_edit = false){
		$master = $this->am->get_auction_tatacara($id_lelang);

		if($master['auction_type'] == "reverse_auction"){ $limit = "minimum"; $indicator = "rendah"; $reverse = "tinggi"; }
		else if($master['auction_type'] == "forward_auction"){ $limit = "maximum"; $indicator = "tinggi"; $reverse = "rendah"; }
		// echo $master['auction_duration'];
		$value = '
			<ol>
				<li>Harga penawaran sudah termasuk pajak-pajak.</li>
				<li>Durasi auction selama '.$master['auction_duration'].' menit, tanpa ada penambahan waktu.</li>
				<li>';
				if($master['metode_auction'] == "posisi")
					$value .= 'Metode auction menggunakan metode posisi/rangking, dimana para peserta e-auction hanya
					mengetahui posisi/rangking dari penawaran harga yang telah dimasukkan dibandingkan dengan
					penawaran harga peserta e-auction lainnya.';
					
				if($master['metode_auction'] == "indikator")
					$value .= 'Metode auction menggunakan metode indikator, dimana para peserta e-auction akan
					diberikan indikator terhadap penawaran harga yang telah dimasukan, dibandingkan dengan penawaran harga 
					peserta e-auction lainnya.';
		
		$value .= '</li>
				<li>Tidak ada batas harga penawaran '.$limit.' yang dapat dimasukkan.</li>
				<li>Harga penawaran yang dimasukkan tidak boleh sama atau lebih '.$reverse.' dari harga penawaran yang telah dimasukkan sebelumnya.</li>
				<li>';
		if($master['metode_auction'] == "posisi")
			$value .= 'Apabila terdapat penawaran harga yang sama, maka posisi atau ranking yang lebih baik akan diberikan kepada 
					penawar yang memasukkan harga tersebut terlebih dahulu'; 
		if($master['metode_auction'] == "indikator")
			$value .= 'Apabila terdapat penawaran harga yang sama, maka maka indikator lambang medali yang lebih baik akan diberikan kepada 
					penawar yang memasukkan harga tersebut terlebih dahulu'; 
		
		$value .= '</li>
				<li>Selama auction berlangsung, peserta tidak diperkenankan menggunakan tombol Back (backspace) dan Refresh (F5). </li>
				<li>Selama auction berlangsung, para peserta dilarang saling berkomunikasi dan dilarang menggunakan alat komunikasi apapun.</li>
				<li>Seluruh peserta auction wajib menjaga ketertiban.</li>
			</ol>';			 
				
			// echo $value;
			$param = array(
				'description'=>$value,
				'id_proc'=>$id_lelang,
				'entry_stamp'=>date("Y-m-d H:i:s")	
			);
		
		if($is_edit)
			$this->am->edit_persyaratan($param);
		else
			$this->am->insert_persyaratan($param);
	}


	// public function view_edit_peserta($id)
	// {
	// 	$data['id'] = $id;
	// 	$this->load->view('auction/tab/view_edit_peserta', $data, FALSE);
	// 	$this->load->view('auction/tab/view_edit_peserta_js', $data, FALSE);
	// }

	// public function get_data_peserta($id)
	// {
	// 	$config['query'] = $this->am->get_data_procurement();
	// 	$return = $this->tablegenerator->initialize($config);
	// 	echo json_encode($return);
	// }
}