<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Cron extends CI_Controller {
	public $vms_prod;
	public $betalokal;
	public function __construct(){
		parent::__construct();

		//$this->vms_prod = $this->load->database('vms_prod', TRUE);
		//$this->betalokal = $this->load->database('betalokal', TRUE);
		$this->load->model('vendor/admin/daftar_beku_model','dbm');
		$this->load->model('vendor/admin/blacklist_model','bm');
	}

	public function index(){
		$this->drop_dpt();

		$this->dbm->proses_beku();
		$this->bm->end_blacklist();
	}

	public function end_blacklist()
	{
		$this->bm->end_blacklist();
	}

	public function proses_beku()
	{
		$this->dbm->proses_beku();
		$this->bm->end_blacklist();
	}
	public function drop_dpt(){
		/*
			Query dari tr_email_blast untuk mengambil data email yang akan dikirim
		*/
		//error_reporting(E_ALL);

		$query = "SELECT * FROM tr_email_blast a JOIN ms_vendor b ON a.id_vendor = b.id WHERE DATE(`date`) = DATE('".date('Y-m-d')."') AND id_doc != 'ms_pengurus' AND vendor_status = 2";
		

		$query = $this->db->query($query)->result_array();

		foreach ($query as $key => $value) {
			if($value['id_doc']!=''){
				$queryDocument = "SELECT * FROM ".$value['doc_type']." WHERE id = ".$value['id_doc'];
				$queryDocument = $this->db->query($queryDocument);
				$_queryDocument = $queryDocument->row_array();
				$data_vendor = "SELECT *,c.name badan_usaha, a.name name  FROM ms_vendor a JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor JOIN tb_legal c ON b.id_legal = c.id JOIN ms_vendor_pic d ON a.id = d.id_vendor WHERE a.id = ?";
				$data_vendor = $this->db->query($data_vendor, array($_queryDocument['id_vendor']))->row_array();
				
				if($value['distance']=='0'){ 
					if($value['doc_type']=='ms_ijin_usaha'){
						foreach($queryDocument->result_array() as $keyDocument => $valueDocument){
							$selectTransaction = $this->db->where('id_vendor', $valueDocument['id_vendor'])->where('id_dpt_type', $valueDocument['id_dpt_type'])->where('end_date IS NULL')->get('tr_dpt');

							$queryTransaction = $this->db->where('id_vendor', $valueDocument['id_vendor'])->where('id_dpt_type', $valueDocument['id_dpt_type'])->where('end_date IS NULL')->update('tr_dpt', array('status'=>2,'end_date'=>date('Y-m-d H:i:s')));

						

							if($this->db->affected_rows()>0 || $selectTransaction->num_rows()==0){
								$this->db->where('id_vendor' , $valueDocument['id_vendor'])->where('id_dpt_type',$valueDocument['id_dpt_type'])->update('tr_dpt',array(
										'end_date'=> date('Y-m-d H:i:s'), 
										'status'=>2
									)
								);
								$this->db->insert('tr_dpt',array(
									'id_vendor' => $valueDocument['id_vendor'], 
									'id_dpt_type'=> $valueDocument['id_dpt_type'],
									'status'=>0,
									'entry_stamp'=>date('Y-m-d H:i:s')
								));
							}

							//Apakah masih ada DPT yang aktif?
							$queryDPTCheck = $this->db->where(
								array('id_vendor'=> $valueDocument['id_vendor'],
								'status'=> 1,
								'end_date'=>NULL,
								'start_date'=>NULL))->get('tr_dpt');

							//Turunkan dari DPT
							if($queryDPTCheck->num_rows()==0){
								$this->db->where('id', $valueDocument['id_vendor'])->update('ms_vendor', array('vendor_status'=> 1));
							}
							
						}
						
					}else{
						
						foreach($queryDocument->result_array() as $keyDocument => $valueDocument){
								
							$queryTransaction = $this->db->where('id_vendor', $valueDocument['id_vendor'])->where('end_date IS NULL')->update('tr_dpt', array('status'=>2,'end_date'=>date('Y-m-d H:i:s')));
							$selectDPT = $this->db->query("SELECT * FROM tr_dpt WHERE id_vendor = ? GROUP BY id_dpt_type", array($valueDocument['id_vendor']))->result_array();

						

							if($this->db->affected_rows()>0){
								foreach($selectDPT as $keys=>$values){
									$this->db->insert('tr_dpt',array(
										'id_vendor' => $valueDocument['id_vendor'], 
										'id_dpt_type'=> $values['id_dpt_type'],
										'status'=>0,
										'entry_stamp'=>date('Y-m-d H:i:s')
									));
								}
								
							}
							$this->db->where('id', $valueDocument['id_vendor'])->update('ms_vendor', array('vendor_status'=> 1));
							
						}
					}
				
				}
				$value['message'] = str_replace('\n','<br>',$value['message']);
				$message = "
					Kepada Yth. <br/>
					".$data_vendor['badan_usaha']." ".$data_vendor['name'].", <br/><br/>
					
					Bersama ini disampaikan data  perusahaan Saudara pada aplikasi 
					Vendor Management System PT Perusahaan Gas Negara Tbk yang perlu diperhatikan :

					<br/>
					".$value['message']."
					
					Vendor Management System PT Perusahaan Gas Negara Tbk
				";

				$this->email->from(EMAIL_HOSTNAME, 'VMS PGN');
				$this->email->to($data_vendor['vendor_email']); 
				$this->email->cc($data_vendor['pic_email']); 
				$this->email->bcc('muarifgustiar@gmail.com'); 
				$this->email->subject('Notifikasi Masa Berlaku Dokumen Vendor Management System PT Perusahaan Gas Negara Tbk');
				$this->email->message($message);	
				$this->email->send();
				
			}
		}
	}
	public function generate_email_blast($id_vendor){
		$this->db->where('id_vendor', $id_vendor)->delete('tr_email_blast');
		$list_dokumen = array(
			'ms_akta'=>'Akta',
			'ms_situ'=>'SITU/Domisili',
			'ms_tdp'=>'TDP',
			'ms_pengurus'=>'Pengurus',
			'ms_ijin_usaha'=>'Izin Usaha',
			'ms_agen'=>'Pabrikan/Keagenan/Distributor',
		);
		
		foreach ($list_dokumen as $key => $value) {
			
			$data_dokumen = $this->db->where('id_vendor', $id_vendor)
			// ->where('id', 305)
			->get($key)->result_array();
			// echo print_r($data_dokumen);
			// die();
			foreach ($data_dokumen as $key_dokumen => $value_dokumen) {

				if($key=='ms_akta'){
					if($value_dokumen['type']=='pendirian'){
						$subject = 'Akta Pendirian Perusahaan';
					}else{
						$subject = 'Akta Perubahan Terakhir';
					}
					
				}
				if($key=='ms_ijin_usaha'){
					$list_ijin_usaha =	array(
										'siujk'=>'SIUJK',
										'sbu'=>'SBU',
										'siup'=>'SIUP',
										'ijin_lain'=>'Surat Izin Usaha Lainnya',
										'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
									);
					$subject = $list_ijin_usaha[$value_dokumen['type']];
				}

				$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
				$nama_orang = array('ms_pengurus','ms_pemilik');	
				if(in_array($key, $dokumen)){
					$subject_name=	'Nomor Dokumen';
				}
				if(in_array($key, $nama_orang)){
					$subject_name=	'Nama';
				}
				if($key=='ms_pengalaman'){
					$subject_name='Nama Pekerjaan';
				}


				if( $key=='ms_pengurus'){
					$this->set_email_blast($value_dokumen['id'], $key, $value, $value_dokumen['position_expire'], $value_dokumen, $subject_name);
				}else{
					$this->set_email_blast($value_dokumen['id'], $key, $value, $value_dokumen['expire_date'], $value_dokumen, $subject_name);
				}
				
			}
			//break;
		}
		
	}
	function checkmydate($date) {
	  	$tempDate = explode('-', $date);
	  	return checkdate($tempDate[1], $tempDate[2], $tempDate[0]);
	}
	public function set_email_blast($id_doc,$doc_type,$name_file,$expire_date, $document, $subject_name){

		
		$this->db->where('id_doc', $id_doc)->where('doc_type', $doc_type)->delete('tr_email_blast');

		if($expire_date != 'lifetime'&& $expire_date != ''&& $this->checkmydate($expire_date)){
			$array[30]['date'] = date('Y-m-d',strtotime($expire_date.' -30 days'));
			for($i = 7; $i>=0;$i--){
				$array[$i]['date'] = date('Y-m-d',strtotime($expire_date.' -'.$i.' days'));
			}

			$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
			$nama_orang = array('ms_pengurus','ms_pemilik');	
			if(in_array($doc_type, $dokumen)){
				$subject_field=	'no';
			}
			if(in_array($doc_type, $nama_orang)){
				$subject_field=	'name';
			}
			if($doc_type=='ms_pengalaman'){
				$subject_field='job_name';
			}
			$no = $document[$subject_field];
			
			foreach($array as $key=>$val){
				

				// if(strtotime($val['date']) >= strtotime(date('Y-m-d'))){
					$data_array = array(
							'id_doc'=>$id_doc,
							'doc_type'=>$doc_type,
							'distance'=>$key,
							'date'=>$val['date'],
							'message'=>$this->set_message($subject_name, $no,$name_file,$key),
							'id_vendor'=>$document['id_vendor']
						);
					$result = $this->db->insert('tr_email_blast',$data_array);
				// }
				
			}
		}
	}
	public function set_expire(){
		$query = $this->db->query('SELECT * FROM ms_vendor WHERE vendor_status > 0');
		foreach ($query->result_array() as $key =>$value) {
			$this->generate_email_blast($value['id']);
		}
	}

	public function set_message($subject, $no,$name_file,$distance){
		$txt = '';
		if($distance==0){
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' sudah habis masa berlakunya.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}else if($distance==30){
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' menyisakan 30 hari sebelum masa berlakunya habis.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}else{
			$txt .= 'Lampiran file '.$name_file.' dengan '.$subject.' '.$no.' menyisakan '.$distance.' hari sebelum masa berlakunya habis.\n Harap diperbaharui untuk segera kami proses menjadi syarat vendor.\nTerimakasih.';
		}
		return $txt;
	}

	public function set_email($email_original){
		switch (ENVIRONMENT)
		{
			case 'development':
				$email = 'muarifgustiar@gmail.com';
			break;

			case 'testing':
			case 'production':
				$email = $email_original;
			break;
		}
		return $email;
	}
	public function getDatas(){
		echo $this->input->get('username');
	}
	public function encrypt2sha(){
		foreach ($this->db->get('ms_login')->result_array() as $key => $value) {
			$this->db->where('id', $value['id'])->update('ms_login', array('password'=>do_hash($value['password_raw'],'sha1')));
		}

	}
	public function test_email(){
		error_reporting(E_ALL);
		$message = 'asd';
			// email($save['vendor_email'], $message, 'Authentikasi Login VMS PT Perusahaan Gas Negara Tbk');
			$this->email->from('no-reply@pgn.co.id', 'VMS PGN');
			$this->email->to('muarifgustiar@gmail.com'); 


			$this->email->subject('Authentikasi Login PT Perusahaan Gas Negara Tbk Vendor Management System');
			
			$this->email->message($message);	
			$this->email->send();
		echo $this->email->print_debugger();
	}
	public function duplicate(){

		$data = $this->db->where('id_vendor >',7826)->get('ms_vendor_admistrasi');
		
		foreach($data->result_array() as $key=>$value){
			$check = $this->vms_prod->where('id_vendor',$value['id_vendor'])->get('ms_vendor_admistrasi');
			if($check->num_rows()>0){
				$this->vms_prod->where('id_vendor',$value['id_vendor'])->update('ms_vendor_admistrasi', $value);
			}else{
				$this->vms_prod->insert('ms_vendor_admistrasi', $value);
			}
		}
		// $data = $this->betalokal->get('ms_vendor_admistrasi');
		// foreach ($data->result_array() as $key => $value) {
		// 	$this->db->insert('ms_vendor_admistrasi', $value);
		// }
	}
}
