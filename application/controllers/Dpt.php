<?php defined('BASEPATH') OR exit('No direct script access allowed');
class DPT extends MY_Controller {

	public $form;


	public $module = 'DPT';

	public $isClientMenu = true;

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('vendor/izin_model','im');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'filter'=>array(
				array(
		            'field'	=> 	'a|name',
		            'type'	=>	'text',
		            'label'	=>	'Vendor'
	         	),
	         	array(
		            'field'	=> 	'id_legal',
		            'type'	=>	'dropdown',
		            'label'	=>	'Badan Usaha',
		            'source'=>	$this->gm->getdatalegal()
	         	),
	         	array(
		            'field'	=> 	'b|vendor_type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Status Perusahaan',
		            'source' => $this->gm->getTypeCompany()
	         	),
	         	array(
		            'field'	=> 	'id_dpt_type',
		            'type'	=>	'dropdown',
		            'label'	=>	'Tipe DPT',
		            'source'=>	$this->gm->getDptType()
	         	),
	         	array(
                    'field'    =>     'i|id_bidang',
                    'type'    =>    'dropdown',
                    'label'    =>    'Bidang / Sub Bidang',
                    'source' =>  $this->gm->getBsbDpt()
                 ),
	         	array(
		            'field'	=> 	'qualification',
		            'type'	=>	'dropdown',
		            'label'	=>	'Kualifikasi',
		            'source' => array(0 =>'Pilih Disini', 'kecil' => 'Kecil', 'non_kecil' => 'Non Kecil')
	         	),
	         	array(
		            'field'	=> 	'l|merk',
		            'type'	=>	'text',
		            'label'	=>	'Merk Produk'
//,'source'=>	$this->gm->getMerk()
	         	),
	         	array(
		            'field'	=> 	'f|job_name',
		            'type'	=>	'text',
		            'label'	=>	'Pengalaman'
	         	),
			array(
		            'field'	=> 	'b|vendor_province',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi / Domisili'
//,'source' => $this->gm->getProvince()
	         	),	         	
	         	array(
		            'field'	=> 	'g|start_date',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Pengangkatan'
	         	),
			),
		);
		$this->getData = $this->vm->getDataDpt($this->form);
		$this->insertUrl = site_url('dpt/save/');
		$this->updateUrl = 'dpt/update';
		$this->deleteUrl = 'dpt/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('dpt'),
			'title' => 'Daftar Penyedia Barang/Jasa Terdaftar'
		));
		$this->header = 'Daftar Penyedia Barang/Jasa Terdaftar';
		$this->content = $this->load->view('dpt/list',$data, TRUE);
		$this->script = $this->load->view('dpt/list_js', $data, TRUE);
		parent::index();
	}

	public function dpt_print($id)
	{
		$id = base64_decode($id);
		$data['administrasi']	= $this->vm->get_administrasi_list($id,TRUE);
		$data['surat_izin']		= $this->im->get_izin_report($id,TRUE);
		$data['situ']			= $this->im->get_situ_report($id,TRUE);
		$data['tdp']			= $this->im->get_tdp_report($id,TRUE);
		$data['keagenan']		= $this->im->get_keagenan_report($id,TRUE);
		$data['pengalaman']		= $this->im->get_pengalaman_report($id,TRUE);
		$data['klasifikasi']	= array(1=>'non-Konstruksi', 2=>'non-Konstruksi',3=>'non-Konstruksi',4=>'Konstruksi',5=>'Konstruksi');
		$this->load->view('dpt/view_print',$data,false);
	}

	// public function export_dpt()
	// {
	// 	$this->form = array(
	// 		'form'=>array(
	// 			array(
	// 				'field' => 'per',
	// 				'type'	=> 'dropdown',
	// 				'label' => 'Report Per',
	// 				'source'=> array(
	// 					'' => '--- Pilih ---',
	// 					'name' => 'vendor',
	// 					'npwp_code' => 'Kode NPWP',
	// 					'id_legal' => 'Badan Hukum'
	// 				)
	// 			)
	// 		)
	// 	);
	// 	$this->form['url'] = site_url('dpt/export');
	// 	$this->form['button'] = array(
	// 		array(
	// 			'type' => 'submit',
	// 			'label' => 'Export',
	// 		) ,
	// 		array(
	// 			'type' => 'cancel',
	// 			'label' => 'Batal'
	// 		)
	// 	);
	// 	echo json_encode($this->form);
	// }

	public function export()
	{
		$data = $this->vm->export_dpt();
		$table 	=	"<table cellpadding='0' cellspacing='0' border='1'>
						<tr>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>No.</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Nama Vendor</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>NPWP</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>NPPKP</td>
							<td style='background: #8db4e2; font-weight: bold; vertical-align: middle; text-align:center;'>Unit Organisasi</td>
						</tr>";
		$no = 1;
		foreach($data as $value){
			$table	.= "<tr>
							<td>".$no."</td>
							<td width='auto'>".$value['name']."</td>
							<td width='auto'>".$value['npwp_code']."</td>
							<td width='auto'>".$value['nppkp_code']."</td>
							<td width='auto'>".$value['sbu_name']."</td>
						</tr>";
			$no++;
		}
		$table .= "</table>";
		header('Content-type: application/ms-excel');

    	header('Content-Disposition: attachment; filename=Export'.date('YmdHis').'.xls');



		echo $table;

	}
}
