<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Vendor_auction_model extends MY_Model
{
	function get_data($id_lelang = ''){
		$sql = "SELECT a.*,
					   (SELECT GROUP_CONCAT(symbol) FROM tb_kurs WHERE id IN (SELECT id_kurs FROM ms_procurement_kurs WHERE id_procurement = a.id)) AS rate, 
					   b.metode_auction,
					   b.metode_penawaran,
					   b.kriteria_pemenang,
					   b.hps, 
					   b.metode_penawaran
					   
				FROM ms_procurement a 
				LEFT JOIN ms_procurement_tatacara b ON a.id = b.id_procurement 
				
				WHERE a.id = ?";
		
		$sql = $this->db->query($sql, array($id_lelang));
		return $sql->row_array();
	}

	function get_barang($id_lelang = ''){
		$sql = "SELECT a.*,
					   b.symbol,
					   b.name AS kurs 
					   
					   FROM ms_procurement_barang a 
					   LEFT JOIN tb_kurs b ON a.id_kurs = b.id 
					   
					   WHERE a.id_procurement = ?";
		return $this->db->query($sql, array($id_lelang));
	}

	function kurs_info($id_lelang = ""){
		$sql = "SELECT a.*, b.name FROM ms_procurement_kurs a LEFT JOIN tb_kurs b ON a.id_kurs = b. id WHERE a.id_procurement = ? AND a.id_kurs <> 1"; 
		return $this->db->query($sql, $id_lelang);
	}

	function get_penawaran($id_lelang = ''){
		$sql = "SELECT a.* FROM ms_penawaran a
				WHERE id_procurement = ? AND id_vendor = ?";
		return $this->db->query($sql, array($id_lelang, $this->session->userdata('user')['id_user']));
	}

	function get_syarat($id_lelang = ''){
		$sql = "SELECT description FROM ms_procurement_persyaratan WHERE id_proc = ?";
		$sql = $this->db->query($sql, $id_lelang);
		$sql = $sql->row_array();
		
		return $sql['description'];
	}

	function get_kurs($id_lelang = ''){
		$sql = "SELECT b.* 
				FROM tb_kurs b 
				LEFT JOIN ms_procurement_kurs a ON a.id_kurs = b.id 
				WHERE id_procurement = ?";
		
		return $this->db->query($sql, $id_lelang);
	}

	function check_last_value($id_lelang){

		$fill = $this->get_data($id_lelang);
		if($fill['auction_type']=='reverse_auction') {$ord = 'DESC'; $sort = "MIN";}
		if($fill['auction_type']=='forward_auction') {$ord = 'ASC'; $sort = "MAX";}

		$query = "	SELECT id_barang, SUM(nilai) max

					FROM 
					(
						SELECT id_barang, ".$sort."(nilai) nilai
						FROM ms_penawaran a 
						WHERE a.id_procurement = ?
						AND 	a.id_vendor = ? 
						GROUP BY id_barang 
					) as penawaran";
		return $this->db->query($query,array($id_lelang,$this->session->userdata('user')['id_user']))->row_array()['max'];
	}

	function cek_hps($id_lelang = '', $id_barang = ''){
		$sql = "SELECT a.nilai_hps, 
					   a.id_kurs,
					   (SELECT rate FROM ms_procurement_kurs WHERE id_kurs = a.id_kurs AND id_procurement = a.id_procurement) as rate 
					   
					   FROM ms_procurement_barang a 
					   WHERE a.id_procurement = ?"; 
		
		if($id_barang) $sql .= " AND a.id = ? ";
		
		$sql = $this->db->query($sql, array($id_lelang, $id_barang));
		$sql = $sql->row_array();
		// echo $this->db->last_query();

		return $sql;
	}

	function convert_to_idr($nilai = '', $id_kurs = '', $id_lelang = ''){
		$sql = "SELECT * FROM ms_procurement_kurs WHERE id_kurs = ? AND id_procurement= ?";
		$sql = $this->db->query($sql, array($id_kurs, $id_lelang));
		$sql = $sql->row_array();
		// print_r($sql);
		if($sql['id_kurs'] == 1) $sql['rate'] = 1;
		
		return ($nilai * $sql['rate']);
	}

	function cek_percentage($id_lelang = '', $id_barang = '', $nilai = '', $id_before = ''){
		$percent = 0;
		$input = array($id_lelang, $id_barang, $this->session->userdata('user')['id_user']);
		$sql = "SELECT in_rate AS nilai, id_kurs FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ? ";
		
		if($id_before) {

			$sql .= " AND id < ? ";
			$input[] = $id_before;

		}

		$sql .= " ORDER BY id DESC LIMIT 0,1";
		
		$sql = $this->db->query($sql, $input);
		$sql = $sql->row_array();
		
		// if($sql['nilai'] / 100 > 0)
			$percent = number_format(($sql['nilai'] - $nilai) / ($sql['nilai'] / 100), 1);
		
		return $percent;
	}

	function cek_highest($id_lelang = '', $id_barang = ''){
		$sql = "SELECT MAX(in_rate) AS nilai FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ?";
		$sql = $this->db->query($sql, array($id_lelang, $id_barang, $this->session->userdata('user')['id_user']));
		$sql = $sql->row_array();
		
		return $sql;
	}

	function cek_lowest($id_lelang = '', $id_barang = ''){
		$sql = "SELECT MIN(in_rate) AS nilai FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ?";
		$sql = $this->db->query($sql, array($id_lelang, $id_barang, $this->session->userdata('user')['id_user']));
		$sql = $sql->row_array();
		
		return $sql;
	}

	function save_penawaran($param = array(),$return = 'param'){
		$sql = "INSERT INTO ms_penawaran (`id_procurement`,
										  `id_vendor`,
										  `id_barang`,
										  `id_kurs`,
										  `nilai`,
										  `nilai_satuan`,
										  `in_rate`,
										  `in_rate_satuan`,
										  `down_percent`,
										  `entry_stamp`
										  ) VALUES (?,?,?,?,?,?,?,?,?,?)";
		
		$this->db->query($sql, $param);
		
		if($return=='id') { return $this->db->insert_id(); } 
		
		return $param['nilai'];
		
	}

	function save_upload_penawaran($array){
		return $this->db->insert('ms_upload_penawaran',$array);
	}

	function get_data_barang($id = ''){
		$sql = "SELECT * FROM ms_procurement_barang WHERE id = ?";
		$sql = $this->db->query($sql, $id);
		$sql = $sql->row_array();
		
		return $sql;
	}
	function get_default_currency($id_barang = ''){
		$sql = "SELECT b.id, 
					   b.symbol 
					   
				FROM ms_procurement_barang a
				LEFT JOIN tb_kurs b ON b.id = a.id_kurs
				
				WHERE a.id = ?";
				
		$sql = $this->db->query($sql, array($id_barang));
		return $sql->row_array();
	}
	function get_user_currency($id_lelang = '', $position = '', $id_barang = ''){
		$sql = "SELECT a.id_kurs, 
					   b.symbol 
					   
				FROM ms_penawaran a
				-- LEFT JOIN ms_procurement_kurs c ON b.id = a.id_kurs
				LEFT JOIN tb_kurs b ON b.id = a.id_kurs
				
				WHERE a.id_procurement = ? AND a.id_vendor = ? AND a.id_barang = ? ORDER BY a.id ".$position." LIMIT 0,1";
				
		$sql = $this->db->query($sql, array($id_lelang,  $this->session->userdata('user')['id_user'], $id_barang));
		// echo $this->db->last_query();
		return $sql->row_array();
	}
}