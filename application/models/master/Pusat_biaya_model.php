<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pusat_biaya_model extends MY_Model{
	public $table = 'tb_budget_holder';
	function __construct(){
		parent::__construct();
	}
	function getPusatBiaya($form){
		$query = "	SELECT
						id,
						code,
              			name
					FROM ".$this->table."
					WHERE del = 0";
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['code'].'-'.$value['name'];
	    }
	    return $data;
	}
	function getPusatBiayaByKontrak($id_procurement){
		$dataArr = $this->db->where('id', $id_procurement)->get('ms_procurement')->row_array();

		$query = "	SELECT
						id,
						code,
              			name
					FROM ".$this->table."
					WHERE del = 0 ";
		if($dataArr['budget_center']!=''){
			$query .= "AND id IN (".$dataArr['budget_center'].")";	
		}
		
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['code'].'-'.$value['name'];
	    }
	    return $data;
	}
	function getPengguna($form){
		$query = "	SELECT
						id,
						
              			name
					FROM ".$this->table."
					WHERE del = 0";
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['name'];
	    }
	    return $data;
	}

	function getData($form){

		$query = "	SELECT
							name,
							a.id

					FROM ".$this->table." a
					WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT
						code,
						name,
						division_head


					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
}
