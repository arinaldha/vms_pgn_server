<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Bidang_model extends MY_Model{
	public $table = 'tb_bidang';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.name name, 
    					b.name dpt_type,
                        a.del del,
    					a.id id
    				FROM
    					tb_bidang a
    				INNER JOIN 
    					tb_dpt_type b ON b.id=a.id_dpt_type";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function getSubBidang($form){
		$query = "	SELECT 
    					a.name name, 
    					b.name id_bidang,
    					a.id
    				FROM
    					tb_sub_bidang a 
    				INNER JOIN 
    					tb_bidang b ON b.id=a.id_bidang";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    					a.*

                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id=?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function delete($id) { 
        $del = array('del' => 1);
        $this->db->where('id',$id)
                 ->update('tb_bidang',$del);
                 
        $query = $this->db->where('id_bidang',$id)
                          ->update('tb_sub_bidang',$del);

        return $query;
    }

    function active($id) { 
        $del = array('del' => 0);
        $this->db->where('id',$id)
                 ->update('tb_bidang',$del);
                 
        $query = $this->db->where('id_bidang',$id)
                          ->update('tb_sub_bidang',$del);

        return $query;
    }
}
