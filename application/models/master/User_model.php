<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends MY_Model{
	public $table = 'ms_admin';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					b.name role_name,
    				    a.name name,
    				    a.email email, 
    				    c.password_raw password_raw, 
    				    a.id id
					FROM
    					ms_admin a
        			LEFT JOIN
    					tb_role b ON b.id = a.id_role
        			LEFT JOIN
    					ms_login c ON c.id_user = a.id AND c.type = 'admin'";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                        a.*,
    				    a.name name,
    				    a.email email,
    				    b.name role_name, 
    				    c.password_raw password_raw,
                        a.status,
                        a.nipg
					FROM
    					ms_admin a
        			LEFT JOIN
    					tb_role b ON b.id = a.id_role
        			LEFT JOIN
    			        ms_login c ON c.id_user = a.id AND c.type = 'admin'
                    WHERE 
                        a.id = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
    function insert($save) {
       

        if ($save['status'] == 'organik') {
             $unparsed_json = file_get_contents('http://eabsen.pgn.co.id:8080/pgn_absen/api/authenticate/'.$save['username'].'?pass=123');
        $json_object = json_decode($unparsed_json);
		//echo var_dump($json_object);
            if($json_object->count==1){
            $json_object = $json_object->{0};
            // echo $json_object->userprincipalname->{0};die;
            $txt = $json_object->userprincipalname->{0};
            $get_name = explode("@",$txt);
            $username = strtolower($get_name[0]);
                $_save['entry_stamp'] = timestamp();
                $_save['id_role']     = $save['id_role'];
                $_save['id_sbu']      = $save['id_sbu'];
                $_save['name']        = $json_object->displayname->{0};
                $_save['email']       = $json_object->userprincipalname->{0};
                $_save['username']    = $username;
                $_save['status']      = $save['status'];
                $_save['nipg']       = $json_object->employeeid->{0};
                $_save['del']         = 0;

                $this->db->insert('ms_admin',$_save);

                $save_login['id_user']      = $this->db->insert_id();
                $save_login['type']         = 'admin';
                $save_login['username']     = $json_object->userprincipalname->{0};
                $save_login['entry_stamp']  = timestamp();
                $save_login['del']          = 0;

                return $this->db->insert('ms_login',$save_login);
            }

        } else {
		unset($save['username']);
                $save['entry_stamp'] = timestamp();
                $save['id_role']     = $save['id_role'];
                $save['id_sbu']      = $save['id_sbu'];
                $save['name']        = $save['name'];
                $save['email']       = $save['email'];
                // $save['username']    = $username;
                $save['status']      = $save['status'];
                $save['del']         = 0;

            $this->db->insert('ms_admin',$save);

                $save_login['id_user']      = $this->db->insert_id();
                $save_login['type']         = 'admin';
                $save_login['username']     = $save['name'];
                $save_login['password']     = do_hash($save['password'],'sha1');
                $save_login['password_raw'] = $save['password'];
                $save_login['entry_stamp']  = timestamp();
                $save_login['del']          = 0; 
                return $this->db->insert('ms_login',$save_login);
            }
    }

    function update($id, $save)
    { $unparsed_json = file_get_contents('http://eabsen.pgn.co.id:8080/pgn_absen/api/authenticate/'.$save['username'].'/123');
        $json_object = json_decode($unparsed_json);

        if ($save['status'] == 'organik') {
            if($json_object->count==1){
            $json_object = $json_object->{0};
            // echo $json_object->userprincipalname->{0};die;
            $txt = $json_object->userprincipalname->{0};
            $get_name = explode("@",$txt);
            $username = strtolower($get_name[0]);
                $save['edit_stamp'] = timestamp();
                $save['id_role']     = $save['id_role'];
                $save['id_sbu']      = $save['id_sbu'];
                $save['name']        = $json_object->displayname->{0};
                $save['email']       = $json_object->userprincipalname->{0};
                $save['username']    = $username;
                $save['status']      = $save['status'];
                $save['del']         = 0;

                $this->db->where('id',$id)
                         ->update('ms_admin',$save);

                $save_login['type']         = 'admin';
                $save_login['username']     = $json_object->userprincipalname->{0};
                $save_login['password']     = do_hash(123,'sha1');
                $save_login['password_raw'] = 123;
                $save_login['edit_stamp']  = timestamp();
                $save_login['del']          = 0;
                
                return $this->db->where('id_user',$id)
                                ->update('ms_login',$save_login);
            }

            } else {
		unset($save['username']);
                $save['edit_stamp'] = timestamp();
                $save['id_role']     = $save['id_role'];
                $save['id_sbu']      = $save['id_sbu'];
                $save['name']        = $save['name'];
                $save['email']       = $save['email'];
                // $save['username']    = $username;
                $save['status']      = $save['status'];
                $save['del']         = 0;

                $this->db->where('id',$id)
                         ->update('ms_admin',$save);

                $save_login['type']         = 'admin';
                $save_login['username']     = $save['name'];
                $save_login['password']     = do_hash($save['password'],'sha1');
                $save_login['password_raw'] = $save['password'];
                $save_login['edit_stamp']  = timestamp();
                $save_login['del']          = 0; 
                return $this->db->where('id_user',$id)
                                ->update('ms_login',$save_login);
            }
        
    }

    function delete($id) {
        $this->db->where('id',$id)
                 ->delete('ms_admin');          
        $final = $this->db->where('id_user',$id)
                          ->delete('ms_login');
        return $final;
    }
}