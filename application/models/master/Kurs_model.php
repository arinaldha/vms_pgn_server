<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Kurs_model extends MY_Model{
	public $table = 'tb_kurs';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.name,
    					a.symbol,
    					a.id

    				FROM ".$this->table." a WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					a.name,
    					a.symbol
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function update($id, $data){
        foreach ($data as $key => $value) {
            foreach ($value as $keys => $values) {
                    if($values=='') unset($value[$keys]);
                }
            if(is_array($value)){
                $data[$key] = implode(',',$value);
            }
        }

        $final = $this->db->
                   where('id', $id)->
                   update('tb_kurs', array(
                    'name'       => $data['name'],
                    'symbol'     => $data['symbol'],
                    'edit_stamp' => $data['edit_stamp']
                ));

        return $final;
    }

}
