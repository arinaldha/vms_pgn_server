<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sub_bidang_model extends MY_Model{
	public $table = 'tb_sub_bidang';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "    SELECT 
                        a.name name, 
                        b.name id_bidang,
                        a.id,
                        a.del
                    FROM
                        tb_sub_bidang a 
                    INNER JOIN 
                        tb_bidang b ON b.id=a.id_bidang";

        if($this->input->post('filter')){

            $query .= $this->filter($form, $this->input->post('filter'), false);

        }
        return $query;

	}

    function insert($data) {
        $a = $this->db->insert($this->table,array(
                'id_bidang'   => $data['id_bidang'],
                'name'        => $data['name'],
                'entry_stamp' => $data['entry_stamp']
        ));
        return $a;
    }

	function selectData($id){

		$query = "	SELECT 
    					a.name
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id=$id";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function delete($id) { 
        $del = array('del' => 1);
        // $this->db->where('id',$id)
        //          ->update('tb_bidang',$del);
                 
        $query = $this->db->where('id',$id)
                          ->update('tb_sub_bidang',$del);

        return $query;
    }

    function active($id) { 
        $del = array('del' => 0);
        // $this->db->where('id',$id)
        //          ->update('tb_bidang',$del);
                 
        $query = $this->db->where('id',$id)
                          ->update('tb_sub_bidang',$del);

        return $query;
    }
}
