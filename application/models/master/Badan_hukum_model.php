<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Badan_hukum_model extends MY_Model{
	public $table = 'tb_legal';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.name,
    					a.id
    				FROM 
                        ".$this->table." a 
                    WHERE 
                        a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    					a.name
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

}
