<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Progress_model extends MY_Model{
	public $table = 'tb_progress_pengadaan';
	function __construct(){
		parent::__construct();
	}
	function getElemenBiaya($form){
		$query = "	SELECT
						id,
						metode,
						orders,
              			value
					FROM ".$this->table."
					WHERE del = 0";
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['value'];
	    }
	    return $data;
	}

	function getData($form){

		$query = "	SELECT
							value,
							metode,
							a.id

					FROM ".$this->table." a
					WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getDataDetail($form){

		$query = "	SELECT
							orders,
							value,
							metode,
							a.id

					FROM ".$this->table." a
					WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT
						metode,
						value,
						orders


					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
	function getDataTahapan($id){
		$query = "SELECT orders, value, metode, id FROM tb_progress_pengadaan WHERE del = 0 AND metode = ".$id." ";
		if($this->input->post('filter')){
			$query .= $this->filter($this->input->post('filter'), false);
		}
		return $query;
	}
}
