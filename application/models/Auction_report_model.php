<?php
class Auction_report_model extends MY_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	function get_header($id_lelang = ''){
		// echo $id_lelang;
		$sql = "SELECT a.*,
					   b.metode_penawaran,
					   b.metode_auction,
					   b.kriteria_pemenang,
					   c.name budget_spender,
					   d.name AS nama_pejabat,
					   e.name budget_holder,
					   -- a.room AS nama_lokasi,
					   a.auction_type type_lelang
					   -- f.symbol AS kurs
					   
				FROM ms_procurement a 
				
				LEFT JOIN ms_procurement_tatacara b ON a.id = b.id_procurement 
				LEFT JOIN tb_budget_spender c ON a.budget_spender = c.id
				LEFT JOIN tb_pejabat_pengadaan d ON a.id_pejabat_pengadaan = d.id
				LEFT JOIN tb_budget_holder e ON a.budget_holder = e.id
				LEFT JOIN ms_area_sbu f ON a.id_lokasi = e.id
				-- LEFT JOIN ms_procurement_kurs ON ms_procurement_kurs.id_procurement =a.id
				-- LEFT JOIN tb_kurs ON ms_procurement_kurs.id_kurs =tb_kurs.id
				-- LEFT JOIN tb_kurs f ON b.id_kurs = f.id
				
				WHERE a.id = ? ";
		
		$sql = $this->db->query($sql, $id_lelang);
		return $sql->row_array();
	}
	
	function get_pengguna($id_lelang = ''){
		$sql = "SELECT b.name FROM ms_sbu_lelang a LEFT JOIN tb_sbu_lokasi b ON a.id_sbu = b.id WHERE a.id_lelang = ?";
		return $this->db->query($sql, $id_lelang);
	}
	
	function get_barang($id_lelang = ''){
		$sql = "SELECT * FROM ms_procurement_barang WHERE id_procurement = ?";
		return $sql = $this->db->query($sql, $id_lelang);
	}
	
	function get_kurs($id_lelang = ''){
		$sql = "SELECT b.id,b.name FROM ms_procurement_kurs a LEFT JOIN tb_kurs b ON a.id_kurs = b.id WHERE a.id_procurement = ?";
		return $sql = $this->db->query($sql, $id_lelang);
	}
	
	function get_peserta($id_lelang = ''){
		$sql = "SELECT a.*,
					   b.name,
					   d.name legal
					   
				FROM ms_procurement_peserta a
				LEFT JOIN ms_vendor b ON a.id_vendor = b.id 
				LEFT JOIN ms_vendor_admistrasi c ON a.id_vendor = c.id_vendor 
				LEFT JOIN tb_legal d ON c.id_legal = d.id 
				
				WHERE id_proc = ? AND a.del = 0";
		
		return $sql = $this->db->query($sql, $id_lelang);
	}
	
	function get_count_penawaran($id_barang = ""){
		$sql = "SELECT COUNT(id) FROM ms_penawaran WHERE id_barang = ? GROUP BY id_vendor";	
		$sql = $this->db->query($sql, $id_barang);
		$sql = $sql->num_rows();
		
		return $sql;
	}
	
	function get_vendor_ranking($id_lelang = '', $id_barang = '', $type_lelang = ''){
		$ord = '';
		$get_auction = $this->get_header($id_lelang);
		// echo print_r($get_auction);
		$type_lelang = $get_auction['auction_type'];
		if($type_lelang == "forward_auction"){ $sel = "MAX"; $ord = "DESC"; }
		else if($type_lelang == "reverse_auction"){ $sel = "MIN"; $ord = "ASC"; }
		
		if($get_auction['kriteria_pemenang']=='harga_satuan'){
			$sql = "SELECT a.id_vendor AS id_peserta, b.name AS nama_vendor,
						   (SELECT msp.id FROM ms_penawaran msp WHERE msp.id_vendor IS NOT NULL AND msp.id_vendor = a.id_vendor AND id_barang = ? ORDER BY in_rate ".$ord." LIMIT 0,1) AS id_penawaran 
						   
					FROM ms_procurement_peserta a
					LEFT JOIN ms_vendor b ON a.id_vendor = b.id 
					 
					
					WHERE a.id_proc = ? ORDER BY (SELECT ".$sel."(in_rate) FROM ms_penawaran WHERE id_vendor = a.id_vendor AND id_barang = ? LIMIT 0,1) ".$ord.", (SELECT id FROM ms_penawaran WHERE id_vendor = a.id_vendor AND id_barang = ? ORDER BY in_rate ".$ord." LIMIT 0,1) ASC";
			
			$query = $this->db->query($sql, array($id_barang, $id_lelang, $id_barang, $id_barang));
		}else{
			$sql = "SELECT id_vendor AS id_peserta, b.name nama_vendor, SUM( in_rate_total ) in_rate, SUM( nilai_total ) nilai
					FROM (
						SELECT a.id_vendor, ".$sel."( in_rate ) in_rate_total, ".$sel."( nilai ) nilai_total, a.id id_penawaran
						FROM ms_penawaran a
						LEFT JOIN ms_vendor b ON b.id = a.id_vendor
						WHERE id_procurement =  ".$id_lelang."
						GROUP BY id_vendor, id_barang
						ORDER BY in_rate_total ".$ord."
					)penawaran JOIN ms_vendor b ON b.id = penawaran.id_vendor 
					GROUP BY id_vendor
					ORDER BY in_rate ".$ord."
					";
			$query = $this->db->query($sql, array($id_lelang));
			// echo $this->db->last_query();
			// echo print_r($query->result_array());
		}
		return $query;
	}
	
	function get_penawaran($id = ''){
		$sql = "SELECT a.*,
					   b.nama_barang, 
					   c.symbol
					   
					   FROM ms_penawaran a 
					   LEFT JOIN ms_procurement_barang b ON a.id_barang = b.id 
					   LEFT JOIN tb_kurs c ON a.id_kurs = c.id 
					   
					   WHERE a.id = ?";
		
		$sql = $this->db->query($sql, array($id));	
		
		return $sql->row_array();
	}
	function get_penawaran_total($id_lelang, $id_vendor){

		$query = "	SELECT id_barang, SUM(nilai) max, SUM(in_rate) max_in_rate, symbol, id_kurs
					FROM 
					(
						SELECT MAX(a.id) , id_barang, nilai, in_rate, c.name symbol , id_kurs
						FROM ms_penawaran a 
						LEFT JOIN tb_kurs c ON a.id_kurs = c.id  
						WHERE a.id_procurement = ?
						AND 	a.id_vendor = ? 
						GROUP BY id_barang 
					) as penawaran";
		return $this->db->query($query,array($id_lelang,$id_vendor))->row_array();
	}
	function get_history($id_lelang = '', $id_vendor = ''){
		$arr = array($id_lelang);
		$sql = "SELECT a.*, 
					   b.nama_barang AS nama_barang,
					   c.name AS nama_vendor,
					   d.symbol
						
				FROM ms_penawaran a
				
				LEFT JOIN ms_procurement_barang b ON a.id_barang = b.id
				LEFT JOIN ms_vendor c ON a.id_vendor = c.id 
				LEFT JOIN tb_kurs d ON a.id_kurs = d.id 
				
				WHERE a.id_procurement = ?"; 
		
		if($id_vendor) {$sql .= " AND a.id_vendor = ?";$arr[]=$id_vendor;}
		
		$sql .= " ORDER BY a.id ASC";
		
		return $this->db->query($sql, $arr);
	}
}