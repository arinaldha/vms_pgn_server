<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Auction_model extends MY_Model
{
	public $table='ms_procurement';
	public function get_auction_list()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT  
					a.name name,  
					a.auction_date auction_date, 
					a.room lokasi_name,  
					a.proc_date, 
					a.id id,
					a.del del,
					a.is_finished
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 ";
		if($this->session->userdata('admin')['id_role']==7){
			$query .= " AND id_sbu = ".$admin['id_sbu']." ";
		}

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function get_auction_langsung()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT  
					a.name name,  
					a.auction_date auction_date, 
					b.name lokasi_name,  
					a.proc_date, 
					a.id id,
					a.del del,
					a.is_finished
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 AND a.is_started=1 AND a.is_finished=0 ";

      if($this->session->userdata('admin')['id_role']==7){
         $query .= " AND id_sbu = ".$admin['id_sbu']." ";
      }
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function get_auction_selesai()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT  
					a.name name,  
					a.auction_date auction_date, 
					b.name lokasi_name,  
					a.proc_date, 
					a.id id,
					a.del del,
					a.is_finished
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 AND a.is_finished=1 ";
       if($this->session->userdata('admin')['id_role']==7){
         $query .= " AND id_sbu = ".$admin['id_sbu']." ";
      }
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function get_auction($id)
	{
		$query = "SELECT  
					a.name name,  
					a.auction_date auction_date, 
					b.name lokasi_name,  
					a.proc_date, 
					a.id id,
					a.del del,
					a.auction_jenis
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  LEFT JOIN
				  	ms_procurement_peserta as mpp ON mpp.id_proc=a.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->result_array();
	}

	function check_auction_data($id_lelang){

		$query = "SELECT 1 FROM ms_procurement_barang WHERE id_procurement = ?";
   		$query = $this->db->query($query,array($id_lelang))->num_rows();
   		if(!$query){
   			
   			return false;
   		}

   		$query = "SELECT 1 FROM ms_procurement_kurs WHERE id_procurement = ?";
   		$query = $this->db->query($query,array($id_lelang))->num_rows();
   		if(!$query){
   			return false;
   		}


   		$query = "SELECT 1 FROM ms_procurement_persyaratan WHERE id_proc = ?";
   		$query = $this->db->query($query,array($id_lelang))->num_rows();
   		if(!$query){
   			return false;
   		}

   		$query = "SELECT 1 FROM ms_procurement_peserta WHERE id_proc = ?";
   		$query = $this->db->query($query,array($id_lelang))->num_rows();
   		if(!$query){
   			return false;
   		}

   		$query = "SELECT 1 FROM ms_procurement_tatacara WHERE id_procurement = ?";
   		$query = $this->db->query($query,array($id_lelang))->num_rows();
   		if(!$query){
   			return false;
   		}
   		
   		return true;
   	}

   	public function get_auction_vendor()
   	{
   		$user = $this->session->userdata('user');
		$query = 'SELECT  
					a.name name, 
					a.auction_date auction_date,
					c.name lokasi_name,  
					a.work_area work_area, 
					a.proc_date,
					a.id id,  
					a.del del,
					a.is_finished
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	ms_procurement_peserta b ON b.id_proc=a.id
				  LEFT JOIN
				  	tb_sbu_lokasi c ON c.id=a.id_lokasi
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme = 1 AND b.id_vendor = "'.$user['id_user'].'"';
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
   	}

   	function save_data($data){
   		$data_procurement = array(
   			'name' => $data['name'],
   			'budget_holder'=>$data['budget_holder'],
   			'budget_spender'=>$data['budget_spender'],
   			'entry_stamp'=>$data['entry_stamp'],
   			'budget_source'=>$data['budget_source'],
   			'id_pejabat_pengadaan'=>$data['id_pejabat_pengadaan'],
   			'auction_type'=>$data['auction_type'],
   			'work_area'=>$data['work_area'],
   			'room'=>$data['room'],
   			'id_sbu'=>$data['id_sbu'],
   			'auction_jenis'=>$data['auction_jenis'],
   			'id_lokasi'=>$data['id_lokasi'],
   			'id_mekanisme'=>$data['id_mekanisme'],
   			'auction_date'=>date('Y-m-d'),
   			'duration_type'=>$data['duration_type'],
   			'duration'=>$data['duration'],
   			'auction_duration'=>$data['auction_duration'],
   		);
   		$this->db->insert('ms_procurement',$data_procurement);
		$id = $this->db->insert_id();
		
		return $id;
   	}

   	public function get_pejabat()
   	{
   		$query = "	SELECT
						id,name
						FROM tb_pejabat_pengadaan WHERE del = 0";

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
   	}

   	public function get_lokasi()
   	{
   		$query = "	SELECT
						id,name
						FROM tb_sbu_lokasi WHERE del = 0";

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
   	}

   	public function get_budget_holder()
   	{
   		$query = "	SELECT
						id,name
						FROM tb_budget_holder";

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
   	}

   	public function get_budget_spender()
   	{
   		$query = "	SELECT
						id,name
						FROM tb_budget_spender";

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
   	}

   	public function selectDataAuction($id)
   	{
   		$query = "SELECT
   					a.*
				  FROM
				  	ms_procurement a
				  LEFT JOIN
				  	tb_pejabat_pengadaan c ON c.id=a.id_pejabat_pengadaan
				  LEFT JOIN
				  	tb_budget_holder d ON d.id=a.budget_holder
				  LEFT JOIN
				  	tb_budget_spender e ON e.id=a.budget_spender
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  LEFT JOIN
				  	ms_procurement_peserta as mpp ON mpp.id_proc=a.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 AND a.id=".$id;

		$query = $this->db->query($query, array($id));
		return $query->row_array();
   	}
      public function get_auction_for_barang($id)
      {
         $query = "SELECT
                  a.*
              FROM
               ms_procurement a
              
              WHERE 
               a.del = 0 AND a.id=".$id;

         $query = $this->db->query($query, array($id));
         return $query->row_array();
      }
   	public function update_auction($id,$data)
   	{
   		$this->db->where('id',$id);
   		return $this->db->update('ms_procurement',array(
   			'name' => $data['name'],
   			'budget_holder'=>$data['budget_holder'],
   			'budget_spender'=>$data['budget_spender'],
   			'edit_stamp'=>$data['entry_stamp'],
   			'budget_source'=>$data['budget_source'],
   			'id_pejabat_pengadaan'=>$data['id_pejabat_pengadaan'],
   			'auction_type'=>'reverse_auction',
   			'work_area'=>$data['work_area'],
   			'room'=>$data['room'],
   			'id_sbu'=>$data['id_sbu'],
   			'auction_jenis'=>$data['auction_jenis'],
   			'id_lokasi'=>$data['id_lokasi'],
   			'id_mekanisme'=>$data['id_mekanisme'],
   			'duration_type'=>$data['duration_type'],
   			'duration'=>$data['duration'],
   			'auction_duration'=>$data['auction_duration']
   		));
   	}

   	public function selectDataTatacara($id)
   	{
   		$query = "SELECT
   					a.*
				  FROM
				  	ms_procurement_tatacara a
				  WHERE 
				  	a.id_procurement=".$id;

   		$query = $this->db->query($query, array($id));
   		return $query->row_array();
   	}

   	public function insert_tatacara($data)
   	{
   		return $this->db->insert('ms_procurement_tatacara',$data);
   	}

   	public function update_tatacara($id,$data)
   	{
   		$this->db->where('id_procurement',$id);
   		return $this->db->update('ms_procurement_tatacara',$data);
   	}

   	public function get_data_procurement($id)
   	{
   		$query = "SELECT
					b.name name, 
					a.id 
   				  FROM 
   				  	ms_procurement_peserta a
   				  INNER JOIN
					ms_vendor b ON a.id_vendor=b.id
				  LEFT JOIN
				  	ms_vendor_admistrasi as mva ON mva.id_vendor=b.id
				  LEFT JOIN
				  	tb_legal c ON mva.id_legal=c.id
				  WHERE 
				  	a.del = 0 AND a.id_proc = ".$id;
   		if($this->input->post('filter')){

   			$query .= $this->filter($form, $this->input->post('filter'), false);

   		}
   		return $query;
   	}

   	public function get_dpt_list($id)
   	{
   		$query = "SELECT 
					ms_vendor.name name, 
					(SELECT AVG(point) FROM tr_assessment WHERE id_vendor = ms_vendor.id  ) npp ,tb_legal.name legal_name,
					ms_vendor.id as id
				  FROM
				  	ms_vendor
				  LEFT JOIN
				  	ms_vendor_admistrasi ON ms_vendor_admistrasi.id_vendor=ms_vendor.id
   				  LEFT JOIN
   				  	tr_assessment ON tr_assessment.id_vendor=ms_vendor.id
   				  LEFT JOIN
   				  	tb_legal ON ms_vendor_admistrasi.id_legal=tb_legal.id
   				  LEFT JOIN
   				  	tb_sbu ON tb_sbu.id=ms_vendor.id_sbu
   				  WHERE 
   				  	NOT EXISTS(
	   				  	SELECT 1 FROM tr_blacklist_nik c 
	   				  	JOIN ms_pengurus msp ON msp.no = c.nik 
	   				  	WHERE msp.id_vendor = ms_vendor.id 
	   				  	AND (c.del = 0 OR c.del IS NULL)
   				  	)AND NOT EXISTS(
   				  		SELECT 1 FROM tr_blacklist d 
   				  		WHERE d.id_vendor=ms_vendor.id 
   				  		AND (d.del = 0 OR d.del IS NULL)
   				  	) AND NOT EXISTS(
   				  		SELECT 1 FROM ms_procurement_peserta msp 
   				  		WHERE id_vendor = ms_vendor.id 
   				  		AND msp.id_proc = '.$id.' 
   				  		AND (msp.del = 0 OR msp.del IS NULL)
   				  	) AND ms_vendor.del =0";
   		if($this->input->post('filter')){

			   $query .= $this->filter($form, $this->input->post('filter'), false);

		    }
   		return $query; 
   	}

   	public function get_procurement_kurs($id)
   	{
   		$query = "SELECT 
   					tb_kurs.name,
   					tb_kurs.id id_kurs,
   					ms_procurement_kurs.id id
   				  FROM
   				  	ms_procurement_kurs
   				  INNER JOIN 
   				  	tb_kurs ON ms_procurement_kurs.id_kurs=tb_kurs.id
   				  WHERE 
   				  	ms_procurement_kurs.del=0 AND ms_procurement_kurs.id_procurement = ".$id;
   		return $query;
   	}

   	public function get_procurement_kurs_barang($id)
   	{
   		$query = "SELECT 
   					tb_kurs.name,
   					tb_kurs.id id_kurs,
   					ms_procurement_kurs.id id
   				  FROM
   				  	ms_procurement_kurs
   				  INNER JOIN 
   				  	tb_kurs ON ms_procurement_kurs.id_kurs=tb_kurs.id
   				  WHERE 
   				  	ms_procurement_kurs.del=0 AND ms_procurement_kurs.id_procurement = ".$id;

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
   	}

   	function getKurs() 
   	{
   		$query = "	SELECT
   						a.id,
   						a.symbol
   				    FROM 
   						tb_kurs a
   					WHERE 
   						a.del = 0";
   						
   		$query = $this->db->query($query)->result_array();
   		$data = array();
   		foreach ($query as $key => $value) {
   			$data[$value['id']] = $value['symbol'];
   		}
   		return $data;
   	}

	public function insert_kurs($data)
	{
		return $this->db->insert('ms_procurement_kurs',$data);
	}

	function delete($id,$table)
	{
   	$this->db->where('id',$id);
		return $this->db->delete($table);
   }

   	public function get_procurement_barang($id)
   	{
   		$query = "SELECT 
   					a.nama_barang,
   					c.name mata_uang,
   					a.volume,
   					a.satuan,
   					a.nilai_hps,
   					a.id
   				  FROM
   				  	ms_procurement_barang a
   				  LEFT JOIN
   				  	ms_procurement_kurs b ON b.id=a.id_kurs
				  LEFT JOIN
	tb_kurs c ON b.id_kurs=c.id	
   				  WHERE 
   				  	a.del=0 AND a.id_procurement = ".$id;
   		return $query;
   	}

   	public function delete_peserta($id)
   	{
   		$this->db->where('id',$id);
   		return $this->db->update('ms_procurement_peserta',array('del'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
   	}

   	public function insert_barang($data)
   	{
   		return $this->db->insert('ms_procurement_barang',$data);
   	}

   	public function insert_persyaratan($data)
   	{
   		$data_persyaratan = array('id_proc' => $data['id_proc'],'description'=>$data['description'],'entry_stamp'=>date('Y-m-d H:i:s'));
   		return $this->db->insert('ms_procurement_persyaratan',$data);
   	}

   	public function edit_persyaratan($id,$data)
   	{
   		$data_persyaratan = array('id_proc' => $data['id_proc'],'description'=>$data['description'],'entry_stamp'=>date('Y-m-d H:i:s')); 

   		return $this->db->where('id_proc',$id)
   						->update('ms_procurement_persyaratan',$data_persyaratan);
   	}

   	public function get_procurement_persyaratan($id)
   	{
   		$query = $this->db->where('id_proc',$id)->get('ms_procurement_persyaratan');
   		if ($query->num_rows() > 0) {
   			return $query->row();
   		} else {
   			return array();
   		}
   	}

   	public function edit_keterangan($id,$data)
   	{
   		$data_keterangan = array('id' => $data['id'],'remark' => $data['remark'],'entry_stamp'=>date('Y-m-d H:i:s')); 

   		return $this->db->where('id',$id)
   						->update('ms_procurement',$data_keterangan);
   	}

   	public function get_procurement_keterangan($id)
   	{
   		$query = $this->db->where('id',$id)
   						  ->get('ms_procurement');
   		if ($query->num_rows() > 0) {
   			return $query->row();
   		} else {
   			return array();
   		}
   	}

   	public function delete_barang($id)
   	{
   		return $this->db->where('id',$id)
   				 ->update('ms_procurement_barang',array('del'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
   	}

   	public function selectDataBarang($id)
   	{
   		$query = "SELECT 
   					a.*,
   					a.nama_barang,
   					a.id_kurs,
   					a.volume,
   					a.satuan,
   					b.name
   				  FROM
   				  	ms_procurement_barang a
   				  LEFT JOIN
   				  	tb_kurs b ON b.id=a.id_kurs
   				  WHERE 
   				  	a.id= ".$id;
   		$query = $this->db->query($query, array($id));
		   return $query->row_array();
   	}

   	public function update_barang($id,$data)
   	{
   		return $this->db->where('id',$id)
   						->update('ms_procurement_barang',$data);
   	}

   	public function duplikat($id = '',$jumlah=0)
   	{
   		// for($i=0;$i<$total;$i++){
			$sql = "SELECT * FROM ms_procurement WHERE id = ?";
			$sql = $this->db->query($sql, $id);
			$data = $sql->row_array();
      }

   	public function duplikat_data($id,$jumlah = 0)
   	{
		
   		for($i=0;$i<$jumlah;$i++){
			$sql = "SELECT * FROM ms_procurement WHERE id = ?";
			$sql = $this->db->query($sql, $id);
			$data = $sql->row_array();
			
			$param = array(
				'name'=>$data['name']." - ".($i + 2),
				'budget_source'=>$data['budget_source'],
				'id_pejabat_pengadaan'=>$data['id_pejabat_pengadaan'],
				'auction_type'=>$data['auction_type'],
				'work_area'=>$data['work_area'],
				'room'=>$data['room'],
				'auction_date'=>$data['auction_date'],
				'auction_jenis'=>$data['auction_jenis'],
				'id_lokasi'=>$data['id_lokasi'],
				'id_sbu'=>$data['id_sbu'],
				'auction_duration'=>$data['auction_duration'],
				'start_time'=>$data['start_time'],
				'time_limit'=>$data['time_limit'],
				'budget_holder'=>$data['budget_holder'],
				'budget_spender'=>$data['budget_spender'],
				'entry_stamp'=>date("Y-m-d H:i:s"),
				'id_mekanisme'=>$data['id_mekanisme'],
				'duration'=>$data['duration'],
			);
						
			$new_id = $this->save_data($param);
			
			/* SBU */
			$sql = "SELECT * FROM ms_sbu_lelang WHERE id_lelang = ?";
			$sql = $this->db->query($sql, $id);
			
			foreach($sql->result() as $data){
				$sql_1 = "INSERT INTO ms_sbu_lelang (`id_sbu`,`id_lelang`,`entry_stamp`) VALUES (?,?,?)";
				$this->db->query($sql_1, array($data->id_sbu, $new_id, date("Y-m-d H:i:s")));	
			}
			
			/* TATA CARA */
			$sql = "SELECT * FROM ms_procurement_tatacara WHERE id_procurement = ?";
			$sql = $this->db->query($sql, $id);
			$data = $sql->row_array();
			
			$sql = "INSERT INTO ms_procurement_tatacara (`id_procurement`,`metode_auction`,`metode_penawaran`,`kriteria_pemenang`,`entry_stamp`) VALUES (?,?,?,?,?)";
			$this->db->query($sql, array($new_id, $data['metode_auction'], $data['metode_penawaran'], $data['kriteria_pemenang'], date("Y-m-d H:i:s")));	
			// echo $this->db->last_query();
			
			/* BARANG */
			$sql = "SELECT * FROM ms_procurement_barang WHERE id_procurement = ?";
			$sql = $this->db->query($sql, $id);
			$data = $sql->row_array();
			
			$sql = "INSERT INTO ms_procurement_barang (`id_procurement`,`nama_barang`,`nilai_hps`,`id_kurs`,`volume`,`entry_stamp`) VALUES (?,?,?,?,?,?)";
			$this->db->query($sql, array($new_id, $data['nama_barang'],$data['nilai_hps'],$data['id_kurs'],$data['volume'], date("Y-m-d H:i:s")));


			/* PERSYARATAN */
			$sql = "SELECT * FROM ms_procurement_persyaratan WHERE id_proc = ?";
			$sql = $this->db->query($sql, $id);
			$data = $sql->row_array();
			
			$sql = "INSERT INTO ms_procurement_persyaratan (`id_proc`,`description`,`entry_stamp`) VALUES (?,?,?)";
			$this->db->query($sql, array($new_id, $data['description'], date("Y-m-d H:i:s")));
			
			/* PESERTA */
			$sql = "SELECT * FROM ms_procurement_peserta WHERE id_proc = ?";
			$sql = $this->db->query($sql, $id);
			
			foreach($sql->result() as $data){
				$sql_1 = "INSERT INTO ms_procurement_peserta (`id_vendor`,`id_proc`,`entry_stamp`) VALUES (?,?,?)";
				$this->db->query($sql_1, array($data->id_vendor, $new_id,date("Y-m-d H:i:s")));	
			}
			
			/* KURS */
			$sql = "SELECT * FROM ms_procurement_kurs WHERE id_procurement = ?";
			$sql = $this->db->query($sql, $id);
			
			foreach($sql->result() as $data){
				$sql_1 = "INSERT INTO ms_procurement_kurs (`id_procurement`,`id_kurs`,`rate`,`entry_stamp`) VALUES (?,?,?,?)";
				$this->db->query($sql_1, array($new_id, $data->id_kurs, $data->rate, date("Y-m-d H:i:s")));	
			}
		}
		return true;
   	}

   	public function delete_auction($id)
   	{
   		return $this->db->where('id',$id)
   						->update('ms_procurement',array('del'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
   	}

   	public function getPesertaDPT($id, $form){
		$peserta = "SELECT id_vendor FROM ms_procurement_peserta WHERE id_proc = ? AND del = 0";
		$peserta = $this->db->query($peserta, array($id))->result_array();

   		$id_peserta = array();

   		foreach($peserta as $key => $val){
   			$id_peserta[] = $val['id_vendor'];
   		}

   		if(count($id_peserta)>0){
            $peserta_id = " WHERE a.del = 0 AND ((a.id NOT IN ('".implode("','", $id_peserta)."')))";
        }else {
$peserta_id = " WHERE a.del = 0 ";
}

        $query = "SELECT    a.name name,

                            a.id id
                            FROM ms_vendor a
                            LEFT JOIN ms_vendor_admistrasi b ON b.id_vendor=a.id
                            ".$peserta_id."";

        
        // $query  .= "        ) 
                                    
        //                         )"; 
                                // OR is_vms = 0
                            // a.is_vms 
                            // c.point,
                            //  LEFT JOIN tr_nkk c ON c.id_vendor=a.id
                            // LEFT JOIN tb_nkk_category h ON h.id = c.category
//echo $this->db->last_query();die;
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		$query .=	"GROUP BY a.id
				";
		//echo $query.'Ini Query nya';die;
		return $query;
	}
   	function tambahDPT($id_procurement, $id){
		$query =  $this->db->insert(
			'ms_procurement_peserta',
				array(
					'id_vendor'=>$id,
					'id_surat'=>$this->input->post('id_surat'), 
					'id_proc'=>$id_procurement,
					'entry_stamp'=>timestamp()
				)
			);
		return $query;
	}
	function get_auction_tatacara($id){
   		$arr = $this->db->select('*,ms_procurement.name name, tb_sbu_lokasi.name nama_lokasi, tb_budget_holder.name budget_holder_name, tb_budget_spender.name budget_spender_name, tb_pejabat_pengadaan.name pejabat_pengadaan_name,tb_mekanisme.name mekanisme_name')
   		->where('ms_procurement.id',$id)
   		->join('ms_procurement_tatacara','ms_procurement_tatacara.id_procurement=ms_procurement.id','LEFT')
   		->join('tb_pejabat_pengadaan','tb_pejabat_pengadaan.id=ms_procurement.id_pejabat_pengadaan','LEFT')
		->join('tb_budget_holder','tb_budget_holder.id=ms_procurement.budget_holder','LEFT')
		->join('tb_budget_spender','tb_budget_spender.id=ms_procurement.budget_spender','LEFT')
		->join('tb_mekanisme','tb_mekanisme.id=ms_procurement.id_mekanisme','LEFT')
		->join('tb_sbu_lokasi','tb_sbu_lokasi.id=ms_procurement.id_lokasi','LEFT')
   		->get('ms_procurement')->row_array();
		
		return $arr;
   	}

   	
   	public function get_data_isAuction_or_rfiPenunjukkan($id)
   	{
   		$admin = $this->session->userdata('admin');
      
		$query = "SELECT  
					a.name name,  
					a.auction_date auction_date, 
					b.name lokasi_name,  
					a.proc_date, 
					a.id id,
					a.auction_jenis,
					a.del del
				  FROM
				  	ms_procurement a
				  LEFT JOIN 
				  	tb_sbu_lokasi b ON a.id_lokasi=b.id
				  LEFT JOIN
				  	ms_procurement_peserta as mpp ON mpp.id_proc=a.id
				  WHERE 
				  	a.del = 0 AND a.id_mekanisme=1 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

}
