<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Pre_auction_model extends MY_Model
{
	public function select_data($id_lelang)
	{
		$query = "SELECT a.*,
					   (SELECT GROUP_CONCAT(symbol) FROM tb_kurs WHERE id IN (SELECT id_kurs FROM ms_procurement_kurs WHERE id_procurement = a.id)) AS rate, 
					   b.metode_auction,
					   b.hps, 
					   b.metode_penawaran,
					   b.kriteria_pemenang
					   
				FROM ms_procurement a 
				LEFT JOIN ms_procurement_tatacara b ON a.id = b.id_procurement 
				
				WHERE a.id = ? ";
		
		$query = $this->db->query($query, array($id_lelang));
		return $query->row_array();
	}

	function get_barang($id_lelang = ''){
		$sql = "SELECT a.*,
					   b.symbol,
					   b.name AS kurs,
					   b.id _id_kurs
					   FROM ms_procurement_barang a 
					 
					   LEFT JOIN ms_procurement_kurs c ON a.id_kurs = c.id 
					     LEFT JOIN tb_kurs b ON c.id_kurs = b.id 
					   WHERE a.id_procurement = ?";
		return $this->db->query($sql, array($id_lelang));
	}

	function get_penawaran($id_lelang = '',$id_vendor='', $by_vendor=false){
		$lelang = $this->select_data($id_lelang);
		if($id_vendor!=''){
			$where = 'AND a.id_vendor = '.$id_vendor;
		}
		$sql = "SELECT 	a.*,
						c.name vendor_name, 
						
						e.name legal_name

				FROM ms_penawaran a 
				LEFT JOIN ms_vendor c ON a.id_vendor = c.id
				LEFT JOIN ms_vendor_admistrasi d ON d.id_vendor = c.id
				LEFT JOIN tb_legal e ON d.id_legal = e.id


				WHERE a.id_procurement = ? ".$where." ";
		if(!$by_vendor && $lelang['kriteria_pemenang']=='harga_satuan'){
			$sql .= "GROUP BY id_vendor, id_barang";
		}
		$sql .= ' ORDER BY a.id';
		$query = $this->db->query($sql, array($id_lelang));
		// echo $this->db->last_query();
		return $query;
	}

	function convert_to_idr($nilai = '', $id_kurs = '', $id_lelang = ''){
		$sql = "SELECT * FROM ms_procurement_kurs WHERE id_kurs = ? AND id_procurement = ?";
		$sql = $this->db->query($sql, array($id_kurs, $id_lelang));
		$sql = $sql->row_array();
		
		if($sql['id_kurs'] == 1) $sql['rate'] = 1;
		
		return ($nilai * $sql['rate']);
	}

	function end_auction($id_lelang = ''){
		$sql = "UPDATE ms_procurement SET is_finished = ?, end_hour = ?, time_limit = ? WHERE id = ?";
		$this->db->query($sql, array(1, date("H:i:s"), date("Y-m-d H:i:s"), $id_lelang));

	}

	function generate_catalogue($id_lelang){

		$this->load->model('auction_report_model');
		$fill = $this->auction_report_model->get_header($id_lelang);
		$_barang = $this->auction_report_model->get_barang($id_lelang);
		
		// var_dump($_barang->result());

		foreach($_barang->result() as $data){
			if($data->is_catalogue==1) {
				if($data->id_material==0){
					$this->db->insert('ms_material',array('id_barang'=>$data->id,'nama'=>$data->nama_barang,'entry_stamp'=>date('Y-m-d H-i-s')));
					$id_material = $this->db->insert_id();
				}else{
					$id_material = $data->id_material;	
				}
				
			
				$_peserta = $this->auction_report_model->get_vendor_ranking($id_lelang, $data->id, $fill['auction_type']);
				
				$is_first = true;

				$_result_peserta = $_peserta->result_array();
				$penawaran = $this->auction_report_model->get_penawaran($_result_peserta[0]['id_penawaran']);
				
				
				if($penawaran['nilai'] > 0){
					if($penawaran['id_kurs'] == 1)
						$in_rate = number_format($penawaran['in_rate']);	
					else{
						$nilai = $penawaran['symbol']." ".number_format($penawaran['nilai']);
						$in_rate = number_format($penawaran['in_rate']);
					}
				}

				$this->db->insert('tr_material_price',array(
																'id_material'	=>$id_material,
																'id_procurement'=>$id_lelang,
																'id_vendor'		=>$_result_peserta[0]['id_peserta'],
																'price'			=>preg_replace("/[,]/", "", $in_rate),
																'date'			=>date('Y-m-d'),
																'entry_stamp'	=>date('Y-m-d H-i-s')
															)
								);
			}
		}

	}
	function get_bid_peserta($id_procurement){
		$query = "	SELECT a.*, b.name nama_vendor FROM ms_penawaran a
					JOIN ms_vendor b ON b.id = a.id_vendor
							WHERE a.id_procurement = ? 
					GROUP BY a.id_vendor ORDER BY a.id";
		$query = $this->db->query($query, array($id_procurement));
		return $query;
	}
	function get_lowest_price($id_lelang = '', $id_barang = ''){
		
		$sql = "SELECT MIN(a.in_rate) AS nilai,
					   b.name
				
				FROM ms_penawaran a
				LEFT JOIN ms_vendor b ON b.id = a.id_vendor 
				 
				WHERE a.id_procurement = ? AND a.id_barang = ? LIMIT 10";
		 	
		$sql = $this->db->query($sql, array($id_lelang, $id_barang));
		$sql = $sql->row_array();
		
		return $sql;
	}

	function cek_percentage($id_lelang = '', $id_vendor='', $id_barang = '', $nilai = '', $id_before = ''){
		$percent = 0;
		$input = array($id_lelang, $id_barang, $id_vendor);
		$sql = "SELECT in_rate AS nilai, id_kurs FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ? ";
		
		if($id_before) {

			$sql .= " AND id < ? ";
			$input[] = $id_before;

		}

		$sql .= " ORDER BY id DESC LIMIT 0,1";
		
		$sql = $this->db->query($sql, $input);
		$sql = $sql->row_array();
		
		// if($sql['nilai'] / 100 > 0)
			$percent = number_format(($sql['nilai'] - $nilai) / ($sql['nilai'] / 100), 1);
		
		return $percent;
	}

	function get_indicator($id_lelang, $id_barang, $id_user){
		$barang = $this->get_item($id_barang)->row_array();

		$fill = $this->select_data($id_lelang);

		if($fill['auction_type'] == "forward_auction"){ $ord = "DESC"; $symbol = ">"; } 
		else if($fill['auction_type'] == "reverse_auction"){ $ord = "ASC"; $symbol = "<"; } 

		$rank = 1;
		$latest = $this->json_provider_model->get_list($id_lelang, $symbol, $ord, $id_barang, $id_user, true);	
		// echo $this->db->last_query();
		$latest = $latest->row_array();
		$latest = $latest['in_rate'];

		if($fill['auction_jenis']=='rfi_penunjukan'){
			$hps = $this->json_provider_model->convert_to_idr($barang['nilai_hps'], $barang['id_kurs'], $id_lelang);
			if($fill['auction_type'] == "forward_auction")		{ if($hps > $latest) $rank = 0; } 
			else if($fill['auction_type'] == "reverse_auction")	{ if($hps < $latest) $rank = 0; } 
		}	

		if($rank){
			$is_same = $this->json_provider_model->get_same_offers($barang['id'], $latest, $id_user);

			$rank = $this->json_provider_model->get_list($id_lelang, $symbol, $ord, $barang['id'], $id_user, false, $is_same);
			$rank = $rank->num_rows();

			if($is_same) $rank++;
		}

		return $rank;
	}

	function count_total_barang_penawaran($id_lelang, $id_vendor){
		$query = "SELECT count(id_barang) total_barang FROM ms_penawaran WHERE id_procurement = ? AND id_vendor = ? GROUP BY id_barang";
		$query = $this->db->query($query, array($id_lelang, $id_vendor));
		return $query->row_array()['total_barang'];
	}

	function hps_total($id_procurement){
		$query = "	SELECT SUM((nilai_hps * volume)) hps FROM ms_procurement_barang 
							WHERE id_procurement = ? ";
		$query = $this->db->query($query, array($id_procurement));
		return $query->row_array()['hps'];	
	}

	function get_lampiran($id_lelang='', $id_vendor='', $id_barang = ''){

		$query = "SELECT * FROM ms_upload_penawaran WHERE id_procurement = ? AND id_vendor = ?";
		$param = array($id_lelang, $id_vendor);
		if($id_barang){
			$query .= "AND id_barang = ?";
			$param[]= $id_barang;
		}
		$query = $this->db->query($query,$param);

		return $query;
	}

	function check_last_value($fill,$id_vendor){
		if($fill['auction_type']=='reverse_auction') {$ord = 'DESC'; $sort = "MIN";}
		if($fill['auction_type']=='forward_auction') {$ord = 'ASC'; $sort = "MAX";}

		$query = "	SELECT id_barang, SUM(nilai) max_nilai, SUM(in_rate) max_in_rate

					FROM 
					(
						SELECT id_barang, ".$sort."(nilai) nilai, ".$sort."(in_rate) in_rate
						FROM ms_penawaran a 
						WHERE a.id_procurement = ?
						AND 	a.id_vendor = ? 
						GROUP BY id_barang 
						ORDER BY a.id ".$ord."
					) as penawaran ";
		$query = $this->db->query($query,array($fill['id'],$id_vendor));
		return $query->row_array();
	}
	function force_stop($id_lelang = ''){
		$sql = "UPDATE ms_procurement SET is_started = ?, is_finished = ?, is_suspended = ?, is_fail_auction = ?, end_hour = ?, time_limit = ?  WHERE id = ?";
		$this->db->query($sql, array(1, 1, 0, 1, date("H:i:s"), date("Y-m-d H:i:s"), $id_lelang));
	}

	function get_ranking_total($data, $nilai = ''){
		$id_lelang = $data['id'];

		$rank = 1;
		if($data['auction_type']=='reverse_auction') {$ord = '<'; $sort = "MIN";}
		if($data['auction_type']=='forward_auction') {$ord = '>'; $sort = "MAX";}

		$query = "	SELECT * FROM (
						SELECT SUM(ord_rate) ord_rates, id_barang, id_vendor FROM (
							SELECT ".$sort."(in_rate) ord_rate, id_barang,id_vendor 
							FROM ms_penawaran msp 
							WHERE msp.id_procurement = ? 
							GROUP BY id_vendor, id_barang) 
						as penawaran GROUP BY id_vendor) 
					as penawaran_1 HAVING ord_rates ".$ord." ? ";
		$query = $this->db->query($query,array($id_lelang, $nilai));

		$total_rows = $query->num_rows();
		$result = $rank + $total_rows;


		if($data['auction_jenis']=='rfi_penunjukan') {
			$total_hps = $this->hps_total($data['id']);
			if($data['auction_type']=='reverse_auction') {
				if($total_hps<$nilai) $result = 0;
			}
			if($data['auction_type']=='forward_auction') {
				if($total_hps>$nilai) $result = 0;
			}
		}


		return $result;
	}

	function get_last_penawaran($id_lelang, $id_vendor, $id_barang){
		$query = "SELECT * FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ?";
		$query =  $this->db->query($query, array($id_lelang, $id_vendor, $id_barang));

		return $query;
	}

	function is_last($id_lelang, $id_vendor, $id_penawaran){
		$query = "	SELECT id FROM 
						(	SELECT * FROM ms_penawaran 
							WHERE id_procurement = ? 
							AND id_vendor = ? 
							ORDER BY id DESC
						) msp 
					GROUP BY msp.id_barang";
		$query = $this->db->query($query, array($id_lelang, $id_vendor));
		$query = $query->result_array();
		foreach($query as $data){
			if($data['id']==$id_penawaran){
				return true;
			}
		}
		return false;
	}

	function process_rank($data = array(), $id_procurement = 0, $auction_type = '',$type_){
		$data_rank = array();
		
		$rank = ($auction_type=='forward_auction')? $rank = 1 : $this->get_bid_peserta($id_procurement)->num_rows();
		foreach ($data as $key => $value) {

			if($value['is_last']){
				$data_rank[$key] = $rank;;
				if($auction_type=='forward_auction') $rank++;
				else $rank--;
			}
			
		}
		return $data_rank;
	}

	function extend_lelang($id_lelang = ''){
		$time = ($this->input->post('time')!='') ? $this->input->post('time') : 1;
		$time_limit = strtotime(date("Y-m-d H:i:s")." + ".$time." minutes");
		$time_limit = date("Y-m-d H:i:s", $time_limit);
		
		$sql = "UPDATE ms_procurement SET is_started = ?, time_limit = ?, is_suspended = ?  WHERE id = ?";
		$this->db->query($sql, array(1, $time_limit, 0, $id_lelang));
		
		return $time_limit;
	}

	function start_auction($id_lelang = ''){
		$duration = $this->select_data($id_lelang);
		$duration = $duration['auction_duration'];
		 
		$time_limit = strtotime(date("Y-m-d H:i:s")." + ".$duration." minutes");
		$time_limit = date("Y-m-d H:i:s", $time_limit);
		
		$sql = "UPDATE ms_procurement SET is_started = ?, is_finished = ?, start_time = ?, time_limit = ?  WHERE id = ?";
		$this->db->query($sql, array(1, 0, date("Y-m-d H:i:s"), $time_limit, $id_lelang));
		
		return $time_limit;
	}

	function mark_as_extend($id_lelang = ''){
		$sql = "UPDATE ms_procurement SET is_started = ?, 
										   time_limit = ?, 
										   is_finished = ?, 
										   is_suspended = ?
											  
										   WHERE id = ? ";
		
		$this->db->query($sql, array(0, null, 0, 1, $id_lelang));
	}
}