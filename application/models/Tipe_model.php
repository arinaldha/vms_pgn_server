<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tipe_model extends MY_Model{
	public $table = 'tb_type_item';
	function __construct(){
		parent::__construct();
	}
	function getTipe($form){
		$query = "	SELECT
						id,
              			name,
              			code
					FROM ".$this->table."
					WHERE del = 0";
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['code'].' - '.$value['name'];
	    }
	    return $data;
	}

	function getData($form){

		$query = "	SELECT
							name,
							a.id

					FROM ".$this->table." a
					WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT
						name


					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
}
