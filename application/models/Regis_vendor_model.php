<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Regis_vendor_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

    public function insert_vendor($data){
        $this->db->insert('ms_vendor',$data);
        // print_r($this->db->last_query());
        return $this->db->insert_id();
    }

    public function insert_data_vendor($table, $data){
        return $this->db->insert($table,$data);

    }

    public function get_npwp($npwp)
    {
        $query = $this->db->select('npwp_code')->where('npwp_code',$npwp)->get('ms_vendor_admistrasi');
        return $query;
    }
}
