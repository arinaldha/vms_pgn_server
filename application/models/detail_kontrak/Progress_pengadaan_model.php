<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Progress_pengadaan_model extends MY_Model{
	public $table = 'ms_procurement_peserta';
	function __construct(){
		parent::__construct();
	}
	function updateProgress($id, $id_progress, $save){
		$data = $this->db->query("SELECT * FROM tr_progress_pengadaan WHERE id_procurement = ? AND id_progress = ?", array($id, $id_progress))->result_array();
		if(count($data) > 0){
			$this->db->where('id_procurement', $id)->where('id_progress', $id_progress)->update('tr_progress_pengadaan', $save);
		}else{
			$this->db->insert('tr_progress_pengadaan', array(
				'id_procurement'=>$id,
				'id_progress'=>$id_progress,
				'plan_date'=>$save['plan_date'],
				'real_date'=>$save['real_date'],
				'remark'=>$save['remark'],
				'progress_file'=>implode(',',$save['progress_file']),
			));
		}
	}
	function prosesProgress($id=""){
		
		$data = $this->input->post();

			foreach ($data['plan_date'] as $key => $date) {
				$this->db->insert('tr_progress_pengadaan', array('id_progress' => $key, 'id_procurement' => $data['id'], 'plan_date' => $date));	
			}
			
		
		$this->db->where('id', $data['id'])->update('ms_procurement', array('step'=>1));
		return $data['id'];
	}
	function lanjut($id=""){
		$this->db->where('id', $id)->update('ms_procurement', array('step'=>2));
		return $id;
	}
	function get_paket_progress($id){
		$admin = $this->session->userdata('admin');
		$query = "	SELECT * 
					FROM tr_progress_pengadaan a 
					JOIN tb_progress_pengadaan b ON b.id=a.id_progress 
					WHERE a.id_procurement = ?
					AND a.value = 1
					
					AND b.del = 0
				";
		if ($admin['id_role'] == 2) {
			// $query .= "AND a.is_hidden = 0";
		}
		$query = $this->db->query($query, array($id));
   		$result = $query->result_array();

		$total_day = 0;
		foreach ($result as $key => $value) {

			$date_now = $result[$key]['date'];
			$date_next = ( isset($result[$key+1]['date'] ) ? $result[$key+1]['date'] : $date_now);
			$day_range = get_range_date($date_next,$date_now);
			$result[$key]['day_range'] = $day_range;
			
			$total_day += $day_range;

		}

		
		$data['total_day'] = $total_day;

		foreach ($result as $key => $value) {
			$result[$key]['percent'] = floor($value['day_range'] / $total_day * 100);
		}
		return $result;
   	}

	function get_progress_pengadaan($id){
		$query = "	SELECT * 
					FROM tr_progress_pengadaan a 
					LEFT JOIN tb_progress_pengadaan b ON b.id=a.id_progress 
					WHERE a.id_procurement = ?
					AND a.value = 1
					
					AND b.del = 0 ORDER BY orders ASC
				";
		$query = $this->db->query($query, array($id));
   		$result = $query->result_array();
   		$return = array();
   		foreach ($result as $key => $value) {
   			$return[(string)$value['id_progress']] = $value;
   		}
   		return $return;
   	}
   	function get_pengadaan_step($id){
   		$query = "SELECT * FROM tb_progress_pengadaan ORDER BY orders ASC";
   		$query 	= $this->db->query($query)->result_array();

   		$result = array();
   		foreach($query as $value){
   			$result[(string)$value['id']] = $value['value'];
   		}
   		return $result;
   	}
	
	function getData($form, $id=null, $data){
		$admin = $this->session->userdata('admin');
		
		$query = "	SELECT 
						a.value,
						b.plan_date,
						b.real_date,
						b.progress_file,
						b.remark,
						b.value data,
						a.id id_progress,
						-- b.is_hidden,
						b.id_procurement,
						a.orders
						
					FROM tb_progress_pengadaan a 
					LEFT JOIN tr_progress_pengadaan b ON a.id=b.id_progress AND b.id_procurement = ".$id."
					WHERE FIND_IN_SET(".$data['id_mekanisme'].",`metode`)";
		// echo $query;
		if ($admin['id_role'] == 2) {
			# code...
			// $query .= "WHERE is_hidden = 0 OR is_hidden is null";
		}
		return $query;
	}
	function progressDetail($id, $id_progress){
		$data = $this->db->query("SELECT * FROM tr_progress_pengadaan WHERE id_procurement = ? AND id_progress = ?", array($id, $id_progress))->row_array();
		return $data;
	}
	function save_progress_pengadaan($id){
		$post = $this->input->post();
		#print_r($post);die;
   		foreach($this->input->post('progress') as $key => $value){
   			$query = "SELECT * FROM tr_progress_pengadaan WHERE id_procurement = ? AND id_progress = ?";
   			$query = $this->db->query($query, array($id, $key))->num_rows();

   			// $check = $this->db->where('id_procurement',$id)->where('id_progress',$key)->get('tr_progress_pengadaan')->num_rows();
   			if($post['plan_date'][$key]!=''){
   				if($query>0){
	   				$res = $this->db->where('id_procurement',$id)
	   								->where('id_progress',$key)
	   								->update('tr_progress_pengadaan',array(
	   																	'value'=>$value,
	   																	'plan_date'=>$post['plan_date'][$key],
	   																	'edit_stamp'=>timestamp()
	   																	)
	   								);
	   				if(!$res) return false;
	   			}else{
	   				$res = $this->db->insert('tr_progress_pengadaan',array(
																		'value'=>$value,
																		'id_procurement'=>$id,
																		'id_progress'=>$key,
																		'plan_date'=>$post['plan_date'][$key],
																		'entry_stamp'=>timestamp()
																	));
	   				if(!$res) return false;
	   			}
   			}
   			
   		}
   		return true;
   	}
   
}