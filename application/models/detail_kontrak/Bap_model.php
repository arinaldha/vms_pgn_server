<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bap_model extends MY_Model{
	public $table = 'ms_bap';
	function __construct(){
		parent::__construct();

	}
	function selectData($id){
		$query = "	SELECT 
						no,
						date,
						value,
						bobot,
						bap_file
					FROM ".$this->table." a 
					WHERE a.id_procurement = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getData($form, $id=null){
		$query = "	SELECT 	
						no,
						date,
						value,
						bobot,
						bap_file,
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $query;
	}
	function getTerminBapDetail($id){
		$query = "	SELECT 	
						name,
						value,
						a.id
					FROM ms_termin a 
					WHERE a.del = 0 AND a.id_procurement = ?";
		$query = $this->db->query($query, array($id));
		$return = array();
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value;
		}
			
		return $return;
	}
	function getTerminBap($id){
		$return = array();
		$data = $this->getTerminBapDetail($id);

		foreach ($data as $key => $value) {
			$return[$value['id']] = $value['name'];
		}
		return $return;
	}
	function getBarangBapDetail($id){
		$query = "	SELECT 	
						nama_barang,
						nilai_hps,
						a.id
					FROM ms_procurement_barang a 
					WHERE a.del = 0 AND a.id_procurement = ?";
		$query = $this->db->query($query, array($id));
		$return = array();
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value;
		}
		return $return;
	}
	function getBarangBap($id){
		$return = array();
		$data = $this->getBarangBapDetail($id);

		foreach ($data as $key => $value) {
			$return[$value['id']] = $value['nama_barang'];
		}
		return $return;
	}
}