<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends MY_Model{
	public $table = 'ms_procurement_barang';
	function __construct(){
		parent::__construct();

	}
	function selectData($id){
		$query = "	SELECT 
						nama_barang,
						volume,
						nilai_hps,
						satuan,
						id_kurs
					FROM ".$this->table." a 
					WHERE a.id = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getBarang($form, $id=null){
		$query = "	SELECT 	
						
						nama_barang,
						volume,
						nilai_hps,
						id_kurs,
						satuan,
						
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $query;
	}
	function insert($data){
		$data['del'] = 0;	
		$a =  $this->db->insert($this->table, $data);
		return $a;
	}
}