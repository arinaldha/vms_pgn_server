<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pemenang_model extends MY_Model{
	public $table = 'ms_procurement_peserta';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		$query = "	SELECT 	
						b.name,
						a.id
					FROM ".$this->table." a
					LEFT JOIN ms_vendor b ON b.id = a.id_vendor
					WHERE a.del = 0 AND id_procurement = ".$id."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	function getPesertaPenawaran($id){
		$query = "	SELECT a.vendor_name name
					FROM ms_procurement_peserta a 
					
					WHERE a.id_procurement = ?";
		$query = $this->db->query($query, array($id));
		$return = array();
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value['name'];
		}
		return $return;
	}
	function getPemenang($id){
		$query = "	SELECT a.vendor_name name
					FROM ms_procurement_peserta a 
					WHERE a.del = 0 
					AND a.id_procurement = ?";
		$query = $this->db->query($query, array($id));
		$return = $query->row_array();

		return $return;
	}
	function selectData($id){
		$query = "	SELECT a.vendor_name, id_kurs_kontrak, idr_kontrak, kurs_kontrak
					FROM ms_procurement_peserta a 
					WHERE a.id_procurement = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function update($id, $data){
		/*RESET*/
		$this->db
			->where('id_procurement', $id)
			->update('ms_procurement_peserta', array(
				'is_winner'	=>0,
				'id_kurs_kontrak'	=>null,
				'idr_kontrak'	=>null,
				'kurs_kontrak'=>null,
				'edit_stamp' => timestamp()
			));

		return $this->db
			->where('id_procurement', $id)
			->where('id_vendor', $data['id_vendor'])
			->update('ms_procurement_peserta', array(
				'is_winner'	=>1,
				'id_kurs_kontrak'	=>$data['id_kurs_kontrak'],
				'idr_kontrak'	=>currency($data['idr_kontrak']),
				'kurs_kontrak'=>currency($data['kurs_kontrak']),
				'edit_stamp' => timestamp()
			));
	}
	

}