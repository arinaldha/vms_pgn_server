<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Amandemen_model extends MY_Model{
	public $table = 'ms_amandemen';
	function __construct(){
		parent::__construct();

	}
	function selectData($id){
		$query = "	SELECT
						sequence, 
						no,
						about,
						date,
						idr_value,
						contract_end,
						work_end,
						remark,
						amandemen_file
					FROM ".$this->table." a 
					WHERE a.id = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getData($form, $id=null){
		$query = "	SELECT 	
						a.sequence,
						a.no,
						a.about,
						a.date,
						a.idr_value,
						a.work_end,
						a.contract_end,
						
						a.remark,
						a.amandemen_file,
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $query;
	}

   	function get_amandemen_range($id){
   		$this->db
   				->select('max(DATE(`amandemen_end`)) as end_date, min(DATE(`amandemen_start`)) as start_date')
   				->where('id_procurement',$id);
   		
   		$res =  $this->db->get('ms_amandemen');
   		if($res->num_rows()>0){
	   		return $res->row_array();
	   	}
	   	return false;
   	}
   	function insert($data){
   		$query = parent::insert($data);
   		if($query){
   			$id_amandemen = $this->db->insert_id();
   			$diff =  strtotime($data['amandemen_end']) - strtotime($data['amandemen_start']);

	   		return $this->db->insert('tr_progress_kontrak',array(
	   							'id_procurement'=>$data['id_procurement'],
	   							
	   							'step_name'=>'Amandemen',
	   							'id_contract'=>$data['id_contract'],
	   							'start_date'=>$data['amandemen_start'],
	   							'end_date'=>$data['amandemen_end'],
	   							'supposed'=>floor($diff / 86400),
	   							'entry_stamp'=>$data['entry_stamp'],
	   							'type'=>3
	   						));
   		}

   	}
   	function selectAmandemen($id){
   		$query = "	SELECT 	
						a.sequence,
						a.no,
						a.about,
						a.date,
						a.idr_value,
						a.work_end,
						a.contract_end,
						
						a.remark,
						a.amandemen_file,
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $this->db->query($query)->result_array();
   	}
}