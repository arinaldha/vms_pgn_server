<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends MY_Model{
	public $table = 'ms_invoice';
	function __construct(){
		parent::__construct();

	}
	function selectData($id){
		$query = "	SELECT 
						no,
						date,
						value,
						detail,
						date_tagihan_masuk,
						date_input_pembayaran,
						date_verifikasi_pajak,
						date_approve_1,
						date_approve_2,
						date_approve_3,
						date_masuk_treasury,
						date_dibayar,
						invoice_file
					FROM ".$this->table." a 
					WHERE a.id = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		
		return $query->row_array();
	}
	function getData($form, $id=null){
		$query = "	SELECT 	
						no,
						date,
						value,
						invoice_file,
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $query;
	}
	function insert($data){
		$data['del'] = 0;	
		foreach ($data as $key => $value) {
			if(is_array($value)){
				$data[$key] = implode(',',$value);
			}
		}
		$a =  $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
}