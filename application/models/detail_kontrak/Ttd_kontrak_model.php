<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ttd_kontrak_model extends MY_Model{
	public $table = 'ms_contract';
	function __construct(){
		parent::__construct();

	}
	function selectData($id_procurement, $id_contract=null){
		// $dataProc = $this->selectProcurement($id_procurement);
		if($id_contract==null){
			$query = "	SELECT 
						id_vendor,
						no_sppbj,
						sppbj_date,
						no_spmk,
						spmk_date,
						start_work,
						end_work,
						contract_date,
						no_contract,
						po_file,
						contract_price,
						contract_price_kurs,
						contract_kurs,
						start_contract,
						end_contract,
						contract_file, 
						price_file,
						vendor_name,
						address,
						phone,
						fax,
						email,
						bsb, 
						type,
						budget_center
					FROM ".$this->table." a 
					WHERE a.id_procurement = ? 
					AND a.del = 0 ";
			$query = $this->db->query($query, array($id_procurement));
		}else{
			$query = "	SELECT 
						id_vendor,
						no_sppbj,
						sppbj_date,
						no_spmk,
						spmk_date,
						start_work,
						end_work,
						contract_date,
						no_contract,
						po_file,
						contract_price,
						contract_price_kurs,
						contract_kurs,
						start_contract,
						end_contract,
						contract_file, 
						price_file,
						vendor_name,
						address,
						phone,
						fax,
						email,
						bsb, 
						type,
						budget_center
					FROM ".$this->table." a 
					WHERE a.id = ? 
					AND a.del = 0 ";
			$query = $this->db->query($query, array($id_contract));
		}
		
		return $query->row_array();
	}
	function selectProcurement($id_procurement){
		$query = "SELECT * FROM ms_procurement WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id_procurement))->result_array();
		return $query;
	}
	function selectDataTermin($id){
		$query = "	SELECT 
						name,
						percen,
						value
					FROM ms_termin a 
					WHERE a.id = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	
   	function getDataTermin( $form, $id_procurement, $id_contract=null){
		$admin = $this->session->userdata('admin');
		$dataProcurement = $this->selectProcurement($id);
		if($id_contract==null){
			$query = "	SELECT
							a.name,
							a.percen,
							a.value,
							a.id
					FROM ms_termin a
					WHERE a.del = 0 AND id_procurement =  ".$id_procurement;
		}else{
			$query = "	SELECT
							a.name,
							a.percen,
							a.value,
							a.id
					FROM ms_termin a
					WHERE a.del = 0 AND id_contract =  ".$id_contract;
		}
		
		

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function insert($data, $dataBerkas){
   		$query = $this->db->insert($this->table, $data);
   	}
   	function update($id, $data, $type){
   		$query = "SELECT * FROM ".$this->table." a WHERE id_procurement = ? and del = 0";
   		$query = $this->db->query($query, array($id));
   		$query = $query->num_rows();
   		$this->db->where('id',$id)->update('ms_procurement', array('step'=>3));
   		if($query > 0){

   			$query = $this->db->where('id_procurement', $data['id_procurement'])->update($this->table, $data);

   			return $query;
   		}else{
   			
   			$query = $this->db->insert($this->table, $data);
   			$insert_id = $this->db->insert_id();

   			return $query;
   			
   		}
   	}

   	function get_kontrak($id){
   		$query = "	SELECT 	a.*, 
   							a.id id, 
   							a.name vendor_name, 
   							e.symbol kurs_name	
   					FROM ms_contract a
   					LEFT JOIN tb_kurs e ON a.contract_kurs = e.id
   					WHERE a.id_procurement = ?
   					AND a.del = 0";
   		$query = $this->db->query($query, array($id));
   		return $query->row_array();
   	}

   	function get_contract_progress($id){
   		$query = "	SELECT 	a.id,
   							a.step_name,
   							a.start_date,
   							a.end_date,
   							a.supposed,
   							a.type
   					FROM tr_progress_kontrak a
   					WHERE a.id_contract = ?
   					AND a.del = 0
   				";
   		
   		$query = $this->db->query($query, array($id));

   		return $query->result_array();
   	}
   	function insertTermin($id, $data){
   		$data['del'] = 0;	
		$a =  $this->db->insert('ms_termin', $data);
		return $a;
   	}
   	function updateTermin($id, $data){
		$this->table = 'ms_termin';
		return parent::update($id, $data);
	}
	function deleteTermin($id){
		return $this->db->where('id', $id)
					->update('ms_termin', array(
											'del' => 1,
											'edit_stamp' => timestamp()
											)
					);
	}
	function selectKontrak($id_procurement){
		$query = "SELECT * FROM ms_contract WHERE id_procurement = ? AND del = 0";
		$query = $this->db->query($query, array($id_procurement))->result_array();
		// echo print_r($query);
		return $query;
	}
}