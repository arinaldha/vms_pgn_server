<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bast_model extends MY_Model{
	public $table = 'ms_bast';
	function __construct(){
		parent::__construct();

	}
	function selectData($id){
		$query = "	SELECT 
						no,
						date,
						value,
						bobot,
						bast_file,
						denda_start,
						denda_end,
						percentage,
						denda
					FROM ".$this->table." a 
					WHERE a.id = ?
					AND a.del = 0 ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getData($form, $id=null){
		$query = "	SELECT 	
						no,
						date,
						value,
						bobot,
						bast_file,
						a.id
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_procurement = ".$id."";
		
		return $query;
	}
	
}