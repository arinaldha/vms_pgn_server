<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Note_model extends CI_Model{

	
	function save($table, $id, $data){
		$admin = $this->session->userdata('admin');
		$_param = array();
		$list_dokumen = array(
			'ms_akta'=>'Akta',
			'ms_situ'=>'SITU/Domisili',
			'ms_tdp'=>'TDP',
			'ms_pengurus'=>'Pengurus',
			'ms_pemilik'=>'Kepemilikan Saham',
			'ms_ijin_usaha'=>'Izin Usaha',
			'ms_agen'=>'Pabrikan/Keagenan/Distributor',
			'ms_pengalaman'=>'Pengalaman');
		$document = $list_dokumen[$table];
		if($table=='ms_akta'){
			if($data['type']=='pendirian'){
				$document = 'Akta Pendirian Perusahaan';
			}else{
				$document = 'Akta Perubahan Terakhir';
			}
			
		}
		if($table=='ms_ijin_usaha'){
			$list_ijin_usaha =	array(
								'siujk'=>'SIUJK',
								'sbu'=>'SBU',
								'siup'=>'SIUP',
								'ijin_lain'=>'Surat Izin Usaha Lainnya',
								'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
							);
			$document = $list_ijin_usaha[$data['type']];
		}
		$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
		$nama_orang = array('ms_pengurus','ms_pemilik');

		if(in_array($table, $dokumen)){
			$subject = $data['no'];
			$subject_field = 'Nomor';
		}
		if(in_array($table, $nama_orang)){
			$subject = $data['name'];
			$subject_field = 'Nama';
		}
		if($table=='ms_pengalaman'){
			$subject = $data['job_name'];
			$subject_field = 'Nama Pemberi Tugas';
		}
				
		$sql = $this->db->insert('tr_note',
			array(
				'id_vendor'=>$data['id_vendor'],
				'id_document'=>$id,
				'document_type'=>$table,
				'document'=>$document,
				'subject'=>$subject,
				'subject_field'=>$subject_field,
				'value'=>$this->input->post('value'),
				'entry_stamp'=>timestamp(),
				'is_active'=>1,
				'is_vendor_close'=>0,
				'is_admin_close'=>0,
				'entry_by'=>$admin['id_user']
			)
		);
		return $sql;
	}

	function process_close($id){
		$admin = $this->session->userdata('admin');
		if($admin){
			$field = array('is_admin_close'=>1);
		}else{
			$field = array('is_vendor_close'=>1);
		}
		return $this->db->where('id',$id)
						->update('tr_note',$field);
	}

	function get_note($id_vendor){
		$query = 	"	SELECT
							a.*
						FROM
							tr_note a
						WHERE
							a.id_vendor = ? AND a.is_vendor_close = ?";
		$result = $this->db->query($query,array($id_vendor,0));
		return $result->result_array();
	}

	// function close($id){
	// 	return $this->db
	// 		->where('id',$id)
	// 		->update('tr_note',array('is_active'=>0));
	// }

	function get_note_admin($id_vendor){
		$admin = $this->session->userdata('admin');
		$query = 	"	SELECT
							*
						FROM
							tr_note a
						WHERE
							a.is_active = ?
							AND a.is_admin_close 	= ?
							AND id_vendor = ?
					";
		$result = $this->db->query($query,array(1 , 0 , $id_vendor));
		return $result;
	}
}