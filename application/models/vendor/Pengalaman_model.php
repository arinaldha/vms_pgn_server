<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengalaman_model extends MY_Model{
	public $table = 'ms_pengalaman';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		//tb_bidang b
		//tb_sub_bidang c
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.job_name,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start,
						a.contract_end,
						a.price_idr,
						a.currency,
						a.price_foreign,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$user['id_user']; //Hapus agar muncul di table//

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
						a.*,
						a.job_name,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start as contract_period,
						a.contract_end as contract_period,
						a.price_idr,
						a.currency,
						a.price_foreign,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.id = ? AND a.del=0 ";

		$query = $this->db->query($query, array($id));
		// print_r($query->row_array());die;
		return $query->row_array();

	}

	function get_id_ijin_usaha($id_iu_bsb){
		$query = $this->db->select('id_ijin_usaha')->where('id',$id_iu_bsb)->get('ms_iu_bsb')->row_array();
		return $query['id_ijin_usaha'];
	}
	function getBsbIu($id_vendor) {
		$id_vendor = $this->session->userdata('user')['id_user'];

		$query = "	SELECT
						a.id id,
						CONCAT(b.name, '-'  ,c.name) as name
					FROM 
						ms_iu_bsb a
					LEFT JOIN
						tb_bidang b ON b.id = a.id_bidang
					LEFT JOIN
						tb_sub_bidang c ON c.id = a.id_sub_bidang
					JOIN
						ms_ijin_usaha d ON d.id = a.id_ijin_usaha
					WHERE 
						 a.del = 0 AND d.del = 0 AND a.id_vendor = ? ORDER BY name";

		$query = $this->db->query($query, array($id_vendor))->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}

		return $data;
	}
	function insert($data)
	{
		return $this->db->insert('ms_pengalaman',$data);
	}
}
