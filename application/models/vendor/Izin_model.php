<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin_model extends MY_Model{
	public $table = 'ms_ijin_usaha';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.issue_date,
						a.qualification,
						a.authorize_by,
						a.izin_file,
						a.expire_date,
						a.id as id,
						b.name dpt_name,
                        a.no
				   FROM 
				   		".$this->table." a
				   INNER JOIN
				   		tb_dpt_type b ON b.id = a.id_dpt_type
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getBidangSubBidang($ijin){
    	$result = array();
    	$query = "SELECT * FROM tb_bidang WHERE id_dpt_type IN (".implode(',', $ijin['id']).")";
    	$query = $this->db->query($query);
    	// echo print_r(expression)
    	foreach($query->result_array() as $key => $value){
    		$queryBSB = "SELECT * FROM tb_sub_bidang WHERE id_bidang = ?";
    		$queryBSB = $this->db->query($queryBSB, array($value['id']));
    		foreach($queryBSB->result_array() as $keyBSB=>$valueBSB){
    			$result[$value['id']]['label'] = $value['name'];
    			$result[$value['id']]['data'][$valueBSB['id']]['label'] = $valueBSB['name'];
    		}
    	}
    	return $result;
    }
	function selectData($id){

		$query = "	SELECT
						a.*
					FROM 
						".$this->table." a 
					WHERE 
						a.id = ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function insert_izin($data){
		$user = $this->session->userdata('user');
		$bsb = $data['bsb'];
		unset($data['bsb']);
		$data['id_vendor'] = $user['id_user'];
		$data['entry_stamp'] = timestamp();
		$this->insert($data);

		$id = $this->db->insert_id();
		foreach ($bsb as $key => $value) {
			foreach($value as $keybsb){
				
				$databsb['id_vendor'] = $data['id_vendor'];
				$databsb['id_ijin_usaha'] = $id;
				$databsb['id_bidang'] = $key;
				$databsb['id_sub_bidang'] = $keybsb;
				$databsb['entry_stamp'] = timestamp();
				$this->insert_bsb($databsb);
			}
		}
		
		return $id;
	}
    public function checkIzinUsaha($id){
        $querySIUJK = " SELECT b.*, c.name nama_bidang, d.name nama_sub_bidang
                        FROM ms_ijin_usaha a 
                        LEFT JOIN ms_iu_bsb b ON a.id = b.id_ijin_usaha 
                        LEFT JOIN tb_bidang c ON c.id = b.id_bidang
                        LEFT JOIN tb_sub_bidang d ON d.id = b.id_sub_bidang
                        WHERE a.id_vendor = ? 
                        AND a. type='siujk' 
                        AND a.del = 0 
                        AND b.del = 0";
        $querySIUJK = $this->db->query($querySIUJK, array($id))->result_array();
        
        $result = array();
        foreach($querySIUJK as $key=>$value){
            $querySBU = "SELECT b.* FROM ms_ijin_usaha a LEFT JOIN ms_iu_bsb b ON a.id = b.id_ijin_usaha WHERE a.id_vendor = ? AND a. type='sbu'  AND a.del = 0 AND b.del = 0 AND b.id_sub_bidang = ?";
            $querySBU = $this->db->query( $querySBU, array($id, $value['id_sub_bidang']));
            if($querySBU->num_rows()==0){
                $result[$value['nama_bidang']][] = $value['nama_sub_bidang'];
            }
        }
        return $result;
    }
	public function insert_bsb($databsb){
		$this->table = 'ms_iu_bsb';
		$this->insert($databsb);
	}

	function get_bsb_admin_list($id) {
		$query = "	SELECT
						a.id id,
						CONCAT(b.name, '-'  ,c.name) as name

					FROM 
						ms_iu_bsb a
					LEFT JOIN
						tb_bidang b ON b.id = a.id_bidang
					LEFT JOIN
						tb_sub_bidang c ON c.id = a.id_sub_bidang
					WHERE 
						 a.del = 0 AND a.id_vendor = ".$id;

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;	
	}
	public function getBidangList(){
    	$result = array();
    	$query = "SELECT * FROM tb_bidang WHERE del=0";
    	$query = $this->db->query($query);
    	foreach($query->result_array() as $key => $row){
    		$result[$row['id']] = $row['name'];
    	}
    	return $result;
    }
    function selectTypeDPT($id){
        $query = "SELECT b.id_dpt_type FROM ms_iu_bsb a JOIN tb_bidang b ON a.id_bidang = b.id WHERE a.id_ijin_usaha = ?";
        $query = $this->db->query($query, array($id));
        $result = array();
        foreach ($query->result_array() as $key => $value) {
            $result['id'][] = $value['id_dpt_type'];
        }
        return $result;
    }
    function getDPTList($id){
        $query = "SELECT b.id_dpt_type FROM ms_iu_bsb a JOIN tb_bidang b ON b.id = a.id_bidang WHERE a.id_ijin_usaha = ? GROUP BY id_dpt_type";
        $query = $this->db->query($query,array($id));
        $result = array();
        foreach ($query->result_array() as $key => $value) {
            $result[] = $value['id_dpt_type'];
        }
        return $result;
    }
        function getVendorBidangSubBidang($ijin){
        $result = array();
        $query = "SELECT * FROM ms_iu_bsb WHERE id_ijin_usaha = ?";
        $query = $this->db->query($query, array($ijin));
        foreach($query->result_array() as $key => $value){
            $result[$value['id_bidang']][$value['id_sub_bidang']] = 1;  
        }
        return $result;
    }
    public function getSubBidangList(){
    	$result = array();
    	$query = "SELECT * FROM tb_sub_bidang WHERE del=0";
    	$query = $this->db->query($query);
    	foreach($query->result_array() as $key => $row){
    		$result[$row['id']] = $row['name'];
    	}
    	return $result;
    }

    public function ambilSb($id_bidang)
    {
    	$sql = $this->db->select('*')
						  ->from('tb_sub_bidang')
						  ->where('id_bidang', $id_bidang)
						  ->get('');
			if ($sql->num_rows() > 0) {
				return $sql->result();
			}
			else {
				return false;
			}
    }
	public function getBSBList($id){

    	$result = array();
    	$bidang = $this->getBidangList();
    	$sub_bidang = $this->getSubBidangList();
    	$query = "SELECT * FROM ms_iu_bsb WHERE id_ijin_usaha = ? ";
    	$query = $this->db->query($query, array($id));
    	foreach($query->result_array() as $key => $value){
    		$result[$bidang[$value['id_bidang']]][] = $sub_bidang[$value['id_sub_bidang']];
    	}
    	return $result;
    }

    public function get_bidang_sub_bidang($id)
    {
    	$user = $this->session->userdata('user')['id_user'];
    	$query = "SELECT 
    				 a.id id,
    				 b.id id_bidang,
    				 b.name as bidang_name,
    				 c.name as sub_bidang_name
    			  FROM
    			  	 ms_iu_bsb a
    			  LEFT JOIN
    			  	 tb_bidang b ON b.id = a.id_bidang 
    			  LEFT JOIN
    			     tb_sub_bidang c ON c.id=a.id_sub_bidang
    			  WHERE
    			  	 a.del != 1 AND a.id_vendor = $user AND a.id_ijin_usaha = $id";
    	return $query;
    }

    public function cek_bidang_sub_bidang($id)
    {
    	$query = "SELECT 
    				 a.*,
    				 a.id id,
    				 b.id id_bidang,
    				 b.name as bidang_name,
    				 c.name as sub_bidang_name
    			  FROM
    			  	 ms_iu_bsb a
    			  LEFT JOIN
    			  	 tb_bidang b ON b.id = a.id_bidang 
    			  LEFT JOIN
    			     tb_sub_bidang c ON c.id=a.id_sub_bidang
    			  WHERE
    			  	 a.del != 1 AND a.id = $id";
    	$query = $this->db->query($query, array($id));
		return $query->row_array();
    }

    public function get_data_ijin($id)
    {
    	$query = "SELECT * FROM ms_ijin_usaha WHERE id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
    }

    function getBidang($id){
		$query = "	SELECT
						id,
						name

						FROM tb_bidang 

						WHERE
							id_dpt_type = ".$id;

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}
	function getSubBidang($id){
		$query = "	SELECT
						id,
						name

						FROM tb_sub_bidang 

						WHERE
							id_bidang = ".$id;

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}

    public function update_bsb($id, $id_bidang,$data)
    {   
        $user = $this->session->userdata('user');
        $id_vendor = $user['id_user'];
        $data_bsb = array(
            'id_vendor' => $id_vendor,
            'id_ijin_usaha' => $data['id_ijin_usaha'],
            'id_bidang' => $data['id_bidang'],
            'id_sub_bidang' => $data['id_sub_bidang'],
            'entry_stamp' => $data['entry_stamp']
        );
    	$this->db->where('id',$id_bidang);
    	$query = $this->db->update('ms_iu_bsb',$data_bsb);
    	return $query;
    }

    public function getBidangSubBidangByIdIjinUsaha($ijin)
    {
    	$result = array();
    	$query = "SELECT * FROM tb_bidang WHERE id_dpt_type = IN (".implode(',', $ijin['id']).")";
    	$query = $this->db->query($query);
    	// echo print_r(expression)
    	foreach($query->result_array() as $key => $value){
    		$queryBSB = "SELECT * FROM tb_sub_bidang WHERE id_bidang = ?";
    		$queryBSB = $this->db->query($queryBSB, array($value['id']));
    		foreach($queryBSB->result_array() as $keyBSB=>$valueBSB){
    			$result[$value['id']]['label'] = $value['name'];
    			$result[$value['id']]['data'][$valueBSB['id']]['label'] = $valueBSB['name'];
    		}
    	}
    	return $result;
    }

    public function delete_bsb($id)
    {
    	$this->db->where('id',$id);
    	return $this->db->update('ms_iu_bsb',array('del' => 1));
    }

    public function edit_izin($id,$data)
    {
    	$data_bsb = array(
    		'id_bidang' => $data['id_bidang'],
    		'id_sub_bidang' => $data['id_sub_bidang']
    	);
    	$this->db->where('id',$id);
    	return $this->db->update('ms_iu_bsb',$data_bsb);
    }

    public function insert_bsb_vendor($data)
    {
    	$user = $this->session->userdata('user');
    	$id_vendor = $user['id_user'];
    	$data_bsb = array(
    		'id_vendor' => $id_vendor,
    		'id_ijin_usaha' => $data['id_ijin_usaha'],
    		'id_bidang' => $data['id_bidang'],
    		'id_sub_bidang' => $data['id_sub_bidang'],
    		'entry_stamp' => $data['entry_stamp']
    	);
    	return $this->db->insert('ms_iu_bsb',$data_bsb);
    }

    function get_izin_report($id){
        $this->db->select('*')
                ->where('ms_ijin_usaha.del',0)
                ->where('ms_ijin_usaha.id_vendor',$id);

        $query = $this->db->get('ms_ijin_usaha')->result_array();

        $surat_izin =   array();
        foreach ($query as $key => $value) {
            //bidang-sub-bidang
            $this->db->select('*')
                ->where('ms_iu_bsb.del',0)
                ->where('ms_iu_bsb.id_ijin_usaha',$value['id']);
            $bsbquery   = $this->db->get('ms_iu_bsb')->result_array();
            $bidang     = array();

            foreach ($bsbquery as $keybsb => $valuebsb) {
                $value['bsb'][$this->get_bidang_report()[$valuebsb['id_bidang']]['name']][]=$this->get_sub_bidang_report()[$valuebsb['id_sub_bidang']]['name'];
            }
            $surat_izin[$value['type']][] = $value;
        }
        // echo print_r($surat_izin);
        return $surat_izin;
    }

    function get_situ_report($id){
        $this->db->select('*')
                    ->where('id_vendor', $id)
                    ->where('del', 0);
        $query  = $this->db->get('ms_situ');

        return $query->result_array();
    }

    function get_tdp_report($id){
        $this->db->select('*')
                    ->where('id_vendor', $id)
                    ->where('del', 0);
        $query  = $this->db->get('ms_tdp');

        return $query->result_array();
    }

    function get_keagenan_report($id){
        $this->db->select('*')
                    ->where('id_vendor', $id)
                    ->where('ms_agen.del', 0)
                    ->join('ms_agen_produk', 'ms_agen.id=ms_agen_produk.id_agen');


        $query  = $this->db->get('ms_agen');
        return $query->result_array();
    }

    function get_pengalaman_report($id){
        $this->db->select('*')
                ->where('id_vendor', $id);

        $query  = $this->db->get('ms_pengalaman');

        return $query->result_array();
    }

    function get_bidang_report(){
        $this->db->select('*');
            // ->where('tb_bidang.del',0);

        $query  = $this->db->get('tb_bidang')->result_array();

        $bidang_list    =   array();
        foreach ($query as $key => $value) {
            $bidang_list[$value['id']] = $value;
        }
        return $bidang_list;
    }

    function get_sub_bidang_report(){
        $this->db->select('*');
            // ->where('tb_sub_bidang.del',0);

        $query  = $this->db->get('tb_sub_bidang')->result_array();

        $sub_bidang_list    =   array();
        foreach ($query as $key => $value) {
            $sub_bidang_list[$value['id']] = $value;
        }
        return $sub_bidang_list;
    }

    public function getCekDataBsb($id)
    {
        $query = "SELECT 
                    a.name bidang, 
                    b.name sub_bidang 
                  FROM 
                    ms_iu_bsb c 
                  LEFT JOIN 
                    tb_bidang a ON a.id=c.id_bidang
                  LEFT JOIN
                    tb_sub_bidang b ON b.id=c.id_sub_bidang
                  WHERE 
                    c.id_ijin_usaha = ".$id;
        $query = $this->db->query($query)->result_array();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;

    }

    function get_data_by_vendor($id,$type) 
    {
        $this->db->select('*');
        $this->db->where('del',0);
        $this->db->where('id_vendor',$id);
        $this->db->where('type',$type);
        
        
        $query = $this->db->get('ms_ijin_usaha');       
        return $query;
    }

}
