<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Administrasi_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

	function getData($form,$id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT 
						a.id,
						a.id_sbu, 
						a.name as name,
						a.npwp_code as npwp_code, 
						b.id_legal,
						b.npwp_file, 
						b.npwp_date,
						b.nppkp_code, 
						b.nppkp_file, 
						b.nppkp_date, 
						b.vendor_office_status, 
						b.vendor_address, 
						b.vendor_country, 
						b.vendor_phone, 
						b.vendor_province, 
						b.vendor_fax, 
						b.vendor_city, 
						b.vendor_email, 
						b.vendor_postal, 
						b.vendor_website, 
						c.name as sbu_name, 
						d.name as legal_name,
						b.data_status
				 FROM 
						ms_vendor a
				 LEFT JOIN
						ms_vendor_admistrasi b ON b.id_vendor=a.id
				 LEFT JOIN
						tb_sbu c ON c.id=a.id_sbu
				 LEFT JOIN
						tb_legal d ON d.id=b.id_legal
				 WHERE 
				   		a.id = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$user =$this->session->userdata('user');

		$query = "	SELECT 
						ms_vendor.id id,
						id_sbu,
						id_legal, 
						ms_vendor.name as name,
						ms_vendor.npwp_code as npwp_code, 
						npwp_file, 
						npwp_date,
						nppkp_code, 
						nppkp_file, 
						nppkp_date, 
						vendor_office_status, 
						vendor_address, 
						vendor_country, 
						vendor_phone, 
						vendor_province, 
						vendor_fax, 
						vendor_city, 
						vendor_email, 
						vendor_postal, 
						vendor_website,
						is_nppkp, 
						tb_sbu.name as sbu_name, 
						tb_legal.name as legal_name,
						email_pic,
						data_status
				   FROM 
				   		ms_vendor
				   LEFT JOIN
				   		ms_vendor_admistrasi as mva ON mva.id_vendor=ms_vendor.id
				   LEFT JOIN
				   		tb_sbu ON tb_sbu.id=ms_vendor.id_sbu
				   LEFT JOIN
				   		tb_legal ON tb_legal.id=mva.id_legal
				   WHERE 
				   		ms_vendor.id = ?";

		$query = $this->db->query($query, array($user['id_user']));
		return $query->row_array();

	}

	function update($id, $data){
			$save_administrasi = array(
				'id_legal'   => $data['id_legal'],
				'npwp_code'  => $data['npwp_code'],
				'npwp_date'  => $data['npwp_date'],

'npwp_file'  => $data['npwp_file'],
				'nppkp_code' => $data['nppkp_code'],
				'nppkp_date' => $data['nppkp_date'],
				'nppkp_file' => $data['nppkp_file'],
				'vendor_office_status' => $data['vendor_office_status'],
				'vendor_address' => $data['vendor_address'],
				'vendor_country' => $data['vendor_country'],
				'vendor_province'=> $data['vendor_province'],
				'vendor_city'  => $data['vendor_city'],
				'vendor_phone' => $data['vendor_phone'],
				'vendor_fax'   => $data['vendor_fax'],
				'vendor_email' => $data['vendor_email'],
				'vendor_postal'=> $data['vendor_postal'],
				'vendor_website' =>$data['vendor_website'],
				'email_pic' => $data['email_pic'],
				// 'vendor_norek' => $data['vendor_norek'],
				// 'vendor_bank' => $data['vendor_bank'],
				// 'vendor_file' => $data['vendor_file']
			);

			$save_login = array(
				'username'   => $data['vendor_email'],
				'edit_stamp' => $data['edit_stamp']
			);

			$this->db->where('id_vendor',$id)
					 ->update('ms_vendor_admistrasi',$save_administrasi);

			$this->db->where('id',$id)
					 ->update('ms_vendor',array(
					 	'npwp_code'  => $data['npwp_code'],
					 	'name' 		 => $data['name'],
					 	'edit_stamp' => $data['edit_stamp']
					 ));
			// print_r($this->db->last_query());die;

			return $this->db->where('id_user',$id)
					 		  ->update('ms_login',$save_login);
			
	}

	public function get_bank($id_vendor)
	{
		$query = "SELECT 
					no, 
					name,
					bank_name,
					bank_file,
					id 
				  FROM 
				  	ms_rekening
				  WHERE 
				  	del = 0 AND id_vendor =".$id_vendor;
		return $query;
	}

	public function insert_bank($data)
	{
		return $this->db->insert('ms_rekening',$data);
	}

	public function delete_bank($id)
	{
		return $this->db->where('id',$id)->update('ms_rekening',array('del'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
	}
	
}
