<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pass_change_model extends MY_Model{
	public $table = 'ms_login';
	function __construct(){
		parent::__construct();
	}

	function selectData($id){
		$user = $this->session->userdata('user');
		$query = "	SELECT
						a.password
					FROM 
						".$this->table." a 
					WHERE 
						a.del = 0 AND a.id_user = ? ";

		$query = $this->db->query($query, array($user['id_user']));
		return $query->row_array();

	}

	function getCurrentPassword($user_id)
	{
		$query = $this->db->where('id_user',$user_id)
						  ->get('ms_login');
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}

    function change_password($id,$data,$new_password){
		$this->db->where('id_user',$id);
		$res = $this->db->update('ms_login',array(
				'password_raw'=>$data['new_password'],
				'password'=>do_hash($data['new_password'],'sha1')
			));
		
		return $res;
	}
	
}
