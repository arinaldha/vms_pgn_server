<?php 
/**
 * 
 */
class Blacklist_model extends MY_Model
{
	public function get_blacklist_vendor()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
		 			a.name,
		 			b.remark,
		 			b.start_date,
		 			b.end_date,
		 			b.blacklist_file,
		 			a.id id_vendor, 
		 			b.id,
		 			c.npwp_code
		 		FROM 
		 			tr_blacklist b 
		 		INNER JOIN
		 			ms_vendor a ON a.id=b.id_vendor
		 		LEFT JOIN
		 			ms_vendor_admistrasi c ON c.id_vendor=a.id
		 		WHERE 
		 			b.del = 0 ";//AND a.id_sbu = ".$admin['id_sbu'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function select_by_vendor($id)
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
		 			a.name,
		 			a.id id_vendor,
		 			b.start_date,
		 			b.end_date,
		 			b.remark,
		 			b.blacklist_file, 
		 			b.id
		 		FROM 
		 			tr_blacklist b 
		 		INNER JOIN
		 			ms_vendor a ON a.id=b.id_vendor
		 		WHERE 
		 			b.id = ? ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	public function get_blacklist_nik($form)
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
		 			a.nik, 
		 			b.name nama_pengurus, 
		 			c.name vendor_name,
		 			a.remark, 
		 			a.start_date, 
		 			a.end_date, 
		 			a.blacklist_file, 
		 			a.id id,
		 			b.id id_pengurus,
		 			d.npwp_code
		 		FROM 
		 			tr_blacklist_nik a
		 		LEFT JOIN
		 			ms_pengurus b ON b.no=a.nik
		 		INNER JOIN
		 			ms_vendor c ON c.id=b.id_vendor
		 		LEFT JOIN 
		 			ms_vendor_admistrasi d ON d.id_vendor=c.id
		 		WHERE 

		 			a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	public function select_by_nik($id)
	{
		$admin = $this->session->userdata('admin');
		$query = "
		        SELECT 
		 			a.id id, 
		 			b.name nama_pengurus, 
		 			a.nik, 
		 			c.name vendor_name,
		 			b.id id_pengurus, 
		 			a.remark, 
		 			a.start_date, 
		 			a.end_date, 
		 			a.blacklist_file
		 		FROM 
		 			tr_blacklist_nik a
		 		LEFT JOIN
		 			ms_pengurus b ON b.no=a.nik
		 		INNER JOIN
		 			ms_vendor c ON c.id=b.id_vendor
		 		WHERE 
		 			a.id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function edit_by_nik($data,$id){
		$this->db->where('id',$id);

		$result = $this->db->update('tr_blacklist_nik',$data);
		if($result)return $id;
	}
	public function aktif_by_nik($id)
	{
		$res = $this->db->where('id',$id)
				->update('tr_blacklist_nik',array(
											'del'		=> 1,
											'edit_stamp'	=> date('Y-m-d H:i:s')
						)
			);
		return $res;
	}
	public function edit_by_vendor($id,$data)
	{
		$this->db->where('id',$id);
		$result = $this->db->update('tr_blacklist',$data);

		$getVendorId = "SELECT id_vendor FROM tr_blacklist WHERE id = ?";
		$getVendorId = $this->db->query($getVendorId, array($id));

		$pengurusNIK = $this->get_nik_from_vendor($getVendorId->row_array()['id_vendor']);
		$dataPengurus = array(
							'start_date'	=>	$data['start_date'],
							'end_date'		=>	$data['end_date'],
							'remark'		=>	$data['remark'],
							'blacklist_file'=>	$data['blacklist_file']
						);
		foreach ($pengurusNIK->result_array() as $key => $value) {
			$this->db->where('nik',$value['no'])->update('tr_blacklist_nik',$dataPengurus);
		}
		if($result)return $id;
	}

	public function get_nik_from_vendor($id_vendor)
	{
		$queryNIK = "SELECT no FROM ms_pengurus WHERE id_vendor = ?";
		$queryNIK = $this->db->query($queryNIK, array($id_vendor));
		return $queryNIK;
	}

	public function aktif_by_vendor($data)
	{
		$data_blacklist = $this->db->where('id',$data['id'])->get('tr_blacklist')->row_array();
		
		$sql = "UPDATE `ms_vendor` SET  `is_active` = 1, `vendor_status` = 1 WHERE id = ? ";
		$this->db->query($sql,array($data_blacklist['id_vendor']));

		$res = $this->db->where('id',$data['id'])
				->update('tr_blacklist',array(
											'del'		=> 1,
											'white_date' 	=> $data['white_date'],
											'white_file'	=> $data['white_file'],
											'edit_stamp'	=> $data['edit_stamp']
										)
						);
		return $res;
	}

	function search_vendor(){
		$query = "	SELECT
		                id,
		                name
					FROM ms_vendor
					WHERE del = 0 AND vendor_status = 2 AND name LIKE ? ";

	    $query = $this->db->query($query, array('%'.$_POST['search'].'%',))->result_array();
	    // echo $this->db->last_query();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['name'];
	    }
	    return $data;
	}

	public function simpan_by_vendor($data)
	{
		$_param = array();
		$this->db->where('id_vendor',$data['id_vendor'])->update('tr_blacklist',array('del'=>1));
		$this->db->query("UPDATE `ms_vendor` SET `ever_blacklisted` = `ever_blacklisted` + 1 , `is_active` = 0, `vendor_status` = 3 WHERE id =".$data['id_vendor']);
		$this->db->insert('tr_blacklist',array(
			'id_vendor' 	 => $data['id_vendor'],
			'id_procurement' => $data['id_procurement'],
			'start_date' 	 => $data['start_date'],
			'end_date' 		 => $data['end_date'],
			'remark' 		 => $data['remark'],
			'blacklist_file' => $data['blacklist_file'],
			'entry_stamp' 	 => $data['entry_stamp']
		));

		// "INSERT INTO tr_blacklist (
		// 					id_vendor,
		// 					id_procurement,
		// 					start_date,
		// 					end_date,
		// 					remark,
		// 					blacklist_file,
		// 					entry_stamp
		// 					) 
		// 		VALUES (?,?,?,?,?,?,?) ";
		
		
		// foreach($this->field_master as $_param) $param[$_param] = $data[$_param];
		
		// $this->db->query($sql, $param);


		$queryNIK = $this->get_nik_from_vendor($data['id_vendor']);
		foreach($queryNIK->result_array() as $value){
			$data = array(
							'nik' 			=> 	$value['no'],
							'start_date'	=>	$data['start_date'],
							'end_date'		=>	$data['end_date'],
							'remark'		=>	$data['remark'],
							'blacklist_file'=>	$data['blacklist_file']
						);
			$this->simpan_by_nik($data);
		}
		$id = $this->db->insert_id();
		
		return $id;
	}

	function simpan_by_nik($data){
		$this->db->insert('tr_blacklist_nik',$data);
		$id = $this->db->insert_id();
		return $id;
	}
	function search_by_nik(){
		$result = array();
		$admin = $this->session->userdata('admin');		
		$query = "SELECT a.no, a.name, b.name vendor_name
					FROM ms_pengurus a ";
		if($admin['id_role']==1){
			$query .= "LEFT JOIN ms_vendor b ON b.id = a.id_vendor 
						WHERE	a.no LIKE '%".$_POST['search']."%' 
							AND (a.del = 0 OR a.del IS NULL) 
							AND NOT EXISTS(
								SELECT 1 FROM tr_blacklist_nik c WHERE c.nik = a.no AND (c.del = 0 OR c.del IS NULL)
							)
							LIMIT 0,5"; 
		}else{
			$query .= "	LEFT JOIN ms_vendor b ON b.id = a.id_vendor 
						WHERE
							a.no LIKE '%".$_POST['search']."%'
						AND b.id_sbu = ".$admin['id_sbu']." 
						AND (a.del = 0 OR a.del IS NULL) 
						AND NOT EXISTS(
							SELECT 1 FROM tr_blacklist_nik c WHERE c.nik = a.no AND (c.del = 0 OR c.del IS NULL)
						)
						LIMIT 0,5"; 
		}
					
		// echo $query;
		$query = $this->db->query($query);

		foreach($query->result_array() as $key => $value){
			$result[$value['no']] = $value['no'].' - '.$value['name'].' - '.$value['vendor_name']; 
		}
		
		return $result;
	}

	public function report_nik()
	{
		$query = "SELECT 
		 			a.nik, 
		 			b.name nama_pengurus, 
		 			c.name vendor_name,
		 			a.remark, 
		 			a.start_date, 
		 			a.end_date, 
		 			a.blacklist_file, 
		 			a.id id,
		 			b.id id_pengurus,
		 			d.npwp_code
		 		FROM 
		 			tr_blacklist_nik a
		 		LEFT JOIN
		 			ms_pengurus b ON b.no=a.nik
		 		INNER JOIN
		 			ms_vendor c ON c.id=b.id_vendor
		 		LEFT JOIN 
		 			ms_vendor_admistrasi d ON d.id_vendor=c.id
		 		WHERE 
		 			a.del = 0";
		$query = $this->db->query($query);
		return $query->result_array();
	}

	public function report_vendor()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
		 			a.name,
		 			b.remark,
		 			b.start_date,
		 			b.end_date,
		 			b.blacklist_file,
		 			a.id id_vendor, 
		 			b.id,
		 			c.npwp_code
		 		FROM 
		 			tr_blacklist b 
		 		INNER JOIN
		 			ms_vendor a ON a.id=b.id_vendor
		 		LEFT JOIN
		 			ms_vendor_admistrasi c ON c.id_vendor=a.id
		 		WHERE 
		 			b.del = 0 AND a.id_sbu = ".$admin['id_sbu'];
		$query = $this->db->query($query);
		return $query->result_array();
	}

	public function end_blacklist()
	{
		$get_date = $this->db->select('id_vendor')->where('end_date',date('Y-m-d'))->get('tr_blacklist');
		foreach ($get_date->result() as $key) {
			$this->db->where('id',$key->id_vendor)->update('ms_vendor',array('vendor_status'=>1));
			$this->db->where('id_vendor',$key->id_vendor)->update('tr_blacklist',array('del'=>1));
		}
	}
}