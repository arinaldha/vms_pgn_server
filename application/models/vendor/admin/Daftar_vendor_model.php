<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_vendor_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.name as name, 
						b.name as legal_name, 
						c.username, 
						c.password_raw,
						a.id id,
						c.password,
						d.npwp_code,
						d.entry_stamp
				   FROM 
				   		".$this->table." a
				   LEFT JOIN
				   		ms_vendor_admistrasi d ON d.id_vendor = a.id
				   LEFT JOIN
				   		ms_login c ON c.id_user = a.id
				   LEFT JOIN 
				   		tb_legal b ON b.id = d.id_legal
				   WHERE 
				   		a.del is NULL OR a.del = 0 ";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
						a.name as name, 
						b.name as legal_name, 
						c.username,  
						c.password_raw,
						a.id id, 
						c.password
				   FROM 
				   		".$this->table." a
				   LEFT JOIN
				   		ms_vendor_admistrasi d ON d.id_vendor = a.id
				   LEFT JOIN
				   		ms_login c ON c.id_user = a.id
				   LEFT JOIN 
				   		tb_legal b ON b.id = d.id_legal
				   WHERE 
				   		a.del is NULL OR a.del = 0 ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function getDaftarVendor(){
		$this->db->select('ms_vendor.id id, ms_vendor.name name, tb_legal.name legal_name, ms_login.username, ms_login.password,  ms_login.password_raw');
		$this->db->where('(ms_vendor.del IS NULL OR ms_vendor.del = 0)');
		
		$this->db->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor = ms_vendor.id','LEFT');
		$this->db->join('ms_login','ms_login.id_user = ms_vendor.id','LEFT');
		$this->db->join('tb_legal','tb_legal.id = ms_vendor_admistrasi.id_legal','LEFT');
		$query = $this->db->get('ms_vendor');
		return $query->result_array();

	}
	
}
