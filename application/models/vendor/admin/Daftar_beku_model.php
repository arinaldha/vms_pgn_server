<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Daftar_beku_model extends MY_Model
{
	public function getData($form)
	{
		$query = "SELECT 
						b.name legal_name,
						a.name vendor_name,
						a.npwp_code,
						a.edit_stamp,
						a.id
				  FROM 
				  		ms_vendor a
				  LEFT JOIN 
				  		ms_vendor_admistrasi as mva ON mva.id_vendor=a.id 
				  LEFT JOIN
				  		tb_legal b ON b.id=mva.id_legal
				  WHERE 
				  		a.del = 0 AND a.vendor_status = 4 ";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function proses_beku() 
	{
		$date = date('Y-m-d H:i:s',strtotime('-2 Years'));

		$this->db->where('`vendor_status`!=','4');
		$query_beku = $this->db->get('ms_vendor');
		$prevDate = date('Y-m-d',strtotime(date('Y-m-d').'-2 Years'));
		
		$query = "UPDATE ms_vendor SET vendor_status = 4, is_active = 0 WHERE DATE(edit_stamp) <= DATE('".$prevDate."') AND (vendor_status != 3 AND vendor_status != 4)";
		$this->db->query($query);
	}
}