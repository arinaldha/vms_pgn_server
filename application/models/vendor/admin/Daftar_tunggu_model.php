<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_tunggu_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}


	function getDaftarTunggu($status){

		$admin = $this->session->userdata('admin');
		$query = "SELECT 
						b.name legal_name,
						a.name name,        
						a.edit_stamp last_update,
						a.id as id,
						a.is_new,
						mva.npwp_code  
						       
				  FROM           
				  		ms_vendor a        
				  LEFT JOIN          
				  		ms_vendor_admistrasi mva ON mva.id_vendor=a.id       
				  LEFT JOIN          
				  		tb_sbu c ON c.id=a.id_sbu        
				  LEFT JOIN          
				  		tb_legal b ON b.id=mva.id_legal   
				  WHERE           
				  		a.vendor_status = 1 AND a.is_active = 1 
				  		AND a.is_new = $status";
		if($admin['id_role']==8){
			$query .=" AND a.need_approve = 1 AND id_sbu = ".$admin['id_sbu'];
		}
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
						a.*,
						a.name as name,  
						c.name as id_sbu
				  FROM           
				  		ms_vendor a     
				  LEFT JOIN          
				  		tb_sbu c ON c.id=a.id_sbu        
				  WHERE           
				  		 a.id = ? ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function update($id, $data){
		return $this->db->where('id',$id)->update('ms_vendor',$data);
	}

	function getDaftarTungguDashboard(){

		$admin = $this->session->userdata('admin');
		$query = "SELECT 
						a.name name,
						b.name legal_name,
						a.edit_stamp last_update,
						a.id as id,
						a.is_new,        
						d.ba_file,
						mva.npwp_code  
						       
				  FROM           
				  		ms_vendor a        
				  LEFT JOIN          
				  		ms_vendor_admistrasi mva ON mva.id_vendor=a.id       
				  LEFT JOIN          
				  		tb_sbu c ON c.id=a.id_sbu        
				  LEFT JOIN          
				  		tb_legal b ON b.id=mva.id_legal       
				  LEFT JOIN          
				 		ms_ba d ON d.id_vendor=a.id        
				  WHERE           
				  		a.vendor_status = 1 AND a.is_active = 1";
		if($admin['id_role']==8){
			$query .=" AND a.need_approve = 1 AND id_sbu = ".$admin['id_sbu'];
		}
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
}
