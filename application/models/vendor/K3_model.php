<?php defined('BASEPATH') OR exit('No direct script access allowed');

class K3_model extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/admin/blacklist_model','bm');
	}

	function get_master_header(){
		$result = array();
		$res = $this->db->get('tb_ms_quest_k3')->result_array();
		foreach($res as $key_sub => $value_sub){
			$result[$value_sub['id']] = $value_sub['question'];
		}
		return $result;
	}
	function is_konstruksi($id_vendor){
		$query = 	" 	SELECT 1 FROM ms_ijin_usaha 
						WHERE (id_dpt_type = 4 OR id_dpt_type = 5) 
						AND id_vendor = ? 
						AND (del = 0 OR del IS NULL)
						LIMIT 0,1
					";
		return $this->db->query($query,array($id_vendor))->num_rows();
	}
	function get_sub_header(){
		$result = array();
		$res = $this->db->get('tb_sub_quest_k3')->result_array();
		foreach($res as $key_sub => $value_sub){
			$result[$value_sub['id']] = $value_sub;
		}
		return $result;
	}
	function get_quest(){
		$res = $this->db->get('tb_quest')->result_array();
		$result = array();

		foreach($res as $key =>$row){
			$quest = $this->db->select('*')->where('id_question',$row['id'])->get('ms_answer_hse')->result_array();
			foreach($quest as $id => $quest){
				$result[$row['id_ms_header']][$row['id_sub_header']][$row['id']][$quest['id']] = $quest;
			}
		}
		return $result;
	}
	function get_k3_data($id_vendor){
		$get = $this->db->where('id_vendor',$id_vendor)->get('tr_answer_hse')->result_array();
		$result = array();
		foreach ($get as $key => $value) {
			$result[$value['id_answer']] = $value;
		}

		return $result;
	}
	function get_penilaian_value($id_vendor){
		$get = $this->db->select('id_evaluasi,poin')->where('id_vendor',$id_vendor)->get('tr_evaluasi_poin')->result_array();
		$result = array();
		foreach ($get as $key => $value) {
			$result[$value['id_evaluasi']] = $value['poin'];
		}

		return $result;
	}
	function get_field_quest(){
		$quest = $this->db->select('*,ms_answer_hse.id id,')->join('tb_quest','tb_quest.id=ms_answer_hse.id_question')->get('ms_answer_hse')->result_array();
		foreach($quest as $id => $quest){
			$result[$quest['id']] = $quest;
		}
		
		return $result;
	}
	function save_k3_data($post,$id_user){
		$this->db->delete('tr_answer_hse',array('id_vendor'=>$id_user,''));
		foreach($post as $id_question => $data){
			$res = $this->db->insert('tr_answer_hse',array('id_answer'=>$id_question,'value'=>$data,'id_vendor'=>$id_user));
			if(!$res){
				return false;
			}
		}
	}
	function get_k3_dataform($id){
		$query = "SELECT * FROM ms_csms WHERE id_vendor = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function update_k3_data($post,$id_user){
		foreach($post as $id_question => $data){
			$nr = $this->db->select('*')->where(array('id_answer'=>$id_question,'id_vendor'=>$id_user))->get('tr_answer_hse')->num_rows();
			if($nr>0){
				$res = $this->db->where(array('id_answer'=>$id_question,'id_vendor'=>$id_user))->update('tr_answer_hse',array('value'=>$data));
			}else{
				$this->db->insert('tr_answer_hse',array('id_answer'=>$id_question,'value'=>$data,'id_vendor'=>$id_user));
			}
			// echo $this->db->last_query();
		}
	}
	function save_csms_data($post,$id){
		// if($this->get_k3_dataform($id)) {

		// 	$arr = array(
		// 				'id_vendor'=>$id,
		// 				'expire_date'=>$post['expire_date'],
		// 				'score'		=>$post['score']
		// 			);
		// 	if(isset($post['csms_file'])){
		// 		$arr['csms_file']= $post['csms_file'];
		// 	}

		// 	$res = $this->db->where('id_vendor',$id)->update('ms_csms',$arr);

		// 	return $res;
		// }else{

			$res = $this->db->insert('ms_csms',array(
											'id_vendor'=>$id,
											'csms_file'=>$post['csms_file'],
											'score'		=>$post['score'],
											'entry_stamp'=>$post['entry_stamp'],
											'expiry_date'=>$post['expiry_date'],
											'category' => $post['category']		
											)
							);

			return $res;
		// }
	}
	public function edit_csms_data($post,$id)
	{
		$res = $this->db->where('id_vendor',$id)->update('ms_csms',array(
											'id_vendor'=>$id,
											'csms_file'=>$post['csms_file'],
											'score'		=>$post['score'],
											'entry_stamp'=>$post['entry_stamp'],
											'expiry_date'=>$post['expiry_date'],
											'category' => $post['category']	
											)
							);

		return $res;
	}
	function save_hse_data($post,$id){
		$res = $this->db->insert('ms_hse',array(
										'id_vendor'=>$id,
										'hse_file'=>$post['hse_file'],
										'entry_stamp'=>$post['entry_stamp']
										)
							);
		return $res;
	}
	function get_k3_vendor($search='', $sort='', $page='', $per_page='',$is_page=FALSE){
	
		$this->db->select('ms_vendor.id as id ,ms_vendor.name name, mva.npwp_code npwp_code,mva.nppkp_code nppkp_code')
		->where('ms_vendor.vendor_status',1)
		->join('ms_vendor_admistrasi as mva','mva.id_vendor=ms_vendor.id','LEFT')
		->group_by('id');
		// if(isset($_POST['filter'])){
		// 	foreach($_POST['filter'] as $key => $row){
		// 		$field = explode('|',$key);
		// 		foreach($row as $value){
		// 			if($value!=''){
		// 				if($field[1] =='start_date'){
		// 					$this->db->where('`'.$field[0].'`.`'.$field[1].'` > "'.$value.'"',NULL,FALSE);
		// 				}elseif($field[1] =='end_date'){
		// 					$this->db->where('`'.$field[0].'`.`start_date` < "'.$value.'"',NULL,FALSE);
		// 				}
		// 				else{
		// 					$this->db->like($field[0].'.'.$field[1],$value,'both');
		// 					if(isset($field[2])){
		// 						$this->db->where($field[0].'.type',$field[2]);
		// 					}
		// 				}
		// 				$this->db->group_by(`'.$field[0].'`.`'.$field[1].'`);
		// 			}
		// 		}
				
		// 	}
		// }

		if($this->input->get('sort')&&$this->input->get('by')){
			$this->db->order_by($this->input->get('by'), $this->input->get('sort')); 
		}
		if($is_page){
			$cur_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
			$this->db->limit($per_page, $per_page*($cur_page - 1));
		}

		$query = $this->db->get('ms_vendor');
		// echo $this->db->last_query();
		return $query->result_array();
	}
	function get_evaluasi_list(){
		$result = array();
		$res = $this->db->get('tb_evaluasi')->result_array();
		foreach($res as $key_sub => $value_sub){
			$result[$value_sub['id']] = $value_sub;
		}
		return $result;
	}
	function get_evaluasi(){
		$result = array();
		
		foreach($this->get_master_header() as $key_ms => $value_ms){

			$evaluasi = $this->db->where('id_ms_quest',$key_ms)->get('tb_evaluasi')->result_array();
			foreach($evaluasi as $key_ev => $data_ev){

				$quests = $this->db->where('id_evaluasi',$data_ev['id'])->get('tb_quest')->result_array();
				// echo print_r($evaluasi);
				foreach($quests as $key_quest => $quest){

					$answer = $this->db->where('id_question',$quest['id'])->get('ms_answer_hse')->result_array();
					$result[$quest['id_ms_header']][$quest['id_evaluasi']][$quest['id']] = $answer;
				}

			}
		}
		// echo print_r($result);
		return $result;
		
	}
	function save_evaluasi_poin($post, $id){
		$base_total = array();
		$vendor_id = $id;
		$this->db->delete('tr_evaluasi_poin',array('id_vendor'=>$id));
		foreach($post as $key => $row){
			$res = $this->db->insert('tr_evaluasi_poin',array(
									'id_evaluasi'=>$key,
									'poin'=>$row,
									'id_vendor'=>$id,
									'entry_stamp'=>date('Y-m-d H:i:s')
									)
					);
			$base_total[$this->get_evaluasi_list()[$key]['id_ms_quest']][] = $row;
			if(!$res){
				return false;
			}
		}
		$sum_value = 0;
		// print_r($base_total);
		foreach($base_total as $id => $evaluasi){
			$total_row = count($evaluasi);
			$total_value = 0;
			foreach($evaluasi as $value){
				$total_value += $value;
			}
			// echo $total_value.' / '.$total_row.'|';
			$sum_value +=($total_value/$total_row);
		}
		// echo $sum_value;
		$nr = $this->db->select('*')->where(array('id_vendor'=>$vendor_id))->get('ms_score_k3')->num_rows();
		if($nr>0){
			$res = $this->db->where('id_vendor',$vendor_id)->update('ms_score_k3',array(
									'score'=>$sum_value,
									'edit_stamp'=>date('Y-m-d H:i:s')
									));
		}else{
			$res = $this->db->insert('ms_score_k3',array(
									'score'=>$sum_value,
									'data_status'=>0,
									'id_vendor'=>$vendor_id,
									'entry_stamp'=>date('Y-m-d H:i:s')
									));
		}
		
		
		// echo $this->db->last_query();
		return true;
	}
	public function get_poin($vendor_id){
		return $this->db->select('*')->where(array('id_vendor'=>$vendor_id))->get('ms_score_k3')->row_array();

	} 
	public function get_csms($id_vendor){
		return $this->db->where('del',0)->where('id_vendor',$id_vendor)->get('ms_csms')->row_array();
	}
	public function get_hse($id_vendor){
		return  $this->db->where('id_vendor',$id_vendor)->get('ms_hse')->row_array();

	}


	function getData($form)
	{
		$user = $this->session->userdata('user');
		$query = "	SELECT
							a.csms_file,
							a.score,
							b.remark,
							a.expiry_date
					FROM 
						ms_csms a
					LEFT JOIN
						tr_blacklist b on b.id_vendor = a.id_vendor
					WHERE a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
}

	public function insert_blacklist($data)
	{
		$_param = array();
		$this->db->where('id_vendor',$data['id_vendor'])->update('tr_blacklist',array('del'=>1));
		$this->db->query("UPDATE `ms_vendor` SET `ever_blacklisted` = `ever_blacklisted` + 1 , `is_active` = 0, `vendor_status` = 4 WHERE id =".$data['id_vendor']);
		$this->db->insert('tr_blacklist',array(
			'id_vendor' 	 => $data['id_vendor'],
			'id_procurement' => $data['id_procurement'],
			'start_date' 	 => date('Y-m-d H:i:s',strtotime('+2 Years')),
			'end_date' 		 => date('Y-m-d H:i:s',strtotime('+2 Years')),
			'remark' 		 => $data['remark'],
			'entry_stamp' 	 => date('Y-m-d H:i:s')
		));

		$queryNIK = $this->bm->get_nik_from_vendor($data['id_vendor']);
		foreach($queryNIK->result_array() as $value){
			$data = array(
							'nik' 			=> 	$value['no'],
							'start_date'	=>	date('Y-m-d H:i:s',strtotime('+2 Years')),
							'end_date'		=>	date('Y-m-d H:i:s',strtotime('+2 Years')),
							'remark'		=>	$data['remark']
						);
			$this->simpan_by_nik($data);
		}
		$id = $this->db->insert_id();
		
		return $id;
	}
}