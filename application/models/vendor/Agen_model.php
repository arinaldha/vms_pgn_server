<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Agen_model extends MY_Model{
	public $table = 'ms_agen';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user =$this->session->userdata('user');
		$query = "SELECT
						a.no,
						a.issue_date,
						a.type,
						a.expire_date,
						a.agen_file,
						a.id
				  FROM  
				  		".$this->table." a
				  WHERE 
				  		a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function getDataProduk($id){
		$user =$this->session->userdata('user');
		$query = "SELECT
						 produk,
						 merk,
						 id
				  FROM  
				  		 ms_agen_produk
				  WHERE 
				  		del = 0 AND id_agen = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	function getDataBsb($id){
		$user =$this->session->userdata('user');
		$query = "SELECT 
						b.name bidang_name, 
    					c.name sub_bidang_name,
   						a.id
				  FROM
    					ms_agen_bsb a
        		  LEFT JOIN
    					tb_bidang b ON b.id = a.id_bidang
				  LEFT JOIN
    					tb_sub_bidang c ON c.id = a.id_bsb
				  WHERE
    					id_agen = ".$id." AND a.del = 0";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}


	function selectData($id){
		$user =$this->session->userdata('user');

		$query = "	SELECT
						a.*
				  FROM  
				  		".$this->table." a
				  WHERE 
				  		a.del = 0 AND a.id = ? ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function selectDataProduk($id){

		$query = "	SELECT
						a.*
				  FROM  
				  		ms_agen_produk a
				  WHERE 
				  		a.del = 0 AND a.id = ? ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function selectDataBsb($id){
		$query = "SELECT
						 a.*,
						 b.name as bidang_name,
						 c.name as sub_bidang_name
                         
                  FROM  
				  		 ms_agen_bsb a
				  INNER JOIN
				  		 tb_sub_bidang c ON c.id=a.id_bsb
				  INNER JOIN   
				  		 tb_bidang b ON b.id=c.id_bidang
				  WHERE 
				  		 a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function insert_produk($data){
		return $this->db->insert('ms_agen_produk',$data);
	}

	public function insert_bsb($data){
		return $this->db->insert('ms_agen_bsb',$data);
	}

	public function update_produk($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('ms_agen_produk',$data);
	}

	public function update_bsb($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('ms_agen_bsb',$data);
	}

	public function delete_produk($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('ms_agen_produk',array('del' => 1));
	}

	public function delete_bsb($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('ms_agen_bsb',array('del' => 1));
	}

	public function getBidangAgen($form){
		$user =$this->session->userdata('user');
		$query = "	SELECT
						a.id,
						name
						FROM tb_bidang a 
						JOIN ms_iu_bsb b ON a.id = b.id_bidang 
						JOIN ms_ijin_usaha c ON b.id_ijin_usaha = c.id 
						WHERE b.id_vendor = ? 
						AND b.del = 0 
						AND c.del = 0 
						GROUP BY a.id ";

		$query = $this->db->query($query, array($user['id_user']))->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}
	public function getSubBidangAgen($form){
		$user =$this->session->userdata('user');
		$query = "	SELECT
						a.id,
						name

						FROM tb_sub_bidang a JOIN ms_iu_bsb b ON a.id = b.id_sub_bidang JOIN ms_ijin_usaha c ON b.id_ijin_usaha = c.id  
						WHERE b.id_vendor = ? 
						AND b.del = 0 
						AND c.del = 0 
						GROUP BY a.id ";

		$query = $this->db->query($query, array($user['id_user']))->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}
	
}