<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengurus_model extends MY_Model{
	public $table = 'ms_pengurus';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
        $user = $this->session->userdata('user');
		$query = "	SELECT 
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        b.no no_akta,
                        a.pengurus_file,
                        a.id 
                   FROM 
                        ".$this->table." a
                   INNER JOIN
                        ms_akta b ON b.id = a.id_akta
                   WHERE 
                        a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                        a.*,
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        b.no no_akta,
                        a.pengurus_file,
                        a.remark
                   FROM 
                        ".$this->table." a
                   INNER JOIN
                        ms_akta b ON b.id = a.id_akta
                   WHERE 
                       a.id = ? AND a.del=0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

    function get_akta_data($id){
        return $this->db->where('id',$id)->order_by('issue_date', 'desc')->get('ms_akta')->row_array();
    }

  function get_data_by_vendor($id_vendor){
      $query = "SELECT * FROM ms_pengurus WHERE id_vendor = ? AND (del=0 OR del IS NULL)";
      $query = $this->db->query($query,array($id_vendor));
      return $query;
    }
}
