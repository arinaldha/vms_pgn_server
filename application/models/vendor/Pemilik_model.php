<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pemilik_model extends MY_Model{
	public $table = 'ms_pemilik';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.name,
						a.position,
						a.percentage,
						a.id
				   FROM 
				   		".$this->table." a
				   
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){
		$query = "	SELECT 

						a.name,
						a.position,
						a.percentage,
						a.id_akta

				   FROM 
				   		".$this->table." a
				   
				   WHERE 

				   		a.del = 0 AND a.id = ? ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
	
	function get_total_percentage($id=0){
    	$user = $this->session->userdata('user');

    	$res = $this->db->select('SUM(percentage) as sum')->where('del',0)->where('id !=',$id)->where('id_vendor',$user['id_user'])->get('ms_pemilik')->row_array();
    	// echo $this->db->last_query();
    	return $res['sum'];
    }
}
