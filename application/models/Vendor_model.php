<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Vendor_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

	function getDataDpt($form){

		$admin = $this->session->userdata('admin');

		$query = "SELECT  
					    a.name name, 
					    b.npwp_code npwp_code,
					    b.nppkp_code nppkp_code, 
					    c.name sbu_name,
					    h.name legal_name,
						a.id as id, 
					    b.vendor_city, 
					    b.vendor_province, 
					    a.id msv_id,
					    a.id as id_vendor_list,
						b.vendor_office_status,
					    b.id_legal as id_legal,
					       -- CONCAT(i.name, '-'  ,j.name) as bsb,
					    b.vendor_type,
						f.id_bidang,
						f.id_sub_bidang,
						k.id id_agen,
						m.id id_ijin_usaha,
						l.id_agen agen_id,
						l.merk
				   FROM 
				   		ms_vendor a
				   LEFT JOIN
				   		ms_agen k ON k.id_vendor=a.id
				   LEFT JOIN
				   		ms_agen_produk l ON l.id_agen=k.id
				   LEFT JOIN
				   		ms_iu_bsb f ON f.id_vendor=a.id
				   LEFT JOIN
				   		tr_dpt g ON g.id_vendor=a.id
				   LEFT JOIN 
				   		ms_vendor_admistrasi b ON b.id_vendor=a.id
				   LEFT JOIN
				   		tb_legal h ON h.id=b.id_legal
				   LEFT JOIN
				   		tb_sbu c ON c.id=a.id_sbu
				   -- LEFT JOIN
				   -- 		tb_bidang i ON i.id=f.id_bidang
				   -- LEFT JOIN
				   -- 		tb_sub_bidang j ON j.id_bidang=i.id
				   	LEFT JOIN 
				   		ms_ijin_usaha m ON m.id_vendor = a.id
				   	LEFT JOIN
						tr_dpt n ON n.id_vendor = a.id AND n.status = 1 
				   	WHERE 
				   		a.del = 0 AND a.vendor_status = 2 AND a.is_active = 1 AND NOT EXISTS(SELECT 1 FROM tr_blacklist d WHERE d.id_vendor = a.id AND (d.del = 0))";
				

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}

		$query .=" GROUP BY 
				   		a.id, b.npwp_code";

		//$query .=" GROUP BY a.id ";

				   		// echo $query;
		return $query;

	}

	function getDataDaftar($form){
		$admin = $this->session->userdata('admin');

		$query = "SELECT 
						a.id id, 
						a.name name, 
						b.name legal_name, 
						c.username, 
						c.password,  
						c.password_raw
				   FROM 
				   		".$this->table." a
				   LEFT JOIN 
				   		ms_login c ON c.id_user = a.id
				   LEFT JOIN
				   		tb_legal b ON b.id = ms_vendor_admistrasi.id_legal
				   WHERE 
				   		a.del IS NULL OR a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function getDataTunggu($form){
		$query = "SELECT
						ms_vendor.id as id,
						tb_legal.name legal_name,
						ms_vendor.name name, 
						ms_vendor.edit_stamp last_update, 
						is_new, 
						ba_file

				  FROM 
				  		ms_vendor

				  WHERE 
				  		ms_vendor.vendor_status = 1

				  LEFT JOIN
				  		ms_vendor_admistrasi as mva ON mva.id_vendor=ms_vendor.id

				  LEFT JOIN 
				  		tb_sbu ON tb_sbu.id=ms_vendor.id_sbu

				  LEFT JOIN
				  		tb_legal ON tb_legal.id=mva.id_legal

				  LEFT JOIN
				  		ms_ba ON ms_ba.id_vendor=ms_vendor.id

				  WHERE
				  		ms_vendor.is_active = 1

				  WHERE 
				  		vendor_status = 1
				  		";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	function selectData($id){

		$query = "	SELECT
						title,
						publisher
					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function check_pic($id){
		$this->db->select('*')->where('ms_vendor_pic.id_vendor',$id);
		
		$query = $this->db->get('ms_vendor_pic');

		return $query->num_rows();
	}

	function get_data_pic($id){
		$user = $this->session->userdata('user');
		$this->db->select('*')
		->where('id_vendor',$id);
		$query = $this->db->get('ms_vendor_pic');
		return $query->row_array();
	}

	function save_pic($data){
		$insert_data = $this->db->insert('ms_vendor_pic',$data);
		return $insert_data;
	}

	public function get_pt(){
		$this->db->select('tb_legal.name legal_name,ms_vendor.name name')
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor = ms_vendor.id')
		->join('tb_legal','tb_legal.id=ms_vendor_admistrasi.id_legal')
		->where('vendor_status',1);
		$query = $this->db->get('ms_vendor');
		return $query->row_array();
	}

	function to_waiting_list(){
		$user = $this->session->userdata('user');
		
		// if($this->utility->is_konstruksi($user['id_user'])){

		// 	$query_hse 	= "SELECT email, id_sbu, id from ms_admin WHERE id_role = 2 AND id_sbu = ?";
		// 	$query_hse 	= $this->db->query($query_hse,array($user['id_sbu']));

		// 	$message 	= "Tolong Upload CSMS untuk vendor ".$user['name']."";
		// 	$subject_email = "Permintaan Upload Data CSMS Vendor ".$user['name']." Sistem Manajemen Penyedia Barang & Jasa PT Perusahaan Gas Negara";
		// 	$message_email = "Harap untuk login ke sistem dan segera upload data CSMS untuk vendor ".$user['name'].".
		// 	Terimakasih.

		// 	PT Perusahaan Gas Negara
		// 	";
		// 	$this->utility->set_notification_admin($user['id_sbu'],2,$message);
		// 	foreach($query_hse->result_array() as $key => $value) {
				
		// 		$this->utility->send_notification_admin($value['id'],$subject_email, $message_email);
		// 	}
			
		// }
		$get_pemilik = "SELECT SUM(a.percentage) total_percentage FROM ms_pemilik a WHERE a.del = 0 AND a.id_vendor = ".$user['id_user'];
		$queryPemilik = $this->db->query($get_pemilik)->row_array();
		$getPengurus = $this->db->where('id_vendor',$user['id_user'])->where('del',0)->get('ms_pengurus');

		if ($queryPemilik['total_percentage'] < 100 || $queryPemilik['total_percentage'] > 100)
		{
			return array('status'=>false,'message'=>'Total kepemilikan harus 100% !');
		}/*else {
			return array('status'=>true);

		}*/
		if ($getPengurus->num_rows() < 1) {
			return array('status'=>false,'message'=>'Harus memiliki pengurus !');
		}

		$query = $this->db->select('*')
				 		  ->join('ms_pengalaman','ms_pengalaman.id_ijin_usaha=ms_ijin_usaha.id')
				 		  ->where('ms_ijin_usaha.id_vendor',$user['id_user'])
				 		  ->get('ms_ijin_usaha');
		if ($query->num_rows() > 0) {

			$this->db->where('id',$user['id_user']);
			$res = $this->db->update('ms_vendor',array(
				'vendor_status'=>1,
				'is_new'=>1
				)
			);
		
			return array('status'=>true);

		} else {
			return array('status'=>false,'message'=>'Anda harus memiliki minimal 1 pengalaman dengan sub-bidang yang telah diinput!');
		}
		
	}

	public function get_administrasi_list($id){

		$this->db->select('*, ms_vendor_pic.pic_address pic_address, ms_vendor.id as id ,ms_vendor.name name, mva.npwp_code npwp_code,mva.nppkp_code nppkp_code,mva.nppkp_date nppkp_date, tb_legal.name id_legal, mva.vendor_address ,ms_vendor_pic.pic_name pic_name, mva.npwp_date npwp_date,mva.vendor_office_status kantor, mva.vendor_country country, mva.vendor_province province, mva.vendor_city city, mva.vendor_phone phone, mva.vendor_fax fax, mva.vendor_email email, mva.vendor_website website, ms_akta.no no, ms_akta.notaris notaris, ms_akta.issue_date issue_date, ms_akta.authorize_by authorize_by, ms_akta.authorize_no authorize_no, ms_akta.authorize_date authorize_date, ms_pengurus.name name_pengurus, ms_pengurus.no no_ktp, ms_pengurus.position_expire exp,ms_pengurus.position pos')
		->where('mva.id_vendor',$id)
		->join('ms_vendor_admistrasi as mva','mva.id_vendor=ms_vendor.id','LEFT')
		->join('ms_vendor_pic','ms_vendor_pic.id_vendor=mva.id_vendor','LEFT')
		->join('tb_sbu','tb_sbu.id=ms_vendor.id_sbu','LEFT')
		->join('tb_legal','tb_legal.id=mva.id_legal','LEFT')
		->join('ms_akta','ms_akta.id_vendor=ms_vendor.id','LEFT')
		->join('ms_pengurus','ms_pengurus.id_vendor=ms_vendor.id','LEFT')
		->join('ms_pemilik','ms_pemilik.id_vendor=ms_vendor.id','LEFT')
		->join('ms_situ','ms_situ.id_vendor=ms_vendor.id','LEFT')
		->join('tr_dpt','tr_dpt.id_vendor=ms_vendor.id','LEFT')
		->group_by('ms_vendor.id');

		$query = $this->db->get('ms_vendor');
		return $query->result_array();
	}

	function get_vendor_name($id){
		return $this->db->select('*,ms_vendor_admistrasi.vendor_email email')
						->where('ms_vendor.id',$id)
						->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor=ms_vendor.id')
						->get('ms_vendor')->row_array();
	}

	function get_data($id=0){

		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "	SELECT 	b.*,
							a.*,
							e.*,
							a.id id,
						 	a.name as name, 
						 	a.npwp_code as npwp_code, 
						 	c.name as sbu_name, 
						 	d.name as legal_name,
						 	b.data_status data_status

				 	FROM ms_vendor a
				 	LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor
				 	LEFT JOIN ms_vendor_pic e ON a.id = e.id_vendor
				 	LEFT JOIN tb_sbu c ON c.id = a.id_sbu
				 	LEFT JOIN tb_legal d ON d.id = b.id_legal
					WHERE a.id = ?";
		
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_sbu($id = '', $type = '', $sub_type = '', $id_approver = '', $kode_transaksi = ''){

		if($type != "all") $sql = "SELECT a.* FROM ms_ijin_usaha a WHERE a.id_vendor = ? AND a.type = 'sbu' AND del = 0";
		else $sql = "SELECT a.* FROM ms_ijin_usaha a WHERE a.id_vendor = ? AND del = 0";
		
		return $this->db->query($sql, array($id ));
	}

	public function export_dpt($filter=array())
	{
		$admin = $this->session->userdata('admin');
		$query = $this->db->select('ms_vendor.id as id, `ms_vendor`.`id` `msv_id`,ms_vendor.id as id_vendor_list , ms_vendor.name name, ms_vendor_admistrasi.npwp_code npwp_code,ms_vendor_admistrasi.nppkp_code nppkp_code, vendor_city, vendor_province, tb_sbu.name sbu_name,(SELECT AVG(point) FROM tr_assessment WHERE id_vendor = msv_id  ) npp ')
		->where('ms_vendor.vendor_status',2)
		->where('ms_vendor.is_active',1)
		->where('NOT EXISTS(
					SELECT 1 FROM tr_blacklist_nik c JOIN ms_pengurus msp ON msp.no = c.nik WHERE msp.id_vendor = ms_vendor.id AND (c.del = 0 OR c.del IS NULL)
				)')
		->where('NOT EXISTS(
					SELECT 1 FROM tr_blacklist d WHERE d.id_vendor = ms_vendor.id AND (d.del = 0 OR d.del IS NULL)
				)')
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor=ms_vendor.id','LEFT')
		->join('tr_assessment','tr_assessment.id_vendor=ms_vendor.id','LEFT')
		->join('tb_sbu','tb_sbu.id=ms_vendor.id_sbu','LEFT');
		if(!in_array($admin['id_role'], array(1,2,8))){
			$this->db->where('id_sbu',$admin['id_sbu']);
		}
		if($this->input->post('filter')){

			$this->db->order_by($this->input->post('filter'));

		}
		$query = $this->db->get('ms_vendor');
		return $query->result_array();
	}

	function get_total_daftar_tunggu(){
		return $this->db->select('*')->where('vendor_status',1)->where('ms_vendor.del',0)->join('ms_vendor_admistrasi','ms_vendor.id=ms_vendor_admistrasi.id_vendor')->get('ms_vendor')->num_rows();
	}

	function get_total_dpt(){
		return $this->db->select('*')->where('vendor_status',2)->where('ms_vendor.del',0)->join('ms_vendor_admistrasi','ms_vendor.id=ms_vendor_admistrasi.id_vendor')->get('ms_vendor')->num_rows();
	}
}
