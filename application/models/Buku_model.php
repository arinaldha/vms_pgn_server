<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Buku_model extends MY_Model{
	public $table = 'ms_buku';
	function __construct(){
		parent::__construct();
	}
	function getElemenBiaya($form){
		$query = "	SELECT
						id,
						title,
              			publisher
					FROM ".$this->table."
					WHERE del = 0";
	    $query = $this->db->query($query)->result_array();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']] = $value['title'] .' - '.$value['publisher'];
	    }
	    return $data;
	}

	function getData($form){

		$query = "	SELECT
							title,
							publisher,
							contract_file,
							a.id

					FROM ".$this->table." a
					WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT
						title,
						publisher
					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
	
}
