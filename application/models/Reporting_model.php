<?php
class Reporting_model extends MY_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	function get_dpt($id = ""){
		$sql = "SELECT a.*, b.name bidang_name FROM tr_dpt a JOIN tb_bidang b ON a.id_bidang = b.id WHERE id_vendor = ? AND start_date IS NOT NULL GROUP BY id_bidang LIMIT 4";// 

		return $this->db->query($sql, array($id));
	}	
	function get_first($id){
		$sql = "SELECT dpt_first_date start_date FROM ms_vendor WHERE id = ? ORDER BY id ";
		$sql = $this->db->query($sql, array($id));
		$sql =  $sql->row_array();
		
		return $sql;
	}
	function get_detail_data($id = '', $db = ''){
		$sql = "SELECT a.*, b.name AS bidang_name, c.name AS sub_bidang_name FROM ms_iu_bsb a 
				LEFT JOIN tb_bidang b ON a.id_bidang = b.id
				LEFT JOIN tb_sub_bidang c ON a.id_sub_bidang = c.id

				WHERE 
					a.id_ijin_usaha = ?					
				GROUP BY a.id_sub_bidang
				ORDER BY id_bidang ASC ";
		
		return $this->db->query($sql, array($id/*, $db*/));
	}

	function get_dpt_list()
	{
		$_POST['filter'] = $_GET['filter'];
		$admin = $this->session->userdata('admin');
		$filter = array();
		foreach($_POST['filter'] as $key =>$field){
			$_filter[] = $key;
			$select .= $this->field_list($field);
		}

	/*	$this->db->select('')
		->where('ms_vendor.vendor_status',2)		
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor=ms_vendor.id','LEFT')
		->join('tb_sbu','tb_sbu.id=ms_vendor.id_sbu','LEFT')
		->join('tb_legal','tb_legal.id=ms_vendor_admistrasi.id_legal','LEFT');
		if(!in_array($admin['id_role'], array(1,2,8))){
			$this->db->where('id_sbu',$admin['id_sbu']);
		}
		*/
		$query = "SELECT 	b.*,
							a.*, 
							d.name badan_usaha,
							 c.name satuan_unit
							 ".$select." 
					FROM ms_vendor a
					LEFT JOIN ms_vendor_admistrasi b ON b.id_vendor=a.id
					LEFT JOIN tb_sbu c ON c.id=a.id_sbu
					LEFT JOIN tb_legal d ON d.id=b.id_legal
					-- LEFT JOIN tr_dpt e ON e.id_vendor=a.id
					LEFT JOIN ms_pengalaman f ON f.id_vendor=a.id
					LEFT JOIN ms_iu_bsb i ON i.id_vendor=a.id
					LEFT JOIN ms_ijin_usaha ON ms_ijin_usaha.id_vendor=a.id
					WHERE a.vendor_status = 2 ";

		/*if(!in_array($admin['id_role'], array(1,2,8))){
			$query .= " AND a.id_sbu = ".$admin['id_sbu'];
		}*/
		
		if ($admin['id_sbu'] != 40) {
			$query .= " AND a.id_sbu = ".$admin['id_sbu'];
		}
		foreach ($this->input->get('filter') as $key => $value) {

			if($value!=1){
				$data_explode = explode(',', $value[0]);
				foreach ($data_explode as $keys => $values) {
					$_GET['filters'][$key][$keys]=$values;
				}
				
			}

		}

		if($this->input->get('filters')){
			$query .= $this->filter($form, $this->input->get('filters'), false);
		}
		$query .=" GROUP BY a.id";
		// foreach($_POST['filter'] as $field){
		// 	$dataField = $this->join_list($field);
		// 	foreach ($dataField as $key => $value) {
		// 		$this->db->join($value['field'],$value['on'],'LEFT');
		// 	}
		// }
		// $this->db->where('ms_vendor.id', 2641);
		// $this->db->group_by('ms_vendor.id');
		// $a = $this->filter->generate_query($this->db->group_by('ms_vendor.id'),$filter);
		// $query = $this->db->get('ms_vendor');
		$query = $this->db->query($query);
		// echo $this->db->last_query();die;
		$return = $query->result_array();
		foreach ($return as $key => $values) {
			if($_POST['filter']['akta']==1){
				$dataAkta = $this->getAkta($values['id_vendor']);

				if (count($dataAkta) > 0) {
					foreach ($dataAkta as $keyAkta => $valueAkta) {

						$return[$key]['no_akta'][] = $valueAkta['no'];
						$return[$key]['notaris_akta'][] = $valueAkta['notaris'];
						$return[$key]['issue_date_akta'][] = default_date($valueAkta['issue_date']);
						
					}
				}
			}
			if($_POST['filter']['pengurus']==1){
				$datapengurus = $this->getpengurus($values['id_vendor']);

				if (count($datapengurus) > 0) {
					foreach ($datapengurus as $keypengurus => $valuepengurus) {
						if(strpos( 'komisaris', strtolower($valuepengurus['position']) ) !== false){
							$return[$key]['komisaris'][] = $valuepengurus['name'];
						}
						if(strpos( 'direktur', strtolower($valuepengurus['position']) ) !== false){
							$return[$key]['direktur'][] = $valuepengurus['name'];
						}
						
					}
				}
			}
			if($_POST['filter']['asosiasi']==1){
				$dataIzin = $this->getIzinAsosiasi($values['id_vendor']);

				if (count($dataIzin) > 0) {
					foreach ($dataIzin as $keyIzin => $valueIzin) {
						// $return[$key]['no'][] = $valueIzin['no'];
						$return[$key]['authorize_by_asosiasi'][] = $valueIzin['authorize_by'];
						// $return[$key]['bsb'][] = $valueIzin['bsb'];
						$return[$key]['expire_date_asosiasi'][] = default_date($valueIzin['expire_date']);
						
					}
				}
			}
			
			
			if($_POST['filter']['tdp']==1){
				$dataTdp = $this->getTdp($values['id_vendor']);
				$return[$key]['no_tdp'] = $dataTdp['no'];
				$return[$key]['expire_date_tdp'] = default_date($dataTdp['expire_date']);
			}
			if($_POST['filter']['situ']==1){
				$dataSitu = $this->getSitu($values['id_vendor']);
				if (count($dataSitu) > 0) {
					foreach ($dataSitu as $keySitu => $valueSitu) {
						$return[$key]['address'][] = $valueSitu['address'];
						$return[$key]['expire_date_situ'][] = default_date($valueSitu['expire_date']);
					}
				}
			}
			
			if($_POST['filter']['siup']==1){
				$dataIzinSiup = $this->getIzinSiup($values['id_vendor']);
				if (count($dataIzinSiup) > 0) {
					foreach ($dataIzinSiup as $keyIzinSiup => $valueIzinSiup) {
						$return[$key]['no_siup'][] = $valueIzinSiup['no'];
						$return[$key]['bsb_siup'][] = $valueIzinSiup['bsb'];
						$return[$key]['expire_date_siup'][] = default_date($valueIzinSiup['expire_date']);
					}
				}
			}
			if($_POST['filter']['siujk']==1){
				$dataIzinSiujk = $this->getIzinSiujk($values['id_vendor']);
				if (count($dataIzinSiujk) > 0) {
					foreach ($dataIzinSiujk as $keyIzinSiujk => $valueIzinSiujk) {
						$return[$key]['authorize_by_siujk'][] = $valueIzinSiujk['authorize_by'];
						$return[$key]['bsb_siujk'][] = $valueIzinSiujk['bsb'];
						$return[$key]['expire_date_siujk'][] = default_date($valueIzinSiujk['expire_date']);
					}
				}
			}
			if($_POST['filter']['sbu']==1){
				$dataIzinSbu = $this->getIzinSbu($values['id_vendor']);
				if (count($dataIzinSbu) > 0) {
					foreach ($dataIzinSbu as $keyIzinSbu => $valueIzinSbu) {
						$return[$key]['qualification_sbu'][] = $valueIzinSbu['qualification'];
						$return[$key]['bsb_sbu'][] = $valueIzinSbu['bsb'];
						$return[$key]['expire_date_sbu'][] = default_date($valueIzinSbu['expire_date']);
					}
				}
			}
			if($_POST['filter']['ijin_lain']==1){
				$dataIzinLainnya = $this->getIzinLainnya($values['id_vendor']);
				if (count($dataIzinLainnya) > 0) {
					foreach ($dataIzinLainnya as $keyIzinLainnya => $valueIzinLainnya) {
						$return[$key]['authorize_by_lainnya'][] = $valueIzinLainnya['authorize_by'];
						$return[$key]['bsb_lainnya'][] = $valueIzinLainnya['bsb'];
						$return[$key]['expire_date_lainnya'][] = default_date($valueIzinLainnya['expire_date']);
					}
				}
			}
			if($_POST['filter']['agen']==1){
				$dataIzinLainnya = $this->getAgen($values['id_vendor']);
				if (count($dataIzinLainnya) > 0) {
					foreach ($dataIzinLainnya as $keyIzinLainnya => $valueIzinLainnya) {
						$return[$key]['type_agen'][] = $valueIzinLainnya['type'];
						$return[$key]['produk'][] = $valueIzinLainnya['produk'];
						$return[$key]['expire_date_agen'][] = default_date($valueIzinLainnya['expire_date']);
					}
				}
			}
			if($_POST['filter']['bsb']==1){
				$dataBsb = $this->getBsb($values['id_vendor']);
				if (count($dataBsb) > 0) {
					foreach ($dataBsb as $keyBsb => $valueBsb) {
						$return[$key]['bsb'][] = $valueBsb['bidang_name'].' - '.$valueBsb['sub_bidang_name'];
					}
				}
			}
			if($_POST['filter']['barang']==1){
				$databarang = $this->getBarang($values['id_vendor']);
				if (count($databarang) > 0) {
					foreach ($databarang as $keybarang => $valuebarang) {
						$return[$key]['barang'][] = $valuebarang['barang'];
					}
				}
			}
		}
		// echo print_r($return);
		return $return;
	}

	public function getBarang($id)
	{
		$query = "SELECT 
					 a.id_dpt_type,
					 b.name barang,
				  FROM 
					 ms_ijin_usaha a 
				  LEFT JOIN 
				  	tb_dpt_type b ON b.id=a.id_dpt_type
				  WHERE 
				  	a.id_vendor = ".$id."AND a.id_dpt_type = 1";

		$query = $this->db->query($query, array($id))->result_array();

		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		return $return;
	}

	public function getBsb($id)
	{
		$query = "SELECT 
					 a.*,
					 c.name bidang_name, 
					 d.name sub_bidang_name 
				  FROM 
					 ms_iu_bsb a 
				  LEFT JOIN 
				  	ms_ijin_usaha b ON b.id=a.id_ijin_usaha 
				  LEFT JOIN 
				  	tb_bidang c ON c.id=a.id_bidang 
				  LEFT JOIN 
				  	tb_sub_bidang d ON d.id=a.id_sub_bidang 
				  WHERE 
				  	a.id_ijin_usaha = ".$id." AND c.del =0 GROUP BY a.id_sub_bidang";

		$query = $this->db->query($query, array($id))->result_array();

		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		return $return;
	}

	public function getKlasifikasi($id)
	{
		$filter = $this->input->get('filter');

		
				

		$query = "SELECT 
					 a.*,
					 c.name bidang_name, 
					 d.name sub_bidang_name 
				  FROM 
					 ms_iu_bsb a 
				  LEFT JOIN 
				  	ms_ijin_usaha b ON b.id=a.id_ijin_usaha 
				  LEFT JOIN 
				  	tb_bidang c ON c.id=a.id_bidang 
				  LEFT JOIN 
				  	tb_sub_bidang d ON d.id=a.id_sub_bidang 
				  WHERE 
				  	b.id_vendor = ".$id." AND c.del =0 ";
		if($filter['id_dpt_type'][0]!=''){
			$query .= "AND b.id_dpt_type IN(".$filter['id_dpt_type'][0].")";
		}
		$query .=" GROUP BY a.id_sub_bidang";

		$query = $this->db->query($query, array($id));
		$query =  $query->result_array();
		$result = '';
		foreach ($query as $key => $value) {
			$result .=$value['bidang_name'] .' - '.$value['sub_bidang_name'].'<br>';
		}
		return $result; 
	}

	public function getIzinAsosiasi($id)
	{
		$query = "	SELECT
							a.no ,
							a.authorize_by,
							a.type,
							a.expire_date,
							a.id
					FROM ms_ijin_usaha a
					WHERE a.del = 0 AND a.type='asosiasi' AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();

		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}

		
		
		return $return;
	}


	public function getIzinSiup($id)
	{
		$query = "	SELECT
							a.no,
							a.authorize_by,
							a.type,
							a.expire_date,
							a.id
					FROM ms_ijin_usaha a
					WHERE a.del = 0 AND a.type='siup' AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$queryBsb = "SELECT a.*,c.name bidang_name, d.name sub_bidang_name FROM ms_iu_bsb a LEFT JOIN ms_ijin_usaha as b ON b.id=a.id_ijin_usaha LEFT JOIN tb_bidang as c ON c.id=a.id_bidang LEFT JOIN tb_sub_bidang as d ON d.id=a.id_sub_bidang WHERE a.id_ijin_usaha = ".$value['id']." AND c.del =0 GROUP BY a.id_sub_bidang";
			$queryBsb = $this->db->query($queryBsb);
			foreach ($queryBsb->result_array() as $keyBsb => $valueBsb) {
				$value['bsb'] .= '<p>'.$valueBsb['bidang_name'].'-'.$valueBsb['sub_bidang_name'].'</p>';
			}
			$return[] = $value;
		}
		return $return;
	}
	public function getIzinSiujk($id)
	{
		$query = "	SELECT
							a.no,
							a.authorize_by,
							a.type,
							a.expire_date,
							a.id
					FROM ms_ijin_usaha a
					WHERE a.del = 0 AND a.type='siujk' AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$queryBsb = "SELECT a.*,c.name bidang_name, d.name sub_bidang_name FROM ms_iu_bsb a LEFT JOIN ms_ijin_usaha as b ON b.id=a.id_ijin_usaha LEFT JOIN tb_bidang as c ON c.id=a.id_bidang LEFT JOIN tb_sub_bidang as d ON d.id=a.id_sub_bidang WHERE a.id_ijin_usaha = ".$value['id']." AND c.del =0 GROUP BY a.id_sub_bidang";
			$queryBsb = $this->db->query($queryBsb);
			foreach ($queryBsb->result_array() as $keyBsb => $valueBsb) {
				$value['bsb'] .= '<p>'.$valueBsb['bidang_name'].'-'.$valueBsb['sub_bidang_name'].'</p>';
			}
			$return[] = $value;
		}
		return $return;
	}
	public function getIzinSbu($id)
	{
		$query = "	SELECT
							a.qualification,
							a.expire_date,
							a.id
					FROM ms_ijin_usaha a
					WHERE a.del = 0 AND a.type='sbu' AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$queryBsb = "SELECT a.*,c.name bidang_name, d.name sub_bidang_name FROM ms_iu_bsb a LEFT JOIN ms_ijin_usaha as b ON b.id=a.id_ijin_usaha LEFT JOIN tb_bidang as c ON c.id=a.id_bidang LEFT JOIN tb_sub_bidang as d ON d.id=a.id_sub_bidang WHERE a.id_ijin_usaha = ".$value['id']." AND c.del =0 GROUP BY a.id_sub_bidang";
			$queryBsb = $this->db->query($queryBsb);
			foreach ($queryBsb->result_array() as $keyBsb => $valueBsb) {
				$value['bsb'] .= '<p>'.$valueBsb['bidang_name'].'-'.$valueBsb['sub_bidang_name'].'</p>';
			}
			$return[] = $value;
		}
		return $return;
	}
	public function getIzinLainnya($id)
	{
		$query = "	SELECT
							a.authorize_by,
							a.expire_date,
							a.id
					FROM ms_ijin_usaha a
					WHERE a.del = 0 AND a.type='sbu' AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$queryBsb = "SELECT c.name bidang_name, d.name sub_bidang_name, a.* FROM ms_iu_bsb a LEFT JOIN ms_ijin_usaha as b ON b.id=a.id_ijin_usaha LEFT JOIN tb_bidang as c ON c.id=a.id_bidang LEFT JOIN tb_sub_bidang as d ON d.id=a.id_sub_bidang WHERE a.id_ijin_usaha = ".$value['id']." AND c.del =0 GROUP BY a.id_sub_bidang";
			$queryBsb = $this->db->query($queryBsb);
			foreach ($queryBsb->result_array() as $keyBsb => $valueBsb) {
				$value['bsb'] .= '<p>'.$valueBsb['bidang_name'].'-'.$valueBsb['sub_bidang_name'].'</p>';
			}
			$return[] = $value;
		}
		return $return;
	}
	public function getAgen($id)
	{
		$query = "	SELECT
							a.type,
							a.expire_date,
							a.id
					FROM ms_agen a
					WHERE a.del = 0 AND a.id_vendor =  ".$id;
		
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$queryProduk = "SELECT a.*,produk, merk FROM ms_agen_produk a LEFT JOIN ms_agen as b ON b.id=a.id_agen WHERE a.id_agen = ".$value['id']." AND a.del =0 GROUP BY a.id_agen";
			$queryProduk = $this->db->query($queryProduk);
			foreach ($queryProduk->result_array() as $keyProduk => $valueProduk) {
				$value['produk'] .= '<p>'.$valueProduk['produk'].'-'.$valueProduk['merk'].'</p>';
			}
			$return[] = $value;
		}
		return $return;
	}

	public function getAkta($id)
	{
		$query = "SELECT a.no,a.notaris,a.issue_date FROM ms_akta a WHERE a.del = 0 AND a.id_vendor = ".$id;
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		return $return;
	}
	public function getPengurus($id)
	{
		$query = "SELECT * FROM ms_pengurus a WHERE a.del = 0 AND a.id_vendor = ".$id;
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		return $return;
	}

	public function getSitu($id)
	{
		$query = "SELECT a.address,a.expire_date FROM ms_situ a WHERE a.del = 0  AND a.id_vendor = ".$id;
		$query = $this->db->query($query, array($id))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		return $return;
	}
	public function getTdp($id)
	{
		$query = "SELECT a.no,a.expire_date FROM ms_tdp a WHERE a.del = 0 AND a.id_vendor = ".$id;
		$query = $this->db->query($query, array($id))->row_array();
		return $query;
	}

	function join_list($index = ''){
		$array = array(
			'akta' => array(array('field'=>'ms_akta k','on'=>'a.id = k.id_vendor ')),
			
			'pemilik' => array(array('field'=>'ms_pemilik l','on'=>'  a.id = l.id_vendor ')),
			
			'pengurus' => array(array('field'=>'ms_pengurus d','on'=>'  a.id = d.id_vendor ')),
			
			'tdp' => array(array('field'=>'ms_tdp c','on'=>'  a.id = c.id_vendor ')),
			
			'bsb' => array(array('field'=>'ms_iu_bsb bh','on'=>'bh.id_vendor = a.id'),
					 array('field'=>' tb_bidang bf','on'=>'bh.id_bidang =  bf.id '),
					 array('field'=>' tb_sub_bidang bg','on'=>'bh.id_bidang =  bg.id '),
					 array('field'=>' tb_dpt_type bi','on'=>'bf.id_dpt_type = bi.id ')),
		
			'siup' => array(array('field'=>'ms_siup e','on'=>'a.id = e.id_vendor'),
					  array('field'=>' ms_iu_bsb m','on'=>'m.id_location = e.id'),
					  array('field'=>' tb_bidang n','on'=>'m.id_bidang = n.id'),
					  array('field'=>' tb_sub_bidang o','on'=>'m.id_sub_bidang = o.id ')),
		
			'ijin_lain' => array(array('field'=>'ms_ijin_usaha h','on'=>'a.id = h.id_vendor') ,
						array('field'=>'	ms_iu_bsb p','on'=>'p.id_location = h.id '),
						array('field'=>'	tb_bidang q','on'=>'p.id_bidang = q.id '),
						array('field'=>'	tb_sub_bidang r','on'=>'p.id_sub_bidang = r.id ')),
			
			'asosiasi' => array(array('field'=>'ms_asosiasi t','on'=>'a.id = t.id_vendor '),
						  array('field'=>' ms_iu_bsb u','on'=>'u.id_location = t.id'),
						  array('field'=>' tb_bidang v','on'=>'u.id_bidang = v.id'),
						  array('field'=>' tb_sub_bidang w','on'=>'u.id_sub_bidang = w.id ')),
		
			'siujk' => array(array('field'=>'ms_siujk f','on'=>'a.id = f.id_vendor'),
					array('field'=>'	ms_iu_bsb x','on'=>'x.id_location = f.id'),
					array('field'=>'	tb_bidang y','on'=>'x.id_bidang = y.id'),
					array('field'=>'	tb_sub_bidang z','on'=>'x.id_sub_bidang = z.id ')),
		
			'sbu' => array(array('field'=>'ms_sbu g','on'=>'a.id = g.id_vendor '),
					 array('field'=>' ms_iu_bsb aa','on'=>'aa.id_location = g.id'),
					 array('field'=>' tb_bidang ab','on'=>'aa.id_bidang = ab.id'),
					 array('field'=>' tb_sub_bidang ac','on'=>'aa.id_sub_bidang = ac.id ')),
			
			'agen' => array(array('field'=>'ms_agen ad','on'=>'a.id = ad.id_vendor'),
					  array('field'=>' ms_agen_produk ae','on'=>'ae.id_agen = ad.id ')),
		
			'situ' => array(array('field'=>'ms_situ s','on'=>'a.id = s.id_vendor ')),
		
			'pengalaman' => array(array('field'=>'ms_pengalaman bb','on'=>'bbb.id_vendor = a.id '),
							array('field'=>' ms_iu_bsb ba','on'=>'ba.id = bb.id_bsb'),
						    array('field'=>' tb_bidang bc','on'=>'ba.id_bidang = bc.id'),
						    array('field'=>' tb_sub_bidang bd','on'=>'ba.id_sub_bidang = bd.id'),
						    array('field'=>' tb_sbu be','on'=>'bb.id_sbu = be.id  ')),
		
			
		);

		return $array[$index];
	}
	
	function field_list($index = ''){
		$array = array(
			'akta' => ',k.no_akta AS no_akta',
		
			'pengurus' => ',d.nama AS pengurus_nama',
		  	
			'pemilik' => ',l.nama AS pemilik_nama',
			
		  	'tdp' => ',c.no AS tdp_no',
			
			'bsb' => ',bf.name AS bsb_bidang
					  ,bg.name AS bsb_sub_bidang
					  ,bi.name AS bsb_group ',
		
			'siup' => ',e.no AS siup_no
					   ,e.kualifikasi AS siup_kualifikasi
					   ,n.name AS siup_bidang
					   ,o.name AS siup_sub_bidang',
			
			'situ' => ',s.no   AS situ_no 
		   			   ,s.alamat AS situ_alamat',
				   
		   	'ijin lain' => ',h.no AS ijin_lain_no 
						    ,h.kualifikasi AS ijin_lain_kualifikasi 
						    ,h.lembaga_penerbit AS ijin_lain_lembaga_penerbit 
						    ,q.name AS ijin_lain_bidang 
						    ,r.name AS ijin_lain_sub_bidang',
		
		   'asosiasi' => ',t.no AS asosiasi_no
						  ,t.lembaga_penerbit AS asosiasi_lembaga_penerbit
						  ,v.name AS asosiasi_bidang
						  ,w.name AS asosiasi_sub_bidang',
		    
		   
		   'siujk' => ',f.no AS siujk_no 
					   ,f.kualifikasi AS siujk_kualifikasi
					   ,y.name AS siujk_bidang
					   ,z.name AS siujk_sub_bidang',
		   
		   'sbu' => ',g.no AS sbu_no 
				     ,g.anggota_asosiasi AS sbu_anggota_asosiasi 
				     ,aa.no_kode AS sbu_no_kode 
				     ,aa.grade AS sbu_grade 
				     ,aa.kualifikasi AS sbu_kualifikasi 
				     ,ab.name AS sbu_bidang 
				     ,ac.name AS sbu_sub_bidang', 
		   
		   'agen' => ',ad.no AS agen_no
					  ,ad.jenis AS agen_jenis
					  ,ae.produk AS agen_produk
					  ,ae.merk AS agen_merk',
		
			'pengalaman' => ',bb.nama AS pengalaman_nama
							 ,bc.name AS pengalaman_bidang
							 ,bd.name AS pengalaman_sub_bidang
							 ,bb.lokasi AS pengalaman_lokasi
							 ,be.name AS pengalaman_sbu',
			
			'dpt_start' => ',(SELECT dpt_start FROM ms_dpt WHERE id_vendor = a.id GROUP BY id_vendor ORDER BY id ASC) AS dpt_start'
							 
		);
		
		return $array[$index];
	}

}
