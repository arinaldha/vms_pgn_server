<?php defined('BASEPATH') OR exit('No direct script access allowed');
class No_card_model extends MY_Model{
	public $table = 'ms_no_card';
	function __construct(){
		parent::__construct();
	}


	function getData($form){
		$admin = $this->session->userdata('admin');
		$query = "	SELECT
							b.code code_komag,
							c.name gudang,
							d.name tipe,
							a.code,
							a.remark,
							a.id

					FROM ".$this->table." a
					LEFT JOIN ms_komag b ON a.id_komag = b.id
					LEFT JOIN tb_gudang c ON a.id_gudang = c.id
					LEFT JOIN tb_type_item d ON a.id_material_type = d.id
					WHERE a.del = 0";
		if($admin['id_role']==3){

			$query .= " AND a.id_gudang = ".$admin['id_gudang']." ";
		}
		if($admin['id_role']==1){
			$query .= " AND a.id_gudang IN (
							SELECT id_gudang from tr_user_gudang WHERE id_user = ".$admin['id_user']."
						)";
		}
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getMaterial($id){
		$query = "	SELECT 	b.name nama_komag,
							b.code code_komag,
							c.code kode_mro,
							c.proyek_investasi,
							b.satuan,
							c.date,
							d.name lokasi_gudang,
							e.name tipe_material,
							a.code no_card_code,
							a.id_material_type 
					FROM ms_no_card a
					JOIN ms_komag b ON b.id = a.id_komag
					INNER JOIN (
						SELECT code, proyek_investasi, MAX(date) date, id FROM tr_transaksi WHERE id_no_card = ? 
					) c
					JOIN tb_gudang d ON d.id = a.id_gudang
					JOIN tb_type_item e ON a.id_material_type = e.id
					WHERE a.id = ?
				";
		$query = $this->db->query($query, array($id, $id));
		// echo $this->db->last_query();
		return $query->row_array();
	}
	function getTransaksiMaterial($id){
		$query = "	SELECT 	b.date,
							b.no, 
							b.item_from,
							b.item_to,
							a.serial_number,
							a.year,
							a.location_before,
							a.location_after,
							a.condition,
							a.tipe_transaksi,
							a.qty,
							a.remark,
							a.price,
							a.tujuan,
							a.price_contract
					FROM tr_transaksi a 
					JOIN ms_transaksi b ON a.id_transaksi = b.id
					WHERE a.id_no_card = ?
					";
		$query = $this->db->query($query, array($id));
		return $query->result_array();
	}
	function getHistory($form, $id){

		$query = "	SELECT
							a.no_card ,
							c.name,
							a.qty,
							a.entry_stamp,
							a.remark
							

					FROM tr_transaksi a
					LEFT JOIN ms_transaksi b ON a.id_transaksi = b.id
					LEFT JOIN tb_tipe_transaksi c ON b.detail = c.id 
					WHERE a.del = 0 
					AND id_no_card = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function insertDetail($id){
		$admin = $this->session->userdata('admin');
		$tipe = $this->checkTipe($_POST['id_tipe']);
		$gudang = $this->checkGudang($_POST['id_gudang']);
		$id_no_card = $this->generateNomor($id);

		//Masukkan data nomor kartu
		$queryNoCard = $this->db->insert('ms_no_card',
			array(
				'id_komag'=>$_POST['no_komag'],
				'id_gudang'=>$_POST['id_gudang'],
				'id_item'=>$id_item,
				'id_material_type'=>$_POST['id_tipe'],
				'code'=>$no_id_card,
				'remark'=>$_POST['remark'],
				'entry_by'=>$admin['id_user'],
				'entry_stamp'=>timestamp()
				)
		);
		return $this->db->insert_id();

		
	}
	function checkTipe($id){
		$query = $this->db->select('type,code')->where('id', $id)->get('tb_type_item');
		return $query->row_array();
	}
	function checkGudang($id){
		$query = $this->db->select('code')->where('id', $id)->get('tb_gudang');
		return $query->row_array();
	}
	function check_nomor($id_komag, $id_tipe, $id_gudang){
		$digit = 5;
		$__id = 1;
		$return = '';
		$query = $this->db 	->where(array(
										'id_material_type'=>$id_tipe,
										'id_gudang'=>$id_gudang,
									))
							->order_by('id','desc')
							->limit(1)
							->get('ms_no_card');
		if($query->num_rows() > 0){
			$data 	= $query->row_array();
			// if(strlen)
			$num	= intval(ltrim($data['id_item'],'0'));
			$__id 	= $num;

			$str_repeat = intval(str_repeat(9, $digit));

			if($__id == $str_repeat){
				$digit++;
			}
			$return = sprintf("%0".$digit."s", ($__id+1));
		}else{
			$return	= sprintf("%0".$digit."s", $__id);
		}
		return $return;
	}
	function searchNoCard(){
		$admin = $this->session->userdata('admin');
		$query = "	SELECT
		                a.id,
		                a.code,
		                a.remark,
		                a.id_komag,
		                b.code code_komag,
		                b.name name_komag,
		                (SELECT aa.location FROM tr_transaksi aa WHERE aa.id_no_card = a.id ORDER BY id ASC LIMIT 0, 1) location
					FROM ".$this->table." a
					JOIN ms_komag b ON a.id_komag = b.id
					WHERE a.del = 0 AND (a.code LIKE ? OR a.remark LIKE ?) AND id_material_type = ?";
		if($admin['id_role']==3){

			$query .= "AND a.id_gudang = ".$admin['id_gudang']."";
		}

		if($_POST['id_komag']!=''){
			$query .= " AND a.id_komag = ".$_POST['id_komag'];
		}
	    $query = $this->db->query($query, array('%'.$_POST['search'].'%', '%'.$_POST['search'].'%', $_POST['id_tipe']))->result_array();
	    // echo $this->db->last_query();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']]['label'] 	= $value['code'];
	        $data[$value['id']]['data']		= $value;
	        $data[$value['id']]['data']['kode_komag']		= $value['code_komag'] .' - '.$value['name_komag'];;
	    }
	    return $data;
	}
	function checkQty($id_no_card){
		$pos = $this->db->select("SUM(qty) pos")->where(array('id_no_card'=>$id_no_card,  'type'=>1))->get('tr_item_movement')->row_array();
		$neg = $this->db->select("SUM(qty) neg")->where(array('id_no_card'=>$id_no_card,  'type'=>2))->get('tr_item_movement')->row_array();
		return $pos['pos'] - $neg['neg'];
	}
	function getTotalMaterial($id_material_type){
		$admin = $this->session->userdata('admin');
		$query = "	SELECT 
						c.value klasifikasi,
						a.id,
						b.name,
						b.code,
						b.description,
						b.satuan,
						a.initial_balance,
						a.price,
						e.name nama_gudang,
						b.id_klasifikasi,
						a.code mro_abt,
						a.code no_card,
						d.location_before
					FROM ms_no_card a
					LEFT JOIN ms_komag b ON a.id_komag = b.id 
					LEFT JOIN tb_klasifikasi c ON b.id_klasifikasi = c.id 
					LEFT JOIN tr_transaksi d ON a.id = d.id_no_card
					LEFT JOIN tb_gudang e ON e.id = a.id_gudang
					WHERE a.del = 0 AND id_material_type = ? AND ";
		if($admin['id_role']==3){

			$query .= "AND a.id_gudang = ".$admin['id_gudang']." ";
		}
		$query .= " GROUP BY a.id_komag";
		$query = $this->db->query($query,array($id_material_type))->result_array();
		$return = array();
		foreach ($query as $key => $value) {
			
			$return['data'][$value['id_klasifikasi']][$value['id']] = $value;
			$return['data'][$value['id_klasifikasi']][$value['id']]['price'] = $this->getLastPrice($value['id']);
			$return['data'][$value['id_klasifikasi']][$value['id']]['mutasi'] = $this->getMutasi($value['id']);
		} 
		$return['klasifikasi'] = $this->db->where('del',0)->get('tb_klasifikasi')->result_array();
		// echo print_r($return);
		return $return;
	}
	function getMutasi($id_no_card){
		$query = "SELECT * FROM tr_item_movement WHERE id_no_card = ? AND tipe_transaksi IS NOT ?";
		$query = $this->db->query($query, array($id_no_card, 12));
		$return = array('penerimaan'=>0, 'pengeluaran'=>0);
		foreach ($query->result_array() as $key => $value) {
			if($value['type']==1){
				$return['penerimaan'] += $value['qty'];
			}else{
				$return['pengeluaran'] += $value['qty'];
			}
			
		}
		// echo print_r($return);
		// echo $this->db->last_query();
		return $return;
	}
	function getLastPrice($id_no_card){
		$query = "SELECT * FROM `tr_transaksi` WHERE id_no_card = ? AND price > 0 ORDER BY id DESC LIMIT 0,1";
		$query = $this->db->query($query, array($id_no_card))->row_array();
		// echo $this->db->last_query();
		// echo $query['price'];
		return $query['price'];
	}
	
}
