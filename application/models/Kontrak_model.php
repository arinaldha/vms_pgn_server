<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Kontrak_model extends MY_Model{

	public $table = 'ms_procurement';

	function __construct(){

		parent::__construct();
	}

	function getData($form){
		$admin = $this->session->userdata('admin');
		#print_r($admin);die;
		$query = "	SELECT
							a.no,
							a.issue_date,
							a.name,
							a.id,
							a.step
					FROM ".$this->table." a
					WHERE a.del = 0 ";
		
		if ($admin['id_role'] == 2) {
			# code...
			$query .= "AND id_pengguna = ".$admin['id_division'];
		}
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getKontrak($id_mekanisme){
		foreach ($_POST['filter'] as $key => $value) {
			$_filter[] = $key;
		}
		$jenis_anggaran = $_POST['jenis_anggaran'];
		if($_POST['date_from']!=''){
			$date = " AND DATE(issue_date) >= '".date('Y-m-d', strtotime($_POST['date_from']))."'";
		}

		if($_POST['date_to']!=''){
			$date = " AND DATE(issue_date) <= '".date('Y-m-d', strtotime($_POST['date_to']))."'";
		}

		if($_POST['date_from']!='' && $_POST['date_to']!=''){
			$date = " AND DATE(issue_date) >= '".date('Y-m-d', strtotime($_POST['date_from'])) ."' AND DATE(issue_date) <= '".date('Y-m-d', strtotime($_POST['date_to']))."'";
		}
		$query = "	SELECT 
						a.name,
						b.name user,
						a.id_mekanisme,
						a.issue_date,
						a.status_procurement,
						a.remark,
						a.idr_value hps,
						a.jenis_anggaran,
						c.vendor_name,
						d.date date_evaluasi,
						d.score,
						a.id,
						a.idr_budget_operasi idr_value_operasi,
						a.idr_budget_investasi idr_value_investasi,
						e.idr_budget_".$jenis_anggaran." nilai_kontrak,
						a.step,
						a.pic_name pic

					FROM ms_procurement a 
					LEFT JOIN tb_budget_holder b ON a.id_pengguna = b.id
					LEFT JOIN ms_contract c ON a.id = c.id_procurement
					LEFT JOIN tr_assessment d ON a.id = d.id_procurement
					LEFT JOIN ms_baseline e ON a.id_baseline = e.id
					WHERE a.id_mekanisme = ? ".$date."
					AND a.del = 0 ";
		if ($admin['id_role'] == 2) {
			$query .= " AND id_pengguna = ".$admin['id_division'];
		}

		$query .=' GROUP BY a.id';
		$query = $this->db->query($query, array($id_mekanisme));
		$return = $query->result_array();
		foreach($return as $key=>$values){

			$kontrakKey  = array('nilai_kontrak','tanggal_kontrak','no_kontrak','tanggal_kerja','tanggal_penyelesaian');
			
			if(count(array_intersect($kontrakKey, $_filter))>0){
				$dataKontrak = $this->getTtdKontrak($values['id']);
				foreach ($dataKontrak as $keyKontrak => $valueKontrak) {
					$return[$key]['contract_price'][] = $valueKontrak['contract_price'];
					$return[$key]['contract_price_kurs'][] = $valueKontrak['contract_price_kurs'];
					$return[$key]['tanggal_kontrak'][] =  default_date($valueKontrak['contract_date']);
					$return[$key]['no_kontrak'][] = $valueKontrak['no_contract'];
					$return[$key]['tanggal_kerja'][] = $valueKontrak['start_work'];
					$return[$key]['work_end'][] = $valueKontrak['end_work'];

					$datetime1 = date_create($valueKontrak['start_work']);
				    $datetime2 = date_create($valueKontrak['end_work']);
				    
				    $interval = date_diff($datetime1, $datetime2);
				    
				    $return[$key]['durasi'][] = $interval->format('%a').' Hari';

				}
			}
			
			$payKey  = array('realisasi_pembayaran','bap');
			if(count(array_intersect($payKey, $_filter))>0){
				$dataBap = $this->getBap($values['id']);
				
				foreach ($dataBap as $key => $value) {
					$return[$key]['termin'][] = $value['termin'];
					$return[$key]['idr_value_bap'][] = $value['idr_value'];
					$return[$key]['nomor_bap'][] = $value['nomor_bap'];
					$return[$key]['date_bap'][] = default_date($value['date_bap']);

				}
			}
			if($_POST['filter']['bast']==1){
				$dataBast = $this->getBast($values['id']);
				
				foreach ($dataBast as $key => $value) {

					$return[$key]['nomor_bast'][] = $value['nomor_bast'];
					$return[$key]['date_bast'][] =  default_date($value['date_bast']);

				}
			}
			switch ($values['step']) {
				case "0" :
					$return[$key]['status'] ='Permintaan Pengadaan';
					break;
				case "1" : 
					$return[$key]['status'] ='Persiapan Pengadaan';
					break;
				case "2" : 
					$return[$key]['status'] ='Proses Pengadaan';
					break;
				case "3" : 
					$return[$key]['status'] ='Kontrak';
					break;
				case "4" : 
					$return[$key]['status'] ='Gagal';
					break;
				case "5" : 
					$return[$key]['status'] ='Batal';
					break;
			}
			$return[$key]['idr_value'] = $values['idr_value_operasi'] + $values['idr_value_investasi'];
		}

		return $return;
	}
	function getDashboard($form){
		$admin = $this->session->userdata('admin');
		$query = "	SELECT
							a.no,
							a.issue_date,
							a.name,
							a.id
							
					FROM ".$this->table." a
					WHERE a.del = 0 AND a.step = 0";
		
		if ($admin['id_role'] == 2) {
			$query .= " AND id_pengguna = ".$admin['id_division'];
		}
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getBap($id){
		$query = "	SELECT
							a.bobot termin,
							a.value idr_value,
							a.no nomor_bap,
							a.date date_bap
					FROM ms_bap a
					WHERE a.del = 0 AND id_procurement =  ".$id;
		
		$query = $this->db->query($query, array($id_procurement))->result_array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		
		return $return;
	}
	function getBast($id){
		$query = "	SELECT
							a.no nomor_bast,
							a.date date_bast
					FROM ms_bast a
					WHERE a.del = 0 AND id_procurement =  ".$id;
		
		$query = $this->db->query($query, array($id_procurement))->result_array();
		foreach ($query as $key => $value) {
			$return[] = $value;
		}
		
		return $return;
	}
	function getTtdKontrak($id_procurement){
		$return = array();
		$kontrak = "	SELECT 
						contract_price ,
						contract_price_kurs,
						contract_date,
						no_contract,
						start_work,
						end_work

						FROM ms_contract a
						WHERE a.id_procurement = ? 
						AND a.del = 0";
		$kontrak = $this->db->query($kontrak, array($id_procurement))->result_array();
		foreach ($kontrak as $key => $value) {
			$return[] = $value;
		}
		$amandemen = "	SELECT 
						idr_value contract_price,
						a.kurs_value contract_price_kurs,
						a.date contract_date,
						no no_contract,
						b.start_work,
						work_end end_work

						FROM ms_amandemen a
						JOIN ms_contract b ON b.id_procurement = a.id_procurement
						WHERE a.id_procurement = ? 
						AND a.del = 0";
		$amandemen = $this->db->query($amandemen, array($id_procurement))->result_array();
		foreach ($amandemen as $key => $value) {
			$return[] = $value;
		}
		
		return $return;
	}
	
	function insert($data){
		$data['del'] = 0;	
		foreach ($data as $key => $value) {
			if(is_array($value)){
				$data[$key] = implode(',',$value);
			}
		}
		$a =  $this->db->insert($this->table, $data);
		$id = $this->db->insert_id();


		$baseline = $this->db->where('id', $data['id_baseline'])->get('ms_baseline')->row_array();
		$dataBerkas = $this->km->selectData($id);
		$id_progress =$this->db->query("SELECT * FROM tb_progress_pengadaan where `metode` = ". $dataBerkas['id_mekanisme']." ORDER BY orders ASC LIMIT 0,1")->row_array();
		$this->db->insert('tr_progress_pengadaan', array('id_procurement' => $id, 'value' => 1, 'plan_date' => $baseline['plan_date'], 'real_date' => $data['issue_date'], 'id_progress' => $id_progress['id']));
		return $id;
	}
	function saveProsesTerima($id){
		$admin = $this->session->userdata('admin');
		$this->db  	->where('id', $id)
					->update('ms_transaksi', array(
						'entry_by'=>$admin['id_user'],
						'role'=>$admin['id_role'],
						'no'=>$_POST['no'],
						'date'=>$_POST['date'],
						'item_from'=>$_POST['item_from'],
						'item_to'=>$_POST['item_to'],
						// 'tujuan'=>$_POST['tujuan'],
						'remark'=>$_POST['remark'],
						'entry_stamp'=>timestamp()
					));
		// $_query = $this->db 	->where('id_transaksi', $id)
		// 			->where('del', 0)
		// 			->get('tr_transaksi');
		// foreach ($_query->result_array() as $key => $value) {
		// 	$_query = 
		// }
		
	}
	
	function movement($id){
		$query = $this->db->where('id_transaksi', $id)->get('tr_transaksi')->result_array();
		foreach ($query as $key => $value) {
			
			$queryMovement = $this->db->insert('tr_item_movement',
				array(
					'id_no_card'=>$value['id_no_card'],
					'no_card'=>$value['no_card'],
					'type'=>$value['tipe_transaksi'],
					'qty'=>$value['qty'],
					'entry_stamp'=>timestamp()
					)
			);
		}

	}
	function approve($id){
		$dataTransaksi = $this->selectData($id);
		$admin = $this->session->userdata('admin');
		if($admin['id_role']==1){
			if($dataTransaksi['detail']==1){
				$query = $this->db->where('id', $id)->update('ms_transaksi',array('status'=>1, 'approve_by'=>1, 'is_reject'=>0));
			}else{
				
				$query = $this->db->where('id', $id)->update('ms_transaksi',array('status'=>1, 'approve_by'=>2, 'is_reject'=>0));
			}
		}else{
			if($dataTransaksi['detail']==4||$dataTransaksi['detail']==3){
				$this->prosesNoKartu($id);
			}
			$this->movement($id);
			if($dataTransaksi['detail']==9||$dataTransaksi['detail']==8){
				$this->duplicate($id);
			}
			
			$query = $this->db->where('id', $id)->update('ms_transaksi',array('status'=>2, 'is_reject'=>0));
		}
		
		if($query){
			return array('status'=>'success');
		}else{
			return array('status'=>'success');
		}
	}
	
	function check_nomor($data){
		$digit = 5;
		$__id = 1;
		$return = '';
		
			$num	= intval(ltrim($data['id_item'],'0'));
			$__id 	= $num;

			$return = $__id+1;
		
		return $return;
	}
	function selectData($id){
		$query = "	SELECT
						a.jenis_permintaan,
						a.nama_baseline,
						a.nilai_jaminan,
						a.nota_dinas_file,
						a.no,
						a.issue_date,
						a.type,
						a.name,
						a.id_pengguna,
						a.idr_value,
						a.kurs_value,
						a.id_kurs,
						a.budget_source,
						a.budget_year,
						a.budget_center,
						a.budget_element,
						a.kode_akun,
						a.idr_budget_investasi,
						a.idr_budget_operasi,
						a.jenis_anggaran,
						a.contract_type,
						a.id_mekanisme,
						a.kualifikasi,
						a.metode_penyampaian,
						a.evaluation_method,
						a.remark,
						a.contract_file,
						a.status_procurement,
						a.step,
						a.pic_name,
						a.pic_email,
						a.id_pengguna,
						a.id_mekanisme,
						b.plan_date,
						a.step,
						a.budget_element_category 
						
					FROM ".$this->table." a 
					LEFT JOIN ms_baseline b ON a.id_baseline = b.id 
					WHERE a.id = ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		// echo $this->db->last_query();
		return $query->row_array();
	}
	function getTransaksiDetail($form, $id){

		$query = "	SELECT
							b.code,
							c.code komag,
							c.name nama_komag,
							a.tipe_transaksi,
							e.name nama_gudang,
							d.name nama_tipe,
							a.qty,
							a.remark,
							a.location,
							a.id,
							a.attachment_file

					FROM tr_transaksi a
					LEFT JOIN ms_no_card b ON a.id_no_card = b.id
					JOIN ms_komag c ON a.id_komag = c.id
					JOIN tb_type_item d ON a.id_tipe = d.id
					JOIN tb_gudang e ON a.id_gudang = e.id
					WHERE a.del = 0 AND id_transaksi = ".$id." ";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function savePosisi($id){
		$data = $this->db->where('id', $id)->get('tr_transaksi')->row_array();

		
		$query = $this->db->where('id', $id)
					->update('tr_transaksi', array(
											'location' => $_POST['location']
											)
					);
					$this->checkIsEmpty($data['id_transaksi']);
		if($data['location']==''||$data['location']==null){

				$value = $this->db->where('id', $id)->get('tr_transaksi')->row_array();
				// echo print_r($value);
					$queryMovement = $this->db->insert('tr_item_movement',
						array(
							'id_no_card'=>$value['id_no_card'],
							'no_card'=>$value['no_card'],
							'type'=>$value['tipe_transaksi'],
							'qty'=>$value['qty'],
							'entry_stamp'=>timestamp()
							)
					);
		}
	
	}
	function saveQty($id){

		$query = $this->db->where('id', $id)
					->update('tr_transaksi', array(
											'qty' => str_replace(',','',$_POST['qty']),
											)
					);

		
	
	}
	function checkIsEmpty($id){
		$query = $this->db->where(array('id_transaksi'=>$id, 'location'=>''))->get('tr_transaksi');
		if($query->num_rows()==0){
			$this->db->where('id',$id)->update('ms_transaksi', array('approve_by'=>1));
		}

	}
	function deleteDetail($id){

		return $this->db->where('id', $id)
					->update('tr_transaksi', array(
											'del' => 1,
											'edit_stamp' => timestamp()
											)
					);
	}
	function checkQuantity($str, $id_no_card){
		$str = intval($str);
		$pos = $this->db->select("SUM(qty) pos")->where(array('id_no_card'=>$id_no_card,  'type'=>1))->get('tr_item_movement')->row_array();
		$neg = $this->db->select("SUM(qty) neg")->where(array('id_no_card'=>$id_no_card,  'type'=>2))->get('tr_item_movement')->row_array();
		
		if($str > ($pos['pos'] - $neg['neg'])){
			return array(false, $pos['pos'] - $neg['neg']);
		}else{
			return true;
		}
	}
	function searchNoOutput(){
		$query = "	SELECT
		                id,
		                name,
		                date
					FROM ".$this->table."
					WHERE del = 0 AND name LIKE ? AND tipe = 2";

	    $query = $this->db->query($query, array('%'.$_POST['search'].'%'))->result_array();
	    // echo $this->db->last_query();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']]['no'] = $value['no'];
	        $data[$value['id']]['date'] = $value['date'];
	    }
	    return $data;
	}
	function getDataDetail($id){

		$query = "	SELECT
							b.code,
							c.name nama_komag,
							a.tipe_transaksi,
							e.name nama_gudang,
							d.name nama_tipe,
							a.qty,
							a.remark,
							a.location,
							a.id

					FROM tr_transaksi a
					LEFT JOIN ms_no_card b ON a.id_no_card = b.id
					LEFT JOIN ms_komag c ON a.id_komag = c.id
					LEFT JOIN tb_type_item d ON a.id_tipe = d.id
					LEFT JOIN tb_gudang e ON a.id_gudang = e.id
					WHERE a.del = 0 AND a.id = ".$id." ";

		$query = $this->db->query($query, array($id));
		// echo $this->db->last_query();
		return $query->row_array();

	}
	function duplicate($id){
		$dataTransaksi = $this->selectData($id);
		if($data['detail']==9){
			$this->db->insert('ms_transaksi',array(
				'tipe'			=>1,
				'detail'		=>4,
				'id_gudang'		=>$dataTransaksi['gudang_tujuan'],
				'no_output_form'=>$dataTransaksi['no'],
				'date_output_form'=>$dataTransaksi['date'],
				'entry_stamp'=>timestamp(),
				'status'=>0,
				'approve_by'=>1,

			));
		}else{
			$this->db->insert('ms_transaksi',array(
				'tipe'			=>1,
				'detail'		=>3,
				'id_gudang'		=>$dataTransaksi['gudang_tujuan'],
				'no_output_form'=>$dataTransaksi['no'],
				'date_output_form'=>$dataTransaksi['date'],
				'entry_stamp'=>timestamp(),
				'status'=>0,
				'approve_by'=>1,

			));
		}
		

		$__id = $this->db->insert_id();

		$query = "	SELECT *
					FROM tr_transaksi a
					WHERE a.del = 0 AND id_transaksi = ".$id." ";
		$query = $this->db->query($query)->result_array();
		$admin = $this->session->userdata('admin');


		foreach ($query as $key => $value) {
			$dataKartu = $this->db->where('id_no_card', $value['id_no_card'])->get('tr_transaksi')->row_array();
			$dataLastTransaksi = $this->db->query('SELECT * FROM tr_transaksi WHERE id_no_card = ? AND price IS NOT NULL', array($value['id_no_card']))->row_array();
			$_price = ($dataLastTransaksi['price']!=NULL) ?  $dataLastTransaksi['price'] : $dataKartu['price'];
			$dataLast = $this->getLastTransaksi($value['id_no_card']);

			//cekRata2Harga
			if($dataLast['price']!=$_price){
				$price = $this->cekRataHarga($dataLast['qty'], $qty, $dataLast['price'], currency($_POST['price']));
			}else{
				$price = $_price;
			}

			$this->db->insert('tr_transaksi', array(
				'id_transaksi'=>$__id,
				'id_komag'=>$value['id_komag'],
				'id_gudang'=>$dataTransaksi['gudang_tujuan'],
				'id_tipe'=>$value['id_tipe'],
				'tipe_transaksi'=>1,
				'code'=>$value['code'],
				'qty'=>$value['qty'],
				'date'=>date('Y-m-d'),
				'proyek_investasi'=>$value['proyek_investasi'],
				'price'=>$price,
				'price_contract'=>$price,
				'serial_number'=>$value['serial_number'],
				'year'=>$value['year'],
				'location_before'=>$value['location_before'],
				'condition'=>$value['condition'],
				'entry_stamp'=>timestamp(),
				'entry_by'=>$admin['id_user']
			));
		}

	
	}
	function duplikatDetail($id_source, $id_target){
		$query = "	SELECT * 
					FROM tr_transaksi a 
					WHERE a.del = 0 AND id_transaksi = ".$id_source." ";
		$query = $this->db->query($query)->result_array();
		$admin = $this->session->userdata('admin');
		foreach ($query as $key => $value) {
			$dataKartu = $this->db->where('id_no_card', $value['id_no_card'])->get('tr_transaksi')->row_array();
			$dataLastTransaksi = $this->db->query('SELECT * FROM tr_transaksi WHERE id_no_card = ? AND price IS NOT NULL', array($value['id_no_card']))->row_array();
			// echo print_r($dataLastTransaksi);
			$_price = ($dataLastTransaksi['price']!=NULL) ?  $dataLastTransaksi['price'] : $dataKartu['price'];
			// echo $_price;
			$dataLast = $this->getLastTransaksi($value['id_no_card']);
			
			if($dataLast['price']!=$_price){
				$price = $this->cekRataHarga($dataLast['qty'], $qty, $dataLast['price'], currency($_POST['price']));
			}else{
				$price = $_price;
			}
			
			$this->db->insert('tr_transaksi', array(
				'id_transaksi'=>$id_target,
				'id_komag'=>$value['id_komag'],
				'id_no_card'=>$value['id_no_card'],
				'id_gudang'=>$value['id_gudang'],
				'id_tipe'=>$value['id_tipe'],
				'tipe_transaksi'=>1,
				'code'=>$value['code'],
				'no_card'=>$value['no_card'],
				'date'=>date('Y-m-d'),
				'proyek_investasi'=>$value['proyek_investasi'],
				'price'=>$price,
				'price_contract'=>$price,
				'serial_number'=>$value['serial_number'],
				'year'=>$value['year'],
				'location_before'=>$value['location_before'],
				'location_after'=>$value['location_after'],
				'condition'=>$value['condition'],
				'location'=>$value['location'],
				'remark'=>$value['remark'],
				'entry_stamp'=>timestamp(),
				'entry_by'=>$admin['id_user']
			));
		}
	}
	function prosesNoKartu($id){
		$data = $this->db->where('id_transaksi', $id)->get('tr_transaksi')->result_array();

		foreach($data as $key => $value){
			$_POST['no_komag'] = $value['id_komag'];
			$_POST['id_gudang']=$value['id_gudang'];
			$_POST['id_tipe']=$value['id_tipe'];
			$id_no_card = $this->generateNomor();
			$this->db 	->where('id', $value['id'])
						->update('tr_transaksi', array(
							'id_no_card'=>$id_no_card['id'],
							'no_card'=>$id_no_card['code'],
						));

			// $queryMovement = $this->db->insert('tr_item_movement',
			// 			array(
			// 				'id_no_card'=>$id_no_card['id_no_card'],
			// 				'no_card'=>$id_no_card['no_card'],
			// 				'type'=>$value['tipe_transaksi'],
			// 				'qty'=>$value['qty'],
			// 				'entry_stamp'=>timestamp()
			// 				)
			// 		);
		}
		
	}
	function getLastTransaksi($id_no_card){
		$query = "SELECT * FROM `tr_transaksi` WHERE id_no_card = ? ORDER BY id DESC LIMIT 0,1";
		$query = $this->db->query($query, array($id_no_card))->row_array();
		// echo $this->db->last_query();
		return $query;
	}
	function cekRataHarga($qty1,$qty2, $harga1, $harga2){
		$qty1 = intval($qty1);
		$qty2 = intval($qty2);
		$harga1 = intval($harga1);
		$harga2 = intval($harga2);
		// echo $qty1 .' * '.$harga1.' + '. $qty2 .' * '.$harga2.' / '.($qty1+$qty2);
		return (($qty1 * $harga1) + ($qty2 * $harga2)) / ($qty1 + $qty2);
	}
	function reject($id){
		$query = $this->db 	->where('id', $id)
							->update('ms_transaksi', array(
								'reject'=>$_POST['reject'],
								'is_reject'=>1
							));
		return $query;
	}
	function resend($id){
		return $this->db->where('id', $id)->update('ms_transaksi', array('is_reject'=>0, 'reject'=>null));
	}
	function getLastPrice($id_no_card){
		$query = "SELECT * FROM `tr_transaksi` WHERE id_no_card = ? AND price > 0 ORDER BY id DESC LIMIT 0,1";
		$query = $this->db->query($query, array($id_no_card))->row_array();
		return $query['price'];
	}
	function gagal($id=""){
		$this->db->where('id', $id)->update('ms_procurement', array('step'=>4));
		$this->db->insert('tr_remark', array('id_procurement'=>$id, 'status'=>4,'remark'=>$_POST['remark']));
		return $id;
	}
	function batal($id=""){
		$this->db->where('id', $id)->update('ms_procurement', array('step'=>5));
		$this->db->insert('tr_remark', array('id_procurement'=>$id, 'status'=>5,'remark'=>$_POST['remark']));
		return $id;
	}
	function get_rekap_kontrak($mekanisme, $step, $jenis_anggaran){
		$query = "SELECT COUNT(*) ct, (SELECT SUM(contract_price) FROM ms_contract b WHERE b.id_procurement = a.id) contract_price,
		(SELECT SUM(c.value) FROM ms_invoice c WHERE c.id_procurement = a.id) contract_invoice FROM ms_procurement a WHERE a.id_mekanisme = ? AND a.step = ? AND jenis_anggaran REGEXP ? ";
		if ($admin['id_role'] == 2) {
			# code...
			$query .= " AND a.id_pengguna = ".$admin['id_division'];
		}
		$query = $this->db->query($query, array($mekanisme, $step, $jenis_anggaran));
		return $query->row_array();
	}

	function get_rekap_prosentase($mekanisme){
		$query = "SELECT COUNT(*) ct, SUM(idr_budget_investasi) + SUM(idr_budget_operasi) nilai_anggaran, SUM(idr_value) hps,
		(SELECT SUM(contract_price) FROM ms_contract d WHERE d.id_procurement = a.id) contract_price FROM ms_procurement a WHERE a.id_mekanisme = ? AND a.step = 3";
		if ($admin['id_role'] == 2) {
			# code...
			$query .= " AND a.id_pengguna = ".$admin['id_division'];
		}
		$query = $this->db->query($query, array($mekanisme));
		return $query->row_array();
	}
	function getRemark($id, $step){
		$data = $this->db->where('id_procurement', $id)->where('status', $step)->get('tr_remark');
		$data = $data->row_array();
		return $data['remark'];
	}
}
