<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Json_provider_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}
	
	function get_barang($id_lelang = ''){
		$sql = "SELECT * FROM ms_procurement_barang WHERE id_procurement = ? ORDER BY id ASC"; 
		return $this->db->query($sql, array($id_lelang));
	} 

	function select_barang($id_barang = ''){
		$sql = "SELECT a.nama_barang, a.nilai_hps, b.symbol, a.id_kurs FROM ms_procurement_barang a LEFT JOIN tb_kurs b ON a.id_kurs = b.id WHERE a.id = ?";
		$sql = $this->db->query($sql, array($id_barang));

		return $sql->row_array();
	}
	
	function select_lelang($id_lelang = ''){
		$sql = "SELECT a.*, b.kriteria_pemenang FROM ms_procurement a LEFT JOIN ms_procurement_tatacara b ON a.id = b.id_procurement WHERE a.id = ?";
		$sql = $this->db->query($sql, array($id_lelang));

		return $sql->row_array();
	}
	
	function get_peserta($id_lelang = ''){
		$sql = "SELECT a.id_vendor id_vendor, 
					   b.name AS name
				
				FROM ms_procurement_peserta a 
				LEFT JOIN ms_vendor b ON a.id_vendor = b.id 
				WHERE a.del = 0 AND a.id_proc = ? ORDER BY a.id ASC"; 
		
		return $this->db->query($sql, array($id_lelang));
	}
	
	function get_total_penawaran($id_barang = '', $id_vendor = ''){
		$sql = "SELECT COUNT(id) AS nilai FROM ms_penawaran WHERE id_barang = ? AND id_vendor = ?";	
		$sql = $this->db->query($sql, array($id_barang, $id_vendor));
		$sql = $sql->row_array(); 
		
		return $sql['nilai'];
	}
	
	function get_initial_data($id_lelang = '', $id_barang = '', $is_update = false){
		$peserta = $this->get_peserta($id_lelang);
		$fill = $this->select_lelang($id_lelang);
		$barang = $this->get_barang($id_lelang);
		$return = array();
		
		foreach($peserta->result() as $_peserta){
			$_return = array();

			if($fill['kriteria_pemenang']=='harga_satuan'){
				$sql = "SELECT in_rate AS nilai, in_rate_satuan AS nilai_satuan, entry_stamp FROM ms_penawaran WHERE id_barang = ? AND id_vendor = ? AND id_procurement = ? ORDER BY entry_stamp "; 
				if($is_update) $sql .= " DESC LIMIT 0,1";
				else $sql .= " ASC";
				
				$sql = $this->db->query($sql, array($id_barang, $_peserta->id_vendor, $id_lelang));

				foreach($sql->result() as $data) {

					array_push($_return, array('x' => $data->entry_stamp, 'y' => $data->nilai_satuan));
				}
					
			}else if($fill['kriteria_pemenang']=='lump_sum')
			{

				$sql = "SELECT in_rate AS nilai, entry_stamp, id_barang FROM ms_penawaran WHERE id_vendor = ? AND id_procurement = ? ORDER BY entry_stamp "; 
				if($is_update) $sql .= " DESC LIMIT 0,".count($barang->num_rows());
				else $sql .= " ASC";


				$sql = $this->db->query($sql, array($_peserta->id_vendor, $id_lelang));

				
				$data_penawaran = array();
				$count_penawaran = 0;
				
				foreach($sql->result_array() as $data){
					$data_penawaran[$count_penawaran]['id_vendor'] = $_peserta->id_vendor;
					$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['nilai'] = $data['nilai'];
					$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['entry_stamp'] = $data['entry_stamp'];
					
					if(count($data_penawaran[$count_penawaran]['val'])==$barang->num_rows()){
						$count_penawaran++;
					}
				}
				
				if(!$is_update){
					foreach ($data_penawaran as $key => $value) {
						$nilai = 0;
						if($value['id_vendor']==$_peserta->id_vendor){
							foreach ($value['val'] as $key_penawaran=> $result_penawaran) {

								$nilai 		+= $result_penawaran['nilai'];
								$entry_stamp 	= $result_penawaran['entry_stamp'];

							}
							array_push($_return, array('x' => $entry_stamp, 'y' => $nilai));
						}
						
					}	
					// 
				}
				else{
					$last_id = (count($data_penawaran))-1;
					
					$nilai = 0;
					foreach ($data_penawaran[$last_id]['val'] as $key_penawaran=> $result_penawaran) {
						$nilai 		+= $result_penawaran['nilai'];
						$entry_stamp 	= $result_penawaran['entry_stamp'];
						
					}
					array_push($_return, array('x' => $entry_stamp, 'y' => $nilai));
					
				}
				
			}
			array_push($return, array('name' => $_peserta->name, 'data' => $_return));
			
		}

		
		return $return;
	}
	
	function get_chart_update($id_lelang = ''){
		$sql = "SELECT id FROM ms_procurement_barang WHERE id_procurement = ?";
		$sql = $this->db->query($sql, $id_lelang);
		$fill = $this->select_lelang($id_lelang);
		$return = array();
		if($fill['kriteria_pemenang']=='harga_satuan'){
			foreach($sql->result() as $data){
				array_push($return, array('id' => $data->id, 'data' => $this->get_initial_data($id_lelang, $data->id, true)));
			}
				
		}elseif($fill['kriteria_pemenang']=='lump_sum'){
			array_push($return, array('id' => $id_lelang, 'data' => $this->get_initial_data($id_lelang, null, true)));	
		}
		
		return $return;
	}
	
	function cek_hps($id_lelang = '', $id_barang = ''){
		$sql = "SELECT a.hps, 
					   a.id_kurs,
					   (SELECT rate FROM ms_procurement_barang WHERE id_kurs = a.id_kurs AND id_procurement = a.id_procurement) as rate 
					   
					   FROM ms_barang_lelang a 
					   WHERE a.id_procurement = ?"; 
		
		if($id_barang) $sql .= " AND a.id = ? ";
		
		$sql = $this->db->query($sql, array($id_lelang, $id_barang));
		$sql = $sql->row_array();
		
		return $sql;
	}
	
	function convert_to_idr($nilai = '', $id_kurs = '', $id_lelang = ''){
		$sql = "SELECT * 

					FROM ms_procurement_barang a

					JOIN ms_procurement_kurs b

					ON b.id_procurement = a.id_procurement

					WHERE a.id_kurs = ? 
					AND a.id_procurement = ? 


		";
		$sql = $this->db->query($sql, array($id_kurs, $id_lelang));
		$sql = $sql->row_array();
		
		if($sql['id_kurs'] == 1) $sql['rate'] = 1;
		
		return ($nilai * $sql['rate']);
	}
	
	function get_same_offers($id_barang = '', $value = '', $id_vendor = ''){
		$ord = '';
		$is_latest = $this->get_latest_pos($id_barang, $ord);
		
		$sql = "SELECT a.in_rate
						   
					   FROM ms_penawaran a
					   
					   WHERE a.id_barang = ? 
					   AND a.in_rate = ?
					   AND a.id_vendor <> ?";
		
		$sql = $this->db->query($sql, array($id_barang, $value, $id_vendor));
		$sql = $sql->num_rows();
		
		if(($is_latest == $value) and $sql) return 1;
		else return 0;
	}
	
	function get_latest_pos($id_barang = '', $ord = ''){
		$sql = "SELECT in_rate FROM ms_penawaran WHERE id_barang = ? ORDER BY in_rate ".$ord;
		$sql = $this->db->query($sql, $id_barang);
		$sql = $sql->row_array();
		
		return $sql['in_rate'];
	}
	
	function get_list($id_lelang='', $symbol = '', $ord = '', $id_barang = '', $id_vendor = '', $is_latest = false, $is_same = false){
		$fill = $this->select_lelang($id_lelang);
		if($fill['kriteria_pemenang']=='harga_satuan'){
			$field = 'in_rate_satuan';
		}else{
			$field = 'in_rate';
		}
		$arr = array();
		$sql = "SELECT a.id,
					   a.".$field."
					   FROM ms_penawaran a
					   WHERE a.id_procurement = ? AND a.id_barang = ? ";
		$arr[] = $id_lelang;
		$arr[] = $id_barang;
		if(!$is_latest){ 
			$sql .= "AND(a.".$field." ".$symbol."= (SELECT ".$field." FROM ms_penawaran WHERE id_barang = a.id_barang AND id_vendor = ? ORDER BY ".$field." ".$ord." LIMIT 0,1)) "; 
			$arr[] = $id_vendor;
			if($is_same){
				$sql .= " AND (a.entry_stamp < (SELECT entry_stamp FROM ms_penawaran WHERE id_barang = a.id_barang AND id_vendor = ? ORDER BY ".$field." ".$ord." LIMIT 0,1)) ";
				$arr[] = $id_vendor;
			}
			$sql .= " GROUP BY id_vendor ";
		}
		else {
			$sql .= ' AND a.id_vendor = ?';
			$arr[] = $id_vendor;
		}
			
		$sql .= " ORDER BY in_rate ".$ord."";
		if($is_latest) $sql .= " LIMIT 0,1";

		$res = $this->db->query($sql, $arr);

		return $res;
	}
	
	function get_rank($id_lelang = '', $id_user = ''){	
		$query = $this->get_barang($id_lelang);
		$fill = $this->select_lelang($id_lelang);
		$return = array();
						
		if($fill['auction_type'] == "forward_auction"){ $ord = "DESC"; $symbol = ">"; } 
		else if($fill['auction_type'] == "reverse_auction"){ $ord = "ASC"; $symbol = "<"; } 
		
		foreach($query->result() as $data){
			$rank = 1;
			
			$latest = $this->get_list($id_lelang, $symbol, $ord, $data->id, $id_user, true);
			$latest = $latest->row_array();

			$latest = $latest['in_rate'];

			$hps = $this->convert_to_idr($data->nilai_hps, $data->id_kurs, $id_lelang);
			
			if($fill['auction_jenis']=='auction'||$fill['auction_jenis']=='rfi_penunjukkan'){
				if($fill['auction_type'] == "forward_auction")		{ if($hps > $latest) $rank = 0; } 
				else if($fill['auction_type'] == "reverse_auction")	{ if($hps < $latest) $rank = 0; } 
			}			
			if($rank){
				$is_same = $this->get_same_offers($data->id, $latest, $id_user);
				$rank = $this->get_list($id_lelang, $symbol, $ord, $data->id, $id_user, false, $is_same);
				// echo $this->db->last_query();
				$rank = $rank->num_rows();

				if($is_same) $rank++;
			}
		
			array_push($return, array('id' => $data->id, 'rank' => $rank));
		}
		
		return $return;
	}
	
	function get_rank_lump_sum($id_lelang = '', $id_user = ''){	
		$query = $this->get_barang($id_lelang);
		$fill = $this->select_lelang($id_lelang);
		$return = array();
		$query = "SELECT 
				    b.id_vendor,
				    SUM(b._min) __min,
				    d.multiply
				FROM
				    ms_penawaran as a
				        JOIN
				    (SELECT 
						c.id, 
				        MIN(in_rate) _min,
				        c.id_vendor
				    FROM
				        ms_penawaran c
				    WHERE
				        id_procurement = ? GROUP BY c.id_vendor , id_barang) b ON b.id = a.id
					JOIN (
						SELECT sum((nilai_hps * volume)) multiply FROM ms_procurement_barang WHERE id_procurement = ?
					) d

				GROUP BY b.id_vendor ORDER BY SUM(b._min) ASC";
		$query = $this->db->query($query, array($id_lelang,$id_lelang));
		foreach($query->result() as $key => $data){
			array_push($return, array('id' => $data->id_vendor, 'is_hps'=>($data->__min>$data->multiply),'rank' => ($key+1)));
		}
		return $return;
	}

	function get_lowest($id_lelang = '', $id_barang = '', $id_user = ''){
		$sql = "SELECT id 
					   FROM ms_penawaran 
					   WHERE 
					   		in_rate < (SELECT in_rate FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? AND id_vendor = ? ORDER BY nilai ASC LIMIT 0,1) AND  
					    	id_barang = ? 
					   
					   GROUP BY id_vendor ORDER BY nilai ASC";

		$sql = $this->db->query($sql, array($id_lelang, $id_barang, $id_user, $id_barang));
		$data = $sql->num_rows();
		$data++;
				
		return $data;
	}
	
	function cek_posisi_penawaran($id_lelang = '', $id_barang = '', $type = ''){
		$hps = $this->cek_hps($id_lelang, $id_barang);
		$hps = $this->convert_to_idr($hps['hps'], $hps['id_kurs'], $id_lelang);
				
		if($type == "forward_auction")		$ord = "DESC";
		else if($type == "reverse_auction")	$ord = "ASC";
		
		$sql = "SELECT in_rate FROM ms_penawaran WHERE id_procurement = ? AND id_barang = ? ORDER BY in_rate ".$ord." LIMIT 0,1";
		$sql = $this->db->query($sql, array($id_lelang, $id_barang));
	 	$sql = $sql->row_array();
	 		 	
	 	if($type == "forward_auction"){
	 		if($sql['in_rate'] < $hps) return true;
	 	}
		else if($type == "reverse_auction"){
	 		if($sql['in_rate'] > $hps) return true;
	 	}
	 	else 
	 		return false;
	}
	
}