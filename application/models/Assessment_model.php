<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Assessment_model extends MY_Model{
	function __construct(){
		
		parent::__construct();
	}
	function getData($form){
		$admin = $this->session->userdata('admin');
		
		$query = "	SELECT 
						a.name, 
						b.vendor_name, 
						c.score,
						d.name category,
						a.id
					FROM ms_procurement a 
					
					LEFT JOIN ms_contract b ON b.id_procurement = a.id
					LEFT JOIN tr_assessment c ON c.id_procurement = a.id AND c.del = 0
					LEFT JOIN tb_assessment_category d ON c.category = d.id
					WHERE a.del = 0 AND a.id_pengguna = ".$admin['id_division']."
					
					GROUP BY a.id
					";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), TRUE);
		}	
		// echo $query;
		return $query;
	}
	function getAssessment($id_procurement){
		$query = "SELECT *, 
					b.name kategori, 
					c.name name_procurement, 
					d.vendor_name, 
					d.contract_date,
					 d.no_contract,
					 d.contract_price,
					 c.type assessment_type,
					 a.date assessment_date
					FROM tr_assessment a
					JOIN tb_assessment_category b ON a.category = b.id 
					JOIN ms_procurement c ON a.id_procurement = c.id 
					LEFT JOIN ms_contract d ON d.id_procurement = c.id
					JOIN tb_pengguna e ON c.id_pengguna = e.id
					WHERE a.id_procurement = ? AND a.del = 0";
		$query = $this->db->query($query, array($id_procurement));
		return $query->row_array();
	}
	function getProcurement($id_procurement){
		$dpt_type = "SELECT * FROM ms_procurement a WHERE a.id = ? AND a.del = 0";
		$dpt_type = $this->db->query($dpt_type, array($id_procurement))->row_array();
		return $dpt_type;

	}
	function getHeader($id_procurement){
		$type =$this->getProcurement($id_procurement);

		if($type['type']==1||$type['type']==3){
			$id_tipe_contract = 1;
		}else{
			$id_tipe_contract = 2;
		}

		$query = "SELECT a.* FROM tb_assessment_group a WHERE a.id_dpt_type = ? AND a.del = 0";
		$query = $this->db->query($query, array($id_tipe_contract));

		return $query->result_array();
	}
	function getOption($id_procurement){
		$type=$this->getProcurement($id_procurement);
		
		if($type['type']==1||$type['type']==3){
			$id_tipe_contract = 1;
		}else{
			$id_tipe_contract = 2;
		}

		$query = "SELECT a.* FROM tb_assessment a RIGHT JOIN tb_assessment_group b ON a.id_group = b.id WHERE b.id_dpt_type = ? AND a.del = 0";
		$query = $this->db->query($query, array($id_tipe_contract));
		return $query->result_array();
	}
	function getAnswer($id_procurement){
		$query = "SELECT a.* FROM tr_assessment_result a WHERE a.id_procurement = ? AND a.del = 0";
		$query = $this->db->query($query, array($id_procurement));
		$query = $query->result_array();
		$result = array();
		foreach ($query as $key => $value) {
			$result[$value['id_assessment']] = $value;
		}
		return $result;
	}
	function save_assessment_data($data, $id_procurement){
		
		$point = 100;
		$this->db->where('id_procurement',$id_procurement)->update('tr_assessment_result', array('del'=>1,'edit_stamp'=>timestamp()));
		foreach ($data as $key => $value) {

			$this->db->insert('tr_assessment_result', array(
				'id_procurement'=>$id_procurement,
				'id_assessment'=>$key,
				'value'=>$value,
				'entry_stamp'=>timestamp(),
				'del'=>0
				));
			
			$point-=$value;

		}

		$query = "SELECT id FROM tb_assessment_category WHERE max >= ".$point." AND min < ".$point;
		$query = $this->db->query($query)->row_array();
		$category = $query['id'];
		$this->db->where('id_procurement',$id_procurement)->update('tr_assessment', array('del'=>1,'edit_stamp'=>timestamp()));
		$this->db->insert('tr_assessment', array(
				'date'=>date('Y-m-d'),
				'id_procurement'=>$id_procurement,
				'score'=>$point,
				'category'=>$category,
				'date'=>date('Y-m-d'),
				'entry_stamp'=>timestamp(),
				'del'=>0
				));

		$query = "SELECT SUM(a.score) total, COUNT(*) ct FROM tr_assessment a WHERE del = 0 AND a.id_procurement = ?";
		$query = $this->db->query($query, array($id_procurement));
		$query = $query->row_array();
		$pointNKK = $query['total'] / $query['ct'];

		$nkk = "SELECT * FROM tr_nkk WHERE id_vendor = ?";
		$nkk = $this->db->query($nkk, array($id_vendor))->row_array();

		$categoryNKK = "SELECT id FROM tb_nkk_category WHERE max >= ".$pointNKK." AND min < ".$pointNKK;
		$categoryNKK = $this->db->query($categoryNKK)->row_array();
		$category = $categoryNKK['id'];

		if($nkk==null){
			$this->db->insert('tr_nkk', array(
				'id_vendor' => $id_vendor,
				'point'=>$pointNKK,
				'category'=>$category,
				'entry_stamp'=>timestamp()
				));
		}else{
			$this->db->update('tr_nkk', array(
				'id_vendor' => $id_vendor,
				'point'=>$pointNKK,
				'category'=>$category,
				'edit_stamp'=>timestamp()
				));
		}
		
		return true;
	}
	function get_pemenang($id_procurement){
		$query = "	SELECT c.id_vendor
					FROM ms_procurement a
					LEFT JOIN ms_contract c ON c.id_procurement = a.id
					WHERE a.del = 0 AND id_procurement = ? ";
		$query = $this->db->query($query, array($id_procurement));
		$query =  $query->row_array();
		return $query['id_vendor'];
	}
	function get_default_template(){
		$query = "	SELECT 
						*
					FROM
						tb_bast_print
					ORDER BY id DESC
					LIMIT 0,1
				";
		$query = $this->db->query($query);

		return $query;
	}
	function get_data_bast($id_procurement){
		$query = "	SELECT 
						a.*,
						b.name
					FROM
						tr_bast a

					JOIN ms_procurement b ON b.id = a.id_procurement

					WHERE 
						id_procurement = ?
						
					
				";
		$query = $this->db->query($query, array($id_procurement));
		return $query;
	}
	function get_bast_fill($id){
		$query =	"	SELECT
							a.name pekerjaan,
							b.contract_price besaran,
							b.contract_price_kurs besaran_kurs,
							b.no_contract no_kontrak,
							b.contract_date tanggal_kontrak,
							b.contract_date tanggal_kontrak,
							c.symbol,
							c.name nama_kurs,
							b.vendor_name,
							a.contract_type tipe_kontrak
						FROM
							ms_procurement a

						LEFT JOIN ms_contract b ON b.id_procurement = a.id
						LEFT JOIN tb_kurs c ON b.contract_kurs = c.id						
						

						WHERE a.id = ?
					";
		$query = $this->db->query($query,array($id));
		
		return $query;
	}
	function get_amandemen($id){
		$query = "SELECT * FROM ms_amandemen WHERE id_procurement = ? ORDER BY id DESC ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
	function insert_bast($id){
		$bast = $this->get_data_bast($id)->row_array();
		if(empty($bast)){
			return $this->db->insert('tr_bast',
					array(
						'id_procurement'=>$id,
						'value'=>$this->input->post('text'),
						'entry_stamp'=>date('Y-m-d H:i:s')
					)
				);
		}else{
			return $this->db 	->where('id_procurement',$id)
						->update('tr_bast',
									array(
										'value'=>$this->input->post('text'),
										'edit_stamp'=>date('Y-m-d H:i:s')
										)
								);
		}
		
	}
	function get_rekap_assessment(){
		$query = "	SELECT 
						b.name,
						b.id_mekanisme,
						c.vendor_name,
						a.date,
						a.score,
						d.name name_category
					FROM tr_assessment a 
					LEFT JOIN ms_procurement b ON a.id_procurement = b.id
					LEFT JOIN ms_contract c ON a.id_procurement = c.id_procurement
					LEFT JOIN tb_assessment_category d ON a.category = d.id WHERE DATE(a.date) BETWEEN DATE(?) AND DATE(?)";
		$query = $this->db->query($query, array($_POST['day_start'], $_POST['day_end']));
		$return =  $query->result_array();
		$result = array();
		foreach ($return as $key => $value) {
			$mekanisme = array(1=>'Pembelian Langsung', 2=>'Penunjukan Langsung', 3=>'Penunjukan Langsung Kondisi Tertentu', 4=>'Pemilihan Langsung', 5=>'Pelelangan', 6=>'Penunjukkan Langsung Melalui Penugasan');
			$result[$value['vendor_name']][$value['id']] = $value;
			$result[$value['vendor_name']][$value['id']]['mekanisme'] = $mekanisme[$value['id_mekanisme']];
		}

		foreach ($result as $key => $value) {
			$nilai_konsolidasi = 0;
			foreach ($value as $keyVendor => $valueVendor) {
				$nilai_konsolidasi +=$valueVendor['score'];
			}
			$result[$key]['nilai_konsolidasi'] = $nilai_konsolidasi;
		}
		return $result;
	}
}