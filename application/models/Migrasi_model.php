<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Migrasi_model extends CI_Model{
	public $dev;
	function __construct(){
		parent::__construct();
		$this->dev = $this->load->database('vms_pgn_dev', TRUE);
		$this->beta = $this->load->database('betavms', TRUE);
	}

	function migrasi_admin_dan_user(){

		$query = $this->beta->query("SELECT * FROM ms_login WHERE type = 'user'")->result_array();
		foreach ($query as $key => $value) {
			$array = array(
				'id_user'=>$value['id_user'],
				'username'=>$value['username'],
				'type'=>'user',
				'password'=>do_hash($value['password_raw'], 'sha1'),
				'password_raw'=>$value['password_raw'],
				'entry_stamp'=>date('Y-m-d H:i:s')
			);
			$this->dev->insert('ms_login', $array);
		}
	}
	public function proses_beku() 
	{
		$prevDate = date('Y-m-d',strtotime(date('Y-m-d').'-2 Years'));
		
		$query = "UPDATE ms_vendor SET vendor_status = 4, is_active = 0 WHERE DATE(edit_stamp) <= DATE('".$prevDate."') AND vendor_status != 2";
		$this->dev->query($query);
	}
}