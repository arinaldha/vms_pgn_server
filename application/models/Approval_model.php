<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Approval_model extends MY_Model {
	
	function __construct(){
		parent::__construct();
	}

	function get_data_administrasi($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT 
						a.id id,
						a.id_sbu,
						id_legal, 
						a.name as name,
						a.npwp_code as npwp_code, 
						npwp_file, 
						npwp_date,
						nppkp_code, 
						nppkp_file, 
						nppkp_date, 
						vendor_office_status, 
						vendor_address, 
						vendor_country, 
						vendor_phone, 
						vendor_province, 
						vendor_fax, 
						vendor_city, 
						vendor_email, 
						vendor_postal, 
						vendor_website,
						vendor_type, 
						c.name as sbu_name, 
						d.name as legal_name,
						b.id id_admistrasi,
						b.email_pic,
						data_status,
						( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_vendor_admistrasi' AND e.is_active = 1 AND e.id_vendor = ".$id." AND e.id_document = b.id) total_note
				   FROM 
				   		ms_vendor a
				   LEFT JOIN
				   		ms_vendor_admistrasi b ON b.id_vendor=a.id
				   LEFT JOIN
				   		tb_sbu c ON c.id=a.id_sbu
				   LEFT JOIN
				   		tb_legal d ON d.id=b.id_legal
				   WHERE 
				   		a.id = ? ";

		$query = $this->db->query($query, array($id));

		return $query->row_array();
	}
	public function get_bank($id_vendor)
	{
		$query = "SELECT 
					no, 
					name,
					bank_name,
					bank_file
				  FROM 
				  	ms_rekening
				  WHERE 
				  	del = 0 AND id_vendor =".$id_vendor;
		return $query;
	}
	function get_data_situ($id)
	{
		$query = "SELECT 
						a.type,
						a.no,
						a.issue_date,
						a.address,
						a.file_photo,
						a.expire_date,
						a.situ_file,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_situ' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_situ a 

				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	function verifikasi_situ($id)
	{
		$query = "SELECT 
						a.type,
						a.no,
						a.issue_date,
						a.issued_by,
						a.address,
						a.file_photo,
						a.expire_date,
						a.situ_file,
						a.id,
						a.data_status
				   FROM 
				   		ms_situ a 

				   WHERE 
				   		a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_akta($id, $type){
		$query = "SELECT 
						a.type,
						a.notaris,
						a.no,
						a.issue_date,
						a.akta_file,
						a.authorize_by,
						a.authorize_no,
						a.authorize_date,
						a.authorize_file,
						a.id,
						a.expire_date,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_akta' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_akta a
				   WHERE 
				   		a.del = 0 AND id_vendor = $id AND type = '".$type."'";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	function verifikasi_akta($id){

		$query = "SELECT 
						a.type,
						a.notaris,
						a.no,
						a.issue_date,
						a.akta_file,
						a.authorize_by,
						a.authorize_no,
						a.authorize_date,
						a.authorize_file,
						a.id,
						a.expire_date,
						a.id_vendor,
						a.data_status
				   FROM 
				   		ms_akta a 
				   WHERE 
				   		a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_tdp($id)
	{
		$query = "SELECT 
						a.no,
						a.issue_date,
						a.authorize_by,
						a.expire_date,
						a.tdp_file,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_tdp' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_tdp a
				   WHERE 
				   		a.del = 0 AND id_vendor = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	function verifikasi_tdp($id)
	{
		$query = "SELECT 
						a.*
				   FROM 
				   		ms_tdp a
				   WHERE 
				   		a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_pengalaman($id)
	{
		$query = "SELECT 
						a.job_name,
						b.name bidang,
						c.name sub_bidang,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start,
						a.price_idr,
						a.price_foreign,
						a.contract_end,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pengalaman' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}

	function verifikasi_pengalaman($id)
	{

		$query = "SELECT 
						a.*,
						a.job_name,
						b.name bidang,
						c.name sub_bidang,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start,
						a.currency,
						a.price_idr,
						a.price_foreign,
						a.contract_end,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id,
						a.data_status
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.del = 0 AND a.id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_pemilik($id){
		$query = "SELECT 
						a.name,
						a.position,
						a.percentage,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pemilik' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_pemilik a
				   
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	function verifikasi_pemilik($id){
		$query = "SELECT 
						a.name,
						a.position,
						a.percentage,
						a.data_status,
						a.id,
						a.id_akta,
						a.id_vendor,
						a.data_status
				   FROM 
				   		ms_pemilik a
				   
				   WHERE 
				   		a.del = 0 AND a.id = ? ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_pengurus($id)
	{
		$query = "	SELECT 
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        b.no no_akta,
                        a.pengurus_file,
                        a.id,
                        a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pengurus' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
                   FROM 
                        ms_pengurus a
                   INNER JOIN
                        ms_akta b ON b.id = a.id_akta
                   WHERE 
                        a.del = 0 AND a.id_vendor = ".$id;
        return $query;
	}
	function verifikasi_pengurus($id)
	{
		$query = "	SELECT 
						a.*,
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        b.no no_akta,
                        a.pengurus_file,
                        a.id,
                        a.data_status,
                        a.id_vendor
                   FROM 
                        ms_pengurus a
                   INNER JOIN
                        ms_akta b ON b.id = a.id_akta
                   WHERE 
                        a.del = 0 AND a.id = ?";
        $query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_agen($id)
	{
		$query = "SELECT
						a.no,
						a.issue_date,
						a.type,
						a.expire_date,
						a.agen_file,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_agen' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				  FROM  
				  		ms_agen a
				  WHERE 
				  		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}
	function get_data_product($id_agen)
	{
		$query = "SELECT
						a.produk,
						a.merk,
						a.data_status,
						a.id
				  FROM  
				  		ms_agen_produk a
				  WHERE 
				  		a.del = 0 AND a.id_agen = ".$id_agen;
		return $query;
	}

	function verifikasi_agen($id)
	{
		$query = "SELECT
						a.no,
						a.issue_date,
						a.type,
						a.expire_date,
						a.agen_file,
						a.id,
						a.data_status,
						a.id
				  FROM  
				  		ms_agen a
				  WHERE 
				  		a.del = 0 AND a.id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function verifikasi_agen_produk($id)
	{
		$query = "SELECT
						 produk,
						 merk,
						 id,
						 data_status
				  FROM  
				  		 ms_agen_produk
				  WHERE 
				  		del = 0 AND id_agen = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_izin($id,$type)
	{
		$query = "SELECT 
						a.issue_date,
						a.no,
						a.qualification,
						a.authorize_by,
						a.izin_file,
						a.expire_date,
						a.id as id,
						b.name dpt_name,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_ijin_usaha' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_ijin_usaha a
				   INNER JOIN
				   		tb_dpt_type b ON b.id = a.id_dpt_type
				   WHERE 
				   		a.del = 0 AND a.id_vendor = $id AND a.type ='".$type."'";

		return $query;
	}

	function get_bsb_dropdown($id)
	{
		$query = "	SELECT
						a.id id,
						CONCAT(b.name, '-'  ,c.name) as name

					FROM 
						ms_iu_bsb a
					LEFT JOIN
						tb_bidang b ON b.id = a.id_bidang
					LEFT JOIN
						tb_sub_bidang c ON c.id = a.id_sub_bidang
					WHERE 
						 a.del = 0 AND a.id_vendor = ? ";

		$query = $this->db->query($query,array($id))->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}

	function verifikasi_izin($id){
		$query = "SELECT 
						a.*,
						a.issue_date,
						a.qualification,
						a.authorize_by,
						a.izin_file,
						a.expire_date,
						a.id as id,
						b.name dpt_name
				   FROM 
				   		ms_ijin_usaha a
				   INNER JOIN
				   		tb_dpt_type b ON b.id = a.id_dpt_type
				   WHERE 
				   		a.del = 0 AND a.id = ?  ";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function approve($id){

		$this->db->where('id',$id);

		$update_status = $this->db->update('ms_vendor',array('vendor_status'=>2,'need_approve'=>0,'is_new'=>0));

		$data = $this->db->where('id',$id)->get('ms_vendor')->row_array();

		if($data['dpt_first_date']==''){
			$this->db->where('id',$id)->update('ms_vendor',array('dpt_first_date'=>date('Y-m-d H:i:s')));
		}

		$getIjinBsb = $this->getIjinBsb($id);
		if (count($getIjinBsb) > 0) {
			foreach ($getIjinBsb as $key => $value) {
				$this->db->insert('tr_dpt',array(
					'id_vendor' 	=>$value['id_vendor'],
					'id_dpt_type'	=>$value['id_dpt_type'],
					'id_bidang'		=>$value['id_bidang'],
					'id_sub_bidang'	=>$value['id_sub_bidang'],
					'start_date'	=>$data['dpt_first_date'],
					'entry_stamp'	=>date('Y-m-d H:i:s'),
					'status'		=>1
				));
			}
		}

		$cleanDB = $this->cleanDB();

		foreach($cleanDB as $key => $value){
			if(count($cleanDB)  > 0){
				$this->db->insert('tr_dpt',array(
						'id_vendor'		=>$value['id_vendor'],
						'id_dpt_type'	=>$value['id_dpt_type'],
						'id_bidang'		=>$value['id_bidang'],
						'id_sub_bidang'	=>$value['id_sub_bidang'],
						'start_date'	=> $data['dpt_first_date'],
						'entry_stamp'	=>date('Y-m-d H:i:s'),
						'status'		=>1
					)
				);
			}
		}

		$query = "	UPDATE tr_dpt a 
				RIGHT JOIN ms_ijin_usaha b 
					ON ( 		b.id_dpt_type = a.id_dpt_type 
							AND b.id_vendor = a.id_vendor 
							AND (b.del IS NULL OR b.del = 0)
						)
   				RIGHT JOIN ms_pengalaman c 
   					ON ( 		b.id = c.id_ijin_usaha 
   							AND (c.del = NULL OR c.del = 0)
   						) 

   				SET a.status = 1,
   					a.start_date = ?,
   					a.edit_stamp = ?

				WHERE a.id_vendor = ? 
				AND ( a.status = 0 OR a.status IS NULL)
				AND c.id IS NOT NULL";

    	return $this->db->query($query, array( date('Y-m-d') , date('Y-m-d H:i:s'), $id));
    	// echo $this->db->last_query();die;
	}

	public function getIjinBsb($id)
	{
		$query = "	SELECT
						a.id,
						b.id_vendor,
						a.id_ijin_usaha,
						b.id_dpt_type,
						a.id_bidang,
						a.id_sub_bidang,
						b.data_status
					FROM 
						ms_iu_bsb a
					INNER JOIN
						ms_ijin_usaha b ON b.id=a.id_ijin_usaha
					INNER JOIN
						ms_pengalaman c ON c.id_iu_bsb=a.id
					WHERE 
						b.id_vendor = ? AND (b.data_status = 1 OR b.data_status = 2)
					GROUP BY 
						a.id_sub_bidang";
		$query = $this->db->query($query,array($id));
		return $query->result_array();
	}

	public function cleanDB()
	{
		$query = "	SELECT 
					    *
					FROM
					    ms_vendor a
					        LEFT JOIN
					    (SELECT 
					        COUNT(*) cts, b.id_vendor
					    FROM
					        tr_dpt b WHERE b.status = 1
					    GROUP BY b.id_vendor ) b ON a.id = b.id_vendor
							LEFT JOIN
						ms_ijin_usaha c ON a.id = c.id_vendor
							LEFT JOIN 
						ms_iu_bsb d ON c.id = d.id_ijin_usaha
							LEFT JOIN
						ms_pengalaman e ON e.id_iu_bsb = d.id
					WHERE a.vendor_status = 2 AND cts IS NULL AND e.del = 0 AND c.del = 0";
		return $this->db->query($query)->result_array();
	}
	function checkPengalamanBSB($id_vendor){
		$query = "	SELECT a.id, e.name bidang_name, f.name sub_bidang_name -- ,b.name bidang_name, c.name sub_bidang_name
					FROM ms_iu_bsb a 
                    LEFT JOIN (
						SELECT b.id, c.id_sub_bidang FROM ms_pengalaman b JOIN ms_iu_bsb c ON b.id_iu_bsb = c.id WHERE b.id_vendor = ?
					) d ON a.id_sub_bidang = d.id_sub_bidang
                    
                    JOIN tb_bidang e ON a.id_bidang=e.id
					JOIN tb_sub_bidang f ON a.id_sub_bidang=f.id
                    WHERE d.id IS NULL AND a.id_vendor = ? GROUP BY a.id_sub_bidang";
		$query = $this->db->query($query,array($id_vendor, $id_vendor));
		
		return $query;
	}

	function get_total_data($id){
		$this->load->library('DPT');
		$table = array(
			'ms_akta'=>'Akta',
			'ms_situ'=>'SITU/Domisili',
			'ms_tdp'=>'TDP',
			'ms_pengurus'=>'Pengurus',
			'ms_pemilik'=>'Kepemilikan Saham',
			'ms_ijin_usaha'=>'Izin Usaha',
			'ms_agen'=>'Pabrikan/Keagenan/Distributor',
			'ms_pengalaman'=>'Pengalaman');
		$table_surat = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha');
		$result = array(0=>array(),1=>array(),2=>array(),3=>array(),4=>array());
		$total=0;

		$adm = $this->db->select('data_status')->where('id_vendor',$id)->get('ms_vendor_admistrasi')->row_array();
		$result[($adm['data_status']==NULL)?0:$adm['data_status']][]['title'] = 'Data Administrasi Vendor';
		$total+=1;
		foreach($table as $field=>$label){
			$no = '';
			$this->db->where('id_vendor',$id);

			$this->db->where('('.$field.'.del IS NULL OR '.$field.'.del = 0)');

			
			$res = $this->db->get($field)->result_array();

			foreach($res as $key=>$data){
				$arrayResult = array();
				$arrayResult['title'] = $table[$field];
				$arrayResult['field'] = $field;
				$arrayResult['data'] = $data;
				$result[(($data['data_status']==NULL)?0:$data['data_status'])][]= $arrayResult;
				
				$total+=1;
			}
		}
		
		$result['total'] = $total;
		return $result;
	}

	public function update_ms_vendor_administrasi($id, $data)
	{
		$this->db->where('id_vendor',$id);
		return $this->db->update('ms_vendor_admistrasi',array('vendor_type'=>$data['vendor_type']));
	}
	function get_recipient_admin($id_role, $id_sbu){
		$query = 	"	SELECT 
							email
						FROM 
							ms_admin a
						WHERE 
							a.id_role = ?
							AND a.id_sbu = ?
					";
		return $this->db->query($query,array($id_role,$id_sbu))->result_array();

	}
	function angkat_vendor($id){
		return $this->db->where('id',$id)->update('ms_vendor',array('need_approve'=>1));
	}
	function reject($id){
		$this->db->where('id',$id);
		return $this->db->update('ms_vendor',array('need_approve'=>0,'edit_stamp'=>date('Y-m-d H:i:s')));
	}
}