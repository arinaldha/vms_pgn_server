<?php

class Main_model extends CI_model{

	public function check()
	{
		$username = $this->input->post('username');

		$_password = $this->input->post('password');
		$password = do_hash($this->input->post('password'),'sha1');

		$ldap = $this->authenticate($username,$_password);
		// print_r($ldap);die;
		// echo $ldap['employeeid'][0];die;
		if (!empty($ldap)) {
			$ct_sql = "SELECT *,
						ms_admin.name,
						tb_role.name role_name, 
						tb_sbu.name sbu_name
						FROM ms_admin 
						JOIN tb_role ON ms_admin.id_role = tb_role.id 
						LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
						WHERE ms_admin.nipg=?";
				$ct_sql = $this->db->query($ct_sql, array($ldap['employeeid'][0]));
				$data = $ct_sql->row_array();
				if ($ct_sql->num_rows() == 0) {
					$set_session = array(
						'id_user' 		=> 	$ldap['employeeid'][0],
						'name'			=>	$ldap['displayname'][0],
						'id_sbu'		=>	41,
						'id_role'		=>	5,
						'role_name'		=>	'Public User',
						'sbu_name'		=>	$ldap['department'][0],
					);	
				} else {
					$set_session = array(
						'id_user' 		=> 	$ldap['employeeid'][0],
						'name'			=>	$ldap['displayname'][0],
						'id_sbu'		=>	$data['id_sbu'],
						'id_role'		=>	$data['id_role'],
						'role_name'		=>	$data['role_name'],
						'sbu_name'		=>	$ldap['department'][0],
					);
					
				}

				$this->session->set_userdata('admin',$set_session);
				return true;
		} 
		else {
			$sql = "SELECT * FROM ms_login WHERE username = ? AND password = ?";

			$sql = $this->db->query($sql, array($username, $password));
			// echo $this->db->last_query();
			$_sql = $sql->row_array();
			$ct_sql = '';
			if($sql){

				if($_sql['type'] == "user"){

					$ct_sql = "SELECT ms_vendor.*  FROM ms_vendor WHERE ms_vendor.id=? AND ms_vendor.vendor_status != 3";
					$ct_sql = $this->db->query($ct_sql,array($_sql['id_user']));
					$data = $ct_sql->row_array();
						

					if ($ct_sql->num_rows() > 0) {
						$set_session = array(
							'id_user' 		=> 	$data['id'],
							'name'			=>	$data['name'],
							'id_sbu'		=>	$data['id_sbu'],
							'vendor_status'	=>	$data['vendor_status'],
							'is_active'		=>	$data['is_active'],
							'id_role'		=>	11,
							'npwp_code'		=>  $data['npwp_code']
						);
					
						$this->session->set_userdata('user',$set_session);

						return true;
					}
					
				}else if($_sql['type'] == "admin"){
					$ct_sql = "SELECT *,
								ms_admin.name,
								tb_role.name role_name, 
								tb_sbu.name sbu_name
								FROM ms_admin 
								JOIN tb_role ON ms_admin.id_role = tb_role.id 
								LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
								WHERE ms_admin.id=?";
					$ct_sql = $this->db->query($ct_sql, array($_sql['id_user']));

					$data = $ct_sql->row_array();
					
					$set_session = array(
						'id_user' 		=> 	$data['id'],
						'name'			=>	$data['name'],
						'id_sbu'		=>	$data['id_sbu'],
						'id_role'		=>	$data['id_role'],
						'role_name'		=>	$data['role_name'],
						'sbu_name'		=>	$data['sbu_name'],
					);
					
					$this->session->set_userdata('admin',$set_session);

					return true;
				}

			}
			else{
				return false;
			}
		}
	}

	public function authenticate($usr, $pwd) {
		$_usr = $usr;
		// echo $pwd;die;
		$domain = "corp\\";
		if (substr($usr,0,5) == $domain){
		$usr = strtolower ($usr);
		}
		else {	
			$usr = strtolower($domain.$usr);
		}
		// echo $usr;die;
		$ldapconn = ldap_connect("corp.pgn.co.id", 389) or die("Tidak terhubung ke server LDAP.");
	 	if ($ldapconn && ldap_bind($ldapconn, $usr, $pwd)) {
	 		// echo "123".$pwd;die;
	       	$dn = "ou=Pusat, ou=UserAccounts, dc=corp, dc=pgn, dc=co, dc=id";
			$filter="(sAMAccountName=" . $_usr . ")";

			$sr=ldap_search($ldapconn, $dn, $filter);
			$info = ldap_get_entries($ldapconn, $sr);
			if($info['count']>0){
				for($x = 0; $x < $info['count']; $x++){
					return $info[$x];
				}
			}else{
				return false;
			}
		}else{
			// echo "123".$pwd;die;
			return false;
		}
		
	}

	public function check__()
	{
		$username = $this->input->post('username');

		$_password = $this->input->post('password');
		$password = do_hash($this->input->post('password'),'sha1');

		$url = 'https://hcm-api.pgn.co.id/api/vms/authenticate';
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "usr=".$username."&pwd=".$_password."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);

		$out = curl_exec($ch);
		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch);
		}
		curl_close($ch);
		// print_r($out);

		$json_object = json_decode($out);

				if($json_object->count==1){
					$json_object = $json_object->{0};
					$get_username =$json_object->userprincipalname->{0};
					$get_name = explode('@',$name);
					$ldap_username = $get_name[0];
					$ct_sql = "SELECT *,
							ms_admin.name,
							tb_role.name role_name, 
							tb_sbu.name sbu_name
							FROM ms_admin 
							JOIN tb_role ON ms_admin.id_role = tb_role.id 
							LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
							WHERE ms_admin.nipg=?";
					$ct_sql = $this->db->query($ct_sql, array($json_object->employeeid->{0}));
					$data = $ct_sql->row_array();
					if ($ct_sql->num_rows() == 0) {
						$set_session = array(
							'id_user' 		=> 	$json_object->employeeid->{0},
							'name'			=>	$json_object->displayname->{0},
							'id_sbu'		=>	41,
							'id_role'		=>	5,
							'role_name'		=>	'Public User',
							'sbu_name'		=>	$json_object->department->{0},
						);	
					} else {
						$set_session = array(
							'id_user' 		=> 	$json_object->employeeid->{0},
							'name'			=>	$json_object->displayname->{0},
							'id_sbu'		=>	$data['id_sbu'],
							'id_role'		=>	$data['id_role'],
							'role_name'		=>	$data['role_name'],
							'sbu_name'		=>	$json_object->department->{0},
						);
						
					}

					$this->session->set_userdata('admin',$set_session);
					return true;
				}else {
					$sql = "SELECT * FROM ms_login WHERE username = ? AND password = ?";

					$sql = $this->db->query($sql, array($username, $password));
					// echo $this->db->last_query();
					$_sql = $sql->row_array();
					$ct_sql = '';
					if($sql){

						if($_sql['type'] == "user"){

							$ct_sql = "SELECT ms_vendor.*  FROM ms_vendor WHERE ms_vendor.id=? AND ms_vendor.vendor_status != 3";
							$ct_sql = $this->db->query($ct_sql,array($_sql['id_user']));
							$data = $ct_sql->row_array();
								

							if ($ct_sql->num_rows() > 0) {
								$set_session = array(
									'id_user' 		=> 	$data['id'],
									'name'			=>	$data['name'],
									'id_sbu'		=>	$data['id_sbu'],
									'vendor_status'	=>	$data['vendor_status'],
									'is_active'		=>	$data['is_active'],
									'id_role'		=>	11,
									'npwp_code'		=>  $data['npwp_code']
								);
							
								$this->session->set_userdata('user',$set_session);

								return true;
							}
							
						}else if($_sql['type'] == "admin"){
							$ct_sql = "SELECT *,
										ms_admin.name,
										tb_role.name role_name, 
										tb_sbu.name sbu_name
										FROM ms_admin 
										JOIN tb_role ON ms_admin.id_role = tb_role.id 
										LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
										WHERE ms_admin.id=?";
							$ct_sql = $this->db->query($ct_sql, array($_sql['id_user']));

							$data = $ct_sql->row_array();
							
							$set_session = array(
								'id_user' 		=> 	$data['id'],
								'name'			=>	$data['name'],
								'id_sbu'		=>	$data['id_sbu'],
								'id_role'		=>	$data['id_role'],
								'role_name'		=>	$data['role_name'],
								'sbu_name'		=>	$data['sbu_name'],
							);
							
							$this->session->set_userdata('admin',$set_session);

							return true;
						}

					}
					else{
						return false;
					}
				}
	}

	function check_($data = array()){

		$username = $this->input->post('username');

		$_password = $this->input->post('password');
		$password = do_hash($this->input->post('password'),'sha1');

		$unparsed_json = file_get_contents('http://hc-services.pgn.co.id/pgn_absen/api/authenticate/'.$username.'?pass='.$_password);
		//echo 'http://hc-services.pgn.co.id:8080/pgn_absen/api/authenticate/'.$username.'?pass='.$_password;
		$json_object = json_decode($unparsed_json);
		
		// print_r($json_object);
				// if($json_object->count==1){
				// 	$json_object = $json_object->{0};
				// 	$get_username =$json_object->userprincipalname->{0};
				// 	$get_name = explode('@',$name);
				// 	$ldap_username = $get_name[0];
				// 	$ct_sql = "SELECT *,
				// 			ms_admin.name,
				// 			tb_role.name role_name, 
				// 			tb_sbu.name sbu_name
				// 			FROM ms_admin 
				// 			JOIN tb_role ON ms_admin.id_role = tb_role.id 
				// 			LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
				// 			WHERE ms_admin.nipg=?";
				// 	$ct_sql = $this->db->query($ct_sql, array($json_object->employeeid->{0}));
				// 	$data = $ct_sql->row_array();
				// 	if ($ct_sql->num_rows() == 0) {
				// 		$set_session = array(
				// 			'id_user' 		=> 	$json_object->employeeid->{0},
				// 			'name'			=>	$json_object->displayname->{0},
				// 			'id_sbu'		=>	41,
				// 			'id_role'		=>	5,
				// 			'role_name'		=>	'Public User',
				// 			'sbu_name'		=>	$json_object->department->{0},
				// 		);	
				// 	} else {
				// 		$set_session = array(
				// 			'id_user' 		=> 	$json_object->employeeid->{0},
				// 			'name'			=>	$json_object->displayname->{0},
				// 			'id_sbu'		=>	$data['id_sbu'],
				// 			'id_role'		=>	$data['id_role'],
				// 			'role_name'		=>	$data['role_name'],
				// 			'sbu_name'		=>	$json_object->department->{0},
				// 		);
						
				// 	}

				// 	$this->session->set_userdata('admin',$set_session);
				// 	return true;
				// }else {
					$sql = "SELECT * FROM ms_login WHERE username = ? AND password = ?";

					$sql = $this->db->query($sql, array($username, $password));
					// echo $this->db->last_query();
					$_sql = $sql->row_array();
					// print_r($_sql);die;
					$ct_sql = '';
					if($sql){

						if($_sql['type'] == "user"){

							$ct_sql = "SELECT ms_vendor.*  FROM ms_vendor WHERE ms_vendor.id=? AND ms_vendor.vendor_status != 3";
							$ct_sql = $this->db->query($ct_sql,array($_sql['id_user']));
							$data = $ct_sql->row_array();
								

							if ($ct_sql->num_rows() > 0) {
								$set_session = array(
									'id_user' 		=> 	$data['id'],
									'name'			=>	$data['name'],
									'id_sbu'		=>	$data['id_sbu'],
									'vendor_status'	=>	$data['vendor_status'],
									'is_active'		=>	$data['is_active'],
									'id_role'		=>	11,
									'npwp_code'		=>  $data['npwp_code']
								);
							
								$this->session->set_userdata('user',$set_session);

								return true;
							}
							
						}else if($_sql['type'] == "admin"){
							$ct_sql = "SELECT *,
										ms_admin.name,
										tb_role.name role_name, 
										tb_sbu.name sbu_name
										FROM ms_admin 
										JOIN tb_role ON ms_admin.id_role = tb_role.id 
										LEFT JOIN tb_sbu ON ms_admin.id_sbu = tb_sbu.id 
										WHERE ms_admin.id=?";
							$ct_sql = $this->db->query($ct_sql, array($_sql['id_user']));

							$data = $ct_sql->row_array();
							
							$set_session = array(
								'id_user' 		=> 	$data['id'],
								'name'			=>	$data['name'],
								'id_sbu'		=>	$data['id_sbu'],
								'id_role'		=>	$data['id_role'],
								'role_name'		=>	$data['role_name'],
								'sbu_name'		=>	$data['sbu_name'],
							);
							
							$this->session->set_userdata('admin',$set_session);

							return true;
						}

					}
					else{
						return false;
					}
				// }
				
	}
	
	function get_daftar_tunggu_chart(){

		$query = " 	SELECT 
						*

					FROM 
						ms_vendor a

					WHERE 
						a.vendor_status = 1
						AND a.is_active = 1
					";
		// if($this->session->userdata('admin')['id_role']==8){
		// 	$query .= " AND a.need_approve = 1 ";
		// }
		// if(!in_array($this->session->userdata('admin')['id_role'], array(1,2))){
		// 	$query .= " AND a.id_sbu = ".$this->session->userdata('admin')['id_sbu'];
		// }
		$query .=	" ORDER BY 
						a.edit_stamp DESC
						
					";
		$result = $this->db->query($query);
		return $result;

	}

	function daftar_hitam_chart(){

		$query = " 	SELECT 
						*

					FROM 
						tr_blacklist b

					LEFT JOIN 
						ms_vendor a ON b.id_vendor = a.id

					WHERE 
						a.is_active = 0 
						AND (b.del = 0 OR b.del IS NULL)
						AND (a.del = 0 OR b.del IS NULL)";
		// if(!in_array($this->session->userdata('admin')['id_role'], array(1,2,8))){
		// 	$query .= " AND a.id_sbu = ".$this->session->userdata('admin')['id_sbu'];
		// }
		
		$result = $this->db->query($query);
		// echo $this->db->last_query();
		return $result;

	}

	function dpt_chart(){

		$query = " 	SELECT 
						*,a.id
					FROM 
						ms_vendor a

					LEFT JOIN 
						tr_dpt b ON b.id_vendor = a.id 

					WHERE 
						a.is_active = 1
						AND a.vendor_status = 2
						AND a.del = 0
						AND NOT EXISTS(
							SELECT 1 FROM tr_blacklist_nik c JOIN ms_pengurus msp ON msp.no = c.nik WHERE msp.id_vendor = a.id AND (c.del = 0 OR c.del IS NULL)
						)
						AND NOT EXISTS(
							SELECT 1 FROM tr_blacklist d WHERE d.id_vendor = a.id AND (d.del = 0 OR d.del IS NULL)
						)";
		// if(!in_array($this->session->userdata('admin')['id_role'], array(1,2,8))){
		// 	$query .= " AND a.id_sbu = ".$this->session->userdata('admin')['id_sbu'];
		// }
		$query .= "
					GROUP BY
						a.id


					ORDER BY 
						b.start_date DESC


					";
		$result = $this->db->query($query);
		return $result;

	}
}
