<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Baseline_kontrak_model extends MY_Model{

	public $table = 'ms_baseline';

	function __construct(){

		parent::__construct();
	}
	function updateProgress($id, $data){
		$data['del'] = 0;
		foreach ($data as $key => $value) {
			if(is_array($value)){
				$data[$key] = implode(',',$value);
			}
		}
		$data['idr_budget_operasi'] = str_replace(",", "", $data['idr_budget_operasi']);
		$data['idr_budget_investasi'] = str_replace(",", "", $data['idr_budget_investasi']);
		$this->db->where('id', $id)->update('ms_baseline', $data);
	}

	function getData($form){
		$admin = $this->session->userdata('admin');
		// print_r($admin);die;
		$query = "	SELECT
							a.name,
							a.plan_date,
							a.id,
							a.status
					FROM ".$this->table." a
					WHERE a.del = 0 ";
		
		if ($admin['id_role'] == 2) {
			# code...
			$query .= "AND id_pengguna = ".$admin['id_division'];
		}

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function getDashboard($form){
		$admin = $this->session->userdata('admin');
		// print_r($admin);die;
		$query = "	SELECT
							a.name,
							a.id,
							a.status
					FROM ".$this->table." a
					WHERE a.del = 0 ";
		
		if ($admin['id_role'] == 2) {
			$query .= " AND id_pengguna = ".$admin['id_division'];
		}
		if ($admin['id_role'] == 3) {
			$query .= " AND status = 0";
		}

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	function insert($data){
		$data['del'] = 0;	
		foreach ($data as $key => $value) {
			if(is_array($value)){
				$data[$key] = implode(',',$value);
			}
		}
		$a =  $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	function selectData($id){
		$query = "	SELECT
						a.name,
						a.jenis_anggaran,
						a.id_pengguna,
						a.idr_budget_investasi,
						a.idr_budget_operasi,
						a.type,
						a.id_mekanisme,
						a.plan_date,
						a.real_date,
						a.contract_date,
						a.status,
						a.pic_name,
						a.pic_email
						
						
					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		// echo $this->db->last_query();
		return $query->row_array();
	}
	public function approve($id){
		return $this->db->where('id', $id)->update($this->table, array('status'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
	}
	
	
	function searchBaseline(){
		$query = "	SELECT
		                id,
		                name,
		                plan_date,
		                pic_email,
		                pic_name,
						id_pengguna,
						id_mekanisme,
						jenis_anggaran,
						idr_budget_investasi,
						idr_budget_operasi

					FROM ".$this->table."
					WHERE del = 0 AND name LIKE ? AND status = 1";

	    $query = $this->db->query($query, array('%'.$_POST['search'].'%'))->result_array();
	    // echo $this->db->last_query();
			$data = array();
	    foreach($query as $key => $value){
	        $data[$value['id']]['name'] 		= $value['name'];
	        $data[$value['id']]['plan_date'] 	= $value['plan_date'];
	        $data[$value['id']]['pic_name'] 	= $value['pic_name'];
	        $data[$value['id']]['pic_email'] 	= $value['pic_email'];
	        $data[$value['id']]['id_pengguna'] 	= $value['id_pengguna'];
	        $data[$value['id']]['id_mekanisme'] 	= $value['id_mekanisme'];
	        $data[$value['id']]['jenis_anggaran'] 	= $value['jenis_anggaran'];
	        $data[$value['id']]['idr_budget_investasi'] 	= number_format((float)$value['idr_budget_investasi'], 2, '.', ',');
	        $data[$value['id']]['idr_budget_operasi'] 	= number_format((float)$value['idr_budget_operasi'], 2, '.', ',');
	    }
	    return $data;
	}
	
	function get_baseline($id_mekanisme){
		// echo print_r($_POST);
		$query = "SELECT COUNT(*) ct, (SUM(idr_budget_investasi) + SUM(idr_budget_operasi)) nilai FROM ms_baseline WHERE status = 1 AND id_mekanisme = ? AND DATE(plan_date) BETWEEN DATE(?) AND DATE(?)";
		$query = $this->db->query($query, array($id_mekanisme, $_POST['day_start'], $_POST['day_end']));
		return $query->row_array();
	}
	function getKontrakDate($id_mekanisme){
		$year = 2018;
		$query = "SELECT a.id id_procurement, a.name,a.kurs_value, a.jenis_anggaran, c.name budget_holder, a.remark FROM ms_procurement a JOIN ms_baseline b ON a.id_baseline = b.id JOIN tb_budget_holder c ON a.id_pengguna = c.id WHERE  a.id_mekanisme = ? AND YEAR(plan_date) = ".$year;
		$result = $this->db->query($query, array($id_mekanisme))->result_array();
		

		foreach ($result as $key => $value) {
			$data_progress = "SELECT MIN(real_date) min_real_date, MAX(real_date) max_real_date FROM tr_progress_pengadaan WHERE id_procurement = ?";
			$data_progress = $this->db->query($query, array($value['id_procurement']))->row_array();
			$result[$key]['data_progress'] = $data_progress;

			$kontrak = $this->db->where('id_procurement', $value['id_procurement'])->get('ms_contract')->row_array();

			$result[$key]['contract_date'] = $kontrak['contract_date'];

			$bast = $this->db->where('id_procurement', $value['id_procurement'])->get('ms_bast')->row_array(); 
			$result[$key]['bast_date'] = $bast['date'];
		}

		return $result;

	}
}
