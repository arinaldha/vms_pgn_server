<!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
<!-- <script src="https://code.highcharts.com/modules/variable-pie.js"></script> -->
<script type="text/javascript">

Highcharts.chart('graph', {
    chart: {
        type: 'pie',
    },
    title: {
        text: 'Grafik Penyedia Barang/Jasa.'
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'Total : <b>{point.y}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '0%',
        zMin: 0,
        name: 'DPT',
        data: [{
            name: "DPT (<?php echo $chart['dpt_chart']->num_rows(); ?>)",
            y: <?php echo $chart['dpt_chart']->num_rows(); ?>,
            z: <?php echo $chart['dpt_chart']->num_rows(); ?>
        }, {
            name: "Daftar Hitam (<?php echo $chart['daftar_hitam_chart']->num_rows(); ?>)",
            y: <?php echo $chart['daftar_hitam_chart']->num_rows(); ?>,
            z: <?php echo $chart['daftar_hitam_chart']->num_rows(); ?>
        }, {
            name: "Daftar Tunggu (<?php echo $chart['daftar_tunggu_chart']->num_rows(); ?>)",
            y: <?php echo $chart['daftar_tunggu_chart']->num_rows(); ?>,
            z: <?php echo $chart['daftar_tunggu_chart']->num_rows(); ?>
        }]
    }]
});

$(function(){

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    dataPost = {
        order: 'id',
        sort: 'desc',
    };

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var table = $('#daftar_tunggu').tableGenerator({
        url: '<?php echo site_url('dashboard/get_daftar_tunggu/'.$id); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "name",
                "value" : "Nama Vendor"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                html += '<a href="'+site_url+"approval/index/"+btoa(data[1].value)+'" class="button is-primary"><span class="icon"><i class="fas fa-edit"></i></span>Cek Data</a>'
                return html;
            },
            target : [1]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){

        }
    });

    dataPostBlacklist = {
        order: 'id',
        sort: 'desc',
    };

    var table_blacklist = $('#blacklist').tableGenerator({
        url: '<?php echo site_url('dashboard/get_blacklist/'.$id); ?>',
        data: dataPostBlacklist,
        headers: [
            {
                "key"   : "name",
                "value" : "Nama Vendor"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                if (id_role != 5) {
                    html +=editButton(site_url+"vendor/admin/black_list/blacklist_vendor/edit/"+btoa(data[1].value), btoa(data[1].value));
                    html += '<a href="'+site_url+"vendor/admin/black_list/blacklist_vendor/aktif/"+btoa(data[1].value)+'" class="button is-primary buttonAktif"><span class="icon"><i class="fas fa-check-square"></i></span>Aktifkan</a>'
                }
                return html;
            },
            target : [1]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){
            var edit = $('.buttonEdit').modal({
                render : function(el, data){

                data.onSuccess = function(){
                    $(edit).data('modal').close();
                    table_blacklist.data('plugin_tableGenerator').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');

            }
            });
            var aktif = $('.buttonAktif').modal({
                header: 'Aktifkan Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(aktif).data('modal').close();
                    table_blacklist.data('plugin_tableGenerator').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');

            }
            });
        }
    });

    // dataPostDpt = {
    //     order: 'id',
    //     sort: 'desc',
    // };

    var table_Dpt = $('#DPT').tableGenerator({
        url: '<?php echo site_url('dashboard/get_Dpt/'.$id); ?>',
        headers: [
            {
                "key"   : "name",
                "value" : "Nama Vendor"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                if (id_role != 5) {
                    html += '<a href="'+site_url+"dpt/dpt_print/"+btoa(data[1].value)+'" class="button is-primary"><span class="icon"><i class="fas fa-print"></i></span>Print Data</a>'
                    html += '<a href="'+site_url+"certificate/index/"+btoa(data[1].value)+'" class="button is-primary"><span class="icon"><i class="fas fa-print"></i></span>Print Sertifikat</a>'
                }
                return html;
            },
            target : [1]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){

        }
    });

    if (id_role == 8) {
        var table_approve = $('#approve').tableGenerator({
        url: '<?php echo site_url('vendor/admin/daftar_tunggu/get_daftar_tunggu_dashboard/'.$id); ?>',
        headers: [
            {
                "key"   : "name",
                "value" : "Nama Vendor"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                html += '<a href="'+site_url+"approval/index/"+data[3].value+'" class="button is-primary"><i class="fa fa-upload"></i>&nbsp Angkat Vendor</a>';
                return html;
            },
            target : [1]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){

        }
    });
    }
});

</script>
