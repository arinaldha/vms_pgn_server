	<div class="blockMenuWrapper">
		<div class="mg-sm-6" >
			<div class="panel bordered">
				<div class="scrollbar" id="custom-scroll">
					<div id="graph" style="height: 500px;">
						
					</div>
				</div>
			</div>
		</div>
		<?php if ($this->session->userdata('admin')['id_role'] == 8) { ?>
		<div class="mg-sm-6">
			<div class="panel bordered">
				<div class="scrollbar" id="custom-scroll">
					<h1>Angkat Vendor</h1>
					<div id="approve">
						
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if ($this->session->userdata('admin')['id_role'] != 8) { ?>
		<div class="mg-sm-6">
			<div class="panel bordered">
				<div class="scrollbar" id="custom-scroll">
					<h1>Daftar Tunggu Penyedia Barang / Jasa.</h1>
					<div id="daftar_tunggu" style="height: 500px;">
						
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="mg-sm-6" >
				 <div class="panel bordered">

	              <div class="container-title">
	                <h3>Note</h3>
	                <div class="badge is-primary is-noticable"><?php echo count($notifikasi)?></div>
	              </div>

	              <div class="scrollbar" id="custom-scroll" style="height: 470px; overflow-x: auto;">

	              <?php foreach ($notifikasi as $key => $value) { ?>
	                 <div class="notification is-warning listNotification">
	                  <p>
	                  	<b>Nama Vendor : </b> <?php echo $value['name']; ?>
	                  </p>
	                  <p>   
	                    <b>Dokumen</b> : <?php echo $value['document']?>
	                  </p>
	                  <p>   
	                    <b><?php echo $value['subject_field']?></b> :  <?php echo $value['subject']?>
	                  </p>
	                  <p>   
	                    <b>Note</b> : <?php echo $value['value']?>
	                  </p>

	                  <a class="delete" href="<?php echo site_url('note/close/'.$value['id'])?>">X</a>

	                </div>
	               
	              <?php } ?>
		      </div>

		    </div>
		</div>
		<div class="mg-sm-6">
			<div class="panel bordered">
				<div class="scrollbar" id="custom-scroll">
					<h1>Daftar Hitam.</h1>
					<div id="blacklist">
						
					</div>
				</div>
			</div>
		</div>
		<div class="mg-sm-6">
			<div class="panel bordered">
				<div class="scrollbar" id="custom-scroll">
					<h1>DPT.</h1>
					<div id="DPT">
						
					</div>
				</div>
			</div>
		</div>
	</div>
