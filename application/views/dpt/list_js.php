<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('dpt/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('dpt/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Penyedia Barang/Jasa"
			},{
				"key"	: "npwp_code",
				"value"	: "NPWP"
			},{
				"key"	: "nppkp_code",
				"value"	: "NPPKP"
			},{
				"key"	: "sbu_name",
				"value"	: "Lokasi Pendaftaran"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				_link = '';
				return _link += '<a href="'+site_url+"vendor/view/view_data/index/"+data[5].value+'">'+data[0].value+'</a>'
			},
			target : [0]
			
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				if (id_role != 5) {
					html += '<a href="'+site_url+"dpt/dpt_print/"+data[5].value+'" class="button is-primary" target="blank"><i class="fa fa-edit"></i>&nbsp Print Data</a>';
					html += '<a href="'+site_url+"certificate/index/"+data[5].value+'" class="button is-primary" target="blank"><i class="fa fas fa-check-square"></i>&nbsp Print Sertifikat</a>';
				}
				
				return html;
			},
			target : [4]

		}
		],
		additionFeature: function(el){
			el.prepend('<a href="'+site_url+'dpt/export" class="button is-primary btnEx"><i class="fas fa-file-export"></i>Export DPT</a>')
		},
		finish: function(){
			
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		},

	});
	var ex = $('.btnEx').modal({
header: 'export',
		render : function(el, data){			
			 $.ajax({
		        url : '<?php echo site_url('reporting/getHeader/')?>',
		        method: 'POST',
		        async : false,
		        dataType : 'json',
		        success: function(xhr){
		            _xhr = xhr;
		        }
		    });
			 _filter ='';
			// console.log(encodeURIComponent(table.data('plugin_tableGenerator').options.data));
			$.each(table.data('plugin_tableGenerator').options.data, function(key, value){
				_filter += "<input type='hidden' name='"+key+"' value='"+value+"''>";
			})
			el.html('<div class="report-wrapper"><form action="<?php echo site_url('reporting/cetak')?>" method="GET">'+_filter+'<div id="base"></div><button class="button is-primary">Submit</button></form></div>');
			 var base = $('#base');
		   	 $.each(_xhr, function(key, value){
		        if(typeof value.header !='undefined'){
		            var wrapper = $('<div class="headerWrapper mg-lg-4 force-half"><h3 class="force-full">'+value.header+'</h3><ul></ul></div>');
		            base.append(wrapper);
		        }else{

		            if($('.lastWrapper').length == 0){
		                var wrapper = $('<div class="headerWrapper mg-lg-4 lastWrapper"><h3>LAINNYA</h3><ul></ul></div>');
		                base.append(wrapper);
		            }else{
		                wrapper = $('.lastWrapper');
		            }
		        }
		        
		        $.each(value.field, function(keyField, valueField){
		            if(typeof valueField=='object'){
		                var list = $('<button class="fieldOpt unchecked custom-button btn">'+valueField.title+'<label class="toggleButton"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1>&nbsp;<div><svg viewBox="0 0 44 44"><path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path></svg></label></div></button>');
		            }else{
		                var list = $('<button class="fieldOpt unchecked custom-button btn">'+valueField+'<label class="toggleButton"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1><div><svg viewBox="0 0 44 44"><path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path></svg></label></div></button>');
		            }


		            list.on('click', function(e){
		                e.preventDefault();
		                $(this).find(".input").trigger('click');
		                $(this).toggleClass('checked');
		                var cb = $('.checkboxOpt',list);
		                var ic = $('.iconOpt',list);
		                // ic.removeClass('fa-plus fa-check');
		                list.removeClass('checked unchecked');
		                if(cb.is(':checked')){
		                    cb.prop('checked',false);
		                    // ic.addClass('fa-plus');
		                    list.addClass('unchecked');
		                }else{
		                    cb.prop('checked',true);
		                    // ic.addClass('fa-check');
		                    list.addClass('checked');
		                    
		                }
		            })
		            wrapper.append(list);
		        })
		    })
		}
	});
	// var ex = $('.btnEx').modal({
	// 	header: 'Export Dpt',
	// 	render : function(el, data){
	// 		data.onSuccess = function(){
	// 			$(ex).data('modal').close();
	// 			table.data('plugin_tableGenerator').fetchData();
	// 		}
	// 		$(el).form(data);

	// 		$('.form-control').on('click', function() {
	// 			var id = $(this).val();
	// 			if (id == 'name') {
	// 				$('.form-group.form0').append('<fieldset class="form-group"><label>Nama Vendor</label><input type="text" class="form-control"></fieldset>');
	// 			}
	// 		})
	// 	}
	// });

});


</script>