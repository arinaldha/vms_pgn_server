<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('detail_kontrak/bast/getSingleData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('detail_kontrak/bast/view/'.$id);?>';

			}
			xhr.successMessage = 'Berhasil Membuat Kontrak';

			$('.formKontrak').form(xhr);
		}


	});
});


</script>