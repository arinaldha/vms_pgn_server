<?php $admin = $this->session->userdata('admin');?>
<script type="text/javascript">

$(function(){

$('#formlanjut_pengadaan').hide();
	dataPost = {
		order: 'id_progress',
		sort: 'asc',
		limit: 100
	};

	var table = $('#tableprogress_pengadaan').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/progress_pengadaan/getData/'.$id); ?>',
		data: dataPost,
		limit: false,
		pagination: false,
		headers: [
			{
				"key"	: "name",
				"value"	: "Proses Pengadaan",
				"sort"	: false
			},{
				"key"	: "plan_date",
				"value"	: "Tanggal Rencana",
				"sort"	: false
			},{
				"key"	: "real_date",
				"value"	: "Tanggal Realisasi",
				"sort"	: false
			},{
				"key"	: "attachment",
				"value"	: "Lampiran",
				"sort"	: false
			},{
				"key"	: "remark",
				"value"	: "Catatan",
				"sort"	: false
			},{
				"key"	: "remark",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [1,2]
		},{
			renderCell: function(data, row, key, el){
				<?php if($admin['id_role']==3){ ?> 
				if(row!=null && typeof row !='undefined' && row !=''){
					html = '';
					data = row.split(',');
					$.each(data, function(key, value){
						html += '<p><a href="'+base_url+'assets/lampiran/temp/'+value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a></p>';
					})
					
					return html;

				} 
				<?php }else{ ?>
					return '--';
				<?php } ?>
				
			},
			target : [3]
		},{
			renderCell: function(data, row, key, el){
				// console.log(data);
				var  _default = '';
				<?php if($dataBerkas['step']==0){ ?> 
				
				if(data[6].value=="21"&&data[1].value==null){
					var _default = '<?php echo $dataBerkas['plan_date']?>';
				}
				
				if(data[key].value!=null){
					// console.log(data[key].value);
					return '<fieldset class="form-group   form6" for=""><div class="wrapper"><input type="text" class="form-control datePicker" id="'+data[key].key+'" value="'+data[key].value+'" name="plan_date['+data[6].value+']"></div></fieldset><div class="wrapper"></div>';	
				}else{
					return '<div class="wrapper"><input type="text" class="form-control datePicker" id="'+data[key].key+'" value="'+_default+'" name="plan_date['+data[6].value+']"></div>';
				}
				<?php }?>
			},
			target : [1]
		},{
			renderCell: function(data, row, key, el){
				// console.log(data);
				var  _default = '';
				
				if(data[6].value=="21"){
					//var _default = defaultDate('<?php echo $dataBerkas['issue_date']?>');

					//return _default;
				}
				
			},
			target : [2]
		},{
			renderCell: function(data, row, key, el){
			
				var html = '';
				
				<?php if($dataBerkas['step']==1 && $admin['id_role'] ==3){ ?> 
					//if(data[6].value!="21"){
						html += '<a class="btn btn-primary btnUpdate" href="'+site_url+"detail_kontrak/Progress_pengadaan/update/<?php echo $id?>/"+data[6].value+'"><i class="fa fa-gear-o"></i>&nbsp;Update</a>';
					//}
					
				<?php }?>
				
				return html;
			},
			target : [5]

		}],
		finish: function(){
			var update = $('.btnUpdate').modal({
					header: 'Update Data',
					render : function(el, data){
						
						data.onSuccess = function(){
							$(update).data('modal').close();
							table.data('plugin_tableGenerator').fetchData();

							var dataProgress = table.data('plugin_tableGenerator').data.data;
	
							if(dataProgress[dataProgress.length - 1].real_date !=null){
								$('#formlanjut_pengadaan').show();
							}
						};
						data.isReset = true;
						$(el).form(data).data('form');
					}
				});
			var dataProgress = table;

			
		},


	});
	var dataProgress = table.data('plugin_tableGenerator').data.data;
	
	if(dataProgress[dataProgress.length - 1].real_date !=null){
		$('#formlanjut_pengadaan').show();
	}
	
	$('.graph').tooltip();
});
$('.datePicker').datetimepicker({
					timepicker: false,
					format: 'Y-m-d'
				});
</script>