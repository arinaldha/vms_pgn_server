<script type="text/javascript">

$(function(){
	$.ajax({
		url : '<?php echo site_url('procurement/amandemen/getData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			$('.formAmandemen').form(xhr);
		}


	});
	var ubah = $('.btn-ubah').modal({
		header: 'Ubah Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><div class="form"></div><div>');
			data.onSuccess = function(xhr){
				var current_index = $(".tab").tabs("option","active");
				$(".tab").tabs('load',current_index);
				$(ubah).data('modal').close();
			};
			data.isReset = true;
			
			$('.form', el).form(data).data('form');
		}
	});
});


</script>