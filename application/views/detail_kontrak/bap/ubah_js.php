<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('procurement/amandemen/edit/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('procurement/amandemen/view/'.$id);?>';

			}
			xhr.successMessage = 'Berhasil Membuat Kontrak';

			$('.formAmandemen').form(xhr);
		}


	});
});


</script>