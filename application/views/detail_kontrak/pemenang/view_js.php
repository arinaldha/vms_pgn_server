<script type="text/javascript">

$(function(){
	$.ajax({
		url : '<?php echo site_url('detail_kontrak/pemenang/getData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			$('.formPemenang').form(xhr);
		}


	});
	var ubah = $('.btn-ubah').modal({
		header: 'Ubah Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><div class="formPemenang"></div><div>');
			data.onSuccess = function(){
				var current_index = $(".tab").tabs("option","active");
				$(".tab").tabs('load',current_index);
				$(ubah).data('modal').close();
			};
			data.isReset = true;
			
			$('.formPemenang', el).form(data).data('form');
		}
	});
});


</script>