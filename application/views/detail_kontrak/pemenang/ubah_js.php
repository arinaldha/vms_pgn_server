<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('detail_kontrak/pemenang/edit/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('detail_kontrak/pemenang/view/'.$id);?>';

			}
			xhr.successMessage = 'Berhasil Menunjuk Pemenang';

			$('.form').form(xhr);
		}


	});
});


</script>