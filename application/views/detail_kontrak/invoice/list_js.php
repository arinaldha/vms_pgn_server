<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var __currency;
	
	var table = $('#tableInvoice').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/invoice/getData/'.$id); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "no",
				"value"	: "Nomor"
			},{
				"key"	: "date",
				"value"	: "Tanggal"
			},{
				"key"	: "value",
				"value"	: "Nilai"
			},{
				"key"	: "invoice_file",
				"value"	: "Lampiran"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				console.log(data);
				return 'Rp '+$.number(row,0, '.',',');
			},
			target : [2]
		},{
			renderCell: function(data, row, key, el){
				
				return '<a href="'+base_url+'assets/lampiran/'+data[3].key+'/'+data[3].value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;</span></a>';
			},
			target : [3]
		},{
			renderCell: function(data, row, key, el){
				<?php if($admin['id_role']==3){ ?> 
				html =editButton(site_url+"detail_kontrak/invoice/edit/<?php echo $id; ?>/"+data[4].value, data[4].value);
				html +=deleteButton(site_url+"detail_kontrak/invoice/remove/"+data[4].value, data[4].value);
				<?php } ?>
				
				return html;
			},
			target : [4]
		}],
		
		additionFeature: function(el){
			<?php //if($admin['id_role']==3){ ?> 
			el.append(insertButton(site_url+"detail_kontrak/invoice/insert/<?php echo $id?>"));
			<?php //} ?>
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				header: 'Ubah Data',
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						$(edit).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			data.url = site_url+"detail_kontrak/invoice/save/<?php echo $id?>";
			$(el).form(data);
		}
	});
});


</script>