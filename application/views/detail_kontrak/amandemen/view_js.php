<script type="text/javascript">
<?php $admin = $this->session->userdata('admin');?>
$(function(){
	$('.tabsAmandemen').tabs();
	var TambahAmandemen = $('.buttonTambahAmandemen').modal({
		header: 'Tambah Amandemen',
		render : function(el, data){
			data.onSuccess = function(data){
				TambahAmandemen.data('modal').close();
				location.reload = '<?php echo site_url('kontrak/view/'.$id.'#amandemen'); ?>';
			}
			$(el).form(data);
		}
	});

<?php 
	foreach ($dataAmandemen as $key => $value) { 
	?>
	$.ajax({
		url : '<?php echo site_url('detail_kontrak/amandemen/getSingleData/'.$value['id'])?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			$('#formAmandemen<?php echo $value['id']?>').form(xhr);
		}
	});

	$('#tabsAmandemen<?php echo $value['id']?>').tabs();
	$('.tabsAmandemen<?php echo $value['id']?>').tabs();
	
	
	// var type = <?php echo $dataBerkas['contract_type'];?>;
	var isTermin = 0;
	var isBarang = 0;
	$('#listTab > li').hide();
	$('#tabsAmandemen<?php echo $value['id']?> > div').hide();

		$('a[href="#terminAmandemen<?php echo $value['id']?>"]').parent().show();
		$('#terminAmandemen<?php echo $value['id']?>').show();

	
	var ubah = $('.btn-ubah').modal({
		header: 'Ubah Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><div class="form"></div><div>');
			data.onSuccess = function(xhr){
				var current_index = $(".tab").tabs("option","active");
				$('#tabs').tabs('load',1);
				$(ubah).data('modal').close();
				// location.reload = '<?php echo site_url('kontrak/view/'.$id.'#kontrak'); ?>';
			};
			data.isReset = true;
			
			$('.form', el).form(data).data('form');

		}
	});


	var table1 = $('#tableTerminAmandemen<?php echo $value['id']?>').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/ttd_kontrak/termin/'.$id.'/'.$value['id']); ?>',
		data: {},
		
		headers: [
			{
				"key"	: "termin",
				"value"	: "Termin ke - "
			},{
				"key"	: "percen",
				"value"	: "Besaran (%)"
			},{
				"key"	: "value",
				"value"	: "Besaran (Rp.)"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				
				var html = '';
				
				if(!is_empty(data[2].value)){
					html += rupiah(data[2].value);
				}
				return html;
			},
			target : [2]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if($admin['id_role']==3){ ?> 
				html +=editButton(site_url+"detail_kontrak/ttd_kontrak/editTermin/"+data[3].value, data[3].value);
				html +=deleteButton(site_url+"detail_kontrak/ttd_kontrak/deleteTermin/"+data[3].value, data[3].value);
				<?php } ?>
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
				<?php if($admin['id_role']==3){ ?> 
				el.append(insertButton(site_url+"detail_kontrak/ttd_kontrak/insertTermin/<?php echo $value['id']?>/2"));	
				<?php } ?>

		},
		finish: function(){
			var edit1 = $('.buttonEdit').modal({
		        render : function(el, data){
					data.onSuccess = function(){
						$(edit1).data('modal').close();
						table1.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = false;

					$(el).form(data).data('form');
		        }
     	 	});
			var del1 = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del1).data('modal').close();
						table1.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			
		}
	});
	var add1 = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(data){
				add1.data('modal').close();
				table1.data('plugin_tableGenerator').fetchData();
				
			}
			$(el).form(data);
		}
	});
	<?php
	}
?>

});


</script>