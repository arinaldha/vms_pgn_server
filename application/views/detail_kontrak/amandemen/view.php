<div class="mg-lg-12 row">
	<a class="btn btn-primary buttonTambahAmandemen row"  href="<?php echo site_url('detail_kontrak/amandemen/insert/'.$id)?>"><i class="fa fa-file"></i>&nbsp; Tambah Amandemen</a>
	<div class="tabsAmandemen">
		<ul class="nav-tabs">
			<?php foreach ($dataAmandemen as $key => $value) { ?>
				<li style="display: list-item;"><a href="#tabAmandemen<?php echo $value['id'];?>"><?php echo $value['no']?></a></li>
			<?php } ?>
			
		</ul>
		<?php foreach ($dataAmandemen as $key => $value) {
				?>
		<div id="tabAmandemen<?php echo $value['id'];?>">
			<div class="mg-lg-12">
				<div class="mg-lg-7">
					<div class="block ">
						<div id="formAmandemen<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
						</div>
						<div class="form-group btn-group">
							<?php $admin= $this->session->userdata('admin'); 
							if($admin['id_role']==3){ ?> 
							<a href="<?php echo site_url('detail_kontrak/amandemen/edit/'.$value['id'])?>" class="btn btn-warning btn-ubah"><i class="fa fa-gears"></i>&nbsp;Ubah</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="mg-lg-12 ">
					<div class="tabsAmandemen<?php echo $value['id'];?>">
						<ul class="nav-tabs" id="listTab">
							<li ><a href="#terminAmandemen<?php echo $value['id'];?>">Termin</a></li>
						</ul>
						<div id="terminKontrak">
							<div id="tableTerminAmandemen<?php echo $value['id'];?>">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	
</div>
