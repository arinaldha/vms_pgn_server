<script type="text/javascript">

$(function(){
    $.ajax({
        url : '<?php echo site_url('regis_vendor/insert')?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                window.location = '<?php echo site_url('regis_vendor/confirmation');?>';
            }
            xhr.successMessage = 'Berhasil Mendaftarkan Penyedia Barang / Jasa Baru';

            $('.signupWrapper').form(xhr);

            $('.form6').before('<table><tbody><tr><td style="text-align: center;vertical-align: middle;"><input type="checkbox" name="is_nppkp" value="1" class="is_nppkp"></td><td width="90%"><span>Dengan ini menyatakan bahwa saya bukan Pengusaha Kena Pajak (Non PKP). Sebagaimana dimaksud pada Undang - Undang Pajak Pertambahan Nilai.</span></td></tr></tbody></table>');
            // $('.is_nppkp').on('change', function(){
            //     $('.form6').toggle();
            //     $('.form7').toggle();
            //     $('.form8').toggle();
            // })
            $('.is_nppkp').on('change', function(){
                if (!$(this).prop('checked')) {
                    $('.form6').show()
                    $('.form7').show()
                    $('.form8').show()
                } else {
                    $('.form6').hide()
                    $('.form6 input').val('')
                    $('.form7').hide()
                    $('.form7 input').val('')
                    $('.form8').hide()
                    $('.form8 input[type="hidden"]').val('')
                    $('.form8 input[type="file"]').show()
                    $('.form8 .fileUploadBlock').remove()
                }
            });

            // page = '<fieldset class="form-group read_only   form0 " for="type"><label for="undefined"></label><b></b><span><a href="'.base_url('assets/user_manual/user_manual.pdf').'" target="blank" style="margin-left: auto;margin-right: auto;"><i class="fa fa-user"></i><span class="icon-text"> User Manual</span></a></span></fieldset>'
            // $('.form8').append(page);
            // $('.form9 .radioWrapper').on('click',function(){
            //     var val = $(this).val();
            //     alert(val)
            // })
        }


    });
    
});


</script>