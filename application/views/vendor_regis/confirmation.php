 <section class="main-content"> 

     <div class="wrapper ">

      <div class="col col-12 is-centered" style="margin-top: 5rem">

      	<div class="col col-4">
      		<div class="card">
      		
				<div class="card-header">
					<div class="pulse">
            <img src="<?php echo base_url('assets/images/checklist.png')?>" alt="" height="100px">     
          </div>
				</div>

				<div class="card-title">
					Tahap Registrasi Berhasil!
				</div>

				<div class="card-footer">
					<p>Username dan password anda untuk login ke Aplikasi Manajemen Penyedia Barang/Jasa PT Perusahaan Gas Negara Tbk 
					telah dikirim ke alamat email <b><?php echo (isset($vendor_email))?$vendor_email:''; ?></b>. </p>
					<p>Silahkan masukkan username dan password anda untuk proses pendaftaran Penyedia Barang/Jasa.</p>
					<div class="form-group btn-group">
						<a href="<?php echo site_url()?>" class="btn btn-primary">Lanjutkan</a>
					</div>
				</div>

	      	</div>
      	</div>

      </div>

    </div>

  </section>
