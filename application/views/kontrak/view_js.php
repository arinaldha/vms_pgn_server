<script type="text/javascript">
$(function(){
	var __entry_by;
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;

	var __id_user 		= <?php echo $admin['id_user']?>;
	var step = <?php echo $dataBerkas['step']?>;
	var id = <?php echo $id?>;

	var _xhr;
	$.ajax({
		url : '<?php echo site_url('kontrak/viewData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			
			$('.form').form(xhr);
			_xhr = xhr;
		}
	});

	var btnBatal = $('.btnBatal').modal({
		header:'Alasan Pembatalan',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});
	var btnGagal = $('.btnGagal').modal({
		header:'Alasan Penggagalan',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});
	
	$('#tabs').tabs({
		load: function(event, ui){
			window.location.hash = '#'+ui.tab.attr('id');
	}});
	if(window.location.hash){
		var index = $('#tabs li'+window.location.hash).index();
		$("#tabs").tabs("option", "active", index);
	}
	if(step==0){
		$('#tabs').tabs("option", "disabled", [1,2,3,4,5]);
	}
	if(step==1){
		$('#tabs').tabs("option", "disabled", [1,2,3,4,5]);
	}
	if(step==2){
		$('#tabs').tabs("option", "disabled", [2,3,4,5]);
	}
});
</script>
