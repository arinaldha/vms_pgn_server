<script type="text/javascript">

$(function(){
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('kontrak/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('kontrak/getData/'.$id); ?>',
		data: dataPost,
		headers: [{
			"key"	: "no",
			"value"	: "Nomor Permintaan"
		},
		{
			"key"	: "issue_date",
			"value"	: "Tanggal Permintaan"
		},
		{
			"key"	: "name",
			"value"	: "Nama Paket Permintaan"
		},
	  	{
			"key"	: "action",
			"value"	: "Action",
			"sort"	: false
		}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(data[1].value)
				
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				var html = '';

				switch(data[4].value){
					case "0" :
						html +='<label class="label label-default">Permintaan Pengadaan </label>';
						break;
					case "1" : 
						html +='<label class="label label-warning">Persiapan Pengadaan </label>';
						break;
					case "2" : 
						html +='<label class="label label-primary">Proses Pengadaan </label>';
						break;
					case "3" : 
						html +='<label class="label label-success">Kontrak </label>';
						break;
					case "4" : 
						html +='<label class="label label-danger">Gagal </label>';
						break;
					case "5" : 
						html +='<label class="label label-danger">Batal </label>';
						break;
				}
				html += '<a class="btn btn-default " href="'+site_url+"kontrak/view/"+data[3].value+'"><i class="fa fa-search"></i>&nbsp;Lihat Data</a>';
				html +=editButton(site_url+"kontrak/edit/"+data[3].value, data[3].value);

				
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
			<?php if($admin['id_role']==3){ ?>
				el.append(insertButton(site_url+"kontrak/insert/"));	
			<?php } ?>	
			el.append('<a href="'+site_url+'kontrak/form_rekap_kontrak/" class="btn btn-primary rekapKontrak">Rekap Pengadaan</a>');
			el.append('<a href="'+site_url+'kontrak/form_rekap_prosentase/" class="btn btn-primary rekapProsentase">Rekap Prosentase Anggaran</a>');

		},
		finish: function(){
			var edit = $('.buttonEdit').modal({ 
		render : function(el, data){
			data.onSuccess = function(data){
				$(edit).data('modal').close();
				
			}
			$(el).form(data);
			hideshow();
			hideAnggaran();
			
			$('.modal [name="budget_source"]').on('change', function(){
				hideshow();
			})
			var jenis_anggaran = $('.modal [name="jenis_anggaran[]"]').on('click', function(e){
				hideAnggaran(jenis_anggaran);
			})
			$('.modal [name="jenis_permintaan"]').on('change', function(){
				hideSearch();
			})

			$('.modal .searchInput').donetyping(function(){
				_self 			= $(this);
				_parent 		= _self.closest('.form');
				_resultWrapper 	= $('.searchOption', _parent);
				_resultWrapper.empty();

				$('.modal [name="no_form"]').remove();
				var id_baseline = $('<input type="hidden" name="id_baseline" id="id_baseline">');
				$('form .btn-group',el).before(id_baseline);
				$.ajax({
					url : '<?php echo site_url('kontrak/searchBaseline')?>',
					data : {
						search : _self.val()
					},
					method : 'POST',
					beforeSend : function(){
						$(_self).addClass('loading');
						_resultWrapper.empty();
					},
					success : function(xhr){
						
						setTimeout(function(){
							$(_self).removeClass('loading');
							var html = '<ul>';
						
							$.each( $.parseJSON(xhr), function(key, value){
								html += '<li data-id_mekanisme="'+value.id_mekanisme+'" data-id_pengguna="'+value.id_pengguna+'" data-name="'+value.name+'" data-pic_name="'+value.pic_name+'" data-pic_email="'+value.pic_email+'" data-date="'+value.plan_date+'" data-jenis_anggaran="'+value.jenis_anggaran+'" data-idr_budget_investasi="'+value.idr_budget_investasi+'" data-idr_budget_operasi="'+value.idr_budget_operasi+'" data-id="'+key+'">'+value.name+' </li>';

							});
							html += '</ul>';
							_resultWrapper.html(html);

							$('ul li', _resultWrapper).on('click', function(e){
								// alert($(this).data('pic_email'));
								id 			= $(this).data('id');
								name 		= $(this).data('name');
								_date 		= $(this).data('date');
								pic_name 	= $(this).data('pic_name');
								pic_email 	= $(this).data('pic_email');
								id_pengguna 	= $(this).data('id_pengguna');
								id_mekanisme	= $(this).data('id_mekanisme');
								jenis_anggaran	= $(this).data('jenis_anggaran');
								idr_budget_investasi	= $(this).data('idr_budget_investasi');
								idr_budget_operasi	= $(this).data('idr_budget_operasi');

								var _jenis_anggaran = jenis_anggaran.split(',');

								$.each(_jenis_anggaran, function(key, value){
									$('[name="jenis_anggaran[]"][value="'+value+'"]').attr('checked','checked');
								})
								$('input[name="nama_baseline"]').val(name);
								$('input[name="name"]').val(name);
								$('input[name="issue_date"]').val(_date);
								$('input[name="pic_name"]').val(pic_name);
								$('input[name="pic_email"]').val(pic_email);
								$('input[name="idr_budget_investasi"]').val(idr_budget_investasi);
								$('input[name="idr_budget_operasi"]').val(idr_budget_operasi);
								$('select[name="id_pengguna"]').val(16);

								$('input[name="type"] option:selected').val(id_mekanisme).change();
								$('input[name="id_baseline"]').val(id);
								hideAnggaran();
								_resultWrapper.empty();
							});
						}, 1000);
						
					}	
				});
			})

			$('.modal [name="jenis_anggaran[]"][value="investasi"]').on('change ready', function(){

				var budget_center 		= $('.modal [name="budget_center[]"]');
				var budget_element 		= $('.modal [name="budget_element[]"]');
				if($('.modal [name="jenis_anggaran[]"][value="investasi"]:checked').length > 0){

					budget_center.closest('.form-group').hide();
					budget_element.closest('.form-group').hide();
					budget_center.attr('disable','disable');
					budget_element.attr('disable','disable');
				}else{
					budget_center.closest('.form-group').show();
					budget_element.closest('.form-group').show();
					budget_center.attr('disable',false);
					budget_element.attr('disable',false);
				}

				if($('.modal [name="jenis_anggaran[]"][value="operasi"]:checked').length > 0){
					budget_center.closest('.form-group').show();
					budget_element.closest('.form-group').show();
					budget_center.attr('disable',false);
					budget_element.attr('disable',false);
				}
			})

			

			

			
		}
	});
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var rekap = $('.rekapKontrak').modal({
		header:'Rekap Pengadaan',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});var rekap = $('.rekapProsentase').modal({
		header:'Rekap Prosentase Anggaran',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});
	
	var add = $('.buttonAdd').modal({ 
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
				window.location.href=data.url;
			}
			$(el).form(data);
			hideshow();
			hideAnggaran();
			
			$('.modal [name="budget_source"]').on('change', function(){
				hideshow();
			})
			var jenis_anggaran = $('.modal [name="jenis_anggaran[]"]').on('click', function(e){
				hideAnggaran(jenis_anggaran);
			})
			$('.modal [name="jenis_permintaan"]').on('change', function(){
				hideSearch();
			})

			$('.modal .searchInput').donetyping(function(){
				_self 			= $(this);
				_parent 		= _self.closest('.form');
				_resultWrapper 	= $('.searchOption', _parent);
				_resultWrapper.empty();

				$('.modal [name="no_form"]').remove();
				var id_baseline = $('<input type="hidden" name="id_baseline" id="id_baseline">');
				$('form .btn-group',el).before(id_baseline);
				$.ajax({
					url : '<?php echo site_url('kontrak/searchBaseline')?>',
					data : {
						search : _self.val()
					},
					method : 'POST',
					beforeSend : function(){
						$(_self).addClass('loading');
						_resultWrapper.empty();
					},
					success : function(xhr){
						
						setTimeout(function(){
							$(_self).removeClass('loading');
							var html = '<ul>';
						
							$.each( $.parseJSON(xhr), function(key, value){
								html += '<li data-id_mekanisme="'+value.id_mekanisme+'" data-id_pengguna="'+value.id_pengguna+'" data-name="'+value.name+'" data-pic_name="'+value.pic_name+'" data-pic_email="'+value.pic_email+'" data-date="'+value.plan_date+'" data-jenis_anggaran="'+value.jenis_anggaran+'" data-idr_budget_investasi="'+value.idr_budget_investasi+'" data-idr_budget_operasi="'+value.idr_budget_operasi+'" data-id="'+key+'">'+value.name+' </li>';

							});
							html += '</ul>';
							_resultWrapper.html(html);

							$('ul li', _resultWrapper).on('click', function(e){
								// alert($(this).data('pic_email'));
								id 			= $(this).data('id');
								name 		= $(this).data('name');
								_date 		= $(this).data('date');
								pic_name 	= $(this).data('pic_name');
								pic_email 	= $(this).data('pic_email');
								id_pengguna 	= $(this).data('id_pengguna');
								id_mekanisme	= $(this).data('id_mekanisme');
								jenis_anggaran	= $(this).data('jenis_anggaran');
								idr_budget_investasi	= $(this).data('idr_budget_investasi');
								idr_budget_operasi	= $(this).data('idr_budget_operasi');

								var _jenis_anggaran = jenis_anggaran.split(',');

								$.each(_jenis_anggaran, function(key, value){
									$('[name="jenis_anggaran[]"][value="'+value+'"]').attr('checked','checked');
								})
								$('input[name="nama_baseline"]').val(name);
								$('input[name="name"]').val(name);
								$('input[name="issue_date"]').val(_date);
								$('input[name="pic_name"]').val(pic_name);
								$('input[name="pic_email"]').val(pic_email);
								$('input[name="idr_budget_investasi"]').val(idr_budget_investasi);
								$('input[name="idr_budget_operasi"]').val(idr_budget_operasi);
								$('select[name="id_pengguna"]').val(16);

								$('input[name="type"] option:selected').val(id_mekanisme).change();
								$('input[name="id_baseline"]').val(id);
								hideAnggaran();
								_resultWrapper.empty();
							});
						}, 1000);
						
					}	
				});
			})

			$('.modal [name="jenis_anggaran[]"][value="investasi"]').on('change ready', function(){

				var budget_center 		= $('.modal [name="budget_center[]"]');
				var budget_element 		= $('.modal [name="budget_element[]"]');
				if($('.modal [name="jenis_anggaran[]"][value="investasi"]:checked').length > 0){

					budget_center.closest('.form-group').hide();
					budget_element.closest('.form-group').hide();
					budget_center.attr('disable','disable');
					budget_element.attr('disable','disable');
				}else{
					budget_center.closest('.form-group').show();
					budget_element.closest('.form-group').show();
					budget_center.attr('disable',false);
					budget_element.attr('disable',false);
				}

				if($('.modal [name="jenis_anggaran[]"][value="operasi"]:checked').length > 0){
					budget_center.closest('.form-group').show();
					budget_element.closest('.form-group').show();
					budget_center.attr('disable',false);
					budget_element.attr('disable',false);
				}
			})

			

			

			
		}
	});
	function hideshow(){
		var budget_source 		= $('.modal [name="budget_source"]');
		var budget_center 		= $('.modal [name="budget_center[]"]');
		var budget_element 		= $('.modal [name="budget_element[]"]');
		var kode_akun 			= $('.modal [name="kode_akun"]');
		var contract_type		= $('.modal [name="contract_type"]');
		var ele = [budget_center, budget_element,budget_element,kode_akun, contract_type];
		
		if(budget_source.val()==2){
			$.each(ele, function(key, value){
				
				value.closest('.form-group').hide();
				value.attr('disable','disable');
			})
		}else{
			$.each(ele, function(key, value){
				value.closest('.form-group').show();
				value.attr('disable',false);
			})
		}
	}
	function hideAnggaran(){
		var operasi 		= $('.modal [name="idr_budget_operasi"]');
		var investasi 		= $('.modal [name="idr_budget_investasi"]');
		var jenis 		= $('.modal [name="jenis_anggaran[]"]');
		var list = [operasi, investasi];
		$.each(list, function(key, value){
			value.closest('.form-group').hide();
			value.attr('disable','disabled');
		});
		$.each(jenis, function(key, value){
			if($(value).is(':checked')){
				var _input = $('.modal [name="idr_budget_'+$(value).val()+'"]');
				_input.closest('.form-group').show();
				_input.attr('disable',true);

			}
		});
	}
	function hideSearch(){
		var jenis_permintaan 	= $('.modal [name="jenis_permintaan"]');
		var nama_baseline 		= $('.modal [name="nama_baseline"]');
		
		if(jenis_permintaan.val()==1){
			nama_baseline.closest('.form-group').show();
		}else{
			nama_baseline.closest('.form-group').hide();
		}
	}
	
	
});
</script>
