<div class="mg-lg-7 ">

	<div class="block ">

		<div class="form">

		</div>

		<div class="form-group btn-group">
			<?php //if($admin['id_role']==3){ ?> 
			<a class="btn btn-primary" href="<?php echo site_url('kontrak/cetak_nota/'.$id)?>"><i class="fa fa-file-o"></i>&nbsp;Cetak Nota Dinas</a>
			<?php if($dataBerkas['step']!=4 && $dataBerkas['step']!=5){ ?>
			<a class="btn btn-danger btnBatal" href="<?php echo site_url('kontrak/formBatal/'.$id)?>"><i class="fa fa-close"></i>&nbsp;Batal</a>
			<a class="btn btn-danger btnGagal" href="<?php echo site_url('kontrak/formGagal/'.$id)?>"><i class="fa fa-close"></i>&nbsp;Gagal</a>
			<?php } ?>
		</div>

	</div>

</div>
<div class="mg-lg-12 ">
	<div class="block">
		<div id="tabs">
			<ul class="tabs nav-tabs" id="tabAbsent">

				<li id="progress_pengadaan"><a href="<?php echo site_url('detail_kontrak/progress_pengadaan/index/'.$id);?>">Progres Paket Pengadaan</a></li>
				<li id="kontrak"><a href="<?php echo site_url('detail_kontrak/ttd_kontrak/index/'.$id);?>">Kontrak</a></li>
				<li id="amendemen"><a href="<?php echo site_url('detail_kontrak/amandemen/index/'.$id);?>">Amendemen</a></li>
				<li id="bap"><a href="<?php echo site_url('detail_kontrak/bap/index/'.$id);?>">Berita Acara Pemeriksaan</a></li>
				<li id="bast"><a href="<?php echo site_url('detail_kontrak/bast/index/'.$id);?>">Berita Acara Serah Terima</a></li>
				<li id="invoice"><a href="<?php echo site_url('detail_kontrak/invoice/index/'.$id);?>">Invoice</a></li>
				

			</ul>
		</div>
	</div>
</div>