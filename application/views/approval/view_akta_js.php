<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    var table = $('#tabs-1').tableGenerator({
        url: '<?php echo site_url('approval/viewAkta/'.$id); ?>',
        data: dataPost,
        headers: [
            {
            "key" : "no",
            "value" : "No.Akta"
            },
            {
            "key" : "notaris",
            "value" : "Notaris"
            },
            {
            "key" : "issue_date",
            "value" : "Tanggal"
            },
            {
            "key" : "akta_file",
            "value" : "Lampiran Akta"
            },
            {
            "key" : "authorize_by",
            "value" : "Lembaga Pengesah"
            },
            {
            "key" : "authorize_no",
            "value" : "No.Pengesahan"
            },
            {
            "key" : "authorize_date",
            "value" : "Tanggal Pengesahan"
            },
            {
            "key" : "authorize_file",
            "value" : "Lampiran Pengesahan Akta"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                
                return html;
            },
            target : [10]

        }],
        additionFeature: function(el){
            

        },
        finish: function(){
        
        }
    });
})
</script>
