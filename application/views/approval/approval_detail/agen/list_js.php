<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };
    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;
    $.ajax({
                    url: '<?php echo site_url('approval_detail/agen/formFilter')?>',
                    async: false,
                    dataType: 'json',
                    success:function(xhr){
                        _xhr = xhr;
                    }
                })

    var folder = $('#folderGeneratorAgen').folder({
        url: '<?php echo site_url('approval_detail/agen/view/'.$id); ?>',
        data: dataPost,


        renderContent: function(el, value, key){
            html = '';
            _dateFormat = '';
            _linkNo = '';
            _file = '';
            if (value[4].value != '') {
                file = btoa('assets/lampiran/agen_file/'+value[4].value);
                _file += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fa fa-download"></i></a>';
            }
            var button = '';
            var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
                switch(value.data_status){
                    case '0' :
                        button += '<a href="'+site_url+"approval_detail/agen/viewVerifikasi/"+btoa(value.id)+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                        break;
                    case '1' :
                    case '2' :
                        button += '<a href="'+site_url+"approval_detail/agen/viewVerifikasi/"+btoa(value.id)+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                        break;
                    case '3' :
                    case '4' :
                        button += '<a href="'+site_url+"approval_detail/agen/viewVerifikasi/"+btoa(value.id)+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                        break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_agen/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            } 
             else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/agen/agenView/"+btoa(value.id)+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[2].value+'</p><p>'+_file+'</p>'+button+btnNote+'</p></div>';
            return html;
        },
        header: [
            {
                "key"   : "type",
                "value" : "Nomor"
            },
            {
                "key"   : "no",
                "value" : "Tipe"
            },{
                "key"   : "lampiran",
                "value" : "Lampiran"
            },
        ],
        finish: function(){
            
            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });

            var verifikasiProduk= $('.btnVerifikasiProduk').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });
            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Seumur Hidup';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
    });
});


</script>