<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/agen/verifikasi/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            // console.log(xhr)

            $('#formAgen .form').form(xhr);
            html = '';
            if (xhr.form[3].value == 'lifetime') {
                html += 'Selama Perusahaan Berdiri';
            } else {
                html += defaultDate(xhr.form[3].value)
            }
            console.log(html)
            $('.form3 span').html(html);
        }
    });
    var table = $('#formAgen .table').tableGenerator({
        url: '<?php echo site_url('approval_detail/agen/getProduk/'.$id); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "produk",
                "value" : "Produk"
            },
            {
                "key"   : "merk",
                "value" : "Merk"
            },
            // {
            //     "key"   : "action",
            //     "value" : "Action",
            //     "sort"  : false
            // }
            ],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                // console.log(data);
                // var html = '<label class="orangeAtt"><input type="checkbox" name="mandatoryProduct['+data[3].value+']" value="1" '+data[4].value+'>&nbsp;<i class="fa fa-exclamation-triangle"></i>&nbsp;Mandatory</label>'+
                // '<label class="nephritisAtt"><input type="radio" name="statusProduct['+data[3].value+']" value="1" '+data[6].value+'>&nbsp;<i class="fa fa-check" ></i>&nbsp;OK</label>'+
                // '<label class="pomegranateAtt"><input type="radio" name="statusProduct['+data[3].value+']" value="0" '+data[5].value+'>&nbsp;<i class="fa fa-times"></i>&nbsp;Not OK</label>';
                // return html;
            },
            target : [2]

        }],
    });
    $('#formAgen').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();

        $.ajax({
            url : '<?php echo site_url('approval_detail/agen/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                console.log(current_index)
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>