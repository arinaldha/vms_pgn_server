<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/izin/verifikasi/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';

            $('#formSiup .form').form(xhr);
            el = $('#formSiup .form');
            console.log(xhr);
            var _formField = {
                'siup': ['no', 'issue_date','qualification','expire_date','izin_file'],
                'siupal': ['no', 'issue_date','qualification','expire_date','izin_file'],
                'sbu': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
                'asosiasi': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
                'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expire_date','izin_file'],
                'siujk': ['no', 'grade','issue_date','qualification','expire_date','izin_file']
            };
            $('fieldset.form-group',el).hide();
            $.each(_formField[xhr.type], function(key, value){
                $('[for="'+value+'"]',el).show();
            });
              
            $.ajax({
                url: '<?php echo site_url('vendor/izin/getBSB')?>',
                async: false,
                data:{
                    id_ijin_usaha : xhr.id
                },
                method: 'POST',
                dataType: 'json',
                success:function(xhr){

                    __bsb = xhr;
                    $('form .btn-group',el).after('<h3>Bidang / Sub-bidang</h3><div class="treeView" id="parentTree"><ul></ul></div>')
                    var i = 0;

                    $.each(__bsb, function(key, value){
                        $('#parentTree > ul',el).append('<li class="treeDropdown"><span>'+key+'</span><ul id="head'+i+'"></ul></li>');
                        $.each(value, function(keyBSB, valueBSB){
                            console.log($('#head'+i, el));
                            $('#head'+i, el)
                            .append('<li><label style="width:100%"><div class="inline-form">&nbsp;'+valueBSB+'</div></label></li>');
                        })
                        
                        i++;
                    });
                    
                }
            });
        }
    });
    $('#formSiup').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/izin/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                console.log(current_index)
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>