<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

	var folder = $('#folderGenerator3').folder({
		url: '<?php echo site_url('approval_detail/izin/view/'.$id.'/siujk'); ?>',
		data: dataPost,
		header: [
			{
				"key"	: "type",
				"value"	: "Klasifikasi"
			},
			{
				"key"	: "no",
				"value"	: "Nomor"
			},{
				"key"	: "issue_date",
				"value"	: "Masa Berlaku "
			},
		],
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			console.l
			if(value[5].value == 'lifetime'){
				_dateFormat += 'Selama Perusahaan Berdiri';
			}
			else{
				_dateFormat += defaultDate(value[5].value);
			}
			var button = '';
			var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
            switch(value.data_status){
                case '0' :
                    button += '<a href="'+site_url+"approval_detail/izin/viewVerifikasi/"+btoa(value.id)+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                    break;
                case '1' :
                case '2' :
                    button += '<a href="'+site_url+"approval_detail/izin/viewVerifikasi/"+btoa(value.id)+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                    break;
                case '3' :
                case '4' :
                    button += '<a href="'+site_url+"approval_detail/izin/viewVerifikasi/"+btoa(value.id)+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                    break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_ijin_usaha/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            }
             else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/izin/izinView/"+btoa(value.id)+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
			html += '<div class="caption"><p>'+value[7].value+'</p><p>'+value[1].value+'</p><p>'+_dateFormat+'</p>'+button+btnNote+'</p></div>';
			return html;
		},
		finish: function(){
     		var verifikasi = $('.btnVerifikasi').modal({
     			header: 'Verifikasi Data',
     			dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
     		});
     		var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
            var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){
	          		data.onSuccess = function(){
		          		$(cek).data('modal').close();
		            	folder.data('plugin_folder').fetchData();
	          		};
	          		data.isReset = false;

	          		$(el).form(data).data('form');
	          		// console.log(data);
	          		html = '';
		            if (data.form[2].value == 'lifetime') {
		                html += 'Seumur Hidup';
		            } else {
		                html += defaultDate(data.form[2].value)
		            }
		            console.log(html)
		            $('.form2 span').html(html);

					_formField = {
						'siup': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'siupal': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'sbu': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'asosiasi': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expire_date','izin_file'],
						'siujk': ['no', 'grade','issue_date','expire_date','izin_file']
					}
					$(' .form-group',el).not('.btn-group').hide();

					$.each(_formField[data.data.type], function(key, value){
						$('[for="'+value+'"]',el).show();
						
					});
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        		}
      		});
		}
	});
});


</script>