<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/administrasi/view/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            xhr.formWrap = false;
            $('.form').form(xhr);
            $('#formAdministrasi .buttonRegBox').append('<a href="'+site_url+"note/insert/ms_vendor_admistrasi/"+xhr.data.id_admistrasi+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+xhr.data.total_note+'</span></a>')

            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        var current_index = $("#tabs").tabs("option","active");
                        $('#tabs').tabs('load',current_index);
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
        }
    });
    $('#formAdministrasi').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/administrasi/index/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ //
            console.log(response.status)
             if(response.status!='fail'){
                alert('Sukses!!');
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
            } else {
                alert('Gagal!, Harap lengkapi data status perusahaan!');
            }
        });
    })
     var dataPost = {
                order: 'id',
                sort: 'desc'
            };

        var table = $('.tableBank').tableGenerator({
            url: '<?php echo site_url('approval_detail/administrasi/getDataBank/'.$id); ?>',
            data: dataPost,
            headers: [
                {
                    "key"   : "no",
                    "value" : "No.Rekening"
                },
                {
                    "key"   : "name",
                    "value" : "Atas Nama"
                },
                {
                    "key"   : "bank_name",
                    "value" : "Nama Bank"
                },
                {
                    "key"   : "bank_file",
                    "value" : "Lampiran"
                },],
            additionFeature: function(el){
                
            },
            finish: function(){

            }
        });
});


</script>