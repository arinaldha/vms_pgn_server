<div class="wrapper">
<?php if(count($pengalaman_bsb->result_array())>0){ ?>
<div class="alert alert-warning">
    <h3><i class="fa fa-exclamation-triangle"></i>&nbsp;<b>Peringatan</b></h3>
    <p>Vendor tidak memiliki pengalaman untuk bidang/sub-bidang :</p>
    <ul style="list-style-type: circle; padding: 0px 20px;">
    <?php foreach($pengalaman_bsb->result_array() as $key => $row){ ?>
      <li><?php echo $row['bidang_name'].' - '.$row['sub_bidang_name'];?> </li>
    <?php } ?>
    </ul>
    Vendor hanya terdaftar menjadi DPT pada bidang/sub-bidang yang memiliki pengalaman yang sesuai.
</div>
  <?php } ?>
<div class="col col-6">
    <div class="panel">
      
      	<div class="container-title">
        	<h3>Summary Data</h3>
      	</div>

      	<div class="summary">
	        	<div class="summary-title">
	          		Approved
	          	<span><?php echo $bar['approved']['value']?>/<?php echo $approval_data['total']?></span>
	        </div>
	        <div class="summary-bars">
	          	<span class="bar-top is-success" style="width:<?php echo $bar['approved']['percentage']?>%"></span>
	          	<span class="bar-bottom"></span>
	        </div>
	    </div>

      	<div class="summary">
        	<div class="summary-title">
          		Pending
				<span><?php echo $bar['pending']['value']?>/<?php echo $approval_data['total']?></span>
        	</div>
	        <div class="summary-bars">
	          	<span class="bar-top is-warning" style="width:<?php echo $bar['pending']['percentage']?>%"></span>
	          	<span class="bar-bottom"></span>
	        </div>
      	</div>

      	<div class="summary">
        	<div class="summary-title">
          		Rejected
          		<span><?php echo $bar['rejected']['value']?>/<?php echo $approval_data['total']?></span>
        	</div>
        	<div class="summary-bars">
          		<span class="bar-top is-danger" style="width:<?php echo $bar['rejected']['percentage']?>%"></span>
          		<span class="bar-bottom"></span>
       		</div>
      	</div>

      	<div class="container-title">
        	<h3>Overview</h3>
      	</div>

      	<div class="is-block">
       		<button class="accordion-header" for="approved_content">Approved <span class="badge is-success"><?php echo $bar['approved']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
	        <ul class="accordion-panel" id="approved_content">
            <?php echo generateAccordionVerifikasi(array_merge($approval_data[1], $approval_data[2]))?>	
	        </ul>
        	<button class="accordion-header" for="pending_content">Pending <span class="badge is-warning"><?php echo $bar['pending']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
        	<ul class="accordion-panel" id="pending_content">
          		<?php echo generateAccordionVerifikasi($approval_data[0])?> 
        	</ul>
        	<button class="accordion-header" for="reject_content">Rejected <span class="badge is-danger"><?php echo $bar['rejected']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
        	<ul class="accordion-panel" id="reject_content">
          	 <?php echo generateAccordionVerifikasi(array_merge($approval_data[3], $approval_data[4]))?> 
        	</ul>
      	</div>

    </div>

</div>
 <div class="col col-6">

            <div class="panel">

              <div class="container-title">
                <h3>Note</h3>
                <div class="badge is-primary is-noticable"><?php echo count($notifikasi)?></div>
              </div>

              <div class="scrollbar" id="custom-scroll" style="height: 470px; overflow-x: auto;">

              <?php foreach ($notifikasi as $key => $value) { ?>
                 <div class="notification is-warning listNotification">
                  <p>   
                    <b>Dokumen</b> : <?php echo $value['document']?>
                  </p>
                  <?php if($value['document'] != 'Administrasi') {  ?>
                  <p>   
                    <b><?php echo $value['subject_field']?></b> :  <?php echo $value['subject']?>
                  </p>
                <?php } ?>
                  <p>   
                    <b>Note</b> : <?php echo $value['value']?>
                  </p>

                  <a class="delete" href="<?php echo site_url('note/close/'.$value['id'])?>">X</a>

                </div>
               
              <?php } ?>

             

             

              </div>

            </div>
                      
          </div>
<div class="form-group btn-group">
<?php 
if(($need_approve==null||$need_approve==0) && !$bar['rejected']['value'] > 0 && $admin['id_role']==6){ ?>
  <a href="<?php echo site_url('approval_detail/verifikasi/approveForm/'.$id)?>" class="btn btn-primary btnApprove" ><i class="fas fa-envelope"></i>&nbsp;Kirim untuk di approve atasan langsung</a>
<?php }else if($need_approve&&$admin['id_role']==8){ ?>
  <div class="buttonRegBox clearfix">
      <a href="<?php echo site_url('approval_detail/verifikasi/reject/'.$id);?>" class="btn btn-danger btnTolak">Tolak</a>
      <a href="<?php echo site_url('approval_detail/verifikasi/angkatForm/'.$id);?>" class="btn btn-primary btnAngkat">Angkat Menjadi DPT</a>
  </div>
<?php } ?>
</div>
 </div>

