<script type="text/javascript">
$(function(){
	var approve = $('.btnApprove').modal({
		header: 'Pengajuan Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengajukan penyedia barang / jasa ini menjadi DPT?<span><div class="form"></div><div>');
			data.onSuccess = function(){
				$(approve).data('modal').close();
				window.location.href="<?php echo site_url('vendor/admin/daftar_tunggu')?>";
			};
			data.isReset = true;
			$('.form', el).form(data).data('form');
		}
	});

	var btnAngkat = $('.btnAngkat').modal({
		header: 'Pengajuan Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengajukan penyedia barang / jasa ini menjadi DPT?<span><div class="form"></div><div>');
			data.onSuccess = function(){
				$(btnAngkat).data('modal').close();
				window.location.href="<?php echo site_url('vendor/admin/daftar_tunggu')?>";
			};
			data.isReset = true;
			$('.form', el).form(data).data('form');
		}
	});
			
	var close_note = $('.notification .delete').modal({
		header: 'Hapus Notifikasi',
		render : function(el, data){
			el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus catatan?<span><div class="form"></div><div>');
			data.onSuccess = function(){
				$(close_note).data('modal').close();
				location.reload();
			};
			data.isReset = true;
			$('.form', el).form(data).data('form');
		}
	});
	var close_note1 = $('.btnTolak').modal({
		header: 'Reject Vendor',
		render : function(el, data){
			el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mereject vendor ini??<span><div class="form"></div><div>');
			data.onSuccess = function(){
				$(close_note1).data('modal').close();
				location.reload();
			};
			data.isReset = true;
			$('.form', el).form(data).data('form');
		}
	});
});


</script>