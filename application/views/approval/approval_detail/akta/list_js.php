<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;
    $.ajax({
            url: '<?php echo site_url('vendor/view/akta/formFilter')?>',
            async: false,
            dataType: 'json',
            success:function(xhr){
                _xhr = xhr;
            }
        })

    var folder = $('#folderGenerator').folder({
        url: '<?php echo site_url('approval_detail/akta/view/'.$id.'/pendirian'); ?>',
        data: dataPost,

        dataRightClick: function(key, btn, value){
            _id = btoa(value[key][9].value);

            btn = [{
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"approval_detail/akta/aktaCek/"+_id
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })

            }
            });
        },

        header: [
            {
                "key"   : "type",
                "value" : "Jenis Akta"
            },
            {
                "key"   : "no",
                "value" : "Nomor Akta"
            },{
                "key"   : "notaris",
                "value" : "Notaris"
            },{
                "key"   : "authorize_date",
                "value" : "Tanggal Ditetapkan"
            },
        ],
        renderContent: function(el, value, key){
            html = '';
            _file = '';
            _dateFormat = '';
            if(value[3].value != ''){
                file = btoa('assets/lampiran/akta_file/'+value[3].value);
                _file += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fa fa-download"></i></a>' 
            }
            if(value[7].value != ''){
                _dateFormat += defaultDate(value[7].value);
            }
            var button = '';
            var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
            switch(value.data_status){
                case '0' :
                    button = '<a href="'+site_url+"approval_detail/akta/viewVerifikasi/"+btoa(value.id)+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                    break;
                case '1' :
                case '2' :
                    button = '<a href="'+site_url+"approval_detail/akta/viewVerifikasi/"+btoa(value.id)+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                    break;
                case '3' :
                case '4' :
                    button = '<a href="'+site_url+"approval_detail/akta/viewVerifikasi/"+btoa(value.id)+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                    break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_akta/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            }
             else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/akta/aktaView/"+btoa(value.id)+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[2].value+'</p><p>'+value[1].value+'</p><p>'+_dateFormat+'</p><p>'+button+btnNote+'</p></div>';

            return html;
        },
        finish: function(){
            
            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }

            });
            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

    });
});


</script>