<script type="text/javascript">

$(function(){
    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>'
    $.ajax({
        url : '<?php echo site_url('approval_detail/k3/viewForm/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
                xhr.successMessage = 'Berhasil !!';
                xhr.formWrap = false;
                $('#formK3').form(xhr);
                $('#formK3 .btn-group').remove()
                if (xhr.form[4].value == '') {
                    $('.form4').hide()
                }
                var id = '<?php echo $id ?>'
                var html = '';
                // if(id_role == 3){
 var link = '';
                // if(id_role == 3){
                if (xhr.form[0].value != '') {
                    file = btoa('assets/lampiran/csms_file/'+xhr.form[0].value);
                    link += '<a href="'+base_url+"open/file/"+file+'" target="blank">'+xhr.form[0].value+'</a>'
                } else {
                    link += '-'
                }
$('.form0 span').empty();
                $('.form0 span').append(link);
                if (xhr.form[0].value != '') {
                    html += '<a href="'+site_url+"approval_detail/k3/update/"+id+'" class="button is-primary buttonUpload"><span><i class="fas fa-file-upload"></i> Upload CSMS</span></a>'
                } else {
                    html += '<a href="'+site_url+"approval_detail/k3/add/"+id+'" class="button is-primary buttonAdd"><span><i class="fas fa-file-upload"></i> Update CSMS</span></a>'
                }
                $('.btnMargin').html(html)
            // }
        }
    });

    var add = $('.buttonAdd').modal({
        header: 'Tambah CSMS',
        render : function(el, data){
            data.onSuccess = function(){
                $(add).data('modal').close();
                var current_index = $("#tabsK3").tabs("option","active");
                $('#tabsK3').tabs('load',current_index);
            }
            $(el).form(data);
            $('.form3').hide();
            $('.form2 .form-control').on('keyup',function() {
                var val = $(this).val()
                // console.log(val)
                if(val <= 55.00){
                     $('.form4').show();
                }
                else{
                    $('.form4').hide();
                }
            })
            $('.modal-content .form1').removeClass('wrapper')
        }
    });

    var edit = $('.buttonUpload').modal({
                header: 'Update CSMS',
                render : function(el, data){

                data.onSuccess = function(){
                 location.reload()
                };
                data.isReset = false;
                $(el).form(data).data('form');
                $('.form3').hide();
                $('.form2 .form-control').on('keyup',function() {
                    var val = $(this).val()
                    // console.log(val)
                    if(val <= 55.00){
                         $('.form4').show();
                    }
                    else{
                        $('.form4').hide();
                    }
                })
            }
          });
    
});


</script>
