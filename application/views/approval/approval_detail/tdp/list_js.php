<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

	$.ajax({
        url: '<?php echo site_url('vendor/view/tdp/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })
	var folder = $('#folderGeneratorTdp').folder({
		url: '<?php echo site_url('approval_detail/tdp/view/'.$id); ?>',
		data: dataPost,

			dataRightClick: function(key, btn, value){
			_id = btoa(value[key][5].value);

			btn = [{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/tdp/tdpView/"+_id
			}];
			return btn;
		},
		header: [
			{
				"key"	: "no",
				"value"	: "Nomor"
			},
			{
				"key"	: "issue_date",
				"value"	: "Tanggal Dikeluarkan"
			},
			{
				"key"	: "expire_date",
				"value"	: "Masa Berlaku"
			},
			{
				"key"	: "tdp_file",
				"value"	: "Bukti scan dokumen"
			},
		],
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			_issueDate = '';
			_link = '';
			if (value[4].value != '') {
				file = btoa('assets/lampiran/tdp_file/'+value[4].value);
				_link += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fa fa-download"></i></a>';
			}
			if (value[1].value != '') {
				_issueDate += defaultDate(value[1].value);
			}
			if (value[3].value == 'lifetime') {
				_dateFormat += 'Selama Perusahaan Berdiri';
			} else {
				_dateFormat += defaultDate(value[3].value);
			}
			var button = '';
			var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
            switch(value.data_status){
                case '0' :
                    button = '<a href="'+site_url+"approval_detail/tdp/viewVerifikasi/"+btoa(value.id)+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                    break;
                case '1' :
                case '2' :
                    button = '<a href="'+site_url+"approval_detail/tdp/viewVerifikasi/"+btoa(value.id)+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                    break;
                case '3' :
                case '4' :
                    button = '<a href="'+site_url+"approval_detail/tdp/viewVerifikasi/"+btoa(value.id)+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                    break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_tdp/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            }
             else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/tdp/tdpView/"+btoa(value.id)+'" class="button is-success buttonCek"></i>&nbsp;Cek Data&nbsp;</a>';
            }
			html += '<div class="caption"><p>'+value[0].value+'</p><p>'+_issueDate+'</p><p>'+_dateFormat+'</p><p>'+_link+'</p>'+button+btnNote+'</p></div>';
			console.log(folder);
			return html;
		},
		finish: function(){

     		var verifikasi = $('.btnVerifikasi').modal({
     			header: 'Verifikasi Data',
				dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
	    	});
     		 var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }
            });
     		var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;
          		$(el).form(data).data('form');
          		// console.log(data);
          		html = '';
	            if (data.form[2].value == 'lifetime') {
	                html += 'Selama Perusahaan Berdiri';
	            } else {
	                html += defaultDate(data.form[2].value)
	            }
	            console.log(html)
	            $('.form2 span').html(html);
	            $('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
		},
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
	});
  	
});


</script>