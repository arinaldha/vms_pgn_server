<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };
    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;
    $.ajax({
        url: '<?php echo site_url('vendor/view/pemilik/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

    var folder = $('#folderGeneratorPemilik').folder({
        url: '<?php echo site_url('approval_detail/pemilik/view/'.$id); ?>',
        data: dataPost,
        header: [
            {
                "key"   : "name",
                "value" : "Nama"
            },
            {
                "key"   : "position",
                "value" : "Jabatan"
            },{
                "key"   : "percentage",
                "value" : "Persentase Kepemilikan"
            }
        ],
        renderContent: function(el, value, key){
            html = '';
            var id_vendor = '<?php echo $id ?>'
            var button = '';
            var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
            switch(value.data_status){
                case '0' :
                    button += '<a href="'+site_url+"approval_detail/pemilik/viewVerifikasi/"+btoa(value.id)+'/'+id_vendor+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                    break;
                case '1' :
                case '2' :
                    button += '<a href="'+site_url+"approval_detail/pemilik/viewVerifikasi/"+btoa(value.id)+'/'+id_vendor+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                    break;
                case '3' :
                case '4' :
                    button += '<a href="'+site_url+"approval_detail/pemilik/viewVerifikasi/"+btoa(value.id)+'/'+id_vendor+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                    break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_pemilik/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            }
             else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/pemilik/pemilikView/"+btoa(value.id)+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[1].value+'</p><p>'+value[2].value+'%</p>'+button+btnNote+'</p></div>';
            console.log(folder);
            return html;
        },
        finish: function(){
            
            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });
             var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[2].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[2].value)
                }
                console.log(html)
                $('.form2 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

    });
});


</script>