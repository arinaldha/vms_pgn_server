<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/situ/verifikasi/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            // console.log(xhr)
            
            // console.log(xhr.form[6].value);
            $('#formSitu .form').form(xhr);
            html = '';
            if (xhr.form[6].value == 'lifetime') {
                html += 'Selama Perusahaan Berdiri';
            } else {
                html += defaultDate(xhr.form[6].value)
            }
            console.log(html)
            $('.form6 span').html(html);
        }
    });
    $('#formSitu').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/situ/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                console.log(current_index)
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>