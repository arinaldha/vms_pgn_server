<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };
    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    $.ajax({
            url: '<?php echo site_url('vendor/view/situ/formFilter')?>',
            async: false,
            dataType: 'json',
            success:function(xhr){
                _xhr = xhr;
            }
        });
    var folder = $('#folderGeneratorSitu').folder({
        url: '<?php echo site_url('approval_detail/situ/view/'.$id); ?>',
        data: dataPost,
        header: [
            {
                "key"   : "type",
                "value" : "Nomor"
            },
            {
                "key"   : "no",
                "value" : "Masa Berlaku"
            },{
                "key"   : "situ_file",
                "value" : "Lampiran"
            },{
                "key"   : "file_photo",
                "value" : "Foto Lokasi"
            }
        ],
        renderContent: function(el, value, key){
            html = '';
            _dateFormat = '';
            _fileSitu = '';
            _filePhoto = '';
            
            if (value[5].value == 'lifetime') {
                _dateFormat += 'Selama Perusahaan Berdiri';
            } else {
                _dateFormat += defaultDate(value[5].value);
            }
            if (value[6].value != '') {
                file = btoa('assets/lampiran/situ_file/'+value[6].value);
                _fileSitu += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="far fa-file-alt" style="font-size: 1.5em"></i></a>'
            }
            if (value[4].value != '') {
                file = btoa('assets/lampiran/file_photo/'+value[4].value);
                _filePhoto += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fas fa-images" style="font-size: 1.5em"></i></a>'
            }
            var button = '';
            var btnNote = '';
            if(id_role != 1 && id_role != 8 && id_role != 12){
            switch(value.data_status){
                case '0' :
                    button = '<a href="'+site_url+"approval_detail/situ/viewVerifikasi/"+btoa(value.id)+'" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                    break;
                case '1' :
                case '2' :
                    button = '<a href="'+site_url+"approval_detail/situ/viewVerifikasi/"+btoa(value.id)+'" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                    break;
                case '3' :
                case '4' :
                    button = '<a href="'+site_url+"approval_detail/situ/viewVerifikasi/"+btoa(value.id)+'" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                    break;
                }
                btnNote += '<a href="'+site_url+"note/insert/ms_situ/"+btoa(value.id)+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+value.total_note+'</span></a>';
            }
              else if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/situ/situView/"+btoa(value.id)+'" class="button is-success buttonCek"><i class="fa fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            html += '<div class="caption"><p>'+value[1].value+'</p><p>'+_dateFormat+'</p><p>'+_fileSitu+'</p><p>'+_filePhoto+'</p>'+button+btnNote+'</p></div>';
           
            return html;
        },
        finish: function(){
            
            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });
            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

    });
});


</script>