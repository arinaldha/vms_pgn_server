<div id="tabs" class="tabs">

  <ul>

    <li id="tabAdministrasi">
      <a href="<?php echo site_url('approval_detail/administrasi/index/'.base64_encode($id)) ?>" >Administrasi</a>
    </li>
    <li id="tabAkta">
      <a href="<?php echo site_url('approval_detail/akta/index/'.base64_encode($id)) ?>"  >Akta</a>
    </li>
    <li id="tabSitu">
      <a href="<?php echo site_url('approval_detail/situ/index/'.base64_encode($id)) ?>" >SITU</a>
    </li>
    <li id="tabTdp">
      <a href="<?php echo site_url('approval_detail/tdp/index/'.base64_encode($id)) ?>" >TDP</a>
    </li>
    <li id="tabPengurus">
      <a href="<?php echo site_url('approval_detail/pengurus/index/'.base64_encode($id)) ?>" >Pengurus</a>
    </li>
    <li id="tabPemilik">
      <a href="<?php echo site_url('approval_detail/pemilik/index/'.base64_encode($id)) ?>" >Pemilik</a>
    </li>
    <li id="tabIzinUsaha">
      <a href="<?php echo site_url('approval_detail/izin/index/'.base64_encode($id)) ?>" >Izin Usaha</a>
    </li>
    <li id="tabAgen">
      <a href="<?php echo site_url('approval_detail/agen/index/'.base64_encode($id)) ?>" >Pabrikan/Keagenan/Distributor</a>
    </li>
    <li id="tabPengalaman">
      <a href="<?php echo site_url('approval_detail/pengalaman/index/'.base64_encode($id)) ?>" >Pengalaman</a>
    </li>
    <li  id="tabK3">
      <a href="<?php echo site_url('approval_detail/k3/index/'.base64_encode($id)) ?>">K3</a>
    </li>
    <?php if ($this->session->userdata('admin')['id_role'] != 12) { ?>
    <li id="tabVerifikasi">
      <a href="<?php echo site_url('approval_detail/verifikasi/index/'.base64_encode($id)) ?>">Verifikasi DPT</a>
    </li>
    <?php } ?>

  </ul>
  
</div>