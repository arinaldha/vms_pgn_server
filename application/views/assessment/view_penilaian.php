<div class="mg-lg-12">
	<div class="block ">
		<div id="form" class="blockWrapper">
			<div>
				<form method="POST" enctype="multipart/form-data" id="penilaianAssessment" action="<?php echo site_url('assessment/save_penilaian/'.$id_procurement)?>">
					<table class="tableData assessmentInfo">
						
						<tr>
							<td>
								Nilai Kinerja : <?php echo $assessment['score'];?>
							</td>
						</tr>
						<tr>
							<td>
								Kategori : <?php echo $assessment['kategori'];?>
							</td>
						</tr>
					</table>
				
					<table id="scoreTable" class="scoreTable">
						
		            </table>

		            <div class="form-group btn-group">
						<a href="<?php echo site_url('assessment/print_penilaian/'.$id_procurement)?>" class="btn btn-primary"><i class="fa fa-print"></i>Print</a>
					</div>
				</form>
			</div>
		</div>
				
	</div>
</div>
