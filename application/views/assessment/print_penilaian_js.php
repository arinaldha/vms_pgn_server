<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('assessment/get_form_assessment/'.$id_procurement)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			var _table = $('#scoreTable');
			var html = '';
			$.each(xhr.data, function(key, value){
				__score = 0;
				_table.append('<tr class="doubleBorder" id="header'+key+'" bgcolor="#eee"><td style="text-align: center;"><b>'+value.name+'</b></td><td></td><td></td></tr>');
				var _scoreHeader = $('#scoreHeader'+key);
				
				$.each(value.option, function(keyQuestion, valueQuestion){
					__checked =  (typeof xhr.answer[key] != 'undefined' && valueQuestion.point == xhr.answer[key].value) ? '&#10004;' : '';
					_table.append('<tr><td>'+valueQuestion.value+'</td><td style="text-align: center;">'+valueQuestion.point+'</td><td style="text-align: center;">'+__checked+'&nbsp;</td></tr>');
				});
			})
			_table.append('<tr bgcolor="#b2b2b2"><td><b>Nilai Evaluasi Kinerja </b></td><td></td><td style="text-align: center;"><b><?php echo $assessment['score']?></td></b></tr>');
		}
	});
});


</script>