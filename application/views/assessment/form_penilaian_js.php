<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('assessment/get_form_assessment/'.$id_procurement)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			var _table = $('#scoreTable');
			var html = '';
			$.each(xhr.data, function(key, value){
				__score = 0;
				_table.append('<tr class="doubleBorder" id="header'+key+'"><td><b>'+value.name+'</b></td><td></td></tr>');
				var _scoreHeader = $('#scoreHeader'+key);
				
				$.each(value.option, function(keyQuestion, valueQuestion){
					__checked = (typeof xhr.answer[key] != 'undefined' && valueQuestion.point == xhr.answer[key].value) ? 'checked' : '';

					_table.append('<tr><td>'+valueQuestion.value+'</td><td><input type="radio" '+__checked+' value="'+valueQuestion.point+'" name="evaluasi['+key+']" class="evaluasiCheckbox"><label>'+valueQuestion.point+'</label></td></tr>');
				});
			})
			
		}
	});
	$('#penilaianAssessment').on('submit', function(e){
		_this = $(this);
		e.preventDefault();
		_form = $('#penilaianAssessment');
		formData = new FormData( _form[0]);
		// formData.append('evaluasi',);
		$.ajax({
			url : _this.attr('action'),
			method: 'POST',
			async : false,
			processData: false,
			contentType: false,
			data: formData,
			dataType : 'json',
			success: function(xhr){
				if(xhr.status=='success'){
					alert('Penilaian berhasil!');
					window.location = '<?php echo site_url('assessment/view_penilaian/'.$id_procurement)?>';
				}else{
					alert('Penilaian Gagal!')
				}
			}
		});
	})
});


</script>