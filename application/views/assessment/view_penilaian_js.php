
<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('assessment/get_form_assessment/'.$id_procurement)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			var _table = $('#scoreTable');
			var html = '';
			$.each(xhr.data, function(key, value){
				__score = 0;
				_table.append('<tr class="doubleBorder" id="header'+key+'"><td><b>'+value.name+'</b></td><td></td></tr>');
				var _scoreHeader = $('#scoreHeader'+key);
				
				$.each(value.option, function(keyQuestion, valueQuestion){
					__checked =  (typeof xhr.answer[key] != 'undefined' && valueQuestion.point == xhr.answer[key].value) ? '<i class="fa fa-check-square" data-count="'+key+'"></i>' : '<i class="fa fa-square" data-count="'+key+'"></i>';
					_table.append('<tr><td>'+valueQuestion.value+'</td><td width="15%">'+__checked+'&nbsp;<label>'+valueQuestion.point+'</label></td></tr>');
				});
			})
			_table.append('<tr class="totalResult"><td>Nilai Kinerja </td><td><?php echo $assessment['score']?></td></tr>');
		}
	});
});


</script>