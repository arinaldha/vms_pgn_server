	<link rel="stylesheet" href="<?php echo base_url('assets/styles/print.css'); ?>" type="text/css" media="screen"/>
<div class="mg-lg-12">
	<div class="block ">
		<div id="form" class="blockWrapper">
			<div>
				<form method="POST" enctype="multipart/form-data" id="penilaianAssessment" action="<?php echo site_url('assessment/save_penilaian/'.$id_procurement)?>">
					<table class="tableData assessmentInfo" cellpadding="0" cellspacing="0" border="0" width="100%">
						
						<tr>
							<td>
								Nama Pengadaan Barang / Jasa : <?php echo $assessment['name_procurement'];?>
							</td>
						</tr>
						<tr>
							<td>
								Nama Penyedia Barang / Jasa : <?php echo $assessment['vendor_name'];?>
							</td>
						</tr>
						<tr>
							<td>
								Nilai Evaluasi Kinerja : <?php echo $assessment['score'];?>
							</td>
						</tr>
						<tr>
							<td>
								Kategori : <?php echo $assessment['kategori'];?>
							</td>
						</tr>
						<tr>
							<td>
								Tanggal Dokumen Kesepakatan : <?php echo default_date($assessment['contract_date']);?>
							</td>
						</tr>
					</table>
					<br>
					<table id="scoreTable" class="scoreTable" cellpadding="3" cellspacing="0" border="1" width="100%">
						<tr style="background: grey">
							<td style="font-weight: bold;text-align: center;">Uraian Pengurang</td>
							<td width="200px" style="font-weight: bold;text-align: center;">Nilai Pengurang</td>
							<td width="200px" style="font-weight: bold; text-align: center;">Nilai Awal = 100</td>
						</tr>
		            </table>
				</form>
			</div>
		</div>
		<p>Jakarta, <?php echo default_date(date('Y-m-d'))?></p>
		<p>Pengguna Barang/Jasa</p>
		<br />
		<p><u><?php echo $assessment['nama_pejabat_pengadaan']?></u></p>
		<p><b><?php echo $assessment['jabatan_satuan_kerja']?></b></p>
		<small>*Laporan Evaluasi Kinerja Penyedia Barang / Jasa ini dicetak menggunakan Sistem Manajemen Pengadaan Barang / Jasa PT PGN LNG Indonesia</small>

	</div>
</div>
