<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
		url: '<?php echo site_url('assessment/formFilter')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			_xhr = xhr;
		}
	})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('assessment/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Paket Pengadaan"
			},{
				"key"	: "pemenang",
				"value"	: "Nama Penyedia Barang / Jasa"
			},{
				"key"	: "score",
				"value"	: "Nilai Evaluasi Kinerja"
			},{
				"key"	: "category",
				"value"	: "Kategori"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				html ='<a class="btn btn-default buttonLihat" href="'+base_url+'assessment/form_penilaian/'+data[4].value+'"><i class="fa fa-list-ul"></i>&nbsp;Evaluasi</a>';
				html +='<a class="btn btn-default buttonLihat" href="'+base_url+'assessment/view_penilaian/'+data[4].value+'"><i class="fa fa-search"></i>&nbsp;Lihat</a>';
				html +='<a class="btn btn-default buttonLihat" href="'+base_url+'assessment/print_penilaian/'+data[4].value+'"><i class="fa fa-print"></i>&nbsp;Print</a>';
				html +='<a class="btn btn-default buttonLihat buttonbast" href="'+base_url+'assessment/cetak_dokumen/'+data[4].value+'" data-score="'+data[2].value+'"><i class="fa fa-file-o"></i>&nbsp;Sertifikat</a>';	
				
				
				return html;
			},
			target : [4]
		}],
		additionFeature: function(el){
			el.append('<a href="'+site_url+'assessment/form_rekap_assessment/" class="btn btn-primary rekapPengadaan">Rekap Penilaian Kinerja</a>');
		},
		
		finish: function(){
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	$('.buttonbast').on('click', function(){
		score = $(this).data('score');
		if(score==null){
			alert('Harap melakukan evaluasi kinerja Penyedia Barang / Jasa sebelum mencetak BAST');
			return false;
		}else{
			return true;
		}
	})
	var rekap = $('.rekapPengadaan').modal({
		headers:'Rekap Rencana Pengadaan',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});
});


</script>
