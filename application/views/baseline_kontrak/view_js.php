<script type="text/javascript">
$(function(){
	var __entry_by;
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;
	var __id_user 	= <?php echo $admin['id_user']?>;

	var _xhr;
	$.ajax({
		url : '<?php echo site_url('baseline_kontrak/viewData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			
			$('.form').form(xhr);
			_xhr = xhr;
			hideAnggaran();
			var jenis_anggaran = $('.modal [name="jenis_anggaran[]"]').on('click', function(e){
				hideAnggaran(jenis_anggaran);
			})
		},
		finish: function(){
			
		},
	});
	var update = $('.btnUpdate').modal({
					header: 'Update Data',
					render : function(el, data){
						
						data.onSuccess = function(){
							$(update).data('modal').close();
							
							location.reload();
						};
						data.isReset = true;
						$(el).form(data).data('form');
						
					}
				});
	function hideAnggaran(){
		var operasi 		= $('.modal [name="idr_budget_operasi"]');
		var investasi 		= $('.modal [name="idr_budget_investasi"]');
		var jenis 		= $('.modal [name="jenis_anggaran[]"]');
		var list = [operasi, investasi];
		$.each(list, function(key, value){
			value.closest('.form-group').hide();
			value.attr('disable','disabled');
		});
		$.each(jenis, function(key, value){
			if($(value).is(':checked')){
				var _input = $('.modal [name="idr_budget_'+$(value).val()+'"]');
				_input.closest('.form-group').show();
				_input.attr('disable',true);

			}
		});
	}
});
</script>
