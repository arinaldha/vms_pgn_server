<script type="text/javascript">

$(function(){
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('baseline_kontrak/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('baseline_kontrak/getData/'.$id); ?>',
		data: dataPost,
		headers: [
		{
			"key"	: "name",
			"value"	: "Nama Paket Permintaan"
		},
		{
			"key"	: "plan_date",
			"value"	: "Tanggal Rencana Permintaan"
		},
		
	  	{
			"key"	: "action",
			"value"	: "Action",
			"sort"	: false
		}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(data[1].value)
				
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				if(data[3].value==1){
					html +='<label class="label label-success">Telah disetujui </label>';
				}else{
					html +='<label class="label label-warning">Menunggu Persetujuan </label>';
				}
				html += '<a class="btn btn-default " href="'+site_url+"baseline_kontrak/view/"+data[2].value+'"><i class="fa fa-search"></i>&nbsp;Lihat Data</a>';
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			<?php if($admin['id_role']!=3){ ?>
				el.append(insertButton(site_url+"baseline_kontrak/insert/"));	
			<?php } ?>
			el.append('<a href="'+site_url+'baseline_kontrak/form_rekap_baseline/" class="btn btn-primary rekapPengadaan">Rekap Rencana Pengadaan</a>');
			el.append('<a href="'+site_url+'baseline_kontrak/export/" class="btn btn-primary">Report Rencana Pengadaan</a>');
		},
		finish: function(){
			
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
				window.location.href=data.url;
			}
			$(el).form(data);
			hideAnggaran();
			var jenis_anggaran = $('.modal [name="jenis_anggaran[]"]').on('click', function(e){
				hideAnggaran(jenis_anggaran);
			})
		}
	});
	var rekap = $('.rekapPengadaan').modal({
		headers:'Rekap Rencana Pengadaan',
		render : function(el, data){
			data.onSuccess = function(data){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form form', el).unbind('submit');
			
		}
	});
	function hideAnggaran(){
		var operasi 		= $('.modal [name="idr_budget_operasi"]');
		var investasi 		= $('.modal [name="idr_budget_investasi"]');
		var jenis 		= $('.modal [name="jenis_anggaran[]"]');
		var list = [operasi, investasi];
		$.each(list, function(key, value){
			value.closest('.form-group').hide();
			value.attr('disable','disabled');
		});
		$.each(jenis, function(key, value){
			if($(value).is(':checked')){
				var _input = $('.modal [name="idr_budget_'+$(value).val()+'"]');
				_input.closest('.form-group').show();
				_input.attr('disable',true);

			}
		});
	}
	
});
</script>
