<script type="text/javascript">

$(function(){

    $('#tabs').tabs({
        load: function(event, ui){
            window.location.hash = '#'+ui.tab.attr('id');
    }});

     $.ajax({
        url : '<?php echo site_url('auction/viewEditAuction/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                window.location = '<?php echo site_url('vendor/dashboard');?>';
            }
           
            xhr.successMessage = 'Berhasil !!';
            $('.form').form(xhr);
            $('.form .btn-group').remove()
            
            var id = xhr.form[9].value;
            var html = '';
            html += '<a class="button is-primary buttonEdit" href="'+site_url+"auction/editDataAuction/"+id+'">Edit</a>'

            $('.btnMargin').append(html);
         
            var html = '';
            if (xhr.form[6].value !='' && xhr.form[7].value !='') {
                html += xhr.form[7].value +'-'+xhr.form[6].value;
            }
            
            $('.form6').hide();
            $('.form7 span').html(html);
        }
    });

    var edit = $('.buttonEdit').modal({
                header: 'Update Data',
                render : function(el, data){

                data.onSuccess = function(){
                 $(edit).data('modal').close();
                 location.reload();
                };
                data.isReset = false;

                $(el).form(data).data('form');
                $('.form9').hide();
                if ($('.form0 [value="forward_auction"]').prop('checked')) {
                    $('.form2').hide();
                    $('.form3').hide();
                }
                if ($('.form5 [value="internet"]').prop('checked')) {
                    $('.form6').hide();
                    $('.form7').hide();
                }
                if ($('.form8 [value="days"]').prop('checked') || $('.form8 [value="hours"]').prop('checked') || $('.form8 [value="minutes"]').prop('checked')) {
                    $('.form9').show();
                }
                $('.form5 [name="work_area"]').click(function() {
                    var val = $(this).val();
                    if(val == 'internet'){
                        $('.form6').hide();
                        $('.form7').hide();
                    } else {
                        $('.form6').show();
                        $('.form7').show();
                    }
                })
                $('.form0 [name="auction_type"]').click(function(){
                    var type = $(this).val();
                    if (type == 'forward_auction') {
                        $('.form2').hide();
                        $('.form3').hide();
                    } else {
                        $('.form2').show();
                        $('.form3').show();
                    }
                })
                $('.form8 [name="duration_type"]').click(function(){
                        $('.form9').show();
                })
             }
          });
});


</script>