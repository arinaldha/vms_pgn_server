<div class="col col-12">
	<div class="row">
		<p id="auction-report-bar" class="msgSuccess text-center alert alert-success">
			<i class="far far-check-square-o"></i>Auction telah selesai.
			<b style="color : #617ac6; cursor : pointer">
				<a href="<?php echo site_url('report/index/'.$id_lelang)?>" target="_blank">Klik disini</a>
			</b> untuk melihat report.
		</p>
	</div>
	<div class="lelang">
		<div class="col col-4">
			<div class="panel">
				<h1 class="gigantic headerAuction"><i class="fas fa-gavel"></i>&nbsp;<b><?php echo strtoupper($fill['name']); ?></b></h1>
				<table class="table table-borderless content-group-sm">
					<tbody>
						<tr>
							<td>
								Mata Uang
							</td>
							<td class="text-right">
								<?php echo $fill['rate']; ?>
							</td>
						</tr>
						<tr>
							<td>
								Metode Penawaran
							</td>
							<td class="text-right">
								<?php 
									if($fill['metode_penawaran'] == "lump_sum") 
										echo "Total";
									if($fill['metode_penawaran'] == "harga_satuan") 
										echo "Itemize";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Kriteria Pemenang
							</td>
							<td class="text-right">
								<?php 
									if($fill['kriteria_pemenang'] == "lump_sum") 
										echo "Total";
									if($fill['kriteria_pemenang'] == "harga_satuan") 
										echo "Itemize";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Durasi
							</td>
							<td class="text-right">
								<?php if($fill['auction_jenis']=='auction'){
									echo $fill['auction_duration'].' Menit';
								}else{
									echo default_date($fill['start_time']).' - '.default_date($fill['time_limit']);
								} ?>
							</td>
						</tr>
						<tr>
							<td>
								Tipe Auction
							</td>
							<td class="text-right">
								<?php 
									if($fill['auction_type'] == "forward_auction") 
										echo "Forward Auction";
									if($fill['auction_type'] == "reverse_auction") 
										echo "Reverse Auction";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Jenis Auction
							</td>
							<td class="text-right">
								<?php 
									switch ($fill['auction_jenis']) {
										case 'auction':
											echo 'Auction';
											break;
										case 'rfi':
											echo 'RFI';
											break;
										case 'rfi_pembelian':
											echo 'RFI Pembelian Langsung';
											break;
										case 'rfi_penunjukan':
											echo 'RFI Penunjukkan Langsung';
											break;
									}
								?>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="panel-body">
					<?php 
					if($fill['metode_auction']=='indikator'&&$fill['auction_jenis']!='auction'){ ?>
					<div class="noticeMedia">
						<ul>
							<?php if($fill['auction_jenis']=='rfi_penunjukan'){ ?>
							<li class="media">
								<div class="media-left">
									<i class="fa fa-exclamation-triangle warnColor"></i>
								</div>
								<div class="media-body">
									Penawaran masih diatas HPS/dibawah nilai limit
								</div>
							</li>
							<?php } ?>
							<li class="media">
								<div class="media-left">
									<i class="auction-prize3"></i>
								</div>
								<div class="media-body">
									Penawaran adalah yang <?php echo $limit; ?>
								</div>
							</li>
							<li class="media">
								<div class="media-left">
									<i class="auction-thumb1"></i>
								</div>
								<div class="media-body">
									Penawaran bukan yang <?php echo $limit; ?>
								</div>
							</li>
						</ul>
					</div>
					<?php } ?>
				</div>
				<div id="start-auction" class="panel-body">
					
				</div>
			</div>
			<div class="panel" id="timer-container">
				<h4><i class="fa fa-clock-o"></i>&nbsp;Waktu tersisa</h4>
				<div class="panel-body">
					<div id="timer">-- : -- : --</div>
				</div>
			</div>
		</div>
		<?php if($fill['auction_jenis']=='auction'){?>
		<div class="col col-8">
			<div id="chart-container" class="panel wrapper">
				
			</div>
		</div>
		<?php }else{?>
		<div class="col col-8">
			<div style="overflow:auto" class="panel">
				<h2>Rekap penawaran terendah</h2>
				<table cellpadding="0" cellspacing="0" border="0" class="table" width="100%">
					<form id="auction-penawaran-form" method="post" action="<?php echo site_url('auction/user/vendor_dash/save_penawaran/'); ?>">
						<thead>
							<tr>
								<th class="header-table">No.</th>
								<th class="header-table">Nama Barang</th>
								<th class="header-table">Vendor</th>
								<th class="header-table">Penawaran Ke-</th>
								<th class="header-table">Penawaran Harga</th>
								<th class="header-table">Penawaran Harga<br/>(dalam IDR)</th>
								<th class="header-table"><?php echo $persentase; ?> (%)</th>
								<?php if($fill['auction_jenis']!='rfi'){ ?>
								<th width="80" class="header-table">
									<?php if($fill['metode_auction'] == "posisi") echo "Posisi/Ranking"; else echo "Indikator"; ?>
								</th>
								<?php } ?>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<th width="80" class="header-table">
									Lampiran
								</th>
								<?php } ?>
							</tr>
						</thead>

						<tbody>
							<?php 
								$is_total = ($fill['kriteria_pemenang']=='lump_sum');
								$y = 1;
								$count = 0;
								$detail = $penawaran->result_array();
								
								$total = count($detail) / $barang->num_rows();
								$mark_last = '';
								$useRowspan = '';
								$penawaran_ke = $nilai_list = array();
								$peserta = $this->pam->get_bid_peserta($fill['id']);
								$class="";

								
								
								if(count($total)>0){
									
									$count_penawaran = 0;
									$data_barang = array();
									// print_r($detail);
									$data_count =  array();
									$barang_count = 0;
									

									
									$useRowspan = $peserta->num_rows();
									$useRowspan = 'rowspan ="'.$useRowspan.'"';
									

											$id_vendor = array();

											$index = $y;
											
											
											$x = 0; 
											
											foreach($barang->result() as $data){ 
												$x = 0;
												echo '<tr>';

											echo '<td '.$useRowspan.' align="center">'.$index.'</td>';
										
												foreach($peserta->result_array() as $key_peserta => $data_peserta){
													$lampiran_list	= $this->pam->get_lampiran($fill['id'],$data_peserta['id_vendor'],$data->id_barang)->row_array();
													$penawaran = $this->pam->get_last_penawaran($id_lelang, $data->id, $data_peserta['id_vendor'])->result_array();

													$last_penawaran = $penawaran[count($penawaran) - 1];

													$nilai		= number_format($last_penawaran['nilai']);
													$in_rate	= number_format($last_penawaran['in_rate']);
													$percentage	= $this->pam->cek_percentage($id_lelang, $data_peserta['id_vendor'], $data->id, $last_penawaran['in_rate'], $last_penawaran['id']);
													$penawaran_ke = count($penawaran);
													$indicator	= '';
													$td_class	= '';
													if($x==0) {
													echo '<td'.$td_class.' '.$useRowspan.'>'.$data->nama_barang.'</td>';
													$x++;
													}
														echo '<td'.$td_class.'><a href="'.site_url('auction_tabs/auction_progress/by_vendor/'.$fill['id'].'/'.$data_peserta['id_vendor']).'">'.$data_peserta['nama_vendor'].'</a></td>'; 
														
													echo '<td'.$td_class.'>'.$penawaran_ke.'</td>';
													echo '<td'.$td_class.'>'.$nilai.'</td>';
													echo '<td'.$td_class.'>'.$in_rate.'</td>';
													echo '<td'.$td_class.'><span class="gr-text">'.$percentage.'</span></td>';
													if($fill['auction_jenis']!='rfi'){

														$rank = $this->pam->get_indicator($fill['id'],$data->id, $data_peserta['id_vendor']);
														// $is_last = $this->pam->is_last($fill['id'], $data_peserta['id_vendor'],$detail[$count]['id']);
														// if($is_last){

																
															switch ($rank) {
																case 0:
																default:
																	$indicator = '<i class="fa fa-exclamation-triangle warnColor"></i>';
																	break;
																case 1:
																	if($fill['metode_auction']	== "indikator")	{
																		$indicator = '<i class="auction-prize3"></i>';
																	}else{
																		$indicator = '<b style="font-size : 12px">1</b>';	
																		
																	}
																	break;
																case $rank > 1:
																	if($fill['metode_auction']	== "indikator")	{
																		$indicator = '<i class="auction-thumb1"></i>';
																	}else{	
																		$indicator = '<b style="font-size : 12px">'.$rank.'</b>';
																	}
																	break;

															}
														// }
														echo '<td'.$td_class.' align="center">'.$indicator.'</td>';
													}
													if($fill['auction_jenis']!='auction'){
													echo '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $lampiran_list['file']).'">'. $lampiran_list['file'].'</a></td>';	
													}
													
													echo '</tr>';
												$count++;
												}


											}
											

											$y++;
										
								}else{
									echo '<tr><td colspan="8" class="noData">Belum Ada Penawaran</td></tr>';
								}
								
							?>
						</tbody>
						</form>
					</table> <br><br>
			</div>
			<div style="overflow:auto" class="panel">
				<h2>Rekap Penawaran Perusahaan</h2>
				<table cellpadding="0" cellspacing="0" border="0" class="table" width="100%">
					<form id="auction-penawaran-form" method="post" action="<?php echo site_url('auction/user/vendor_dash/save_penawaran/'); ?>">
						<thead>
							<tr>
								<th class="header-table">No.</th>
								<th class="header-table"><?php echo (($fill['kriteria_pemenang']=='lump_sum') ? 'Nama Paket' : 'Nama Barang');?></th>
								<th class="header-table">Vendor</th>
								<th class="header-table">Penawaran Ke-</th>
								<th class="header-table">Penawaran Harga</th>
								<th class="header-table">Penawaran Harga<br/>(dalam IDR)</th>
								<th class="header-table"><?php echo $persentase; ?> (%)</th>
								<?php if($fill['auction_jenis']!='rfi'){ ?>
								<th width="80" class="header-table">
									<?php if($fill['metode_auction'] == "posisi") echo "Posisi/Ranking"; else echo "Indikator"; ?>
								</th>
								<?php } ?>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<th width="80" class="header-table">
									Lampiran
								</th>
								<?php } ?>
							</tr>
						</thead>

						<tbody>
							<?php 
							
								
								// print_r($detail);
								
								if(count($detail)>0){
									
									$count_penawaran = 0;
									$data_barang = array();
									// print_r($detail);
									$data_count =  array();
									$barang_count = 0;
									foreach ($detail as $key_penawaran => $value_penawaran) {
										if($data_count[$value_penawaran['id_vendor']]=='') $data_count[$value_penawaran['id_vendor']] = 0;
										$data_barang[$value_penawaran['id_vendor']]['value'][$data_count[$value_penawaran['id_vendor']]][$value_penawaran['id_barang']] = $value_penawaran['in_rate'];
										$barang_count++;
										
										if($barang_count==$barang->num_rows()){
											$data_count[$value_penawaran['id_vendor']] ++;
											$penawaran_ke = $this->pam->count_total_barang_penawaran($fill['id'], $value_penawaran['id_vendor']);
											// if($count==$penawaran_ke) $data_count = 0;
											$barang_count = 0;
										}
									}
									foreach ($peserta->result_array() as $key_peserta => $value_peserta) {
										foreach($data_barang[$value_peserta['id_vendor']]['value'] as $key_barang => $row){
											foreach ($barang->result_array() as $key_result => $value) {
												if(!isset($row[$value['id']])){
													$data_barang[$value_peserta['id_vendor']]['value'][$key_barang][$value['id']] = $data_barang[$value_peserta['id_vendor']][$key_barang-1][$value['id']];
												}
											}
										}
									}
									// echo print_r($data_barang);
									// print_r($peserta->result());
									
									if($is_total){

											$hps_total = $this->pam->hps_total($fill['id']);
											$id_vendor = 0;
											foreach($peserta->result() as $key => $value){
												$lampiran_list	= $this->pam->get_lampiran($fill['id'],$value->id_vendor)->result_array();
												$useRowspan = count($lampiran_list);
												$useRowspan = 'rowspan ="'.$useRowspan.'"';

												$nilai = $this->pam->check_last_value($fill,$value->id_vendor);
												$penawaran_ke = $this->pam->count_total_barang_penawaran($fill['id'], $value->id_vendor);
												$prev_penawaran = end(array_keys($data_barang[$value->id_vendor]['value'])) - 1;
												$percentage = number_format(($nilai['max_in_rate'] - array_sum($data_barang[$value->id_vendor]['value'][$prev_penawaran]))/($nilai['max_in_rate']/100),1);
												if($penawaran_ke == 1){
													$percentage = 0;	
												}

												if($key==0) $id_vendor = $value->id_vendor;
												echo '<tr class="row">';
												echo '<td '.$useRowspan.' align="center">'.($key+1).'</td>';
												echo '<td'.$td_class.' '.$useRowspan.'>'.$fill['name'].'</td>';
												echo '<td'.$td_class.' '.$useRowspan.'><a href="'.site_url('auction/admin/auction_progress/by_vendor/'.$fill['id'].'/'.$value->id_vendor).'">'.$value->nama_vendor.'</a></td>';
												echo '<td'.$td_class.' '.$useRowspan.'>'.$penawaran_ke.'</td>';
												echo '<td'.$td_class.' '.$useRowspan.'>'.number_format($nilai['max_nilai']).'</td>';
												echo '<td'.$td_class.' '.$useRowspan.'>'.number_format($nilai['max_in_rate']).'</td>';
												echo '<td'.$td_class.' '.$useRowspan.'><span class="gr-text">'.$percentage.'</span></td>';
												$rank = $this->pam->get_ranking_total($fill, $nilai['max_in_rate']);
												if($fill['auction_jenis']!='rfi'){
													switch ($rank) {
														case 0:
														default:
															$indicator = '<i class="fa fa-exclamation-triangle warnColor"></i>';
															break;
														case 1:
															if($fill['metode_auction']	== "indikator")	{
																$indicator = '<i class="auction-prize3"></i>';
															}else{
																$indicator = '<b style="font-size : 12px">1</b>';	
																
															}
															break;
														case $rank > 1:
															if($fill['metode_auction']	== "indikator")	{
																$indicator = '<i class="auction-thumb1"></i>';
															}else{	
																$indicator = '<b style="font-size : 12px">'.$rank.'</b>';
															}
															break;

													}
													
												}
												echo '<td'.$td_class.' align="center" '.$useRowspan.'>'.$indicator.'</td>';
												
												
												$lampiran = '';
												$key_lampiran = 1;
												foreach ($lampiran_list as $key_lampiran=> $result_lampiran) {
													if($fill['auction_jenis']!='auction'){
														
														if($key_lampiran==1) $lampiran .= '<tr>';
														if($result_lampiran['file']==''){
															$lampiran .= '<td'.$td_class.' align="center"> - </td></tr>';
														}else{
															$lampiran .= '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $result_lampiran['file']).'">'.$result_lampiran['file'].'</a></td></tr>';		
														}
														
													}
													if($key_lampiran == count($data_barang[$value->id_vendor]['file'])) $key_lampiran = 0;

												}
												// echo $lampiran;
												if($id_vendor) $id_vendor = 0;
													
											}
										}else{
											$id_vendor = array();
											$useRowspan = $barang->num_rows();
											$useRowspan = 'rowspan ="'.$useRowspan.'"';
											// echo print_r($detail);
											$count = 0;
											for($i=0;$i<$total;$i++){ 
												
												
												$index = $y;
												
													echo '<tr>';

													echo '<td '.$useRowspan.' align="center">'.$index.'</td>';
												
													$x = 0; 
													
													foreach($barang->result() as $data){ 
														$penawaran = $this->pam->get_last_penawaran($id_lelang, $data->id,$detail[$count]['id_vendor'])->result_array();
														$last_penawaran = $penawaran[count($penawaran) - 1];
														$nilai		= number_format($last_penawaran['nilai']);
														$in_rate	= number_format($last_penawaran['in_rate']);
														$percentage	= $this->pam->cek_percentage($id_lelang, $detail[$count]['id_vendor'], $data->id, $last_penawaran['in_rate'], $last_penawaran['id']);
														$penawaran_ke = count($penawaran);

														$indicator	= '';
														$td_class	= '';
														echo '<td'.$td_class.'>'.$data->nama_barang.'</td>';
														if($x==0) {
															echo '<td'.$td_class.' '.$useRowspan.'><a href="'.site_url('auction/admin/auction_progress/by_vendor/'.$fill['id'].'/'.$detail[$count]['id_vendor']).'">'.$detail[$count]['legal_name'].' '.$detail[$count]['vendor_name'].'</a></td>'; 
															$x++;
														}
														echo '<td'.$td_class.'>'.$penawaran_ke.'</td>';
														echo '<td'.$td_class.'>'.$nilai.'</td>';
														echo '<td'.$td_class.'>'.$in_rate.'</td>';
														echo '<td'.$td_class.'><span class="gr-text">'.$percentage.'</span></td>';
														if($fill['auction_jenis']!='rfi'){
															// $is_last = $this->pam->is_last($fill['id'], $detail[$count]['id_vendor'],$detail[$count]['id']);
															// if($is_last){

																$rank = $this->pam->get_indicator($fill['id'],$data->id, $detail[$count]['id_vendor']);
																
																switch ($rank) {
																	case 0:
																	default:
																		$indicator = '<i class="fa fa-exclamation-triangle warnColor"></i>';
																		break;
																	case 1:
																		if($fill['metode_auction']	== "indikator")	{
																			$indicator = '<i class="auction-prize3"></i>';
																		}else{
																			$indicator = '<b style="font-size : 12px">1</b>';	
																			
																		}
																		break;
																	case $rank > 1:
																		if($fill['metode_auction']	== "indikator")	{
																			$indicator = '<i class="auction-thumb1"></i>';
																		}else{	
																			$indicator = '<b style="font-size : 12px">'.$rank.'</b>';
																		}
																		break;

																}
															// }
															echo '<td'.$td_class.' align="center">'.$indicator.'</td>';
														}
														if($fill['auction_jenis']!='auction'){
														echo '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $detail[$count]['file']).'">'. $detail[$count]['file'].'</a></td>';	
														}
														
														echo '</tr>';
													$count++;

												}
												

												$y++;
											}
											// rpint_r($id_vendor);
											
									}
								}else{
									echo '<tr><td colspan="8" class="noData">Belum Ada Penawaran</td></tr>';
								}
								
							?>
						</tbody>
						</form>
					</table>
			</div>
		</div>
		<?php } ?>
</div>
	<div id="auction-blocker">
		<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
			<tr>
				<td width="100%" height="100%" align="center" valign="middle">
					<div id="auction-blocker-message-bar"></div>
				</td>
			</tr>
		</table>
	</div>
	</div>
</div>