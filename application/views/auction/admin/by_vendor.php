<div class="mg-lg-12">
	<div class="block">
		<p id="auction-report-bar" class="msgSuccess text-center">
	<i class="fas fa-check-square"></i> Auction telah selesai.
	<b style="color : #617ac6; cursor : pointer">
		<a href="<?php echo site_url('auction/admin/report/index/'.$id_lelang)?>" target="_blank">Klik disini</a>
	</b> untuk melihat report.
</p>
<p id="auction-report-bar" class="msgSuccess text-center">
	<i class="fas fa-check-square"></i> Auction teah selesai.
	<b style="color : #617ac6; cursor : pointer">
		<a href="<?php echo base_url(); ?>index.php/report/index/<?php echo $id_lelang; ?>" target="_blank">Klik disini</a>
	</b> untuk melihat report.
</p>
<div class="lelang">
	<div class="col-14">
		<div class="panel">
				<h2><i class="fas fa-file-signature"></i> <?php echo $fill['name']; ?></h2>
			<table class="table table-borderless content-group-sm">
				<tbody>
					<tr>
						<td>
							Mata Uang
						</td>
						<td class="text-right">
							<?php echo $fill['rate']; ?>
						</td>
					</tr>
					<tr>
						<td>
							Metode Penawaran
						</td>
						<td class="text-right">
							<?php 
								if($fill['metode_penawaran'] == "lump_sum") 
									echo "Total";
								if($fill['metode_penawaran'] == "harga_satuan") 
									echo "Itemize";
							?>
						</td>
					</tr>
					<tr>
						<td>
							Kriteria Pemenang
						</td>
						<td class="text-right">
							<?php 
								if($fill['kriteria_pemenang'] == "lump_sum") 
									echo "Total";
								if($fill['kriteria_pemenang'] == "harga_satuan") 
									echo "Itemize";
							?>
						</td>
					</tr>
					<tr>
						<td>
							Durasi
						</td>
						<td class="text-right">
							<?php if($fill['auction_jenis']=='auction'){
								echo $fill['auction_duration'].' Menit';
							}else{
								echo default_date($fill['start_time']).' - '.default_date($fill['time_limit']);
							} ?>
						</td>
					</tr>
					<tr>
						<td>
							Tipe Auction
						</td>
						<td class="text-right">
							<?php 
								if($fill['auction_type'] == "forward_auction") 
									echo "Forward Auction";
								if($fill['auction_type'] == "reverse_auction") 
									echo "Reverse Auction";
							?>
						</td>
					</tr>
					<tr>
						<td>
							Jenis Auction
						</td>
						<td class="text-right">
							<?php 
								switch ($fill['auction_jenis']) {
									case 'auction':
										echo 'Auction';
										break;
									case 'rfi':
										echo 'RFI';
										break;
									case 'rfi_pembelian':
										echo 'RFI Pembelian Langsung';
										break;
									case 'rfi_penunjukan':
										echo 'RFI Penunjukkan Langsung';
										break;
								}
							?>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="panel-body">
				<?php 
				if($fill['metode_auction']=='indikator'&&$fill['auction_jenis']!='auction'){ ?>
				<div class="noticeMedia">
					<ul>
						<?php if($fill['auction_jenis']=='rfi_penunjukan'){ ?>
						<li class="media">
							<div class="media-left">
								<i class="fa fa-exclamation-triangle warnColor"></i>
							</div>
							<div class="media-body">
								Penawaran masih diatas HPS/dibawah nilai limit
							</div>
						</li>
						<?php } ?>
						<li class="media">
							<div class="media-left">
								<i class="auction-prize3"></i>
							</div>
							<div class="media-body">
								Penawaran adalah yang <?php echo $limit; ?>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<i class="auction-thumb1"></i>
							</div>
							<div class="media-body">
								Penawaran bukan yang <?php echo $limit; ?>
							</div>
						</li>
					</ul>
				</div>
				<?php } ?>
			</div>
			<div id="start-auction" class="panel-body">
				
			</div>
		</div>
		<div class="panel " id="timer-container">
			<div class="panel-heading">
				<h4><i class="fa fa-clock-o"></i>&nbsp;Waktu tersisa</h4>
			</div>
			<div class="panel-body">
				<div id="timer">-- : -- : --</div>
			</div>
		</div>
	</div>
	<?php if($fill['auction_jenis']=='auction'){?>
	<div class="col-34">
		<div id="chart-container" class="panel"></div>
	</div>
	<?php }else{?>
	<div class="col-34">
		<div style="overflow:auto">
			<table cellpadding="0" cellspacing="0" border="0" class="table" width="100%">
				<form id="auction-penawaran-form" method="post" action="<?php echo site_url('auction/user/vendor_dash/save_penawaran/'); ?>">
					<thead>
						<tr>
							<th class="header-table">No.</th>
							<th class="header-table">Nama Barang</th>
							<th class="header-table">Vendor</th>
							<th class="header-table">Penawaran Ke-</th>
							<th class="header-table">Penawaran Harga</th>
							<th class="header-table">Penawaran Harga<br/>(dalam IDR)</th>
							<th class="header-table"><?php echo $persentase; ?> (%)</th>
							<?php if($fill['auction_jenis']!='rfi'){ ?>
							<th width="80" class="header-table">
								<?php if($fill['metode_auction'] == "posisi") echo "Posisi/Ranking"; else echo "Indikator"; ?>
							</th>
							<?php } ?>
							<?php if($fill['auction_jenis']!='auction'){ ?>
							<th width="80" class="header-table">
								Lampiran
							</th>
							<?php } ?>
						</tr>
					</thead>

					<tbody>
						<?php 
						
							$is_total = ($fill['kriteria_pemenang']=='lump_sum');
							$y = 1;
							$count = 0;
							$detail = $penawaran->result_array();
							
							$total = count($detail) / $barang->num_rows();
							$mark_last = '';
							$useRowspan = '';
							$penawaran_ke = $nilai_list = array();
							$peserta = $this->pam->get_bid_peserta($fill['id']);
							$class="";
							
							
							if(count($detail)>0){
								$count_penawaran = 0;
								// print_r($detail);
								foreach ($detail as $key => $data) {
									$data_peserta[$data['id_vendor']] = $data['legal_name'].' '.$data['vendor_name'];
									$data_penawaran[$count_penawaran]['id_vendor'] = $data['id_vendor'];
									$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['nilai'] = $data['nilai'];
									$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['in_rate'] = $data['in_rate'];
									$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['down_percent'] = $data['down_percent'];
									$data_penawaran[$count_penawaran]['val'][$data['id_barang']]['lampiran'] = $data['file'];
									$data_penawaran[$count_penawaran]['is_last'] = $this->pam->is_last($fill['id'], $data['id_vendor'],$data['id']);
									if(count($data_penawaran[$count_penawaran]['val'])==$barang->num_rows()){
										$count_penawaran++;
									}
								}
								

								$useRowspan = $barang->num_rows();
								$useRowspan = 'rowspan ="'.$useRowspan.'"';

								if($is_total){

										$rank = $this->pam->process_rank($data_penawaran, $fill['id'],$fill['auction_type']);
										$hps_total = $this->pam->hps_total($fill['id']);

										foreach ($data_penawaran as $key => $value) {

											$penawaran_ke[$value['id_vendor']] += 1;
											echo '<tr class="row">';
											echo '<td '.$useRowspan.' align="center">'.($key+1).'</td>';
											
											echo '<td'.$td_class.' '.$useRowspan.'>'.$fill['name'].'</td>';
											echo '<td'.$td_class.' '.$useRowspan.'>'.$data_peserta[$value['id_vendor']].'</td>';
											echo '<td'.$td_class.' '.$useRowspan.'>'.$penawaran_ke[$value['id_vendor']].'</td>';
											$nilai = $in_rate = $down_percent = $index_lampiran = 0;
											$lampiran = '';
											foreach ($value['val'] as $key_penawaran=> $result_penawaran) {
												$nilai 			+= $result_penawaran['nilai'];
												$in_rate 		+= $result_penawaran['in_rate'];
												$down_percent 	+= $result_penawaran['down_percent'];
												if($fill['auction_jenis']!='auction'){
													
													if($index_lampiran!=0) $lampiran .= '<tr>';
													$lampiran .= '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $result_penawaran['lampiran']).'">'.$result_penawaran['lampiran'].'</a></td></tr>';	
												}
											}
											$id_vendor = $value['id_vendor'];
											$index_penawaran = $penawaran_ke[$value['id_vendor']];
											$nilai_list[$id_vendor][$index_penawaran]=$nilai;
											$percentage = number_format(($nilai_list[$id_vendor][$index_penawaran] - $nilai_list[$id_vendor][$index_penawaran-1])/($nilai_list[$id_vendor][$index_penawaran]/100),1);
											
											if($index_penawaran==1){
												$percentage = 0;
											}
											
											echo '<td'.$td_class.' '.$useRowspan.'>'.number_format($nilai).'</td>';
											echo '<td'.$td_class.' '.$useRowspan.'>'.number_format($in_rate).'</td>';
											echo '<td'.$td_class.' '.$useRowspan.'><span class="gr-text">'.$percentage.'</span></td>';
											$nilai_list[$value['id_vendor']]['count']++;
											if($fill['auction_jenis']!='rfi'){

												if($fill['metode_auction']=='ranking'){
													$indicator = $rank[$key];
												}else{
													
													if($rank[$key]==1){
														$indicator = '<i class="auction-prize3"></i>';
													}else{
														$indicator = '<i class="auction-thumb1"></i>';
													}
													if($fill['auction_jenis']=='rfi_penunjukan'){

														if( ($fill['auction_type'] == 'reverse_auction'&&$hps_total<$nilai) || ($fill['auction_type'] == 'forward_auction'&&$hps_total>$nilai)){
															$indicator = '<i class="fa fa-exclamation-triangle warnColor"></i>';
														}
													}
													
												}
												echo '<td'.$td_class.' align="center" '.$useRowspan.'>'.(($value['is_last']) ? $indicator : '').'</td>';
											}

											echo $lampiran;
										// 	$count++;
										}
									}else{
										for($i=0;$i<$total;$i++){ 
											
											$index = $y;
												echo '<tr>';

												echo '<td '.$useRowspan.' align="center">'.$index.'</td>';
											
												$x = 0; 
												
												foreach($barang->result() as $data){ 
												

													
														$nilai		= number_format($detail[$count]['nilai']);
														$in_rate	= number_format($detail[$count]['in_rate']);
														$percentage	= $this->pam->cek_percentage($id_lelang, $detail[$count]['id_vendor'], $data->id, $detail[$count]['in_rate'], $detail[$count]['id']);
														$penawaran_ke[$detail[$count]['id_vendor']][$data->id] += 1;
														$indicator	= '';
														$td_class	= '';
														
														echo '<td'.$td_class.'>'.$data->nama_barang.'</td>';
														if($x==0) {
															echo '<td'.$td_class.' '.$useRowspan.'>'.$detail[$count]['legal_name'].' '.$detail[$count]['vendor_name'].'</td>'; 
															$x++;
														}
														echo '<td'.$td_class.'>'.$penawaran_ke[$detail[$count]['id_vendor']][$data->id].'</td>';
														echo '<td'.$td_class.'>'.$nilai.'</td>';
														echo '<td'.$td_class.'>'.$in_rate.'</td>';
														echo '<td'.$td_class.'><span class="gr-text">'.$percentage.'</span></td>';
														if($fill['auction_jenis']!='rfi'){
															$is_last = $this->pam->is_last($fill['id'], $detail[$count]['id_vendor'],$detail[$count]['id']);
															if($is_last){

																$rank = $this->pam->get_indicator($fill['id'],$data->id, $detail[$count]['id_vendor']);
																switch ($rank) {
																	case 0:
																	default:
																		$indicator = '<i class="fa fa-exclamation-triangle warnColor"></i>';
																		break;
																	case 1:
																		if($fill['metode_auction']	== "indikator")	{
																			$indicator = '<i class="auction-prize3"></i>';
																		}else{
																			$indicator = '<b style="font-size : 12px">1</b>';	
																			
																		}
																		break;
																	case $rank > 1:
																		if($fill['metode_auction']	== "indikator")	{
																			$indicator = '<i class="auction-thumb1"></i>';
																		}else{	
																			$indicator = '<b style="font-size : 12px">'.$rank.'</b>';
																		}
																		break;

																}
															}
															echo '<td'.$td_class.' align="center">'.$indicator.'</td>';
														}
														if($fill['auction_jenis']!='auction'){
														echo '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $detail[$count]['file']).'">'. $detail[$count]['file'].'</a></td>';	
														}
														$count++;
														echo '</tr>';	
												}
												$y++;
											}
										
								}
							}else{
								echo '<tr><td colspan="8" class="noData">Belum Ada Penawaran</td></tr>';
							}
							
						?>
					</tbody>
					</form>
				</table>
		</div>
	</div>
	<?php } ?>
</div>
		
<div id="auction-blocker">
	<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
		<tr>
			<td width="100%" height="100%" align="center" valign="middle">
				<div id="auction-blocker-message-bar"></div>
			</td>
		</tr>
	</table>
</div>
	</div>
</div>