<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/agen/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGeneratorLangsung').tableGenerator({
		url: '<?php echo site_url('auction/getDataLangsung/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Paket"
			},
			{
				"key"	: "auction_date",
				"value"	: "Tanggal"
			},
			{
				"key"	: "lokasi_name",
				"value"	: "Lokasi"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[1].value != '') {
					return defaultDate(data[1].value);
				}
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				// console.log(data[6].value)
				html +='<a href="'+site_url+"auction/duplikatAuction/"+data[4].value+'" class="button is-primary"><span><i class="far fa-copy"></i>  Duplikat</span></a>';
				html +='<a href="'+site_url+"auction_tabs/auction_progress/index/"+data[4].value+'" class="button is-primary"><span><i class="fa fa-search"></i> Lihat Data</span></a>';
				if ('<?php echo $this->session->userdata('admin')['id_role'] ==1 ?>') {
				html +=deleteButton(site_url+"auction/deleteAuction/"+data[4].value, data[4].value);
				}
				// if(data[6].value == 0) {
				// 	html +=editButton(site_url+"auction/editAuction/"+data[4].value, data[4].value);
				// }
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
			
		},
		finish: function(){
	      var edit = $('.buttonEdit').modal({
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(edit).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');

	        }
	      });

		var del = $('.buttonDelete').modal({
			header: 'Hapus Data',
			render : function(el, data){
				el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
				data.onSuccess = function(){
					$(del).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				};
				data.isReset = true;
				$('.form', el).form(data).data('form');
			}
		});

		var aktif = $('.buttonAktif').modal({
			header: 'Aktifkan Data ?',
			render : function(el, data){
				el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengaktifkan data?<span><div class="form"></div><div>');
				data.onSuccess = function(){
					$(aktif).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				};
				data.isReset = true;
				$('.form', el).form(data).data('form');
			}
		});

	  },
	  filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
});
</script>