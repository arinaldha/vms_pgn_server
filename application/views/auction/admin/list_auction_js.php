<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/agen/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGeneratorAuction').tableGenerator({
		url: '<?php echo site_url('auction/getDataAuction/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Paket"
			},
			{
				"key"	: "auction_date",
				"value"	: "Tanggal"
			},
			{
				"key"	: "lokasi_name",
				"value"	: "Lokasi"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[1].value != '') {
					return defaultDate(data[1].value);
				}
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				var html = '';

				html +=editButton(site_url+"auction/editAuction/"+btoa(data[4].value), btoa(data[4].value));
				html +='<a href="'+site_url+"auction/duplikatAuction/"+btoa(data[4].value)+'" class="button is-primary btnDup"><i class="far fa-copy"></i> Duplikat</a>';
				_pengadaan = '<?php echo site_url('auction/cek_auction_data') ?>/'+btoa(data[4].value);
				if (_pengadaan) {
					
					// html +='<a href="'+site_url+"auction_progress/index/"+btoa(data[4].value)+'" class="button is-primary"><i class="far fa-copy"></i> Lihat Data</a>';
				}
				//html +='<a href="'+site_url+"auction/duplikatAuction/"+btoa(data[4].value)+'" class="button is-primary buttonDup"><span><i class="far fa-copy"></i>  Duplikat</span></a>';
				
				html +='<a href="'+site_url+"auction_tabs/auction_progress/index/"+btoa(data[4].value)+'" class="button is-primary"><span><i class="fa fa-search"></i> Lihat Data</span></a>';

				if ('<?php echo $this->session->userdata('admin')['id_role'] ==1 ?>') {
				html +=deleteButton(site_url+"auction/deleteAuction/"+btoa(data[4].value), btoa(data[4].value));
			}

				html +=deleteButton(site_url+"auction/remove/"+btoa(data[4].value), btoa(data[4].value));
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
			el.prepend(insertButton(site_url+"auction/insert/<?php echo $id;?>"));
		},
		finish: function(){

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			 var duplikat = $('.btnDup').modal({
                header: 'Duplikat Data',
                render : function(el, data){

                data.onSuccess = function(){
                 $(duplikat).data('modal').close();
                 location.reload();
                };
                data.isReset = false;

                $(el).form(data).data('form');
             }
          });

	  },
	  filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form9').hide();
			$('.form5 [name="work_area"]').click(function() {
				var val = $(this).val();
				if(val == 'internet'){
					$('.form6').hide();
					$('.form7').hide();
				} else {
					$('.form6').show();
					$('.form7').show();
				}
			})
			$('.form0 [name="auction_type"]').click(function(){
				var type = $(this).val();
				if (type == 'forward_auction') {
					$('.form2').hide();
					$('.form3').hide();
				} else {
					$('.form2').show();
					$('.form3').show();
				}
			})
			$('.form6 [name="duration_type"]').click(function(){
					$('.form9').show();
			})
		}
	});
});
</script>