<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('auction_tabs/tatacara/viewEditTatacara/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                window.location = '<?php echo site_url('vendor/dashboard');?>';
            }
            
            xhr.successMessage = 'Berhasil !!';

            $('.formTatacara').form(xhr);
            
            var id_procurement = '<?php echo $id ?>';
            var html = '';
            
            if (xhr.form[0].value != '') {
                html += '<a class="button is-primary buttonEdit" href="'+site_url+"auction_tabs/tatacara/edit/"+id_procurement+'">Edit</a>'
                $('#tatacara').html(html);
            }else{
                html += '<a class="button is-primary buttonAdd" href="'+site_url+"auction_tabs/tatacara/insert/"+id_procurement+'">Tambah</a>'
                $('#tatacara').html(html);
            }
        }
    });

    var add = $('.buttonAdd').modal({
        header: 'Tambah Data',
        render : function(el, data){
            data.onSuccess = function(){
                $(add).data('modal').close();
                location.reload();
            }
            $(el).form(data);
           
            $('.form1 [name="metode_penawaran"]').click(function(){
                var val = $(this).val();
                if (val != 'harga_satuan') {
                    $(".form2 input[value='lump_sum']").attr('checked','true');
                    $(".form2 input[value='harga_satuan']").attr('disabled','true');
                } else {
                    $(".form2 input[value='harga_satuan']").removeAttr('disabled');
                }
            })
        }
    });

    var edit = $('.buttonEdit').modal({
                header: 'Update Data',
                render : function(el, data){

                data.onSuccess = function(){
                 $(edit).data('modal').close();
                 location.reload();
                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                if ($('.form1 [value="lump_sump"]').prop('checked')) {
                     $('.form2 input[value="harga_satuan"]').attr('disabled','true');
                } else {
                    $(".form2 input[value='harga_satuan']").removeAttr('disabled');
                }
                
                $('[name="metode_penawaran"]').click(function(){
                    var val = $(this).val();
                    
                    if (val != 'harga_satuan') {
                        $(".form2 input[value='lump_sump']").attr('checked','true');
                        $(".form2 input[value='harga_satuan']").attr('disabled','true');
                    } else {
                        $(".form2 input[value='harga_satuan']").removeAttr('disabled');
                    }
                })
             }
          });
});


</script>