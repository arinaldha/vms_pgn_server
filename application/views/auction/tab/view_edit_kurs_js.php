<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGeneratorKurs').tableGenerator({
		url: '<?php echo site_url('auction_tabs/kurs/get_data_kurs/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Mata Uang"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=deleteButton(site_url+"auction_tabs/kurs/remove/"+data[2].value, data[2].value);
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"auction_tabs/kurs/insert/<?php echo $id;?>"));
		},
		finish: function(){

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
			    $(add).data('modal').close();
			    table.data('plugin_tableGenerator').fetchData();
				// location.reload()
			}
			$(el).form(data);
			$('.form1').hide();
			$('[name="id_kurs"]').click(function() {
				var id = $(this).val();
				if (id != 1) {
					$('.form1').show();
				} else {
					$('.form1').hide();
				}
			})	
		}
	});
});
</script>