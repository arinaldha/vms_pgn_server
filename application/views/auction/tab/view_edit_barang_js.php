<script type="text/javascript">
$(function(){
	
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGeneratorBarang').tableGenerator({
		url: '<?php echo site_url('auction_tabs/barang/get_data_barang/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "nama_barang",
				"value"	: "Nama Barang"
			},{
				"key"	: "kurs",
				"value"	: "Kurs"
			},{
				"key"	: "volume",
				"value"	: "Volume"
			},{
				"key"	: "satuan",
				"value"	: "Satuan"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';

				html +=editButton(site_url+"auction_tabs/barang/edit/<?php echo $id;?>/"+data[5].value, data[5].value);
				html +=deleteButton(site_url+"auction_tabs/barang/remove/"+data[5].value, data[5].value+'/'+<?php echo $id;?>);

				return html;
			},
			target : [4]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"auction_tabs/barang/insert/<?php echo $id;?>"));
		},
		finish: function(){

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var edit = $('.buttonEdit').modal({
					header: 'Ubah Barang',
	        		render : function(el, data){

	          		data.onSuccess = function(){
		          		$(edit).data('modal').close();
		            	table.data('plugin_tableGenerator').fetchData();
		            // location.reload()
	          		};
	          		data.isReset = false;

	          		$(el).form(data).data('form');
	       //    		$.ajax({
				    //     url : '<?php echo site_url('auction_tabs/barang/rfi_or_auction/'.$id)?>',
				    //     method: 'POST',
				    //     async : false,
				    //     dataType : 'json',
				    //     success: function(xhr){
				    //         xhr.onSuccess = function(data){
				    //             window.location = '<?php echo site_url('vendor/dashboard');?>';
				    //         }
				            
				    //         xhr.successMessage = 'Berhasil !!';
				    //         console.log(xhr);
				    //         if (xhr.form[0].value == 'rfi_penunjukan' || xhr.form[0].value == 'auction') {
				    //         	$('.form4').show();
				    //         } else {
				    //         	$('.form4').hide();
				    //         }
				    //     }
				    // });	
	        	}
      		});

		},
	});
  	var add = $('.buttonAdd').modal({
  		header:'Tambah Barang',
		render : function(el, data){
			data.onSuccess = function(){
				// location.reload()
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			
			 $.ajax({
		        url : '<?php echo site_url('auction_tabs/barang/rfi_or_auction/'.$id)?>',
		        method: 'POST',
		        async : false,
		        dataType : 'json',
		        success: function(xhr){
		            xhr.onSuccess = function(data){
		                window.location = '<?php echo site_url('vendor/dashboard');?>';
		            }
		            
		            xhr.successMessage = 'Berhasil !!';
		            console.log(xhr);
		            if (xhr.form[0].value == 'rfi_penunjukan' || xhr.form[0].value == 'auction') {
		            	$('.form4').show();
		            } else {
		            	$('.form4').hide();
		            }
		        }
		    });	
			 
			 /*if (xhr.form[0].value == 'rfi_penunjukan' || xhr.form[0].value == 'auction') {
	            		$('.form4').show();
	           	 } else {
	            		$('.form4').hide();
	           	 }*/
			$.ajax({
				url:'<?php echo site_url('auction_tabs/barang/cek_data_isAuction_or_rfiPenunjukkan/'.$id) ?>',
				dataType: 'json',
				succes:function(data) {
					$('.form4').show();
				}
			})
		}
	});

		var edit = $('.buttonEdit').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			// $('.form1').hide();
			$('[name="id_kurs"]').click(function() {
				var id = $(this).val();
				if (id != 1) {
					$('.form1').show();
				} else {
					$('.form1').hide();
				}
			})	
		}
	});
});
</script>