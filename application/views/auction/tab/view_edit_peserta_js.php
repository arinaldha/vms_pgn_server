<script type="text/javascript">
$(function(){
	var id_procurement = '<?php echo $id ?>';
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auction_tabs/peserta/get_data_peserta/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Peserta"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=deleteButton(site_url+"auction_tabs/peserta/remove/"+data[1].value, data[1].value);
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"auction_tabs/peserta/insert/<?php echo $id;?>"));
		},
		finish: function(){

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			var _xhr;
			$.ajax({
				url: '<?php echo site_url('auction_tabs/peserta/formFilter')?>',
				async: false,
				dataType: 'json',
				success:function(xhr){
					_xhr = xhr;
				}
			})

			var tableAdd = $(el).tableGenerator({
				url: '<?php echo site_url('auction_tabs/peserta/get_data_tambah_peserta/'.$id); ?>',
				data: dataPost,
				headers: [
					{
						"key"	: "role_name",
						"value"	: "Nama Peserta"
					}/*,{
						"key"	: "npp",
						"value"	: "NPP"
					}*/,{
						"key"	: "action",
						"value"	: "Action",
						"sort"	: false
					}],
				columnDefs : [{
					renderCell: function(data, row, key, el){
						var html = '';
						html += '<a class="button is-primary checkTrg" href="'+site_url+"auction_tabs/peserta/tambahDPT/"+id_procurement+'/'+data.id+'">Tambah Peserta</a>';
						return html;
					},
					target : [1]

				}],
				finish: function(el){

					$('.checkTrg').off('click').click(function(e) {
						e.preventDefault();
						_el = $(e.target);
						$.ajax({
							url: _el.attr('href'),
							method:'post',
							success:function(data) {
								
								tableAdd.data('plugin_tableGenerator').fetchData();
								table.data('plugin_tableGenerator').fetchData();
							}
						})
					});
				},
				filter: {
					wrapper: $('.contentWrap'),
					data : {
						data: _xhr
					}
				}
			});
			
		}
	});

});
</script>