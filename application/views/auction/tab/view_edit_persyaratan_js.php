<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('auction_tabs/persyaratan/view_edit_persyaratan/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                window.location = '<?php echo site_url('vendor/dashboard');?>';
            }
            // console.log(xhr.form[19].value);
            xhr.successMessage = 'Berhasil !!';

            $('#tableGeneratorPersyaratan').form(xhr);
            // console.log(xhr);
          	var id_procurement = '<?php echo $id ?>';
            var html = '';
            
            if (xhr.form[0].value != '') {
                html += '<a class="button is-primary buttonEdit" href="'+site_url+"auction_tabs/persyaratan/edit/"+id_procurement+'">Edit</a>'
                $('#persyaratan').html(html);
            }else{
                html += '<a class="button is-primary buttonAdd" href="'+site_url+"auction_tabs/persyaratan/insert/"+id_procurement+'">Tambah</a>'
                $('#persyaratan').html(html);
            }

                    }


    });

    var edit = $('.buttonEdit').modal({
                header: 'Update Data',
                render : function(el, data){

                data.onSuccess = function(){
                 $(edit).data('modal').close();
                 location.reload();
                };
                data.isReset = false;

                $(el).form(data).data('form');

             }
          });

    
});


</script>