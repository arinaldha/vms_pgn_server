<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGeneratorTambahPeserta').tableGenerator({
		url: '<?php echo site_url('auction_tabs/peserta/get_data_tambah_peserta/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Peserta"
			},{
				"key"	: "npp",
				"value"	: "NPP"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html += '<input class="check" type="checkbox" name="vendor" value="'+data[3].value+'" hidden><button class="button is-primary checkTrg">Tambah Peserta</button>';
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			
		},
		finish: function(){
			
		},
	});

	$('.checkTrg').click(function() {
		$(this).siblings('.check').trigger('click');
		$(this).parentsUntil('tbody').toggleClass('is-added');
	})

	$(".checkTrg").click(function () {
        $(this).text(function(i, v){
           return v === 'Tambah Peserta' ? 'Hapus Peserta' : 'Tambah Peserta'
        })
    });

	$('.btnSave').click(function() {
		if (confirm('Anda Yakin Ingin Menyimpan Data Ini?')) 
		{
			var id = [];
			var id_procurement = '<?php echo $id ?>';
			$(':checkbox:checked').each(function(i) {
				id[i] = $(this).val();
			});
			if (id.length === 0) {
				alert('Gagal,Anda Tidak Memilih Vendor Manapun!!');
			} else {
				$.ajax({
					url: '<?php echo site_url('auction_tabs/peserta/save') ?>/'+id_procurement,
					method:'post',
					data: {id:id},
					success:function(data) {
						location.reload();
						// for (var i = 0;i < id.length; i++) {
						// 	Things[i]
						// }
					}
				})
			}
		}
	})
	
});
</script>