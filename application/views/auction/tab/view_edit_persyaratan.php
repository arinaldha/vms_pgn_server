<div id="edit" class="tab procView">
	
	<div class="tableWrapper" style="margin-bottom: 20px">
	<?php if($this->session->userdata('admin')['id_role']==6|7){ ?>
	<div class="btnTopGroup clearfix">
		<form <?php if($persyaratan->description != '') { ?> action="<?php echo site_url('auction_tabs/persyaratan/edit/'.$id)?>"<?php } else { ?> action="<?php echo site_url('auction_tabs/persyaratan/save/'.$id)?>" <?php } ?> method="POST" enctype="multipart/form-data">
			<h2>Persyaratan</h2>
			<table style="width:100%">
				<tr class="input-form">
					<td>
						<textarea id="wysiwyg1" class="wysiwyg" style="width:100%; height:350px;" name="description"><?php echo $persyaratan->description ?></textarea>
					</td>
				</tr>
			</table>
				<input type="hidden" name="id_proc" value="<?php echo $id ?>">
			<div class="buttonRegBox clearfix">
				<?php if(isset($persyaratan->description)) { ?>
				<button class="button is-primary">Simpan Perubahan</button>
			<?php } else { ?>
				<button class="button is-primary">Tambah Persyaratan</button>
			<?php }?>
			</div>
		</form>
	</div>
	</div>
	<?php } ?>
</div>
<script>
	$(function(){
		tinymce.init({
				selector:'textarea',
	            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
				toolbar:  ["undo redo | bold italic | link image | alignleft aligncenter alignright | fontselect | fontsizeselect"],
	        style_formats: [
	            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
	            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
	            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
	            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
	            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
	            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
	            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
	            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
	            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
	            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
	            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
	            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
	            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
	        ],
			}
		);
	})
	
</script>
<div class="mg-lg-12">

	<div class="block">



		<div id="tableGeneratorPersyaratan">

		</div>
	
		<div align="center" class="btnMargin" id="persyaratan">

			
		</div>



	</div>

</div>
