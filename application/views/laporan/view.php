<div class="mg-lg-12">
	<div class="block" >
		<form  action="<?php echo site_url('laporan/cetak')?>" method="post">
			<div class="custom_report">
				<fieldset class="form-group" for="">
					<i class="fa fa-calendar"></i> Dari tanggal <input type="text" class="form-control datePicker" value="" name="date_from"> sampai <input type="text" class="form-control datePicker " value="" name="date_to">
				</fieldset>
				<fieldset class="form-group" for="">
					<label for="">Jenis Anggaran</label>
					<div class="selectWrapper">
						<select name="jenis_anggaran" class="form-control ">
							<option value="operasi">Operasi</option>
							<option value="investasi">Investasi</option>
						</select>
					</div>
				</fieldset>
			</div>
			<div id="base">
			</div>
			
			<div class="form-group btn-group">
				<button class="btn btn-primary btnCetak" type="submit" value="submit"><i class="fa fa-file-excel-o"></i>&nbsp;Cetak</button>
			</div>
		</form>

	</div>
</div>