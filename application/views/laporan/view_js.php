<script type="text/javascript">
$(function(){
	var __entry_by;
	$('.datePicker').datetimepicker({
		timepicker: false,
		format: 'Y-m-d'
	});
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;

	var __id_user 		= <?php echo $admin['id_user']?>;

	var _xhr;
	$.ajax({
		url : '<?php echo site_url('laporan/getHeader/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			_xhr = xhr;
		}
	});
	var base = $('#base');
	$.each(_xhr, function(key, value){
		if(typeof value.header !='undefined'){
			var wrapper = $('<div class="headerWrapper mg-lg-4"><h3>'+value.header+'</h3><ul></ul></div>');
			base.append(wrapper);
		}else{

			if($('.lastWrapper').length == 0){
				var wrapper = $('<div class="headerWrapper mg-lg-4 lastWrapper"><h3>LAINNYA</h3><ul></ul></div>');
				base.append(wrapper);
			}else{
				wrapper = $('.lastWrapper');
			}
		}
		
		$.each(value.field, function(keyField, valueField){
			if(typeof valueField=='object'){
				var list = $('<li class="fieldOpt unchecked"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1>'+valueField.title+'&nbsp;<i class="fa iconOpt fa-plus"></i></li>');
			}else{
				var list = $('<li class="fieldOpt unchecked"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1>'+valueField+'&nbsp;<i class="fa iconOpt fa-plus"></i></li>');
			}
			var code = randomColor();
			list.css('background-color',code);
			list.hover(function(){
				list.css('background-color',LightenDarkenColor(code, -20))
			}, function(){
				list.css('background-color',code)
			});

			list.on('click', function(){
				var cb = $('.checkboxOpt',list);
				var ic = $('.iconOpt',list);
				ic.removeClass('fa-plus fa-check');
				list.removeClass('checked unchecked');
				if(cb.is(':checked')){
					cb.prop('checked',false);
					ic.addClass('fa-plus');
					list.addClass('unchecked');
				}else{
					cb.prop('checked',true);
					ic.addClass('fa-check');
					list.addClass('checked');
					
				}
			})
			wrapper.append(list);
			
		})
	})

	
});
</script>
