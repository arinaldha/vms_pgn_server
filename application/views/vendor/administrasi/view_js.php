<script type="text/javascript">

$(function(){

        $.ajax({
            url : '<?php echo site_url('vendor/administrasi/view')?>',
            method: 'POST',
            async : false,
            dataType : 'json',
            success: function(xhr){
                xhr.onSuccess = function(data){
                    window.location = '<?php echo site_url('vendor/dashboard');?>';
                }
                console.log(xhr.form[19].value);
                xhr.successMessage = 'Berhasil !!';
                console.log(xhr.form[6].value)

            $('.form').form(xhr);
            
                console.log($('.form'));
                var html = '';
                html += '<a class="button is-primary buttonEdit1" href="'+site_url+"vendor/administrasi/editAdministrasi/"+btoa(xhr.form[19].value)+'">Update</a>'
                $('.btnMargin').html(html);
                 _defaultDate = '';
                    if (xhr.form[7].value == '0000-00-00') {
                        _defaultDate += 'Tanggal belum diisi';
                    } else {
                        _defaultDate += defaultDate(xhr.form[7].value);
                    }
                    $('.form7 span').html(_defaultDate)

                    if (xhr.form[6].value !='') {
                        $('.form6').show()
                    } else {
                        $('.form6').hide()                    
                    }
                    if (xhr.form[7].value == '0000-00-00') {
                         $('.form7').hide()
                    } else {
                         $('.form7').show()
                    }
                    if (xhr.form[8].value !='') {
                        $('.form8').show()
                    } else {
                        $('.form8').hide()               
                    }
            }
        });

        var edit = $('.buttonEdit1').modal({
                header: 'Update Data',
                render : function(el, data){

                data.onSuccess = function(){
                 $(edit).data('modal').close();
                 location.reload();
                };
                data.isReset = false;

                $(el).form(data).data('form');

            $('.form6').before('<table><tbody><tr><td style="text-align: center;vertical-align: middle;"><input type="checkbox" name="is_nppkp" value="1" class="nppkp_code"></td><td width="90%"><span>Dengan ini menyatakan bahwa saya bukan Pengusaha Kena Pajak (Non PKP). Sebagaimana dimaksud pada Undang - Undang Pajak Pertambahan Nilai.</span></td></tr></tbody></table>');
            // if (xhr.form[]) {

            // }
            // $('.nppkp_code').on('change', function(){
            //     $('.form6').toggle();
            //     $('.form7').toggle();
            //     $('.form8').toggle();
            // })
             $('input[name="is_nppkp"]').on('change',function(){
                    var val = $(this).val();
                    // alert(val);
                    if (!$(this).prop('checked')) {
                        $('.form6').show()
                        $('.form6 input[name="nppkp_code"]').attr('value',data.form[6].value)
                        $('.form7').show()
                        $('.form7 input[name="nppkp_date"]').attr('value',data.form[7].value)
                        $('.form8').show()
                        $('.form8 input[name="nppkp_file"]').attr('value',data.form[8].value)
                    } else {
                        $('.form6').hide()
                        $('.form6 input[name="nppkp_code"]').removeAttr('value')
                        $('.form7').hide()
                        $('.form7 input[name="nppkp_date"]').removeAttr('value')
                        $('.form8').hide()
                        $('.form8 input[name="nppkp_file"]').removeAttr('value')
                    }
                })
             }
          });

        var dataPost = {
                order: 'id',
                sort: 'desc'
            };

        var table = $('.tableBank').tableGenerator({
            url: '<?php echo site_url('vendor/administrasi/getDataBank/'.$id); ?>',
            data: dataPost,
            headers: [
                {
                    "key"   : "no",
                    "value" : "No.Rekening"
                },
                {
                    "key"   : "name",
                    "value" : "Atas Nama"
                },
                {
                    "key"   : "bank_name",
                    "value" : "Nama Bank"
                },
                {
                    "key"   : "bank_file",
                    "value" : "Lampiran"
                },
                {
                    "key"   : "action",
                    "value" : "Action",
                    "sort"  : false
                }],
            columnDefs : [
             {
                renderCell: function(data, row, key, el){
                    var html = '';
                    file = btoa('assets/lampiran/bank_file/'+data[3].value);
                   html +='<a href="'+base_url+"open/file/"+file+'">'+data[3].value+'</a>';
                    return html;
                },
                target : [3]
            },
            {
                renderCell: function(data, row, key, el){
                    var html = '';
                   html +=deleteButton(site_url+"vendor/rekening/remove/"+data[4].value, data[4].value);
                    return html;
                },
                target : [4]
            }],
            additionFeature: function(el){
                el.prepend(insertButton(site_url+"vendor/rekening/insert/<?php echo $id;?>"));
            },
            finish: function(){
                var del = $('.buttonDelete').modal({
                    header: 'Hapus Data',
                    render : function(el, data){
                        el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
                        data.onSuccess = function(){
                            $(del).data('modal').close();
                            table.data('plugin_tableGenerator').fetchData();
                        };
                        data.isReset = true;
                        $('.form', el).form(data).data('form');
                    }
                });

            }
        });
        var add = $('.buttonAdd').modal({
            render : function(el, data){
                data.onSuccess = function(){
                    $(add).data('modal').close();
                    table.data('plugin_tableGenerator').fetchData();
                }
                $(el).form(data);
            }
        });
});
</script>