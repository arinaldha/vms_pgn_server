<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/izin/formFilterBidang')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/izin/getData/'.$id); ?>',
		data: dataPost,
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][5].value);

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href:site_url+"vendor/izin/remove/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/izin/edit/"+_id
			},{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/izin/izinView/"+_id
			}];
			return btn;
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Klasifikasi"
			},
			{
				"key"	: "no",
				"value"	: "Nomor"
			},{
				"key"	: "issue_date",
				"value"	: "Masa Berlaku "
			},
		],
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

	          		data.onSuccess = function(){
		          		$(edit).data('modal').close();
		            	folder.data('plugin_folder').fetchData();

	          		};
	          		data.isReset = false;

	          		$(el).form(data).data('form');

					_formField = {
						'siup': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'siupal': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'sbu': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'asosiasi': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expire_date','izin_file'],
						'siujk': ['no', 'grade','issue_date','qualification','expire_date','izin_file']
					}
					$(' .form-group',el).not('.btn-group').hide();
					$.each(_formField[data.data.type], function(key, value){
						$('input.form-control[name="'+value+'"]',el).closest('.form-group').show();
						$('select.form-control[name="'+value+'"]',el).closest('.form-group').show();
					});
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
	        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
			
			var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){
	          		data.onSuccess = function(){
		          		$(cek).data('modal').close();
		            	folder.data('plugin_folder').fetchData();
	          		};
	          		data.isReset = false;

	          		$(el).form(data).data('form');
	          		// console.log(data);
	          		html = '';
		            if (data.form[2].value == 'lifetime') {
		                html += 'Seumur Hidup';
		            } else {
		                html += defaultDate(data.form[2].value)
		            }
		            console.log(html)
		            $('.form2 span').html(html);

					_formField = {
						'siup': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'siupal': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'sbu': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'asosiasi': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expire_date','izin_file'],
						'siujk': ['no', 'grade','issue_date','expire_date','izin_file']
					}
					$(' .form-group',el).not('.btn-group').hide();

					$.each(_formField[data.data.type], function(key, value){
						$('[for="'+value+'"]',el).show();
						
					});
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})

	          		var html = '';
	          		$.ajax({
						url: '<?php echo site_url('vendor/izin/getBSB')?>',
						async: false,
						data:{
							id_ijin_usaha : data.id
						},
						method: 'POST',
						dataType: 'json',
						success:function(xhr){

							__bsb = xhr;
							$(el).append('<h3>Bidang / Sub-bidang</h3><div class="treeView" id="parentTree"><ul></ul></div>')
							var i = 0;

							$.each(__bsb, function(key, value){
								$('#parentTree > ul',el).append('<li class="treeDropdown"><span>'+key+'</span><ul id="head'+i+'"></ul></li>');
								$.each(value, function(keyBSB, valueBSB){
									console.log($('#head'+i, el));
									$('#head'+i, el)
									.append('<li><label style="width:100%"><div class="inline-form">&nbsp;'+valueBSB+'</div></label></li>');
								})
								
								i++;
							});
							
						}
					})
        		}
      		});
		},
		
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			console.log(value);
			if(value[4].value == 'lifetime'){
				_dateFormat = 'Seumur Hidup';
			}
			else{

				_dateFormat = defaultDate(value[4].value);
			}

			html += '<div class="caption"><p>'+value[6].value+'</p><p>'+value[7].value+'</p><p>'+_dateFormat+'</p><p><a href="'+site_url+"vendor/izin/editBSB/"+btoa(value[5].value)+'" class="button is-primary editBSB"><i class="fa fa-list"></i>&nbsp Bidang & Sub Bidang</a></p></div>';

			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/izin/insert/<?php echo $id;?>"));
		},
		finish: function(){
     		var editBSB = $('.editBSB').modal({
				header: 'Bidang / Sub-bidang',
				render : function(el, data){
					data.onSuccess = function(){
						location.reload();
					}
					$(el).form(data).data('form');
       				$('form',el).prepend('<fieldset class="form-group treeWrapper blockWrapper"><div class="treeView" data-role="treeview"></div></fieldset>');
					$('.treeView', el).append('<ul></ul>');
					$.each(data.tree, function(key, value){
						$('.treeView > ul', el).append('<li class="treeDropdown closeTree"><span>'+value.label+'</span><ul id="head'+key+'"></ul></li>');
						$.each(value.data, function(keyTree, valueTree){
							var _checked = '';

							if(valueTree.value=='1') {
								_checked = 'checked';
							}
							$('.treeView  #head'+key, el)
							.append('<li><label style="width:100%"><div class="inline-form-input"><input type="checkbox" name="bsb['+key+'][]" value="'+keyTree+'" '+_checked+'></div><div class="inline-form">&nbsp;'+valueTree.label+'</div></label></li>');
						})
						
					});
					$('.treeDropdown > span').on('click', function(){
						$(this).closest('.treeDropdown').toggleClass('closeTree');
					});
				}
			});

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			var fw = $(el).formWizard({
				data: data,
				url: '<?php echo site_url('vendor/izin/insert_izin')?>',
				onNext: function(){
					_wizard = $(fw).data('formWizard');
					
					step = _wizard.options.data.step;
					var _step = [];
					var i = 1;
					$.each(step, function(key, value){
						_step[i] = key;
						i++;
					})
					_formWrapper = $('.wizard-content #step'+_wizard.options.currentPosition);
					_form = $('form',_wizard.options.wrapper);
					formData = new FormData( _form[0]);
					formData .append('validation', _step[_wizard.options.currentPosition]);

					$.ajax({
						url: '<?php echo site_url('vendor/izin/simpan')?>',
						data: formData,
						method : 'POST',
						processData: false,
						contentType: false,
						async: false,
						dataType: 'json',	
						success: function(xhr){
							_formWrapper.data('form').element = _formWrapper;
							

							if(xhr.status=='error') {
								_formWrapper.data('form').options.errorMessage = 'Terjadi Kesalahan';
							_formWrapper.data('form').generateError(xhr.form);

								_return = false;
							}else{
								_formWrapper.data('form').removeError(_formWrapper);
								_return = true;
							}
						}
					});


					

					return _return;
				},
				onSubmit: function(){

				},
				onSuccessSubmit: function(){
					folder.data('plugin_folder').fetchData();
					$(add).data('modal').close();
					location.reload();

				},
				onSuccess: function(){

					/*HIDE UNNECESSARY FIELD*/

					var iu = {
						1: ['siup', 'ijin_lain', 'asosiasi','siupal'],
						2: ['siup', 'ijin_lain', 'asosiasi','siupal'],
						3: ['siup', 'ijin_lain', 'asosiasi','siupal'],
						4: ['siujk', 'sbu', 'asosiasi', 'ijin_lain','siupal'],
						5: ['siujk', 'sbu', 'asosiasi', 'ijin_lain']

					};
					$('input.form-control[name="id_dpt_type"]').on('change', function(){
						var dpt_type = $(this).val();
						$('input.form-control[name="type"]').prop('checked',false);
						
						$.each($('input.form-control[name="id_dpt_type"]:checked'), function(key, value){

							$.each(iu[$(value).val()], function(_key, _value){
								
								$('input.form-control[name="type"][value="'+_value+'"]').closest('.radioList').show();

							})
						});
						
					})
					
					_formField = {
						'siup': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'siupal': ['no', 'issue_date','qualification','expire_date','izin_file'],
						'sbu': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'asosiasi': ['authorize_by','no', 'issue_date','expire_date','izin_file'],
						'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expire_date','izin_file'],
						'siujk': ['no', 'grade','issue_date','qualification','expire_date','izin_file']
					}
					$('input.form-control[name="type"]').on('change', function(){
						var iuType = $(this).val();
						if(iuType=='asosiasi'){
							$('.form-control[name="authorize_by"]').siblings('label').text('Anggota Asosiasi*');
						}else{
							$('.form-control[name="authorize_by"]').siblings('label').text('Lembaga Penerbit*');
						}

						$('#step3 .form-group').not('.btn-group').hide();
						$.each(_formField[iuType], function(key, value){
							$('input.form-control[name="'+value+'"]').closest('.form-group').show();
							$('select.form-control[name="'+value+'"]').closest('.form-group').show();
						});

					});
					
					$('input.form-control[name="id_dpt_type"]').on('change', function(){
						__id_dpt_type = $(this);
						var dpt_type = $(this).val();
						console.log(dpt_type)
						var __type = $('input.form-control[name="type"]');
						__type.prop('checked',false);
						__type.closest('.radioList').hide();
						
						$.each($('input.form-control[name="id_dpt_type"]:checked'), function(key, value){
							// console.log(value);
							$.each(iu[$(value).val()], function(_key, _value){
								
								$('input.form-control[name="type"][value="'+_value+'"]').closest('.radioList').show();
								// console.log($('input.form-control[name="type"][value="'+_value+'"]'));
							})
						});
						
						var __dpt_type = { 'id' : []};
						$.each($('input.form-control[name="id_dpt_type"]:checked'), function(key, value){
							__dpt_type['id'].push($(this).val());
							
						})
						_wizard = $(fw).data('formWizard');
						console.log(__dpt_type)
						
						$.ajax({
							url: '<?php echo site_url('vendor/izin/getBidangSubBidang')?>',
							data: {
								id_dpt_type : __dpt_type
							},
							method : 'POST',
							async: false,
							dataType: 'json',	
							success: function(xhr){
								$('.treeWrapper').remove();
								$('.wizard-content #step4 .form.blockWrapper',_wizard.options.wrapper).prepend('<fieldset class="form-group treeWrapper"><div class="treeView" data-role="treeview"></div></fieldset>');
								
								var __treeWrapper = $('.wizard-content #step4 .treeView');
								__treeWrapper.append('<ul></ul>');
								$.each(xhr, function(key, value){
									if (key != 29 && key != 57) {
										$('.wizard-content #step4 .treeView > ul').append('<li class="treeDropdown closeTree"><span>'+value.label+'</span><ul id="head'+key+'"></ul></li>');
										$.each(value.data, function(keyTree, valueTree){
											$('.wizard-content #step4 .treeView #head'+key)
											.append('<li><label style="width:100%"><div class="inline-form-input"><input type="checkbox" name="bsb['+key+'][]" value="'+keyTree+'" class="checkboxBSB"></div><div class="inline-form">&nbsp;'+valueTree.label+'</div></label></li>');
										})
									} else {
										$('.wizard-content #step4 .treeView > ul').append('<li class="treeDropdown"><span>'+value.label+'</span><ul id="head'+key+'"></ul></li>');
										$.each(value.data, function(keyTree, valueTree){
											$('.wizard-content #step4 .treeView #head'+key)
											.append('<li><label style="width:100%"><div class="inline-form-input"><input type="checkbox" name="bsb['+key+'][]" value="'+keyTree+'" class="checkboxBSB"></div><div class="inline-form">&nbsp;'+valueTree.label+'</div></label></li>');
										})
									}
									
								});

								$('.treeDropdown > span').on('click', function(){
									$(this).closest('.treeDropdown').toggleClass('closeTree');
								});
								
								var __treeWrapper = $('.wizard-content #step4 .treeView');
								$(__treeWrapper).on('change', '.checkboxBSB', function(){
									iuType = $('input.form-control[name="type"]:checked').val();
									var __parent = $(this).closest('.treeDropdown');
									var __another = __parent.siblings();
									console.log(iuType);
									if(iuType=='sbu'){
										$('.checkboxBSB',__another).prop('checked',false);
									}
									
								});
							}
						})
					})

					

				}
			})
		}
	});
});


</script>