<div class="col col-8 surat_pernyataan">

	<div class="block" style="padding: 20px">

		<p style="padding: 40px">Menyatakan dengan sesungguhnya bahwa :</p>
					<ol style="padding-left: 40px; text-align: justify;">
						<li>Saya/perusahaan saya tidak sedang dalam pengawasan pengadilan atau tidak sedang dinyatakan pailit atau kegiatan usahanya tidak sedang dihentikan atau tidak sedang menjalani hukuman (sanksi) pidana;</li>
						<li>Saya tidak pernah dihukum berdasarkan putusan pengadilan atas tindakan yang berkaitan dengan kondite profesional saya;</li>
						<li>Perusahaan saya memiliki kinerja baik dan tidak termasuk dalam kelompok yang terkena sanksi atau daftar hitam di <b>PT Perusahaan Gas Negara Tbk</b> maupun di instansi lainnya, dan tidak dalam sengketa dengan <b>PT Perusahaan Gas Negara Tbk;</b></li>
						<li>Informasi/dokumen/formulir yang akan saya sampaikan adalah benar dan dapat dipertanggung jawabkan secara hukum.</li>
					</ol> <br>

		<div class="form">
			
		</div>



	</div>

</div>
