<script type="text/javascript">
$(function(){

	dataPost = {
		// order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/agen/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGeneratorAuction').tableGenerator({
		url: '<?php echo site_url('vendor/K3/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "csms_file",
				"value"	: "Nama File CSMS"
			},
			{
				"key"	: "score",
				"value"	: "Score"
			},
			{
				"key"	: "remark",
				"value"	: "Alasan"
			},
			{
				"key"	: "expiry_date",
				"value"	:"Masa Berlaku"
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[1].value != '') {
					return defaultDate(data[3].value);
				}
			},
			target : [3]
		},		{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"auction/editAuction/"+data[4].value, data[4].value);
				html +='<a href="'+site_url+"auction/duplikatAuction/"+data[4].value+'" class="button is-primary buttonDup"><i class="far fa-copy"></i> Duplikat</a>';
				// _pengadaan = '<?php echo site_url('auction/cek_auction_data') ?>/'+data[4].value;
				// if (_pengadaan) {
					html +='<a href="'+site_url+"auction_tabs/auction_progress/index/"+data[4].value+'" class="button is-primary"><i class="fa fa-search"></i> Lihat Data</a>';
				// }
				if ('<?php echo $this->session->userdata('admin')['id_role'] ==1 ?>') {
				html +=deleteButton(site_url+"auction/deleteAuction/"+data[4].value, data[4].value);
			}
				return html;
			},
			target : [5]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[0].value != '') {
					file = btoa('/assets/lampiran/csms_file/'+data[0].value);
					html += '<a href="'+base_url+'open/file/'+file+'" target="blank">'+data[0].value+'</a>'
				}else{
					html += '-';
				}
				return html;
			},
			target : [0]
		}],
		additionFeature: function(el){
			
		},
		finish: function(){

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

	  },
	  filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form9').hide();
			$('.form5 [name="work_area"]').click(function() {
				var val = $(this).val();
				if(val == 'internet'){
					$('.form6').hide();
					$('.form7').hide();
				} else {
					$('.form6').show();
					$('.form7').show();
				}
			})
			$('.form0 [name="auction_type"]').click(function(){
				var type = $(this).val();
				if (type == 'forward_auction') {
					$('.form2').hide();
					$('.form3').hide();
				} else {
					$('.form2').show();
					$('.form3').show();
				}
			})
			$('.form8 [name="duration_type"]').click(function(){
					$('.form9').show();
			})
		}
	});
});
</script>