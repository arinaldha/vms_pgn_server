<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	// var _xhr;
	// $.ajax({
	// 				url: '<?php echo site_url('vendor/agen/formFilterBsb')?>',
	// 				async: false,
	// 				dataType: 'json',
	// 				success:function(xhr){
	// 					_xhr = xhr;
	// 				}
	// 			})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/agen/bsbView/'.base64_encode($id)); ?>',
		data: dataPost,
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][2].value);

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href: site_url+"vendor/agen/removeBsb/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/agen/editBsb/"+_id
			}];
			return btn;
		},
		header: [
			{
				"key"	: "bidang_name",
				"value"	: "Bidang"
			},
			{
				"key"	: "sub_bidang_name",
				"value"	: "Sub Bidang"
			}
		],
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
			var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
		},
		renderContent: function(el, value, key){
			html = '';
			html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[1].value+'</p></div>';
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/agen/insertBsb/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},
		// filter: {
		// 	wrapper: $('.contentWrap'),
		// 	data : {
		// 		data: _xhr
		// 	}
		// }

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>