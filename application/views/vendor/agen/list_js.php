<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/agen/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/agen/getData/'.$id); ?>',
		data: dataPost,
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][5].value);
			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href: site_url+"vendor/agen/remove/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/agen/edit/"+_id
			},{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/agen/agenView/"+_id
			}];
			return btn;

		},
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
			var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		// console.log(data);
          		html = '';
	            if (data.form[6].value == 'lifetime') {
	                html += 'Seumur Hidup';
	            } else {
	                html += defaultDate(data.form[6].value)
	            }
	            console.log(html)
	            $('.form6 span').html(html);
	            $('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Nomor"
			},
			{
				"key"	: "no",
				"value"	: "Tipe"
			},{
				"key"	: "expire_date",
				"value"	: "Masa Berlaku"
			},
		],
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			if(value[3].value == 'lifetime'){
				_dateFormat += 'Seumur Hidup' 
			}
			else{
				_dateFormat += defaultDate(value[3].value);
			}
			html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[2].value+'</p><p>'+_dateFormat+'</p><p><a class="button is-primary" href="'+site_url+"vendor/agen/produk/"+btoa(value[5].value)+'"><i class="fas fa-box"></i>&nbsp;Produk</a></p><p><a class="button is-primary" href="'+site_url+"vendor/agen/bsb/"+btoa(value[5].value)+'" ><i class="fas fa-list-ul"></i>&nbsp;Bidang & Sub Bidang</a></p></div>';
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/agen/insert/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>