<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/blacklist/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "id_bidang",
				"value"	: "Nama Bidang Usaha"
			},
			{
				"key"	: "name",
				"value"	: "Nama Sub Bidang"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"vendor/blacklist/edit/"+data[2].value, data[2].value);
				html +=deleteButton(site_url+"vendor/blacklist/remove/"+data[2].value, data[2].value);
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/blacklist/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){

            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			hideshow();
			$('[name="id_role"]').on('change', function(){
				hideshow();
			})
		}
	});
});
</script>