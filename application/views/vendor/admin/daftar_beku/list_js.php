<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/admin/daftar_beku/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/admin/daftar_beku/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
			"key" : "legal_name",
			"value" : "Badan Usaha"
			},
			{
			"key"	: "name",
			"value"	: "Nama Badan Usaha"
			},
			{
			"key"	: "npwp_code",
			"value"	: "NPWP"
			},
			{
			"key" : "edit_stamp",
			"value" : "Aktivitas Terakhir"
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				
			},
			target : [4]

		}
		],
		additionFeature: function(el){
			
		},
		finish: function(){
			var ubahLokasi = $('.buttonEdit').modal({
			header: 'Ubah Lokasi',
        	render : function(el, data){
          	data.onSuccess = function(){
          	$(ubahLokasi).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          	};
         	 data.isReset = false;

          	$(el).form(data).data('form');

        }
      });
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
});
</script>