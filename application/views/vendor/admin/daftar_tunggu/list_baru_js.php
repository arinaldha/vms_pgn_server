<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
		url: '<?php echo site_url('vendor/admin/daftar_tunggu_list/daftar_tunggu_baru/formFilter')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			_xhr = xhr;
		}
	})

	var table = $('#dataTable').tableGenerator({
		url: '<?php echo site_url('vendor/admin/daftar_tunggu_list/daftar_tunggu_baru/get_list_daftar_tunggu/1'); ?>',
		data: dataPost,
		headers: [
			{
			"key" : "legal_name",
			"value" : "Badan Usaha"
			},
			{
			"key"	: "name",
			"value"	: "Nama Badan Usaha"
			},
			{
			"key" : "edit_stamp",
			"value" : "Aktivitas Terakhir"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				var id_role = '<?php $this->session->userdata('admin')['id_role'] ?>';
				html += '<a href="'+site_url+"approval/index/"+data[3].value+'" class="button is-primary"><i class="fa fa-edit"></i>&nbsp Cek Data</a>';
				if (id_role == 1 || id_role == 6) {
					html += '<a href="'+site_url+"vendor/admin/daftar_tunggu/ubahLokasi/"+data[3].value+'" class="button is-primary buttonEdit"><i class="fa fa-map-marker"></i>&nbsp Ubah unit organisasi</a>';
				}

				return html;
			},
			target : [3]

		}
		],
		additionFeature: function(el){
			
		},
		finish: function(){
			var ubahLokasi = $('.buttonEdit').modal({
			header: 'Ubah Lokasi',
        	render : function(el, data){
          	data.onSuccess = function(){
          	$(ubahLokasi).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          	};
         	 data.isReset = false;

          	$(el).form(data).data('form');

        }
      });
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
});
</script>