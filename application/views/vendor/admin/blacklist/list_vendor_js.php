<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>'

	var _xhr;
	$.ajax({
			url: '<?php echo site_url('vendor/admin/black_list/blacklist_vendor/formFilter')?>',
			async: false,
			dataType: 'json',
			success:function(xhr){
				_xhr = xhr;
			}
		})

	var table = $('#tableGeneratorVendor').tableGenerator({
		url: '<?php echo site_url('vendor/admin/black_list/blacklist_vendor/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama"
			},
			{
				"key"	: "remark",
				"value"	: "Alasan"
			},{
				"key"	: "start_date",
				"value"	: "Tanggal Mulai"
			},{
				"key"	: "end_date",
				"value"	: "Tanggal Akhir"
			},{
				"key"	: "blacklist_file",
				"value"	: "File"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				_link = '';
				file = btoa('assets/lampiran/blacklist_file/'+data[4].value);
				return _link += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="far fa-file-alt" style="font-size: 1.5em"></i></a>'
			},
			target : [4]
			
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(data[2].value)
			},
			target : [2]

		},
		{
			renderCell: function(data, row, key, el){
				return defaultDate(data[3].value)
			},
			target : [3]

		}
		,{
			renderCell: function(data, row, key, el){
				var html = '';
				if (id_role != 5) {
					html += '<a href="'+site_url+"vendor/admin/black_list/blacklist_vendor/edit/"+data[6].value+'" class="button is-primary buttonEdit"><i class="fa fa-edit"></i>&nbsp Edit</a>';
				}
				if (id_role == 1) {
					html += '<a href="'+site_url+"vendor/admin/black_list/blacklist_vendor/aktif/"+data[6].value+'" class="button is-danger buttonDelete"><i class="fa fas fa-trash"></i>&nbsp Hapus</a>';
				}
				
				return html;
			},
			target : [5]

		}
		],
		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/admin/black_list/blacklist_vendor/insert/<?php echo $id;?>"));
			el.prepend('<a href="'+site_url+'vendor/admin/black_list/blacklist_vendor/report" class="button is-primary"><i class="fas fa-file-export"></i>Report</a>')
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	table.data('plugin_tableGenerator').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');

        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	table.data('plugin_tableGenerator').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');

        	}
			});
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);


			$('.modal input[name="name"]').donetyping(function() {
				_self = $(this);
				_parent = _self.closest('.form-group');
				_resultWrapper = $('.searchOption', _parent);
				_resultWrapper.empty();

				$.ajax({
						url : '<?php echo site_url('vendor/admin/black_list/blacklist_vendor/search_vendor')?>',
						data : {
							search : _self.val()
						},
						method : 'POST',
						beforeSend : function(){
							$(_self).addClass('loading');
							_resultWrapper.empty();
						},
						success : function(xhr){
							
							setTimeout(function(){
								$(_self).removeClass('loading');
								var html = '<ul>';
								console.log(xhr)
								$.each( $.parseJSON(xhr), function(key, value){

									html += '<li data-id="'+key+'" data-name="'+value+'">'+value+'</li>';
								});
								html += '</ul>';
								_resultWrapper.html(html);

								$('ul li', _resultWrapper).on('click', function(e){

									id = $(this).data('id');
									// alert(id);
									$('.modal input[name="id_vendor"] ').val(id);
									// id_komag_field.val(id);
									$(_self).val($(this).data('name'));
									_resultWrapper.empty();
								});
							}, 1000);
							
						}	
					});
			})
		}
	});

});


</script>