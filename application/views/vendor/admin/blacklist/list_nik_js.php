<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>'

	var _xhr;
	$.ajax({
		url: '<?php echo site_url('vendor/admin/black_list/blacklist_nik/formFilter')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			_xhr = xhr;
		}
	})
	
	var table = $('#tableGeneratorNik').tableGenerator({
		url: '<?php echo site_url('vendor/admin/black_list/blacklist_nik/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "nik",
				"value"	: "NIK"
			},
			{
				"key"	: "nama_pengurus",
				"value"	: "Nama"
			},
			{
				"key"	: "vendor_name",
				"value"	: "Vendor"
			},
			{
				"key"	: "remark",
				"value"	: "Alasan (Remark)"
			},{
				"key"	: "start_date",
				"value"	: "Tanggal Mulai"
			},{
				"key"	: "end_date",
				"value"	: "Tanggal Akhir"
			},{
				"key"	: "blacklist_file",
				"value"	: "File"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				_link = '';
				file = btoa('assets/lampiran/blacklist_file/'+data[6].value);
				return _link += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="far fa-file-alt" style="font-size: 1.5em"></i></a>'
			},
			target : [6]
			
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(data[4].value)				
			},
			target : [4]

		},
		{
			renderCell: function(data, row, key, el){
				return defaultDate(data[5].value)				
			},
			target : [5]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (id_role != 5) {
					html += '<a href="'+site_url+"vendor/admin/black_list/blacklist_nik/edit/"+data[7].value+'" class="button is-primary buttonEdit"><i class="fa fa-edit"></i>&nbsp Edit</a>';
					html += '<a href="'+site_url+"vendor/admin/black_list/blacklist_nik/aktif/"+data[7].value+'" class="button is-danger buttonDelete"><i class="fa fas fa-trash"></i>&nbsp Hapus</a>';
				}
				
				return html;
			},
			target : [7]

		}
		],
		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/admin/black_list/blacklist_nik/insert/<?php echo $id;?>"));
			el.prepend('<a href="'+site_url+'vendor/admin/black_list/blacklist_nik/report" class="button is-primary"><i class="fas fa-file-export"></i>Report</a>')
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	table.data('plugin_tableGenerator').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');

        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Aktifkan Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);

			$('.modal [name="nik"]').donetyping(function(){

				_self = $(this);
				_parent = _self.closest('.form-group');
				_resultWrapper = $('.searchOption', _parent);
				_resultWrapper.empty();
				
				$.ajax({
					url : '<?php echo site_url('vendor/admin/black_list/blacklist_nik/search_nik')?>',
					data : {
						search : _self.val()
					},
					method : 'POST',
					beforeSend : function(){
						$(_self).addClass('loading');
						_resultWrapper.empty();
					},
					success : function(xhr){
						
						setTimeout(function(){
							$(_self).removeClass('loading');
							var html = '<ul>';
						
							$.each( $.parseJSON(xhr), function(key, value){
								// html += '<ul>';
								html += '<li data-id="'+key+'" data-name="'+key+'">'+value+' </li>';
								// html += '</ul>';
							});
							html += '</ul>';
							_resultWrapper.html(html);

							$('ul li', _resultWrapper).on('click', function(e){

								id = $(this).data('id');
								$('.modal input[name="nik"] ',_parent).val(id);
								// id_komag_field.val(id);
								$(_self).val($(this).data('name'));
								_resultWrapper.empty();
							});
						}, 1000);
						
					}	
				});
			})
		}
	});

	var ex = $('.btnEx').modal({
		header: 'Report',
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	})

});


</script>
