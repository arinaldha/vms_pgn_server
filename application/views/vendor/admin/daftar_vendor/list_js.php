<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/admin/daftar_vendor/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/admin/daftar_vendor/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
			"key"	: "name",
			"value"	: "Nama Vendor"
			},
			{
			"key" : "legal_name",
			"value" : "Badan Usaha"
			},
			{
			"key"	: "username",
			"value"	: "Username"
			},
			{
			"key" : "password",
			"value" : "Password"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var link = '';
				link += '<a href="'+site_url+"vendor/view/view_data/index/"+data[4].value+'">'+data[0].value+'</a>';
				
				return link;
			},
			target : [0]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (id_role != 1 && id_role != 8 && id_role != 12) {
					html += '<a href="'+site_url+"vendor/admin/daftar_vendor/remove/"+data[4].value+'" class="button is-danger buttonDelete"><i class="fa fa-trash"></i>&nbsp Hapus</a>';
				}
				
				return html;
			},
			target : [4]

		}
		],
		additionFeature: function(el){
			
		},
		finish: function(){
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
});


</script>