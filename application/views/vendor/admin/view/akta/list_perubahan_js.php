<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };
     var xhr;
    $.ajax({
            url: '<?php echo site_url('vendor/view/akta/formFilter')?>',
            async: false,
            dataType: 'json',
            success:function(xhr){
                _xhr = xhr;
            }
        })

    var folder = $('#folderGenerator2').folder({
        url: '<?php echo site_url('vendor/view/akta/view/'.$id.'/perubahan'); ?>',
        data: dataPost,
        header: [
            {
                "key"   : "type",
                "value" : "Jenis Akta"
            },
            {
                "key"   : "no",
                "value" : "Nomor Akta"
            },{
                "key"   : "notaris",
                "value" : "Notaris"
            },{
                "key"   : "authorize_date",
                "value" : "Tanggal Ditetapkan"
            },
        ],
        renderContent: function(el, value, key){
            html = '';
            _file = '';
            _dateFormat = '';
            button = '';
            id_role = <?php echo $this->session->userdata('admin')['id_role'] ?>;
            if(value[3].value != ''){
                _file += '<a href="'+base_url+"assets/lampiran/akta_file/"+value[3].value+'"><i class="fa fa-download"></i></a>' 
            }
            if(value[7].value != ''){
                _dateFormat += defaultDate(value[7].value);
            }

            if(id_role == 1||id_role == 6){
                 button += '<a href="'+site_url+"vendor/akta/aktaView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }

            html += '<div class="caption"><p>'+value[0].value+'</p><p>'+value[2].value+'</p><p>'+value[1].value+'</p><p>'+_dateFormat+'</p><p>'+button+'</p></div>';
            return html;
        },
        finish: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[7].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[7].value)
                }
                console.log(html)
                $('.form7 span').html(html);
                $('.close').on('click', function() {
                 
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

    });
    // dataRightClick: function(key, btn, value){
        //     _id = value[key][9].value;

        //     btn = [
        //     {
        //         icon : 'eye',
        //         label: 'Cek Data',
        //         class: 'buttonCek',
        //         href:site_url+"vendor/view/akta/cekData/"+_id
        //     }
        //     ];
        //     return btn;
        // },
        // callbackFunctionRightClick: function(){
        //     var cek = $('.buttonCek').modal({
        //         header: 'Info Data',
        //         render : function(el, data){

        //         data.onSuccess = function(){
        //             $(cek).data('modal').close();
        //             folder.data('plugin_folder').fetchData();

        //         };
        //         data.isReset = false;

        //         $(el).form(data).data('form');
        //         console.log($(el));

        //     }
        //   });
        // },
});


</script>