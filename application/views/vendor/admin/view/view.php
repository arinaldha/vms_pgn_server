<div id="tabs" class="tabs">

  <ul>

    <li>
      <a href="<?php echo site_url('vendor/view/administrasi/index/'.$id) ?>">Administrasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/akta/index/'.$id) ?>">Akta</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/situ/index/'.$id) ?>">SITU</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/tdp/index/'.$id) ?>">TDP</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/pengurus/index/'.$id) ?>">Pengurus</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/pemilik/index/'.$id) ?>">Pemilik</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/izin/index/'.$id) ?>">Izin Usaha</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/agen/index/'.$id) ?>">Pabrikan/Keagenan/Distributor</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/pengalaman/index/'.$id) ?>">Pengalaman</a>
    </li>
    <li>
      <a href="<?php echo site_url('vendor/view/k3/index/'.$id) ?>">K3</a>
    </li>
  </ul>
  
</div>