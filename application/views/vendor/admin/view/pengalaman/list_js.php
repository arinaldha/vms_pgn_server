<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    $.ajax({
        url: '<?php echo site_url('vendor/view/pengalaman/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

    var folder = $('#folderGeneratorPengalaman').folder({
        url: '<?php echo site_url('vendor/view/pengalaman/view/'.$id); ?>',
        data: dataPost,
        dataRightClick: function(key, btn, value){
            _id = value[key][14].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"vendor/view/pengalaman/cekData/"+_id
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                console.log($(el));

            }
          });
        },
        renderContent: function(el, value, key){
            html = '';
            _link = '';
            button = '';
            id_role = <?php echo $this->session->userdata('admin')['id_role'] ?>;
            if (value[0].value !='') {
                _link += '<a href="'+site_url+"vendor/view/pengalaman/cekDataPekerjaan/"+value[14].value+'" class="cekPekerjaan">'+value[0].value+'</a>'
            }
            if(id_role == 1||id_role == 6){
                 button += '<a href="'+site_url+"vendor/pengalaman/pengalamanView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }

            html += '<div class="caption"><p>'+_link+'</p><p>'+value[1].value+'</p><p>'+button+'</p></div>';
            
            return html;
        },
        finish: function(){
            var pekerjaan = $('.cekPekerjaan').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(pekerjaan).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                console.log($(el));

            }
          });
            
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
});


</script>