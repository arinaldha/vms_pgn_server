<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    $.ajax({
        url: '<?php echo site_url('vendor/view/agen/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

    var folder = $('#folderGeneratorAgen').folder({
        url: '<?php echo site_url('vendor/view/agen/view/'.$id); ?>',
        data: dataPost,
        dataRightClick: function(key, btn, value){
            _id = value[key][5].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"vendor/view/agen/verifikasi/"+_id
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                console.log($(el));

            }
          });
        },
        renderContent: function(el, value, key){
            html = '';
            _link = '';
            button = '';
            id_role = <?php echo $this->session->userdata('admin')['id_role'] ?>;
            if (value[0].value !='') {
                _link += '<a href="'+site_url+"vendor/view/agen/verifikasiProduk/"+value[5].value+'" class="cekPekerjaan">'+value[0].value+'</a>'
            } 

            if(id_role == 1||id_role == 6){
                 button += '<a href="'+site_url+"vendor/agen/agenView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }

            html += '<div class="caption"><p>'+_link+'</p><p>'+value[1].value+'</p><p>'+button+'</p></div>';
            
            return html;
        },
        finish: function(){
            var pekerjaan = $('.cekPekerjaan').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(pekerjaan).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                    var html = '';
                    $.ajax({
                        url: '<?php echo site_url('vendor/view/agen/get_produk/'.$id)?>',
                        method: 'POST',
                        dataType: 'json',
                        success:function(xhr){

                            __produk = xhr;
                            $(el).append('<h3>Produk & Merk</h3><div><ul></ul></div>')
                            var i;
                            $.each(__produk, function(key, value){
                                $('ul',el).append('<li><span>'+key+'</span><ul id="head'+i+'"></ul></li>');
                                $.each(value, function(keyBSB, valueBSB){
                                    $('#head'+i, el)
                                    .append('<li><label style="width:100%"><div class="inline-form">&nbsp;'+valueBSB+'</div></label></li>');
                                })
                                
                                i++;
                            });
                            
                        }
                    })

            }
          });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
});


</script>