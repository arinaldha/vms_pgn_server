<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

    $.ajax({
        url: '<?php echo site_url('vendor/view/izin/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

	var folder = $('#folderGenerator3').folder({
		url: '<?php echo site_url('vendor/view/izin/view/'.$id.'/siujk'); ?>',
		data: dataPost,
        dataRightClick: function(key, btn, value){
            _id = value[key][5].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"vendor/view/izin/cekData/"+_id+"/siujk"
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                    var html = '';
                    $.ajax({
                        url: '<?php echo site_url('vendor/izin/getBSB')?>',
                        async: false,
                        data:{
                            id_ijin_usaha : data.id
                        },
                        method: 'POST',
                        dataType: 'json',
                        success:function(xhr){

                            __bsb = xhr;
                            $(el).append('<h3>Bidang / Sub-bidang</h3><div class="treeView"><ul></ul></div>')
                            var i;
                            $.each(__bsb, function(key, value){
                                $('ul',el).append('<li class="treeDropdown"><span>'+key+'</span><ul id="head'+i+'"></ul></li>');
                                $.each(value, function(keyBSB, valueBSB){
                                    $('#head'+i, el)
                                    .append('<li><label style="width:100%"><div class="inline-form">&nbsp;'+valueBSB+'</div></label></li>');
                                })
                                
                                i++;
                            });
                            
                        }
                    })

            }
          });
        },
		renderContent: function(el, value, key){
            console.log(value);
			html = '';
			_dateFormat = '';
            button = '';
            id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

			if(value.expire_date == 'lifetime'){
				_dateFormat = 'Seumur Hidup';
			}
			else{
				_dateFormat = defaultDate(value.expire_date);
			}
            if(id_role == 1||id_role == 6){
                 button += '<a href="'+site_url+"vendor/izin/izinView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            console.log(_dateFormat);
			html += '<div class="caption"><p>'+value.no+'</p><p>'+_dateFormat+'</p><p>'+button+'</p></div>';
			
			return html;
		},
		finish: function(){
     		var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                /*if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);*/
                $('.close').on('click', function() {
                   
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
		},
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

	});
});


</script>