<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

    $.ajax({
        url: '<?php echo site_url('vendor/view/izin/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

	var folder = $('#folderGenerator4').folder({
		url: '<?php echo site_url('vendor/view/izin/view/'.$id.'/sbu'); ?>',
		data: dataPost,
        dataRightClick: function(key, btn, value){
            _id = value[key][5].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"vendor/view/izin/cekData/"+_id+"/sbu"
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){
                
                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log($(el));

            }
          });
        },
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
            button = '';
            id_role = <?php echo $this->session->userdata('admin')['id_role'] ?>;
			if(value.expire_date == 'lifetime'){
				_dateFormat += 'Seumur Hidup';
			}
			else{
				_dateFormat += defaultDate(value.expire_date);
			}

            if(id_role == 1||id_role == 6){
                 button += '<a href="'+site_url+"vendor/izin/izinView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }

			html += '<div class="caption"><p>'+value.no+'</p><p>'+_dateFormat+'</p><p>'+button+'</p></div>';

			return html;
		},
		finish: function(){
     		var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                /*if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);*/
                $('.close').on('click', function() {
                   
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
		},
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }

	});
});


</script>