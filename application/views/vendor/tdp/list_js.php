<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/tdp/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/tdp/getData/'.$id); ?>',
		data: dataPost,
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][5].value);

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href:site_url+"vendor/tdp/remove/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/tdp/edit/"+_id
			},{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/tdp/tdpView/"+_id
			}];
			return btn;
		},
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){
        			console.log(data);
          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}	
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
			var cek = $('.buttonCek').modal({
				header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		// console.log(data);
          		html = '';
	            if (data.form[2].value == 'lifetime') {
	                html += 'Selama Perusahaan Berdiri';
	            } else {
	                html += defaultDate(data.form[2].value)
	            }
	            console.log(html)
	            $('.form2 span').html(html);
	            $('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Nomor"
			},
			{
				"key"	: "type",
				"value"	: "Tanggal Dikeluarkan"
			},
			{
				"key"	: "no",
				"value"	: "Masa Berlaku"
			},
			{
				"key"	: "type",
				"value"	: "Bukti scan dokumen"
			},
		],
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			_issueDate = '';
			_link = '';
			if (value[0].value != '') {
				file = btoa('assets/lampiran/tdp_file/'+value[4].value);
				_link += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fa fa-download"></i></a>';
			}
			if (value[1].value != '') {
				_issueDate += defaultDate(value[1].value);
			}
			if (value[3].value == 'lifetime') {
				_dateFormat += 'Selama Perusahaan Berdiri';
			} else {
				_dateFormat += defaultDate(value[3].value);
			}
			html += '<div class="caption"><p>'+value[0].value+'</p><p>'+_issueDate+'</p><p>'+_dateFormat+'</p><p>'+_link+'</p></div>';
			
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/tdp/insert/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>