<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/situ/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/situ/getData/'.$id); ?>',
		data: dataPost,
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][7].value);

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href:site_url+"vendor/situ/remove/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/situ/edit/"+_id
			},{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/situ/situView/"+_id
			}];
			return btn;
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Nomor"
			},
			{
				"key"	: "no",
				"value"	: "Masa Berlaku"
			},{
				"key"	: "situ_file",
				"value"	: "Lampiran"
			},{
				"key"	: "file_photo",
				"value"	: "Foto Lokasi"
			}
		],
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
      		var cek = $('.buttonCek').modal({
      			header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		// console.log(data);
          		html = '';
	            if (data.form[6].value == 'lifetime') {
	                html += 'Selama Perusahaan Berdiri';
	            } else {
	                html += defaultDate(data.form[6].value)
	            }
	            console.log(html)
	            $('.form6 span').html(html);
	            $('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
		},
		renderContent: function(el, value, key){
			html = '';
			_dateFormat = '';
			_fileSitu = '';
			_filePhoto = '';
			
			if (value[5].value == 'lifetime') {
				_dateFormat += 'Selama Perusahaan Berdiri';
			} else {
				_dateFormat += defaultDate(value[5].value);
			}
			if (value[6].value != '') {
				file = btoa('assets/lampiran/situ_file/'+value[6].value);
				_fileSitu += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="far fa-file-alt" style="font-size: 1.5em"></i></a>'
			}
			if (value[4].value != '') {
				file = btoa('assets/lampiran/file_photo/'+value[4].value);
				_filePhoto += '<a href="'+base_url+"open/file/"+file+'" target="blank"><i class="fas fa-images" style="font-size: 1.5em"></i></a>'
			}
			html += '<div class="caption"><p>'+value[1].value+'</p><p>'+_dateFormat+'</p><p>'+_fileSitu+'</p><p>'+_filePhoto+'</p></div>';
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/situ/insert/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>