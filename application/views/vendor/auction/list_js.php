<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
			url: '<?php echo site_url('vendor/agen/formFilter')?>',
			async: false,
			dataType: 'json',
			success:function(xhr){
				_xhr = xhr;
			}
		})

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auction/getDataAuctionVendor/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Paket"
			},
			{
				"key"	: "auction_date",
				"value"	: "Tanggal"
			},
			{
				"key"	: "lokasi_name",
				"value"	: "Lokasi"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[1].value != '') {
					return defaultDate(data[1].value);
				}
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[7].value == 1 ){
					html +='<a href="'+site_url+"vendor_auction/index/"+btoa(data[5].value)+'" class="button is-success"><span class="icon"><i class="far fa-eye"></i></span> Lihat Action</a>';
				}else{
					html +='<a href="'+site_url+"vendor_auction/index/"+btoa(data[5].value)+'" class="button is-primary"><span class="icon"><i class="far fa-flag"></i></span> Mulai Action</a>';
				}
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
			
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>