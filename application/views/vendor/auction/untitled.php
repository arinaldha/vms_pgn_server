
<div class="lelang">
	<div class="col-14">
		<div class="panel">
		</div>
		<?php if($fill['auction_jenis']=='auction'){ ?>
		<div class="panel " id="timer-container">
			<div class="panel-heading">
				<h4><i class="fa fa-clock-o"></i>&nbsp;Waktu tersisa</h4>
			</div>
			<div class="panel-body">
				<div id="timer">-- : -- : --</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="col-34">
		<div class="panel" id="timer-container">
			<div class="panel-heading">
				<h4>Penawaran</h4>
			</div>
			<div class="panel-body">
				<?php if($kurs_info->num_rows()){ ?>
				<div class="headerBid">
					<ul>
						<?php foreach($kurs_info->result() as $data){ ?>
						<li>
							<i class="fa fa-money"></i> <?php echo $data->name; ?> : IDR <b class="gr-text"><?php echo number_format($data->rate, 2); ?></b>
						</li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
				<div class="bodyBid tableWrapper">
					
						<table cellpadding="0" cellspacing="0" border="0" class="auction-dash-table tableData" width="100%">
						<form id="auction-penawaran-form" method="post" action="<?php echo site_url('vendor_auction/save_penawaran'); ?>" enctype="multipart/form-data">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Barang</th>
									<th>Harga Satuan</th>
									<th>Volume</th>
									<th>Satuan</th>
									<th>Penawaran Harga</th>
									<th>Penawaran Harga<br/>(dalam IDR)</th>
									<th><?php echo $persentase; ?> (%)</th>
									<?php if($fill['auction_jenis']!='rfi'){  ?>
									<th width="80">
										<?php 
											if($fill['metode_auction'] == "posisi") echo "Posisi/Ranking"; else echo "Indikator"; 
										?>
									</th>
									<?php } ?>
								</tr>
							</thead>

							<tbody>
								<?php 
									
									/* 1 until n-1 */
								
									$y = 1;
									$count = 0;
									$detail = $penawaran->result_array();
									$total = count($detail) / $barang->num_rows();
									$mark_last = '';
									$useRowspan = '';
									$class="";
									$_nilai_satuan = $_nilai_total = $_in_rate = array();
									if($barang->num_rows() > 1) $useRowspan = ' rowspan="'.$barang->num_rows().'"';
									
									for($i=0;$i<$total;$i++){ 
										if($count == ($penawaran->num_rows() - $barang->num_rows())) $mark_last = ' id="updated-row"';
										
										//if($y % 2 == 0) $class = "even"; else $class = "odd";
										
										$index = $y;
										if(($fill['is_started'] or $fill['is_suspended']) and $mark_last)
											$index = '<div id="updated-row-index">'.$index.'</div>';
										
											echo '<tr'.$mark_last.'>';
											echo '<td'.$useRowspan.' align="center">'.$index.'</td>';
										
											$x = 0; 
											$total_last = 0;
											foreach($barang->result() as $data){ 
												$total_last +=$detail[$count]['in_rate'];
												$nilai		= number_format($detail[$count]['nilai']);
												$_nilai[$data->id] = $detail[$count]['nilai'];
												$nilai_satuan= number_format($detail[$count]['nilai_satuan']);

												$_nilai_satuan[$data->id] = $detail[$count]['nilai_satuan'];

												$in_rate	= number_format($detail[$count]['in_rate']);
												$_in_rate[$data->id] = $detail[$count]['_in_rate'];
												$percentage	= $this->vam->cek_percentage($id_lelang, $data->id, $detail[$count]['in_rate'], $detail[$count]['id']);
												
												$indicator	= '';
												$td_class	= '';
												

												if(($fill['is_started'] or $fill['is_suspended']) and $mark_last){
													$nilai		= '<div id="updated-row-nilai-'.$data->id.'">'.$nilai.'</div>';
													$nilai_satuan		= '<div id="updated-row-nilai-satuan-'.$data->id.'">'.$nilai_satuan.'</div>';
													$in_rate	= '<div id="updated-row-rate-'.$data->id.'">'.$in_rate.'</div>';
													
													$percentage	= '<div id="updated-row-percentage-'.$data->id.'"><span class="gr-text">'.$percentage.'</span></div>';
													$indicator	= '<div id="updated-row-warning-'.$data->id.'"></div>'
																  .'<div id="updated-row-lowest-'.$data->id.'"></div>';
													
													$td_class	= ' class="updated-row-td-'.$data->id.'"';
												}
												
												echo '<td'.$td_class.'>'.$data->nama_barang.'</td>';
												echo '<td'.$td_class.'>'.$nilai_satuan.'</td>';
												echo '<td'.$td_class.'>'.$data->volume.'</td>';
												echo '<td'.$td_class.'>'.$data->satuan.'</td>';
												echo '<td'.$td_class.'>'.$nilai.'</td>';
												echo '<td'.$td_class.'>'.$in_rate.'</td>';
												echo '<td'.$td_class.'><span class="gr-text">'.$percentage.'</span></td>';
												if($fill['auction_jenis']!='rfi'){ 
													echo '<td'.$td_class.' align="center">'.$indicator.'</td>';
												}
												if($fill['auction_jenis']!='auction'){
												echo '<td'.$td_class.' align="center"><a href="'.base_url('lampiran/file_penawaran/'. $detail[$count]['file']).'">'. $detail[$count]['file'].'</a></td>';	
												}
												if($x == $barang->num_rows()) 
													echo "</tr>"; else echo '</tr><tr class="'.$class.'">';
												
												$count++;
											}	

										$y++;
									
									}
									
									
									/* n-1 */
						
									if(!$fill['is_started'] and $y <= 1){
										echo '<tr id="updated-row" data-last-value="">';
										echo '<td'.$useRowspan.' align="center"><div id="updated-row-index">'.$y.'</div></td>';
										
										$x = 0; 
										foreach($barang->result() as $data){ 
											echo '<td class="updated-row-td-'.$data->id.'">'.$data->nama_barang.'</td>';
											echo '<td class="updated-row-td-'.$data->id.'"><div id="updated-row-nilai-satuan-'.$data->id.'"></div></td>';
											echo '<td class="updated-row-td-'.$data->id.'">'.$data->volume.'</td>';
											echo '<td class="updated-row-td-'.$data->id.'">'.$data->satuan.'</td>';
											echo '<td class="updated-row-td-'.$data->id.'"><div id="updated-row-nilai-'.$data->id.'"></div></td>';
											echo '<td class="updated-row-td-'.$data->id.'"><div id="updated-row-rate-'.$data->id.'"></div></td>';
											echo '<td class="updated-row-td-'.$data->id.'"><div id="updated-row-percentage-'.$data->id.'"></div></td>';
											if($fill['auction_jenis']!='rfi') { 
											echo '<td class="updated-row-td-'.$data->id.'" align="center">';
												echo '<div id="updated-row-warning-'.$data->id.'"></div>';
												echo '<div id="updated-row-lowest-'.$data->id.'"></div>';
											echo '</td>';
											}
											if($x == $barang->num_rows()) 
												echo "</tr>"; else echo '</tr><tr class="'.$class.'">';
										}
									}
								?>
								<?php if( (strtotime(date('Y-m-d H:i:s'))<=strtotime($fill['time_limit'])) && (strtotime(date('Y-m-d H:i:s'))>=strtotime($fill['start_time']))){ ?>
								<tr class="fixed">
									<td <?php if($barang->num_rows() > 1) echo 'rowspan="'.$barang->num_rows().'"'; ?> align="center"></td>
									<?php $x = 0; 
									foreach($barang->result() as $data){ ?>
									<td><?php echo $data->nama_barang; ?></td>
									<td><div class="wrapperBid">
										<label class="labelAuction">Kurs: </label>
											<div>
												<?php $select = $this->vam->get_user_currency($id_lelang, 'ASC', $data->id); 
												?>
												<select id="select-id-kurs-<?php echo $data->id; ?>" name="id_kurs[<?php echo $data->id; ?>]">
												<?php 
													
													foreach($kurs->result() as $_kurs){
														echo '<option value="'.$_kurs->id.'"';
														if($_kurs->id == $select['id_kurs']) echo ' selected="selected"';
														echo '>'.$_kurs->symbol.'</option>';
													} 
												?>
												</select>
											</div>
										</div>
										<div class="wrapperBid">
											<label class="labelAuction">Harga Satuan :</label>
											<div>
												<input name="satuan_temp[<?php echo $data->id; ?>]" id="terbilang-satuan-temp-<?php echo $data->id; ?>" type="hidden" value="<?php echo $_nilai_satuan[$data->id]?>"/>
												<input id="terbilang-satuan-<?php echo $data->id; ?>" class="auction-dash-prototype-input money-masked terbilang-satuan" data-id="<?php echo $data->id; ?>" name="satuan[<?php echo $data->id; ?>]" type="text" size="50" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b class="terbilang-satuan-container" id="terbilang-satuan-<?php echo $data->id; ?>-container"></b>
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
										<div class="wrapperBid">
											<label class="labelAuction">Harga Total :</label>
											<div>
												<?php if($total!=0){?>
												<div><label><input type="checkbox" class="lock-offer" id="lock-offer-<?php echo $data->id?>"/>Kunci penawaran terakhir</label></div>
												<?php } ?>
												<input name="id_barang_temp[<?php echo $data->id; ?>]" id="terbilang-temp-<?php echo $data->id; ?>" type="hidden" value="<?php echo $_nilai[$data->id]?>"/>
												<input id="terbilang-<?php echo $data->id; ?>" class="auction-dash-prototype-input money-masked terbilang-val"  name="id_barang[<?php echo $data->id; ?>]" type="text" size="50" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b class="terbilang-container" id="terbilang-<?php echo $data->id; ?>-container"></b>
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</td>
									<td><p id="volume-<?php echo $data->id;?>"><?php echo $data->volume; ?></p></td>
									<td><?php echo $data->satuan; ?></td>
									<td colspan="3">
										
									</td>
									<?php if($fill['auction_jenis']!='auction' && $total==0){ ?>
									<td>
										<input type="file" class="file_penawaran" id="file_penawaran_<?php echo $data->id;?>" name="file_penawaran[<?php echo $data->id; ?>]">
									</td>
									<?php } ?>
									<?php if($x == $barang->num_rows()) echo "</tr>"; else echo '</tr><tr class="fixed">'; ?>
									<?php } ?>
								</tr>
								<tr class="fixed">
									<td style="text-align:center;border-bottom: none;" colspan="6">
										<input id="total-last" value="<?php echo $total_last;?>" type="hidden">
										<input id="id_lelang" type="hidden" value="<?php echo $id_lelang; ?>" name="id_lelang"/>
										<?php if($total==0){?>
										<input type="hidden" value="1" name="is_first" class="is_first" />
										<?php } ?>
										<input type="submit" value="Kirim Penawaran" class="button-submit btnBlue" style="float:none"/>
									</td>
								</tr>
								<?php } ?>
							</tbody>
							</form>
						</table>
					
				</div>
			</div>
		</div>
	</div>
	
</div>




<div id="first-offer" style="display : none">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td height="30"></td>
		</tr>
		<tr>
			<td align="center">
				<b style="font-size : 20px">Masukan nilai penawaran awal anda :</b>
			</td>
		</tr>
		<tr>
			<td height="30"></td>
		</tr>
		<tr>
			<td align="center">
				
				<form id="auction-penawaran-form-awal" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
					<table cellpadding="0" cellspacing="0" border="0" class="auction-dash-table">
						<thead>
							<tr>
								<th>Nama Barang</th>
								<th>Kurs / Harga </th>
								<th>Volume / Satuan</th>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<th>Lampiran File</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php $x = 0; foreach($barang->result() as $data){ ?>
							<tr id="rows-barang-<?php echo $data->id; ?>-first" class="fixed">
								<td><?php echo $data->nama_barang; ?></td>
								<td>
									<table cellpadding="4" cellspacing="0" border="0">
										<tr>
											<td valign="top" style="border : none">
												<select id="kurs-<?php echo $data->id; ?>-first" name="id_kurs[<?php echo $data->id; ?>]">
												<?php 
													$select	= $this->vam->get_default_currency($data->id);
													foreach($kurs->result() as $_kurs){
														echo '<option value="'.$_kurs->id.'"';
														if($_kurs->id == $select['id']) echo ' selected="selected"';
														echo '>'.$_kurs->symbol.'</option>';
													} 
												?>
												</select>
												
											</td>
											<td align="left" style="border : none">
												<legend>Harga Satuan</legend>
												<input id="terbilang-satuan-<?php echo $data->id; ?>-first" class="auction-dash-prototype-input money-masked-awal terbilang-satuan" name="satuan[<?php echo $data->id; ?>]" style="width : 250px" type="text" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b style="max-width : 250px" class="terbilang-container" id="terbilang-satuan-<?php echo $data->id; ?>-container-first"></b>
															</td>
														</tr>
													</table>
												</div>
												<legend>Harga Total</legend>
												<input id="terbilang-<?php echo $data->id; ?>-first" class="auction-dash-prototype-input money-masked-awal terbilang-val" name="id_barang[<?php echo $data->id; ?>]" style="width : 250px" type="text" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b style="max-width : 250px" class="terbilang-container" id="terbilang-<?php echo $data->id; ?>-container-first"></b>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td><p id="volume-<?php echo $data->id;?>"><?php echo $data->volume; ?> / <?php echo $data->satuan; ?></p></td>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<td>
									<input type="file" class="file_penawaran" id="file_penawaran_<?php echo $data->id;?>" name="file_penawaran[<?php echo $data->id; ?>]">
									<p><i>*Upload lampiran foto barang sesuai dengan spesifikasi yang ditentukan</i></p>
								</td>
								<?php } ?>
							</tr>	
							<?php } ?>
							
							<tr class="fixed">
								<td style="text-align: center" colspan="4">
									<input id="id_lelang" type="hidden" value="<?php echo $id_lelang; ?>" name="id_lelang"/>
									<input type="hidden" value="1" name="is_first" class="is_first"/>
									<input type="submit" value="Kirim Penawaran"  class="button-submit btnBlue" style="float: none"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				
			</td>
		</tr>
	</table>
</div>