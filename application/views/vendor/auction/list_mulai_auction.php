<style type="text/css">
	.fixed {
		position: relative;
	}
</style>
<div class="col col-12">
	<div class="row">
		<?php echo $this->session->userdata('msgSuccess'); ?>
		<p id="auction-report-bar" class="alert alert-success">
			<i class="fas fa-check-square"></i> Auction telah selesai.
			<b style="color : #617ac6; cursor : pointer">
				<a href="<?php echo site_url('report/index/'.$id_lelang.'/'.$this->session->userdata('user')['id_user'])?>" target="_blank">Klik disini</a>
			</b> untuk melihat report.
		</p>
	</div>
	<div class="lelang">
		<div class="col col-4">
			<div class="panel">
				<h1 class="gigantic headerAuction"><i class="fas fa-gavel"></i>&nbsp;<b><?php echo strtoupper($fill['name']); ?></b></h1>

				<table class="table">
					<tbody>
						<tr>
							<td>
								Mata Uang
							</td>
							<td class="text-right">
								<?php echo $fill['rate']; ?>
							</td>
						</tr>
						<tr>
							<td>
								Metode Penawaran
							</td>
							<td class="text-right">
								<?php 
									if($fill['metode_penawaran'] == "lump_sum") 
										echo "Total";
									if($fill['metode_penawaran'] == "harga_satuan") 
										echo "Itemize";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Kriteria Pemenang
							</td>
							<td class="text-right">
								<?php 
									if($fill['kriteria_pemenang'] == "lump_sum") 
										echo "Total";
									if($fill['kriteria_pemenang'] == "harga_satuan") 
										echo "Itemize";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Durasi Lelang
							</td>
							<td class="text-right">
								<?php if($fill['auction_jenis']=='auction'){
									echo $fill['auction_duration'].' Menit';
								}
								// else{
								// 	echo default_date($fill['start_time']).' - '.default_date($fill['time_limit']);
								// } 
								?>
							</td>
						</tr>
						<tr>
							<td>
								Tipe Auction
							</td>
							<td class="text-right">
								<?php 
									if($fill['auction_type'] == "forward_auction") 
										echo "Forward Auction";
									if($fill['auction_type'] == "reverse_auction") 
										echo "Reverse Auction";
								?>
							</td>
						</tr>
						<tr>
							<td>
								Jenis Auction
							</td>
							<td class="text-right">
								<?php 
									switch ($fill['auction_jenis']) {
										case 'auction':
											echo 'Auction';
											break;
										case 'rfi':
											echo 'RFI';
											break;
										case 'rfi_pembelian':
											echo 'RFI Pembelian Langsung';
											break;
										case 'rfi_penunjukkan':
											echo 'RFI Penunjukkan Langsung';
											break;
									}
								?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php if($fill['auction_jenis']=='auction'){ ?>
					<div class="panel " id="timer-container">
						
							<h4><i class="fa fa-clock-o"></i>&nbsp;Waktu tersisa</h4>
				
						<div class="panel-body">
							<div id="timer">-- : -- : --</div>
						</div>
					</div>
				<?php } ?>
			</div>

		</div>
		<div class="col col-8">
			<div class="panel">
				<div class="panel-body">
						<?php 
						if($fill['metode_auction']=='indikator'&&$fill['auction_jenis']!='rfi'){ ?>
						<div class="noticeMedia">
							<ul>
								<?php if($fill['auction_jenis']!='rfi'&&$fill['auction_jenis']!='rfi_pembelian'){ ?>
								<li class="media">
									<div class="media-left">
										<i class="fa fa-exclamation-triangle warnColor"></i>
									</div>
									<div class="media-body">
										Penawaran masih diatas HPS/dibawah nilai limit
									</div>
								</li>
								<?php } ?>
								<li class="media">
									<div class="media-left">
										<i class="auction-prize3"></i>
									</div>
									<div class="media-body">
										Penawaran adalah yang <?php echo $limit; ?>
									</div>
								</li>
								<li class="media">
									<div class="media-left">
										<i class="auction-thumb1"></i>
									</div>
									<div class="media-body">
										Penawaran bukan yang <?php echo $limit; ?>
									</div>
								</li>
							</ul>
						</div>
						<?php } ?>
				</div>
				<div>
					<form id="auction-penawaran-form" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
					<table class="auction-form">
						<tr class="header">
							<th>Nama Barang</th>
							<th>Kurs</th>
							<th>Harga Satuan</th>
							<th>Volume</th>
							<th>Harga Total</th>
						</tr>
						<?php $x = 0; 
							foreach($barang->result() as $data){ 
								if($fill['kriteria_pemenang'] == "harga_satuan") {
									$id_field = $data->id;
								}else{
									$id_field = $id_lelang;
								}
							?>
						<tr>
							<td><?php echo $data->nama_barang; ?></td>
							<td>
								<div>
									<?php $select = $this->vam->get_user_currency($id_lelang, 'ASC', $data->id); 
									?>
									<select id="select-id-kurs-<?php echo $data->id; ?>" name="id_kurs[<?php echo $data->id; ?>]">
									<?php 
										
										foreach($kurs->result() as $_kurs){
											echo '<option value="'.$_kurs->id.'"';
											if($_kurs->id == $select['id_kurs']) echo ' selected="selected"';
											echo '>'.$_kurs->symbol.'</option>';
										} 
									?>
									</select>
								</div>
							</td>
							<td>
								<div class="wrapperBid">
									<div>
										<input name="satuan_temp[<?php echo $id_field; ?>]" id="terbilang-satuan-temp-<?php echo $data->id; ?>" type="hidden" value="<?php echo $_nilai_satuan[$data->id]?>"/>
										<input id="terbilang-satuan-<?php echo $id_field; ?>" class="form-control terbilang-satuan" data-id="<?php echo $data->id; ?>" name="satuan[<?php echo $data->id; ?>]" type="text" size="50" value=""/>
										<div style="max-width : 300px;">
											<table cellpadding="0" cellspacing="0" border="0">
												<tr class="row">
													<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
													<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
													<td style="border : none; font-size : 10px" valign="top">
														<b class="terbilang-satuan-container" id="terbilang-satuan-<?php echo $id_field; ?>-container"></b>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</td>
							<td><p id="volume-<?php echo $id_field;?>"><?php echo $data->volume; ?></p> </td>
							<td>
								<div>
									<?php if($total!=0){?>
									<div><label><input type="checkbox" class="lock-offer" id="lock-offer-<?php echo $data->id?>"/>Kunci penawaran terakhir</label></div>
									<?php } ?>
									<input name="id_barang_temp[<?php echo $id_field; ?>]" id="terbilang-temp-<?php echo $data->id; ?>" type="hidden" value="<?php echo $_nilai[$data->id]?>"/>
									<input id="terbilang-<?php echo $id_field; ?>" class="form-control auction-dash-prototype-input money-masked terbilang-val"  name="id_barang[<?php echo $data->id; ?>]" type="text" size="50" value=""/>
									<div style="max-width : 300px;">
										<table cellpadding="0" cellspacing="0" border="0">
											<tr class="row">
												<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
												<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
												<td style="border : none; font-size : 10px" valign="top">
													<b class="terbilang-container" id="terbilang-<?php echo $id_field; ?>-container"></b>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</td>
						</tr>
					<?php 

				} ?>
						<tr>
							<td colspan="5">
								<input id="total-last" value="<?php echo $total_last;?>" type="hidden">
								<input id="id_lelang" type="hidden" value="<?php echo $id_lelang; ?>" name="id_lelang"/>
								
								<button type="submit" class="btn btn-primary" style="float:none">Kirim Penawaran</button>
							</td>
						</tr>
					</table>
				</form>
				</div>
				<h2>Penawaran</h2>
				<ul class="auction-timeline" style="background-color: #f3f3f3">
					<?php 
					$y = 1;
					$count = 0;
					$detail = $penawaran->result_array();
					$total = count($detail) / $barang->num_rows();
					$mark_last = '';
					$useRowspan = '';
					$class="";
					$_nilai_satuan = $_nilai_total = $_in_rate = array();
					
					for($i=0;$i<$total;$i++){  

						if($count == ($penawaran->num_rows() - $barang->num_rows())) $mark_last = ' id="updated-row"';
						$index = $y;
					?>
					<li class="timeline-item" <?php echo $td_class; echo $mark_last?> >
							<span class="icon-line"><?php echo $index ?></span>
						
					<?php foreach($barang->result() as $data){ 
							if($fill['kriteria_pemenang'] == "harga_satuan") {
								$id_field = $data->id;
							}else{
								$id_field = $id_lelang;
							}
							$total_last +=$detail[$count]['in_rate'];
							$nilai		= number_format($detail[$count]['nilai']);
							$_nilai[$data->id] = $detail[$count]['nilai'];
							$nilai_satuan= number_format($detail[$count]['nilai_satuan']);

							$_nilai_satuan[$data->id] = $detail[$count]['nilai_satuan'];

							$in_rate	= number_format($detail[$count]['in_rate']);
							$_in_rate[$data->id] = $detail[$count]['_in_rate'];
							if(($fill['is_started'] or $fill['is_suspended']) and $mark_last){
								$nilai		= '<div id="updated-row-nilai-'.$data->id.'">'.$nilai.'</div>';
								$nilai_satuan		= '<div id="updated-row-nilai-satuan-'.$id_field.'">'.$nilai_satuan.'</div>';
								$in_rate	= '<div id="updated-row-rate-'.$data->id.'">'.$in_rate.'</div>';
								$percentage	= '<div id="updated-row-percentage-'.$data->id.'"><span class="gr-text">'.$percentage.'</span></div>';
								$indicator	= '<div id="updated-row-warning-'.$data->id.'"></div>'
											  .'<div id="updated-row-lowest-'.$data->id.'"></div>';
								$td_class	= ' id="updated-row-td-'.$data->id.'"';
							}

					?>
							<div class="timeline-content" <?php echo $td_class?>>
								<ul class="tl">
									<li class="bid-name"><?php echo $data->nama_barang; ?></li>
									<table>
										<tr class="bid-price">
											<td class="bid-number"><?php echo $nilai_satuan ?></td>
											<td class="center">x <?php echo $data->volume; ?></td>
											<td><?php echo number_format($detail[$count]['nilai_satuan'] * $data->volume) ?></td>
										</tr>
										<?php if($detail[$count]['id_kurs']!=1){ ?>
										<tr class="bid-kurs">
											<td class="bid-number">
												<span>USD</span>
												900.000.000,00
											</td>
											<td class="center">
												<span class="icon">
													<i class="fa fa-angle-right"></i>
												</span>
											</td>
											<td>
												<span>IDR</span>
												900.000.000,00
											</td>
										</tr>
										<?php } ?>
									</table>
								</ul>
								<div class="tl-after">
									<div class="decline">
										<span class="icon">
											<i class="fas fa-arrow-down"></i>
										</span>
										<?php echo $detail[$count]['down_percent'] ?> %
									</div>
										<?php echo ($indicator!='') ?'<div class="ranking">'.$indicator.'</div>':'' ?>
								</div>
							</div>
						
				<?php 
							$count++;
						} ?></li><?php 
						$y++;
					} ?>
				</ul>				
			</div>
		</div>
	</div>
	
</div>
<div id="auction-blocker" style="display : none;overflow-y: scroll;">
	<table cellpadding="0" cellspacing="0" border="0" width="100%" >
		<tr>
			<td width="100%" height="100%" align="center" valign="middle">
				<div id="auction-blocker-message-bar"></div>
				
			</td>
		</tr>
	</table>
</div>
<div id="syarat-body" style="display : none"><?php echo $syarat; ?></div>
<div id="first-offer" style="display : none">
	<b>Masukan nilai penawaran awal anda :</b>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="center">
				
				<form id="auction-penawaran-form-awal" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
					<table cellpadding="0" cellspacing="0" border="0" class="auction-dash-table table">
						<thead>
							<tr>
								<th>Nama Barang</th>
								<th>Kurs / Harga </th>
								<th>Volume / Satuan</th>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<th>Lampiran File</th>
								<?php } ?>
							</tr>
						</thead>
						 <tbody>
							<?php $x = 0; foreach($barang->result() as $data){ ?>
							<tr id="rows-barang-<?php echo $data->id; ?>-first" class="fixed">
								<td><?php echo $data->nama_barang; ?></td>
								<td>
									<table cellpadding="4" cellspacing="0" border="0">
										<tr>
											<td valign="top" style="border : none">
												<select id="kurs-<?php echo $data->id; ?>-first" name="id_kurs[<?php echo $data->id; ?>]">
												<?php 
													$select	= $this->vam->get_default_currency($data->id);
													foreach($kurs->result() as $_kurs){
														echo '<option value="'.$_kurs->id.'"';
														if($_kurs->id == $select['id']) echo ' selected="selected"';
														echo '>'.$_kurs->symbol.'</option>';
													} 
												?>
												</select>
												
											</td>
											<td align="left" style="border : none">
												<legend>Harga Satuan</legend>
												<input id="terbilang-satuan-<?php echo $data->id; ?>-first" class="auction-dash-prototype-input money-masked-awal terbilang-satuan form-control" name="satuan[<?php echo $data->id; ?>]" style="width : 250px" type="text" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b style="max-width : 250px" class="terbilang-container" id="terbilang-satuan-<?php echo $data->id; ?>-container-first"></b>
															</td>
														</tr>
													</table>
												</div>
												<legend>Harga Total</legend>
												<input id="terbilang-<?php echo $data->id; ?>-first" class="auction-dash-prototype-input money-masked-awal terbilang-val form-control" name="id_barang[<?php echo $data->id; ?>]" style="width : 250px" type="text" value=""/>
												<div style="max-width : 300px;">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="border : none; font-size : 10px" valign="top"><i>Terbilang</i></td>
															<td style="border : none; font-size : 10px" valign="top" width="20" align="center">:</td>
															<td style="border : none; font-size : 10px" valign="top">
																<b style="max-width : 250px" class="terbilang-container" id="terbilang-<?php echo $data->id; ?>-container-first"></b>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td><p id="volume-<?php echo $data->id;?>"><?php echo $data->volume; ?> / <?php echo $data->satuan; ?></p></td>
								<?php if($fill['auction_jenis']!='auction'){ ?>
								<td>
									<input type="file" class="file_penawaran  form-control" id="file_penawaran_<?php echo $data->id;?>" name="file_penawaran[<?php echo $data->id; ?>]">
									<p><i>*Upload lampiran foto barang sesuai dengan spesifikasi yang ditentukan</i></p>
								</td>
								<?php } ?>
							</tr>	
							<?php } ?>
							
							<tr class="fixed">
								<td style="text-align: center" colspan="4">
									<input id="id_lelang" type="hidden" value="<?php echo $id_lelang; ?>" name="id_lelang"/>
									<input type="hidden" value="1" name="is_first" class="is_first"/>
									<input type="submit" value="Kirim Penawaran"  class="button-submit btn btn-primary" style="float: none"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				
			</td>
		</tr>
	</table>
</div>

