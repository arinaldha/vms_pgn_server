<script type="text/javascript">
$(function(){
	<?php $admin 	= $this->session->userdata('admin');?>
	var __role 		= <?php echo $admin['id_role']?>;
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('daftar_vendor/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/daftar_tunggu/getDataTunggu/'.$id); ?>',
		data: dataPost,
		headers: [
			{
			"key" : "legal_name",
			"value" : "Badan Usaha"
			},
			{
			"key"	: "name",
			"value"	: "Nama Badan Usaha"
			},
			{
			"key" : "last_update",
			"value" : "Aktivitas Terakhir"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=deleteButton(site_url+"buku/remove/"+data[3].value, data[3].value);
				return html;
			},
			target : [4]

		}],
		additionFeature: function(el){
			el.append('<a href="'+site_url+'baseline_kontrak/form_rekap_baseline/" class="btn btn-primary rekapPengadaan">Eksport Ke Excels</a>');
		},
		finish: function(){
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
});
</script>
