<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('vendor/akta/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/akta/getData/'.$id); ?>',
		data: dataPost,
		
		dataRightClick: function(key, btn, value){
			_id = btoa(value[key][9].value);

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href:site_url+"vendor/akta/remove/"+_id
			},{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"vendor/akta/edit/"+_id
			},{
				icon : 'eye',
				label: 'Cek Data',
				class: 'buttonCek',
				href:site_url+"vendor/akta/aktaView/"+_id
			}
			];
			return btn;
		},
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
				header: 'Ubah Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');

          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})

        	}
      		});
      		var cek = $('.buttonCek').modal({
      			header: 'Info Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(cek).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');
          		
          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})

        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');

					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Jenis Akta"
			},
			{
				"key"	: "no",
				"value"	: "Nomor Akta"
			},{
				"key"	: "notaris",
				"value"	: "Notaris"
			},{
				"key"	: "authorize_date",
				"value"	: "Tanggal Ditetapkan"
			},
		],
		renderContent: function(el, value, key){
			html = '';
			_defaultDate = '';
			_fileAkta = '';
			_filePhoto = '';
			var _jenis;
			console.log(value);
			if (value[7].value != '') {
				_defaultDate += defaultDate(value[7].value);
			}
			if(value[0].value){
				_jenis = (value[0].value =='pendirian') ? 'Akta Pendirian' : 'Akta Perubahan';
			}

			if (value[4].value != '') {
				file = btoa('assets/lampiran/akta_file/'+value[4].value);
				_fileAkta += '<a href="'+base_url+"open/file/"+file+'">'+value[4].value+'</a>';
			}
			if (value[8].value != '') {
				file = btoa('assets/lampiran/akta_file/'+value[8].value);
				_filePhoto += '<a href="'+base_url+"open/file/"+file+'">'+value[8].value+'</a>';
			}
			html += '<div class="caption"><p>'+_jenis+'</p><p>'+value[2].value+'</p><p>'+value[1].value+'</p><p>'+_defaultDate+'</p></div>';
			
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"vendor/akta/insert/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>