<script type="text/javascript">
$(function(){
    var __entry_by;
    $('.datePicker').datetimepicker({
        timepicker: false,
        format: 'Y-m-d'
    });
    <?php $admin    = $this->session->userdata('admin');?>
    var __role      = <?php echo $admin['id_role']?>;

    var __id_user       = <?php echo $admin['id_user']?>;

    var _xhr;
    $.ajax({
        url : '<?php echo site_url('reporting/getHeader/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            _xhr = xhr;
        }
    });
    // var _xhrFilter;
    // $.ajax({
    //     url: '<?php echo site_url('reporting/formFilter')?>',
    //     async: false,
    //     dataType: 'json',
    //     success:function(xhr){
    //         _xhrFilter = xhr;
    //     }
    // })

    $('.buttonFilter').on('click', function(){
            // var __id = _this.element.id;
            $('.filterBlock.close').toggleClass('close');
            $('.filter-overlay').toggleClass('filter-overlay-close');
        });

    var base = $('#base');
    $.each(_xhr, function(key, value){
        if(typeof value.header !='undefined'){
            var wrapper = $('<div class="headerWrapper mg-lg-4 force-half"><h3 class="force-full">'+value.header+'</h3><ul></ul></div>');
            base.append(wrapper);
        }else{

            if($('.lastWrapper').length == 0){
                var wrapper = $('<div class="headerWrapper mg-lg-4 lastWrapper"><h3>LAINNYA</h3><ul></ul></div>');
                base.append(wrapper);
            }else{
                wrapper = $('.lastWrapper');
            }
        }
        
        $.each(value.field, function(keyField, valueField){
            if(typeof valueField=='object'){
                var list = $('<button class="fieldOpt unchecked custom-button btn">'+valueField.title+'<label class="toggleButton"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1>&nbsp;<div><svg viewBox="0 0 44 44"><path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path></svg></label></div></button>');
            }else{
                var list = $('<button class="fieldOpt unchecked custom-button btn">'+valueField+'<label class="toggleButton"><input type="checkbox" class="checkboxOpt" name="filter['+keyField+']" value=1><div><svg viewBox="0 0 44 44"><path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path></svg></label></div></button>');
            }


            list.on('click', function(e){
                e.preventDefault();
                $(this).find(".input").trigger('click');
                $(this).toggleClass('checked');
                var cb = $('.checkboxOpt',list);
                var ic = $('.iconOpt',list);
                // ic.removeClass('fa-plus fa-check');
                list.removeClass('checked unchecked');
                if(cb.is(':checked')){
                    cb.prop('checked',false);
                    // ic.addClass('fa-plus');
                    list.addClass('unchecked');
                }else{
                    cb.prop('checked',true);
                    // ic.addClass('fa-check');
                    list.addClass('checked');
                    
                }
            })
            wrapper.append(list);
        })
    })

    
});
</script>
