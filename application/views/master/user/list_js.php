<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('master/user/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Role"
			},
			{
				"key"	: "name",
				"value"	: "Nama User"
			},
			{
				"key"	: "email",
				"value"	: "Email"
			},
			{
				"key"	: "password_raw",
				"value"	: "Password"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"master/user/edit/"+data[4].value, data[4].value);
				html +=deleteButton(site_url+"master/user/remove/"+data[4].value, data[4].value);
				return html;
			},
			target : [4]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"master/user/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();

		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
		          if ($('.form0 [value="organik"]').prop('checked')) {
		          	$('.form1').hide()
					$('.form2').hide()
					$('.form3').hide()
					$('.form6').show()
		          } else {
		          	$('.form0').show()
					$('.form1').show()
					$('.form2').show()
					$('.form3').show()
					$('.form6').hide()
		          }
		          $('.form0 input[name="status"]').on('click',function(){
					var val = $(this).val();
					// alert(val);
					if (val == 'organik') {
						$('.form1').hide()
						$('.form1 input[name="name"]').removeAttr('value')
						$('.form2').hide()
						$('.form2 input[name="password"]').removeAttr('value')
						$('.form3').hide()
						$('.form3 input[name="email"]').removeAttr('value')
						$('.form6').show()
					} else {
						$('.form0').show()
						$('.form1').show()
						$('.form1 input[name="name"]').attr('value',data.form[1].value)
						$('.form2').show()
						$('.form2 input[name="password"]').attr('value',data.form[2].value)
						$('.form3').show()
						$('.form3 input[name="email"]').attr('value',data.form[3].value)
						$('.form6').hide()
					}
				})
		        }
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form0 [value="organik"]').attr('checked','true');
			$('.form0 input[name="status"]').on('change',function(){
				var val = $(this).val();
				// alert(val);
				if (val == 'organik') {
					$('.form1').hide()
					$('.form2').hide()
					$('.form3').hide()
					$('.form6').show()
				} else {
					$('.form0').show()
					$('.form1').show()
					$('.form2').show()
					$('.form3').show()
					$('.form6').hide()
				}
			})
			$('.form0 [value="organik"]').trigger('change')
		}
	});
});
</script>