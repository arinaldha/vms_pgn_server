<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('master/sub_bidang/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "id_bidang",
				"value"	: "Nama Bidang Usaha"
			},
			{
				"key"	: "name",
				"value"	: "Nama Sub Bidang"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				console.log(data[3].value)
				if(data[3].value==0){
					html +=editButton(site_url+"master/sub_bidang/edit/"+data[2].value, data[2].value);
					html += '<a class="button is-danger buttonDelete" href="'+site_url+"master/sub_bidang/remove/"+data[2].value+'"><i class="fa fa-trash"></i>&nbsp;Non Aktifkan</a>';
				}
				else{
					html += '<a class="btn btn-primary buttonAktif" href="'+site_url+"master/sub_bidang/aktif/"+data[2].value+'"><i class="fa fa-reply"></i>&nbsp;Aktifkan</a>';
				}
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"master/sub_bidang/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();

		          };
		          data.isReset = false;

		          $(el).form(data).data('form');

		        }
		      });
			var del = $('.buttonDelete').modal({
				header: 'Non-aktifkan Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin non-aktifkan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
			var aktif = $('.buttonAktif').modal({
				header: 'Aktifkan Data ?',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengaktifkan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(aktif).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>