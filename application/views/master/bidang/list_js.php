<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('master/bidang/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "dpt_type",
				"value"	: "Kategori Bidang"
			},
			{
				"key"	: "name",
				"value"	: "Bidang"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				if(data[2].value==0){
					html +=editButton(site_url+"master/bidang/edit/"+data[3].value, data[3].value);
					html += '<a class="btn btn-danger buttonDelete" href="'+site_url+"master/bidang/remove/"+data[3].value+'"><i class="fa fa-trash"></i>&nbsp;Non Aktifkan</a>';
				}
				else{
					html += '<a class="btn btn-primary buttonAktif" href="'+site_url+"master/bidang/aktif/"+data[3].value+'"><i class="fa fa-reply"></i>&nbsp;Aktifkan</a>';
				}
				return html;
			},
			target : [2]

		// },{
		// 	renderCell: function(data, row, key, el){
				
		// 	},
		// 	target : [3] 
		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"master/bidang/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Non-aktifkan Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin non-aktifkan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var aktif = $('.buttonAktif').modal({
				header: 'Aktifkan Data ?',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengaktifkan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(aktif).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			hideshow();
			$('[name="id_role"]').on('change', function(){
				hideshow();
			})
		}
	});
});
</script>